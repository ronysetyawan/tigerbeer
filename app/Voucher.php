<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function order_item(){
        return $this->belongsTo('App\OrderItem');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function voucher_redeem(){
        return $this->hasMany('App\VoucherRedeem', 'voucher_id', 'voucher_id');
    }

    public static function getVoucherStatus($voucher_id){
        if(\App\Voucher::where("voucher_id", $voucher_id)->count()<=0){
            return "Voucher is not found";
        }else{
            $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->first();
            if($voucher_info->voided){
                $status = array(
                    "value" => "voided",
                    "label" => "Voided"
                );
            }else if($voucher_info->balance <=0){
                $status = array(
                    "value" => "fully-redeemed",
                    "label" => "Fully Redeemed"
                );
            }else if($voucher_info->balance < $voucher_info->value){
                $status = array(
                    "value" => "partial-redeemed",
                    "label" => "Partial Redeemed"
                );
            }else{
                $status = array(
                    "value" => "available",
                    "label" => "Available"
                );
            }

            return $status;
        }
    }

    public static function getSmsText($voucher_id, $type_customer = 'owner'){
        //// shortlink begin
        $username = env('SHORTLINK_USERNAME');
        $password = env('SHORTLINK_PASSWORD');
        // $referrer = explode("/", $_SERVER['HTTP_REFERER']);
        $api_url =  env('SHORTLINK_SERVICE') . '/add/self/qrcode';

        $token = array("voucher_id" => $voucher_id);
        $jwt = \Firebase\JWT\JWT::encode($token, env('JWT_KEY_QRCODE'));

        //$link = url('qrcode' . '/' . $jwt);
        $link = url('/email-preview/' . \Firebase\JWT\JWT::encode($token, env('JWT_KEY_VOUCHER_BROWSER')));
        // Init the CURL session
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, 1);              // This is a POST request
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(     // Data to POST
            'link'      => $link,
            'username'  => $username,
            'password'  => $password
        ));
        // Fetch and return content
        $c_shortlink = curl_exec($ch);
        curl_close($ch);
        
        $data_shortlink = json_decode($c_shortlink);
        //// shortlink end

        $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->first();
        $order_item_recipient = \App\OrderItemRecipient::find($voucher_info->order_item_recipient_id);
        if($type_customer=='sender'){
            if($order_item_recipient->send_type == 'send-now'){
                if($voucher_info->sent){ // voucher has been sent
                    $text = "Dear Riana, your eGfit Cart has been sent to Metal";
                }elseif($voucher_info->owner_email == $voucher_info->order->email){
                    $text = "Dear " . $voucher_info->owner_first_name . ", " . $voucher_info->order->first_name . " has sent you an eGiftCard you can enjoy at any Tivoli property. View your eGift Card: ".$data_shortlink->shorturl;
                }else{
                    $text = "Dear Riana, thank you for buying a Tivoli Surprize eGift Card. Your eGift Card will be sent to Paimin on March 30, 2018";
                }
            }elseif($order_item_recipient->send_type == 'future-send'){
                if($voucher_info->sent){ // voucher has been sent
                    $text = "Dear Riana, your eGfit Cart has been sent to Metal";
                }elseif($voucher_info->owner_email == $voucher_info->order->email){
                    $text = "Dear " . $voucher_info->owner_first_name . ", " . $voucher_info->order->first_name . " has sent you an eGiftCard you can enjoy at any Tivoli property. View your eGift Card: ".$data_shortlink->shorturl;
                }else{
                    $text = "Dear Riana, thank you for buying a Tivoli Surprize eGift Card. Your eGift Card will be sent to Paimin on March 30, 2018";
                }
            }

        }else{ // send to owner
            if($order_item_recipient->send_type == 'send-now'){

                if($voucher_info->sent){ // voucher has been sent
                    $text = "Dear Riana, your eGfit Cart has been sent to Metal";
                }elseif($voucher_info->owner_email == $voucher_info->order->email){
                    $text = "Dear " . $voucher_info->owner_first_name . ", " . $voucher_info->order->first_name . " has sent you an eGiftCard you can enjoy at any Tivoli property. View your eGift Card: ".$data_shortlink->shorturl;
                }else{
                    $text = "Dear " . $voucher_info->owner_first_name . ", " . $voucher_info->order->first_name . " has sent you an eGiftCard you can enjoy at any Tivoli property. View your eGift Card: ".$data_shortlink->shorturl;
                }
            
            }elseif($order_item_recipient->send_type == 'future-send'){
                if($voucher_info->sent){ // voucher has been sent
                    $text = "Dear Riana, your eGfit Cart has been sent to Metal";
                }elseif($voucher_info->owner_email == $voucher_info->order->email){
                    $text = "Dear " . $voucher_info->owner_first_name . ", " . $voucher_info->order->first_name . " has sent you an eGiftCard you can enjoy at any Tivoli property. View your eGift Card: ".$data_shortlink->shorturl;
                }else{
                    $text = "Dear " . $voucher_info->owner_first_name . ", " . $voucher_info->order->first_name . " has sent you an eGiftCard you can enjoy at any Tivoli property. View your eGift Card: ".$data_shortlink->shorturl;
                }
            }
        }

        return $text;
    }
}
