<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherRedeem extends Model
{
    public function voucher(){
        return $this->belongsTo('App\Voucher', 'voucher_id', 'voucher_id');
    }
}
