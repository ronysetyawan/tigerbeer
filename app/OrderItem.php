<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function pint(){
        return $this->belongsTo('App\Pint', 'item_id');
    }
}
