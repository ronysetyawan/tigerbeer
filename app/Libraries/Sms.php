<?php
namespace App\Libraries;

class Sms {
 
    var $to = "";

    var $client = null;

    var $message = "";
    
    var $error = "";

    public function __construct($api_key, $api_secret)
    {
        $this->client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));  
    }

    public function send(){

        try {
            $message = $this->client->message()->send([
                'to' => $this->to,
                'from' => 'EGIFTING',
                'text' => $this->message
            ]);

            if($message){
                //echo "SMS sent!";
            }else{
                //echo "Sorry SMS is failed!";
            }

        }catch(Exception $e) {
            $this->error = $e->getMessage();
        }
        
    }
}
