<?php 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin::'], function () {

    Route::get('login', 'Admin\AuthController@login');
    Route::post('login', 'Admin\AuthController@loginAction');

    Route::get('login-outlet', 'Admin\AuthController@loginOutlet');
    Route::post('login-outlet', 'Admin\AuthController@loginOutletGo');

    Route::get('logout', 'Admin\AuthController@logout');

    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/', 'Admin\DashboardController@index');

        Route::get('/my-profile', 'Admin\MyProfileController@index');
        Route::post('/my-profile', 'Admin\MyProfileController@store');

        Route::get('/change-password', 'Admin\ChangePasswordController@index');
        Route::post('/change-password', 'Admin\ChangePasswordController@store');

        Route::group(['prefix' => 'dashboard', 'middleware' => ['auth:admin']], function () {
            Route::get('widget/purchased', 'Admin\DashboardController@widgetPurchased');
            Route::get('widget/pint-purchased', 'Admin\DashboardController@widgetPintPurchased');
            Route::get('widget/transaction', 'Admin\DashboardController@widgetTransaction');
            Route::get('widget/pint-redeemed', 'Admin\DashboardController@widgetPintRedeemed');
            Route::get('widget/contributors', 'Admin\DashboardController@widgetUniqueContributors');

            Route::get('chart/total-transaction', 'Admin\DashboardController@chartTotalTransaction');
            Route::get('chart/revenue', 'Admin\DashboardController@chartRevenue');
            Route::get('chart/average-transaction', 'Admin\DashboardController@chartAverageTransaction');
            Route::get('chart/voucher-by-occasion', 'Admin\DashboardController@chartVoucherByOccasion');
            Route::get('/', 'Admin\DashboardController@index');
        });

        Route::put('pint/{product_id}/status', 'Admin\PintController@updateStatus');
        Route::resource('pint', 'Admin\PintController', ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);

        Route::put('prize/{prize_id}/status', 'Admin\PrizeController@updateStatus');
        Route::resource('prize', 'Admin\PrizeController', ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);

        Route::group(['prefix' => 'voucher/{voucher_id}', 'middleware' => ['auth:admin']], function () {
            Route::get('history', 'Admin\VoucherController@history');
            Route::get('url', 'Admin\VoucherController@url');
            Route::post('resend-sms', 'Admin\VoucherController@resendSms');
            Route::post('resend-email', 'Admin\VoucherController@resendEmail');
            Route::post('redeem', 'Admin\VoucherController@redeem');
            Route::post('extend', 'Admin\VoucherController@extend');
            Route::get('void', 'Admin\VoucherController@void');
            Route::put('status', 'Admin\VoucherController@updateStatus');
            Route::put('event-status', 'Admin\VoucherController@updateEventTicketStatus');
            Route::get('/', 'Admin\VoucherController@show');
            Route::get('owner', 'Admin\VoucherController@getOwner');
            Route::put('owner', 'Admin\VoucherController@updateOwner');
            Route::get('email-log', 'Admin\VoucherController@emailLog');
            Route::get('sms-log', 'Admin\VoucherController@smsLog');
            Route::get('extend-history', 'Admin\VoucherController@extendHistory');
            Route::get('redemption', 'Admin\VoucherController@redemption');
        });

        Route::get('voucher', 'Admin\VoucherController@index');
        
        Route::get('/voucher-outlet', 'Admin\VoucherController@indexOutlet');

        Route::get('voucher-redeem', 'Admin\VoucherController@voucherredeem')->middleware('auth:admin');
        Route::get('payment', 'Admin\VoucherController@payment')->middleware('auth:admin');

        Route::get('redeem', 'Admin\RedeemController@index')->middleware('adminrole:admin');

        Route::group(['prefix' => 'report', 'namespace' => 'Admin\Report'], function () {
            Route::get('full-transaction/download/excel', 'FullTransactionController@downloadExcel');
            Route::get('full-transaction/download/pdf', 'FullTransactionController@downloadPdf');
            Route::get('full-transaction', 'FullTransactionController@index');

            Route::get('redemption-by-voucher/download/excel', 'RedemptionByVoucherController@downloadExcel');
            Route::get('redemption-by-voucher/download/pdf', 'RedemptionByVoucherController@downloadPdf');
            Route::get('redemption-by-voucher', 'RedemptionByVoucherController@index');

            Route::get('redemption-by-property/download/excel', 'RedemptionByPropertyController@downloadExcel');
            Route::get('redemption-by-property/download/pdf', 'RedemptionByPropertyController@downloadPdf');
            Route::get('redemption-by-property', 'RedemptionByPropertyController@index');

            Route::get('promo-code/download/excel', 'PromoCodeController@downloadExcel')->middleware('adminrole:admin');
            Route::get('promo-code/download/pdf', 'PromoCodeController@downloadPdf')->middleware('adminrole:admin');
            Route::get('promo-code', 'PromoCodeController@index')->middleware('adminrole:admin');

            Route::get('prize-redemption/download/excel', 'PrizeRedemptionController@downloadExcel')->middleware('adminrole:admin');
            Route::get('prize-redemption/download/pdf', 'PrizeRedemptionController@downloadPdf')->middleware('adminrole:admin');
            Route::get('prize-redemption', 'PrizeRedemptionController@index')->middleware('adminrole:admin');

            Route::get('voucher', 'VoucherController@index')->middleware('adminrole:admin');
            Route::get('custom', 'CustomController@index')->middleware('adminrole:admin');
        });

        Route::group(['prefix' => 'setting', 'namespace' => 'Admin\Setting', 'middleware' => ['adminrole:admin']], function () { 
            Route::put('country/{slug}/status', 'CountryController@updateStatus');
            Route::resource('country', 'CountryController', ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);
            
            Route::put('property/{slug}/status', 'PropertyController@updateStatus');
            Route::resource('property', 'PropertyController', ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);
        });

        Route::resource('order', 'Admin\OrderController');

        Route::get('/order/{order_id}/items', 'Admin\OrderController@items');

    });

});

Route::get('/admin/generatePin', 'Admin\HomeController@updateOutletPin');

Route::group([
    'prefix' => 'voucher/{code}/{voucher_id}',
    'namespace' => 'Voucher'
], function () {

    Route::get('/login', 'AuthController@index');
    Route::get('/logout', 'AuthController@logout');
    Route::post('/login', 'AuthController@store');
    Route::post('/set-pin', 'AuthController@setPinStore');

    // we need to move the link outside middleware
    // we are not using login on vouchers
    /* */
    Route::post('account', 'RedeemController@_validate');
    Route::get('redeem', 'RedeemController@index');
    Route::post('redeem', 'RedeemController@store');
    Route::get('prize', 'PrizeController@index');
    Route::post('prize', 'PrizeController@store');
    Route::get('account', 'AccountController@index');
    Route::post('account', 'AccountController@store');
    Route::post('account/subscribe-update', 'AccountController@subscribeUpdate');
    Route::post('delete-account', 'AccountController@destroy');
    Route::get('outlet', 'OutletController@index');
    Route::get('/dash/outlet', 'OutletController@index');
    Route::get('terms-and-conditions', function (){
        return redirect("assets/web-purchase/term.pdf");
    });

    Route::get('redeem/{vid}', 'RedeemController@index');
    Route::post('redeem/{vid}', 'RedeemController@storeVoucher');
    /* */
    Route::get('redirect', 'HomeController@redirectNotify');

    Route::get('/', 'HomeController@index');
    
    // Route::get('/', 'HomeController@welcome');

    // unused --
    Route::group(['middleware' => 'voucher.authenticate'], function () {
    });

});

Route::get('files/pint/deal/{year}/{month}/{filename}', function ($year, $month, $filename){

    $path = storage_path('app/public/pint/deal/' . $year . '/' . $month . '/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('files/pint/{filename}', function ($filename){

    $path = storage_path('app/public/pint/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('files/prize/deal/{year}/{month}/{filename}', function ($year, $month, $filename){

    $path = storage_path('app/public/prize/deal/' . $year . '/' . $month . '/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('files/prize/{filename}', function ($filename){

    $path = storage_path('app/public/prize/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('terms-and-conditions', function (){
    return redirect("assets/web-purchase/term.pdf");

    /*$filename = 'term.pdf';
    
    $path = public_path() . "/assets/web-purchase/". $filename;

    //die($path);

    return Response::make($path, 200, [
        'Content-Type' => 'application/pdf',
        'Content-Disposition' => 'inline; '.$filename,
    ]);*/
});

Route::get('privacy-policy', function (){
    return redirect("assets/web-purchase/privacy-policy.pdf");

    /*$filename = 'term.pdf';
    
    $path = public_path() . "/assets/web-purchase/". $filename;

    //die($path);

    return Response::make($path, 200, [
        'Content-Type' => 'application/pdf',
        'Content-Disposition' => 'inline; '.$filename,
    ]);*/
});

Route::get('test-email/test-notify', 'TestEmailController@testNotify');
Route::get('test-email/send-test-notify', 'TestEmailController@sendTestNotify');
Route::get('test-email/send-notify', 'TestEmailController@sendNotify');     // send live notify

Route::get('age-blocker', 'AgeBlockerController@index');
Route::post('age-blocker', 'AgeBlockerController@store');

//Route::get('test-email/order', 'TestEmailController@order');
Route::get('test-email/test', 'TestEmailController@test');
Route::get('test-email/test-survey', 'TestEmailController@testSurvey');
Route::get('test-email/resend/{orderid}', 'TestEmailController@resend');  // uncomment if want to use
Route::get('test-email/send-survey/{orderid}', 'TestEmailController@sendSurvey');
//Route::get('test-email/confirmation-delivery', 'TestEmailController@confirmation_delivery');
//Route::get('test-sms', 'TestSmsController@index');
Route::get('test-sms/test', 'TestSmsController@test');
Route::get('test-sms/resend/{orderid}', 'TestSmsController@resend');  // uncomment if want to use
Route::get('test-sms/bulk-sms-test', 'TestSmsController@sendSmsReminderTest');
// live reminder
Route::get('test-sms/send-reminder', 'TestSmsController@sendReminder');

Route::get('email-preview/purchase/{token}', 'EmailPreviewController@index'); #email and browser
Route::get('email-preview/delivery/{token}', 'EmailPreviewController@delivery'); #email and browser   

// putting homepage outside session 

Route::get('/captcha/image', 'HomeController@captchaImage');
Route::post('/captcha/check', 'HomeController@captchaCheck');
Route::post('/region/get', 'HomeController@getStoreByRegion');
Route::post('/region/get-by-name', 'HomeController@getStoreByName');
Route::post('/redemtion_code/check', 'HomeController@redemtioncodeCheck');
Route::post('/check_age', 'HomeController@checkAge');

Route::group(['middleware' => 'session.age'], function () {
    
    Route::get('/', 'HomeController@index');

    Route::get('/registration', 'HomeController@registration');

    Route::get('/registration_successful', 'CheckoutController@registration_success');

    Route::get('/recipes', 'HomeController@recipes');

    Route::get('/recipes/{id}', 'HomeController@recipes_details');

    Route::get('login', 'AuthController@login');

    Route::post('login', 'AuthController@store');

    Route::get('logout', 'AuthController@logout');

    Route::group(['middleware' => ['auth:customer']], function () {
        Route::get('account', 'AccountController@index');
        Route::post('account', 'AccountController@store');
        Route::delete('account', 'AccountController@destroy');
    });

    //Route::get('create', 'CreateController@index');
    //Route::post('create', 'CreateController@store');

    Route::get('resend-voucher', 'ResendVoucherController@index');
    Route::post('resend-voucher', 'ResendVoucherController@store');

    Route::get('cart/has-cart', 'CartController@hasCart');
    Route::put('cart/increase-qty/{id}', 'CartController@increaseQty');
    Route::put('cart/decrease-qty/{id}', 'CartController@decreaseQty');
    Route::post('cart/promo-code', 'CartController@storePromoCode');
    Route::delete('cart/promo-code', 'CartController@destroyPromoCode');
    Route::resource('cart', 'CartController', ['only' => ['store', 'edit', 'destroy']]);
    Route::get('cart', function(){
        abort(404);
    });
    Route::get('confirmation', 'CartController@index');
    Route::get('thankyou', 'CartController@thankyou');

    Route::get('checkout', 'CheckoutController@index');
    Route::get('checkout/return', 'CheckoutController@_return');
    Route::get('checkout/notify', 'CheckoutController@notify');
    Route::get('checkout/success', 'CheckoutController@success');
    Route::get('checkout/failure', 'CheckoutController@failure');
    Route::post('checkout', 'CheckoutController@store');
    Route::post('contact', 'ContactController@index');
    //Route::get('/', 'HomeController@index');
    //Route::get('/captcha/image', 'HomeController@captchaImage');
    //Route::post('/captcha/check', 'HomeController@captchaCheck');
    //Route::post('/region/get', 'HomeController@getStoreByRegion');
    //Route::post('/region/get-by-name', 'HomeController@getStoreByName');
});


Route::post('/api/checkout-details', 'CheckoutController@firstTempCheckout');
Route::post('/api/store-payment-properties', 'CheckoutController@storePaymentGatewayProperties');
Route::get('/checkout-success', 'CheckoutController@checkoutSuccess');
Route::post('/checkout-notify', 'CheckoutController@checkoutNotify');
Route::post('/api/store-checkout', 'CheckoutController@storeCheckout');
Route::post('/api/remove-checkout', 'CheckoutController@removeCheckout');

Route::get('/get-signature', 'CheckoutController@getSignature');

Route::get('/api/test1', function(){
    return response()->json([
        'status'=> true,
        'message'=>'success. outside auth-api'
    ]);
});

Route::group(['middleware' => 'auth-api'], function () {
    Route::get('/api/test', function(){
        return response()->json([
            'status'=> true,
            'message'=>'success'
        ]);
    });
    Route::post('/api/store-result', 'SurveyController@storeResult');
    // test api
    Route::post('/api/test-store-result', 'SurveyController@storeResultTest');

}); 

//Route::get('/update-pin', 'Admin\HomeController@updateOutletPin');