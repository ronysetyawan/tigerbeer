<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

use Route;

class PintRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $current_params = Route::current()->parameters();

        if(isset($current_params['pint'])){
            return [
                'image' => 'image',
                'price' => 'required|numeric',
                'pint' => 'required|integer|unique:pints,pint,'. $current_params['pint'],
            ];
        }else{
            return [
                'image' => 'required|image',
                'price' => 'required|numeric',
                'pint' => 'required|integer|unique:pints,pint'
            ];
        }
    }
	
	public function messages()
	{
		return [
            'pint.unique'  => 'Pint already exists!',
		];
	}
}
