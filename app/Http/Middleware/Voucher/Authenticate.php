<?php

namespace App\Http\Middleware\Voucher;

use Closure;

use Illuminate\Support\Facades\Auth;

use Route;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $parameters = Route::current()->parameters();

        $voucher_id = $parameters['voucher_id'];
        $code = $parameters['code'];

        $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->where("code", $code)->whereHas('user', function($q){
            $q->where("status", 1)->where("deleted", 0);
        })->first();

        if(!$voucher_info){
            abort(404);
        }else{

            /*$user_mobile = $voucher_info->user->mobile;
            $len_user_mobile = strlen($user_mobile);
            $trim_user_mobile = $len_user_mobile - 4;
            $string_mobile = substr($user_mobile,($trim_user_mobile),4);*/

            /*if($string_mobile != $_4_last_digit_mobile){
                abort(404);
            }*/

            if(Auth::guard("customer")->check()){
                $user = Auth::guard("customer")->user();
                if($user->id != $voucher_info->user_id){
                    return redirect('voucher/' . $code . "/" . $voucher_id . '/login');
                }
            }else{
                return redirect('voucher/' . $code . "/" . $voucher_id . '/login');
            }
        }

        return $next($request);

    }
}
