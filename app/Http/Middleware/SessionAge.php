<?php

namespace App\Http\Middleware;

use Closure;

class SessionAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($_GET['promocode'])){
            $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $_GET['promocode'])->first();
            if($promo_code){
                $ses_promo_code = session()->get("promo_code");
                $ses_promo_code = [
                    "code" => $promo_code->code
                ];
                session()->put("promo_code", $ses_promo_code);
            }
        }

        if(isset($_GET['ref_id'])){
            $ref_user_info = \App\User::where("status", 1)->where("deleted", 0)->where("id", $_GET['ref_id'])->where("type", "customer")->first();
            if($ref_user_info){
                $ses_ref_id = session()->get("ref_id");
                $ses_ref_id = [
                    "id" => $ref_user_info->id
                ];
                session()->put("ref_id", $ses_ref_id);
            }
        }

        if(session()->has("age")) {
            return $next($request);
        }
        return redirect("age-blocker");
    }
}
