<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $adminrole)
    {
        $user = Auth::guard('admin')->user();
        if($user){
            if($user->role == $adminrole){
                return $next($request);
            }
        }
        return redirect('/admin');
    }
}
