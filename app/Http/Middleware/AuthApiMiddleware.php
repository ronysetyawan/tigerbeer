<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class AuthApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $key = $request->header('apikey');

        // test for calling via java - start
        header("Access-Control-Allow-Origin: *");

        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin'
        ];
        /*
        if($request->getMethod() == "OPTIONS") {
            // The client-side application can set only headers allowed in Access-Control-Allow-Headers
            return Response::make('OK', 200, $headers);
        } */
        // test for calling via java - end

        if (!empty($key)) {
            $check_client = ($key == 'aHR0cHM6Ly9zdXBwb3J0b3VyZm5iLnRpZ2VyYmVlci5jb20uc2cv');

            if ($check_client) {
                return $next($request);
            }
        }

        return response()->json(['message'=>'Unauthorized'], 401);
    }
}
