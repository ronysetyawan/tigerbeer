<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use View;

class BaseController extends Controller
{
    public function __construct(Request $request, $menu_active = 'dashboard') {

        $user_name = "";

        $type = "";

        $user = Auth::guard('admin')->user();
        
        $this->user_id = $user->id;

        if($user->first_name){
            $user_name = $user->first_name;
        }else{
            $user_name = $user->username;
        }
        
        $role = $user->role;

        View::share('menu_active', $menu_active);
        View::share('user_name', $user_name);
        View::share('user_type', $type);
        View::share('user_role', $role);
    }
}
