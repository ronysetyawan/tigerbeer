<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Hash;
use Session;

class ChangePasswordController extends BaseController
{
    public function index(){
        return view('admin.change_password');
    }

    public function store(Request $request){

        $this->validate($request, [
            'new_password' => 'required',
            'new_password_confirm' => 'required|same:new_password',
            'old_password' => 'required',
        ]);

        $user = Auth::guard('admin')->user();

        if(!Hash::check($request->old_password, $user->password)){
            Session::flash('message', 'Your Old Password is not correct!');
            Session::flash('alert-class', 'alert-danger'); 
            return redirect("admin/change-password");
        }else{

            $user->password = Hash::make($request->new_password);
            $user->save();

            Session::flash('message', 'You have changed the password successfully!');
            Session::flash('alert-class', 'alert-success'); 
            return redirect("admin/change-password");
        }

        return view('admin.change_password');
    }
}
