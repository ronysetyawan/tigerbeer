<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class PintController extends BaseController
{

    public function __construct(Request $request){
        parent::__construct($request, 'pint');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $links_appends = array();

        $filter_datas = array(
            "retailer" => ""
        );

        $setting = \App\Setting::where("type", "global")->where("key", "product_name")->first();
        $product_name = $setting->value;

        $query_pints = \App\Pint::query();
        $query_pints->orderBy("sort_order", "asc");
        $query_pints->orderBy("pint", "asc");
        $query_pints->orderBy("id", "desc");
        
        $data_pints = $query_pints->paginate(20);
        $pints = array();
        if($data_pints){
            foreach($data_pints as $dt_pint){
                $pints[] = array(
                    "id" => $dt_pint->id,
                    "image" => url('files/pint/' . $dt_pint->image),
                    "name" => $product_name . " " . $dt_pint->pint . " x pint(s)",
                    "price" => number_format($dt_pint->price, 2),
                    "sort_order" => $dt_pint->sort_order,
                    "status" => $dt_pint->status?true:false,
                    "status_string" => $dt_pint->status?"Active":"Not Active",
                );
            }
        }

        $data = array(
            "pints" => $pints,
            "filter_datas" => $filter_datas,
            "current_url" => $request->fullUrl(),
            "links" => $links_appends?$data_pints->appends($links_appends)->links():$data_pints->links(),
        );

        return view('admin.pint_list', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Admin\PintRequest $request)
    {
        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();
        $filename = date("YmdHis") . "-" . $image->getClientOriginalName();
        Storage::disk('pint')->put($filename,  File::get($image));

        $pint = new \App\Pint;
        $pint->image = $filename;
        $pint->pint = $request->pint;
        $pint->price = $request->price;
        $pint->sort_order = $request->sort_order;
        $pint->status = $request->status;
        $pint->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $pint = \App\Pint::find($id);

        $data_return = array(
            "image" => $pint->image,
            "pint" => $pint->pint,
            "price" => $pint->price,
            "sort_order" => $pint->sort_order,
			"status" => $pint->status
        );

        return response()->json($data_return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\Admin\PintRequest $request, $id)
    {
        $setting = \App\Setting::where("type", "global")->where("key", "product_name")->first();
        $product_name = $setting->value;

        $pint = \App\Pint::find($id);

        $pint->pint = $request->pint;
        $pint->price = $request->price;
        $pint->status = $request->status;
        $pint->sort_order = $request->sort_order;
        
        if(Input::hasFile('image')){

            if($pint->image){
                \Storage::delete("/public/pint/" . $pint->image);
            }

            $image = $request->file('image');
            $ext = $image->getClientOriginalExtension();
            $filename = date("YmdHis") . "-" . $image->getClientOriginalName();
            Storage::disk('pint')->put($filename,  File::get($image));

            $pint->image = $filename;
        }

        $pint->save();

        return response()->json([
            "image" => url('files/pint/' . $pint->image),
            "name" => $product_name . " " . $pint->pint . " x pint(s)",
            "price" => number_format($pint->price, 2),
            "sort_order" => $pint->sort_order,
            "status" => $pint->status?true:false,
            "status_string" => $pint->status?"Active":"Not Active"
        ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pint = \App\Pint::find($id);

        if($pint->image){
            \Storage::delete("/public/pint/" . $pint->image);
        }

        $pint->delete();
    }

    public function updateStatus(Request $request, $pint_id)
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $pint = \App\Pint::find($pint_id);

        $pint->status = $request->status?1:0;

        $pint->save();

        return response()->json([
            "status" => $pint->status?true:false,
            "status_string" => $pint->status?"Active":"Not Active",
        ]);
    }
}
