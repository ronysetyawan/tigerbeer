<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

use DB;
use Auth;

class HomeController extends BaseController
{

    public function index()
    {
        //
    }

    public function updateOutletPin()
    {
        $i = 0;
        $prop_info = \App\Property::get();
        if ($prop_info) {
            foreach($prop_info as $prop) {

                $code = $this->generateCode($prop->zone);
                //var_dump($prop->name, $prop->zone, $code);
                
                $prop->pin = $code;
                $prop->save();
                $i++;
            }

            echo "$i pins generated";
        }
    }

    function generateCode($region) {
        //function generateCode(Request $request) {
        //$region = strtoupper($request->region);
        $pin = $code = "";
        if (trim($region) == 'Central') {
            $code = 'C';
        } elseif (trim($region) == 'East') {
            $code = 'E';
        } elseif (trim($region) == 'North') {
            $code = 'N';
        } elseif (trim($region) == 'North East') {
            $code = 'L';
        } elseif (trim($region) == 'South') {
            $code = 'S';
        } elseif (trim($region) == 'West') {
            $code = 'W';
        } 

        $pin = $code.rand(0000,9999);
        return $pin;
    }

}
