<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Session;

class MyProfileController extends BaseController
{
    public function index(){

        $user = Auth::guard('admin')->user();

        return view('admin.my_profile', [
            "first_name" => $user->first_name,
            "last_name" => $user->last_name
        ]);
    }

    public function store(Request $request){

        $this->validate($request, [
            'first_name' => 'required'
        ]);

        $user = Auth::guard('admin')->user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();

        Session::flash('message', 'You have changed your profile successfully!');
        Session::flash('alert-class', 'alert-success'); 
        return redirect("admin/my-profile");

    }
}
