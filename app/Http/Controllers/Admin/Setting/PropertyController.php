<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Admin\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;

use DB;

class PropertyController extends BaseController
{
    public function __construct(Request $request){
        parent::__construct($request, 'setting-property');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $links_appends = array();
        
        $filter_datas = array();

        $query_properties = \App\Property::query();
        if(Input::get("keyword")){
            $query_properties->where(function($query){
                $query->where('name', 'like', '%' . Input::get("keyword") . '%')
                ->orWhere('address1', 'like', '%' . Input::get("keyword") . '%')
                ->orWhere('address2', 'like', '%' . Input::get("keyword") . '%');
            });

            $filter_datas['keyword'] = Input::get('keyword');
        }
        if(Input::get("zone")){
            $query_properties->where('zone', 'like', '%' . Input::get("zone") . '%');
            $filter_datas['zone'] = Input::get('zone');
        }
        $query_properties->orderBy("id", "desc");

        $total_record = $query_properties->count();

        $data_properties = $query_properties->paginate(20);
        
        $properties = array();
        foreach($data_properties as $dt_property){
            $properties[] = array(
                "id" => $dt_property->id,
                "name" => $dt_property->name,
                "address_1" => $dt_property->address1,
                "address_2" => $dt_property->address2,
                "zone" => $dt_property->zone,
                "pin" => $dt_property->pin,
                "status" => $dt_property->status?true:false,
                "status_string" => $dt_property->status?"Active":"Not Active",
            );
        }

        return view('admin.setting.property', array(
            "total_record" => $total_record,
            "current_url" => $request->fullUrl(),
            "properties" => $properties,
            "filter_datas" => $filter_datas,
            "links" => $links_appends?$data_properties->appends($links_appends)->links():$data_properties->links(),
            "zones" => [
                "North",
                "North East",
                "West",
                "Central",
                "East",
                "South"
            ]
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'pin' => 'required|unique:properties,pin'
        ]);

        $property = new \App\Property;
        $property->name = $request->name;
        $property->address1 = $request->address1;
        $property->address2 = $request->address2;
        $property->zone = $request->zone;
        $property->pin = $request->pin;
        $property->status = $request->status;

        $property->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = \App\Property::find($id);

        $data_return = array(
            "name" => $property->name,
            "address1" => $property->address1,
            "address2" => $property->address2,
            "zone" => $property->zone,
			"pin" => $property->pin,
			"status" => (int)$property->status
        );

        return response()->json($data_return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'pin' => 'required|unique:properties,pin,' . $id
        ]);

        $property = \App\Property::find($id);

        $property->name = $request->name;
        $property->address1 = $request->address1;
        $property->address2 = $request->address2;
        $property->zone = $request->zone;
        $property->pin = $request->pin;
        $property->status = $request->status;
        $property->save();
    
        return response()->json([
            "name" => $property->name,
            "address1" => $property->address1,
            "address2" => $property->address2,
            "zone" => $property->zone,
            "pin" => $property->pin,
            "status" => $property->status?true:false,
            "status_string" => $property->status?"Active":"Not Active"
        ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = \App\Property::find($id);
        $property->delete();
    }

    public function updateStatus(Request $request, $property_id)
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $property = \App\Property::find($property_id);

        $property->status = $request->status?1:0;

        $property->save();

        return response()->json([
            "status" => $property->status?true:false,
            "status_string" => $property->status?"Active":"Not Active",
        ]);
    }
}
