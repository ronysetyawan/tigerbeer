<?php

namespace App\Http\Controllers\Admin\Setting;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\BaseController;

use DB;

class GlobalSettingController extends BaseController
{
    public function __construct(Request $request){
        parent::__construct($request, 'setting-global');
    }

    public function index(){
        $data_global_settings = \App\Setting::where("type", "global")->get();
        $global_settings = array();
        foreach($data_global_settings as $dt){
            $global_settings[$dt->key] = $dt->value;
        }
        return view('admin.setting.global', array(
            "global_settings" => $global_settings
        ));
    }

    public function store(Request $request){
        
        if($request->form){
            \App\Setting::where("type",  "global")->delete();
            foreach($request->form as $key=>$value){
                $setting = new \App\Setting;
                $setting->key = $key;
                $setting->value = $value;
                $setting->type = "global";
                $setting->save();
            }
        }

        return redirect('admin/setting/global');
    }
}
