<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Admin\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;

use DB;

class CountryController extends BaseController
{
    public function __construct(Request $request){
        parent::__construct($request, 'setting-country');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $links_appends = array();

        $filter_datas = array();
        
        $query_countries = \App\Country::query();
        if(Input::get("keyword")){
            $query_countries->where('country_name', 'like', "%" . Input::get("keyword") . "%");
            $filter_datas['keyword'] = Input::get('keyword');
        }
        $query_countries->orderBy("country_name", "asc");

        $total_record = $query_countries->count();

        $data_countries = $query_countries->paginate(20);
        
        $countries = array();
        foreach($data_countries as $dt_country){
            $countries[] = array(
                "id" => $dt_country->id,
                "name" => $dt_country->country_name,
                "code_2" => $dt_country->country_code_2,
                "code_3" => $dt_country->country_code_3,
                "calling_code" => $dt_country->calling_code,
                "status" => $dt_country->status?true:false,
                "status_string" => $dt_country->status?"Active":"Not Active",
            );
        }

        return view('admin.setting.country', array(
            "current_url" => $request->fullUrl(),
            "total_record" => $total_record,
            "countries" => $countries,
            "filter_datas" => $filter_datas,
            "links" => $links_appends?$data_countries->appends($links_appends)->links():$data_countries->links(),
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'code2' => 'required',
            'code3' => 'required',
            'calling_code' => 'required'
        ]);

        $country = new \App\Country;
        $country->country_code_2 = $request->code2;
        $country->country_code_3 = $request->code3;
        $country->country_name = $request->name;
        $country->calling_code = $request->calling_code;
        $country->status = $request->status;

        $country->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = \App\Country::find($id);

        $data_return = array(
            "name" => $country->country_name,
            "code2" => $country->country_code_2,
            "code3" => $country->country_code_3,
			"calling_code" => $country->calling_code,
			"status" => (int)$country->status
        );

        return response()->json($data_return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'code2' => 'required',
            'code3' => 'required',
            'calling_code' => 'required'
        ]);

        $country = \App\Country::find($id);

        $country->country_code_2 = $request->code2;
        $country->country_code_3 = $request->code3;
        $country->country_name = $request->name;
        $country->calling_code = $request->calling_code;
        $country->status = $request->status;

        $country->save();
    
        return response()->json([
            "code2" => $country->country_code_2,
            "code3" => $country->country_code_3,
            "name" => $country->country_name,
            "calling_code" => $country->calling_code,
            "status" => $country->status?true:false,
            "status_string" => $country->status?"Active":"Not Active"
        ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = \App\Country::find($id);
        $country->delete();
    }

    public function updateStatus(Request $request, $country_id)
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $country = \App\Country::find($country_id);

        $country->status = $request->status?1:0;

        $country->save();

        return response()->json([
            "status" => $country->status?true:false,
            "status_string" => $country->status?"Active":"Not Active",
        ]);
    }
}
