<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use DB;

use Illuminate\Support\Facades\Input;

class DashboardController extends BaseController
{
    public function index(Request $request){
        $user = Auth::guard('admin')->user();
        if($user->role == 'property'){
            return redirect('/admin/report/redemption-by-property');
        }

        $filter_datas = array(
            "retailer" => ""
        );

        return view('admin.dashboard', array(
            "filter_datas" => $filter_datas
        ));
    }

    public function widgetPurchased(){
        // $query = \App\Order::query();
        /*if(Input::get("retailer")){
            $query->where('branch_id', Input::get("retailer"));
        }*/
        // $query->where("order_status", 2);
        // $total = $query->count();
        $query = \App\Voucher::query();
        $total = $query->count();

        return response()->json([
            "data" => $total
        ]);
    }

    public function widgetTransaction(){
        $query = \App\ReddotLog::query();
        $query->where('result','successful');

        $total = $query->sum("amount");

        return response()->json([
            "data" => "SGD " . number_format($total, 2)
        ]);
    }

    public function widgetPintPurchased(){
        $query = \App\Order::query();
        $query->where("order_status", 2);

        $total = $query->sum("total_pint");

        return response()->json([
            "data" => number_format($total)
        ]);
    }

    public function widgetPintRedeemed(){
        $query = \App\VoucherRedeem::query();
        
        $total = $query->sum("value");

        return response()->json([
            "data" => number_format($total)
        ]);
    }

    public function widgetUniqueContributors(){
        $query = \App\Voucher::query();
        $query->distinct("owner_email");
        $total = $query->count('owner_email');

        return response()->json([
            "data" => number_format($total)
        ]);
    }

    public function widgetRedemptionRate(){
        /*$query = \App\Voucher::query();
        if(Input::get("retailer")){
            $query->where('branch_id', Input::get("retailer"));
        }
        $query->where("expiry_date",'>',date("Y-m-d H:i:s"));
        $total = $query->count();*/
        return response()->json([
            "data" => "5435"
        ]);
    }

    public function chartTotalTransaction(){
        $datas = DB::table('orders')
        ->select(DB::raw('YEAR(orders.order_date) as year'), DB::raw('MONTH(orders.order_date) as month'), DB::raw('sum(orders.total) as total'))
        ->where("orders.order_status", 2)
        ->groupBy(DB::raw('YEAR(orders.order_date), MONTH(orders.order_date) DESC'))
        ->get();

        $totals = array();
        foreach($datas as $data){
            if(!isset($totals[$data->year][$data->month])){
                $totals[$data->year][$data->month] = 0;
            }
            $totals[$data->year][$data->month] += $data->total;
        }

        $months = array();
        $months[1] = "Jan";
        $months[2] = "Feb";
        $months[3] = "Mar";
        $months[4] = "Apr";
        $months[5] = "May";
        $months[6] = "Jun";
        $months[7] = "Jul";
        $months[8] = "Aug";
        $months[9] = "Sep";
        $months[10] = "Oct";
        $months[11] = "Nov";
        $months[12] = "Dec";

        $cols = array();
        $cols[] = array("id"=>"month", "label"=>"Month", "type"=>"string");
        foreach($totals as $key_year=>$total){
            $cols[] = array("id"=>"year-" . $key_year, "label"=>$key_year, "type"=>"number");
        }

        $rows = array();
        for($i=1; $i<=count($months); $i++){
            $c = array();
            $c[] = array("v"=>$months[$i]);
            foreach($totals as $key_year=>$total){
                if(isset($total[$i])){
                    $c[] = array("v"=>$total[$i]);
                }else{
                    $c[] = array("v"=>0);
                }   
            }
            $rows[] = array("c"=>$c);
        }

        $data = array(
            "_id" => array("year"=>2017,"month"=>9),
            "total" => 1575,
            "cols" => $cols,
            "rows" => $rows
        );

        $return_data = array(
            "data" => $data
        );

        return response()->json($return_data);
    }

    public function chartRevenue(){
        $datas = DB::table('voucher_redeems')
        ->select(DB::raw('YEAR(voucher_redeems.redeem_date) as year'), DB::raw('MONTH(voucher_redeems.redeem_date) as month'), DB::raw(' count(voucher_redeems.value) as total'))
        ->join('vouchers', 'vouchers.voucher_id', '=', 'voucher_redeems.voucher_id')
        ->groupBy(DB::raw('YEAR(voucher_redeems.redeem_date), MONTH(voucher_redeems.redeem_date) DESC'))
        ->get();

        $totals = array();
        foreach($datas as $data){
            if(!isset($totals[$data->year][$data->month])){
                $totals[$data->year][$data->month] = 0;
            }
            $totals[$data->year][$data->month] += $data->total;
        }

        $months = array();
        $months[1] = "Jan";
        $months[2] = "Feb";
        $months[3] = "Mar";
        $months[4] = "Apr";
        $months[5] = "May";
        $months[6] = "Jun";
        $months[7] = "Jul";
        $months[8] = "Aug";
        $months[9] = "Sep";
        $months[10] = "Oct";
        $months[11] = "Nov";
        $months[12] = "Dec";

        $cols = array();
        $cols[] = array("id"=>"month", "label"=>"Month", "type"=>"string");
        foreach($totals as $key_year=>$total){
            $cols[] = array("id"=>"year-" . $key_year, "label"=>$key_year, "type"=>"number");
        }

        $rows = array();
        for($i=1; $i<=count($months); $i++){
            $c = array();
            $c[] = array("v"=>$months[$i]);
            foreach($totals as $key_year=>$total){
                if(isset($total[$i])){
                    $c[] = array("v"=>$total[$i]);
                }else{
                    $c[] = array("v"=>0);
                }   
            }
            $rows[] = array("c"=>$c);
        }

        $data = array(
            "_id" => array("year"=>2017,"month"=>9),
            "total" => 1575,
            "cols" => $cols,
            "rows" => $rows
        );
        $return_data = array(
            "data" => $data
        );

        return response()->json($return_data);
    }

    public function chartAverageTransaction(){

        $datas = DB::table('voucher_redeems')
        ->select(DB::raw('YEAR(voucher_redeems.redeem_date) as year'), DB::raw('MONTH(voucher_redeems.redeem_date) as month'), DB::raw(' sum(voucher_redeems.value) as total'))
        ->join('vouchers', 'vouchers.voucher_id', '=', 'voucher_redeems.voucher_id')
        ->groupBy(DB::raw('YEAR(voucher_redeems.redeem_date), MONTH(voucher_redeems.redeem_date) DESC'))
        ->get();

        $totals = array();
        foreach($datas as $data){
            if(!isset($totals[$data->year][$data->month])){
                $totals[$data->year][$data->month] = 0;
            }
            $totals[$data->year][$data->month] += $data->total;
        }

        $months = array();
        $months[1] = "Jan";
        $months[2] = "Feb";
        $months[3] = "Mar";
        $months[4] = "Apr";
        $months[5] = "May";
        $months[6] = "Jun";
        $months[7] = "Jul";
        $months[8] = "Aug";
        $months[9] = "Sep";
        $months[10] = "Oct";
        $months[11] = "Nov";
        $months[12] = "Dec";

        $cols = array();
        $cols[] = array("id"=>"month", "label"=>"Month", "type"=>"string");
        foreach($totals as $key_year=>$total){
            $cols[] = array("id"=>"year-" . $key_year, "label"=>$key_year, "type"=>"number");
        }

        $rows = array();
        for($i=1; $i<=count($months); $i++){
            $c = array();
            $c[] = array("v"=>$months[$i]);
            foreach($totals as $key_year=>$total){
                if(isset($total[$i])){
                    $c[] = array("v"=>$total[$i]);
                }else{
                    $c[] = array("v"=>0);
                }   
            }
            $rows[] = array("c"=>$c);
        }

        $data = array(
            "_id" => array("year"=>2017,"month"=>9),
            "total" => 1575,
            "cols" => $cols,
            "rows" => $rows
        );

        $return_data = array(
            "data" => $data
        );

        return response()->json($return_data);
        
    }
}