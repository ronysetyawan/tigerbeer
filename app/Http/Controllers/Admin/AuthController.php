<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use Hash;
use Session;

class AuthController extends Controller
{
    public function login(){
        return view('admin.login');
    }

    public function loginAction(Request $request){
        $username = $request->username;
        $password = $request->password;

        $user_info = \App\User::where("username", $username)->where("status", 1)->first();
        
        if ($user_info && Hash::check($password, $user_info->password) && $user_info->type=='admin'
        && Auth::guard('admin')->attempt(['username' => $username, 'password' => $password, 'status' => 1, 'type' => $user_info->type])) {
            if($user_info->role=='property'){
                return response()->json([
                    "redirect" => url('admin/report/redemption-by-property')
                ]);
            }
            return response()->json([
                "redirect" => url('admin/dashboard')
            ]);
        }else{
            return response()->json([
                "message" => "Login Failed!",
            ], 422);
        }
    }

    public function loginOutlet(){
        return view('admin.login_outlet');
    }

    public function loginOutletGo(Request $request){
        $pin = $request->pin;
        $username = 'outlet';
        $password = 'outet1qazxsw2';

        $prop_info = \App\Property::where("pin", $pin)->where("status", 1)->first();
        
        if ($prop_info && Auth::guard('admin')->attempt(['username' => $username, 'password' => $password, 'status' => 1, 'type' => 'admin'])) {
            //$user_info = \App\User::where("username", $username)->where("status", 1)->first();
            Session::set('property',$prop_info);
            return response()->json([
                "message" => "Outlet login success",
                "redirect" => url('admin/voucher-outlet')
            ]);
        }else{
            return response()->json([
                "message" => "Outlet Pin incorrect.",
            ], 422);
        }
    }

    public function logout(Request $request){
        Auth::guard('admin')->logout();

        if (Session::has('property')) {
            $prop = Session::get('property');

            return redirect('admin/login-outlet');
        }
        
        return redirect('admin/login');
    }

    public function logouOutlet(Request $request){
        Session::forget('property');
        Session::flush();

        return view('admin.login-outlet');
    }
}
