<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use \Firebase\JWT\JWT;

use DB;
use Mail;
use Auth;
use App\VoucherRedeem;
use App\ReddotLog;
use App\Voucher;
use App\Order;

class VoucherController extends BaseController
{
    protected $voucher_digit = 10;

    public function __construct(Request $request){
        parent::__construct($request, 'voucher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$this->menu_active = 'voucher';

        $filter_datas = array(
            "voucher_id" => "",
            "brand" => "",
            "name" => "",
            "email" => "",
            "mobile_number" => "",
            "status" => ""
        );
        $query_vouchers = \App\Voucher::query();
        if(Input::get("voucher_id")){
            $query_vouchers->where('voucher_id', Input::get("voucher_id"));
            $filter_datas['voucher_id'] = Input::get('voucher_id');
        }
        if(Input::get("brand")){
            $query_vouchers->where('brand_code', Input::get("brand"));
            $filter_datas['brand'] = Input::get('brand');
        }
        if(Input::get("name")){
            $get_name = Input::get("name");
            $query_vouchers->where(function($query) use($get_name){
                $query->where('owner_name', 'like', '%' . $get_name . '%');
                //$query->orWhere('owner_last_name', 'like', '%' . $get_name . '%');
            });
            $filter_datas['name'] = Input::get('name');
        }
        if(Input::get("email")){
            $query_vouchers->where('owner_email', 'like', '%' . Input::get('email') . '%');
            $filter_datas['email'] = Input::get('email');
        }
        if(Input::get("mobile_number")){
            $query_vouchers->where('owner_mobile', 'like', '%' . Input::get('mobile_number') . '%');
            $filter_datas['mobile_number'] = Input::get('mobile_number');
        }
        if(Input::get("status")){
            $query_vouchers->where('status', Input::get('status'));
            $filter_datas['status'] = Input::get('status');
        }

        $data_vouchers = $query_vouchers->orderBy('id', 'desc')->paginate(100);
        $vouchers = array();
        foreach($data_vouchers as $dt_voucher){
            $names = array();
            if($dt_voucher->owner_name){
                $names[] = $dt_voucher->owner_name;
            }
            // if($dt_voucher->owner_last_name){
            //     $names[] = $dt_voucher->owner_last_name;
            // }

            //$branch_id = $dt_voucher->branch_id;
            //$query_order_item = \App\OrderItem::where("recipient_mobile", $dt_voucher->owner_mobile);
            $total = 0;//number_format($query_order_item->sum("total"),2);
            $order = \App\Order::find($dt_voucher->order_id);
            $order_item = \App\OrderItem::where("order_id",$dt_voucher->order_id)->first();
            $voucher_redeem = \App\VoucherRedeem::where("voucher_id",$dt_voucher->voucher_id)->first();
            $vouchers[] = array(
                "voucher_id" => $dt_voucher->voucher_id,
                "balance" => ' '.number_format($dt_voucher->balance),
                "sender" => $dt_voucher->order->name,
                "owner" => implode(" ", $names),
                "email" => $dt_voucher->owner_email,
                "mobile" => $dt_voucher->owner_mobile,
                "expiry_date" => isset($dt_voucher->expiry_date) ? date("j F Y", strtotime($dt_voucher->expiry_date)) : null,
                "created_date" => date("j F Y", strtotime($order->order_date)),
                "voucher_type" => $dt_voucher->voucher_type,
                "total" => $total,
                "status" => \App\Voucher::getVoucherStatus($dt_voucher->voucher_id),
                "status_string" => $dt_voucher->status?"Active":"Not Active",
                "item_name" => $order_item->item_name,
                "property" => isset($voucher_redeem->property) ? $voucher_redeem->property : '',
            );
        }
 
        //dd($vouchers);
        return view('admin.voucher_list', array(
            "vouchers" => $vouchers,
            "filter_datas" => $filter_datas,
            "data_vouchers" => $data_vouchers,
        ));
    }

    public function indexOutlet(Request $request)
    {
        $this->menu_active = 'voucher-outlet';

        $prop = "";
        if ($request->session()->has('property')) {
            $prop = $request->session()->get('property');
        }

        $filter_datas = array(
            "voucher_id" => "",
            "brand" => "",
            "name" => "",
            "user_id" => "",
            "email" => "",
            "mobile_number" => "",
            "status" => ""
        );
        $search_vid = urlencode($request->input('search'));
        $from = urlencode($request->input('from'));
        $to = urlencode($request->input('to'));

        if (!empty($search_vid)) {
            $query_vouchers = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->where('vouchers.voucher_id', 'like', '%' . $search_vid . '%')
            ->orWhere('vouchers.user_id', 'like', '%' . $search_vid . '%')
            ->orderBy('voucher_redeems.redeem_date','desc');
        }elseif(!empty($from) && !empty($to)){
            $query_vouchers = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->whereBetween('voucher_redeems.redeem_date', [$from.' 00:00:00', $to.' 23:59:59'])
            ->orderBy('voucher_redeems.redeem_date','desc');
        }else{
            $query_vouchers = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->orderBy('voucher_redeems.redeem_date','desc');
        }
        /*
        if(Input::get("voucher_id")){
            $query_vouchers->where('voucher_id', Input::get("voucher_id"));
            $filter_datas['voucher_id'] = Input::get('voucher_id');
        }
        if(Input::get("brand")){
            $query_vouchers->where('brand_code', Input::get("brand"));
            $filter_datas['brand'] = Input::get('brand');
        }
        if(Input::get("name")){
            $get_name = Input::get("name");
            $query_vouchers->where(function($query) use($get_name){
                $query->where('owner_name', 'like', '%' . $get_name . '%');
                //$query->orWhere('owner_last_name', 'like', '%' . $get_name . '%');
            });
            $filter_datas['name'] = Input::get('name');
        }
        if(Input::get("user_id")){
            $query_users->where('user_id', Input::get("user_id"));
            $filter_datas['user_id'] = Input::get('user_id');
        }
        if(Input::get("email")){
            $query_vouchers->where('owner_email', 'like', '%' . Input::get('email') . '%');
            $filter_datas['email'] = Input::get('email');
        }
        if(Input::get("mobile_number")){
            $query_vouchers->where('owner_mobile', 'like', '%' . Input::get('mobile_number') . '%');
            $filter_datas['mobile_number'] = Input::get('mobile_number');
        }
        if(Input::get("status")){
            $query_vouchers->where('status', Input::get('status'));
            $filter_datas['status'] = Input::get('status');
        }
        */
        $query_tigerpint = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->where('voucher_redeems.product_id', 1)
            ->orderBy('voucher_redeems.redeem_date','desc');
        $total_tigerpint = $query_tigerpint->sum('voucher_redeems.value');  

        $query_tigercrystal = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->where('voucher_redeems.product_id', 2)
            ->orderBy('voucher_redeems.redeem_date','desc');
        $total_tigercrystal = $query_tigercrystal->sum('voucher_redeems.value');  

        $query_tigerdraught = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->where('voucher_redeems.product_id', 3)
            ->orderBy('voucher_redeems.redeem_date','desc');
        $total_tigerdraught = $query_tigerdraught->sum('voucher_redeems.value'); 

        $query_tigerquart = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->where('voucher_redeems.product_id', 4)
            ->orderBy('voucher_redeems.redeem_date','desc');
        $total_tigerquart = $query_tigerquart->sum('voucher_redeems.value'); 

        $query_tigercrystalquart = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
            ->join('products','voucher_redeems.product_id','=','products.id')
            ->where('voucher_redeems.property_id',$prop->id)
            ->where('voucher_redeems.product_id', 5)
            ->orderBy('voucher_redeems.redeem_date','desc');
        $total_tigercrystalquart = $query_tigercrystalquart->sum('voucher_redeems.value'); 
        
        // dd($total_tigercrystal);
        // if ($query_total->where('voucher_redeems.product_id', 1))$total_tigerpint = $query_total->sum('voucher_redeems.value');
        // if ($query_total->where('voucher_redeems.product_id', 2))$total_tigercrystal = $query_total->sum('voucher_redeems.value');
        // if ($query_total->where('voucher_redeems.product_id', 3))$total_tigerdraught = $query_total->sum('voucher_redeems.value');
        // if ($query_total->where('voucher_redeems.product_id', 4))$total_tigerquart = $query_total->sum('voucher_redeems.value');
        // if ($query_total->where('voucher_redeems.product_id', 5))$total_tigercrystalquart = $query_total->sum('voucher_redeems.value');
        
        // dd($total_tigercrystal);
        // $query_total = VoucherRedeem::all();
        // $total_tigerpint = $query_total->where('voucher_redeems.product_id', 1)->sum('voucher_redeems.value');
        // $total_tigercrystal = $query_total->where('voucher_redeems.product_id', 2)->sum('voucher_redeems.value');
        // $total_tigerdraught = $query_total->where('voucher_redeems.product_id', 3)->sum('voucher_redeems.value');
        // $total_tigerquart = $query_total->where('voucher_redeems.product_id', 4)->sum('voucher_redeems.value');
        // $total_tigercrystalquart = $query_total->where('voucher_redeems.product_id', 5)->sum('voucher_redeems.value');

        if ($total_tigerpint == null) $total_tigerpint = 0;
        if ($total_tigercrystal == null) $total_tigercrystal = 0;
        if ($total_tigerdraught == null) $total_tigerdraught = 0;
        if ($total_tigerquart == null) $total_tigerquart = 0;
        if ($total_tigercrystalquart == null) $total_tigercrystalquart = 0;
            
        $total = $query_vouchers->sum('voucher_redeems.value');
        if (!$total) {
            $total = 0;
        }
        //$data_vouchers = $query_vouchers->get();
        $data_vouchers = $query_vouchers->paginate(50);     // set limit to 50 datas
        $vouchers = array();
        foreach($data_vouchers as $dt_voucher){
            $names = array();
            if($dt_voucher->owner_first_name){
                $names[] = $dt_voucher->owner_first_name;
            }
            if($dt_voucher->owner_last_name){
                $names[] = $dt_voucher->owner_last_name;
            }
            $url = url('voucher/'.$dt_voucher->code);

            //$query_order_item = \App\OrderItem::where("recipient_mobile", $dt_voucher->owner_mobile);
            //$total = number_format($query_order_item->sum("total"),2);
            //$order = \App\Order::find($dt_voucher->order_id);
            $vouchers[] = array(
                "voucher_id" => $dt_voucher->voucher_id,
                "product_name" => $dt_voucher->name,
                "user_id" => $dt_voucher->user_id,
                "balance" => $dt_voucher->balance,
                "qty_redeem" => $dt_voucher->value,
                "owner" => $dt_voucher->owner_name,
                "email" => $dt_voucher->owner_email,
                "mobile" => $dt_voucher->owner_mobile,
                "redeem_date" => date("j F Y H:i:s", strtotime($dt_voucher->redeem_date)),
                "status" => \App\Voucher::getVoucherStatus($dt_voucher->voucher_id),
                "status_string" => $dt_voucher->status?"Active":"Not Active",
                "voided" => $dt_voucher->voided,
                "voucher_link" => $url
            );

        }

        $query_today = Voucher::join('voucher_redeems','vouchers.voucher_id','=','voucher_redeems.voucher_id')
                                ->whereDate('voucher_redeems.redeem_date','=',date('Y-m-d'))
                                ->where('voucher_redeems.property_id',$prop->id);
       
        $total_today = $query_today->sum('voucher_redeems.value');
        if (!$total_today) {
            $total_today = 0;
        }
        
        $data_return = array(
            "vouchers" => $vouchers,
            "prop_name" => $prop->name,
            "total" => $total,
            "total_tigerpint" => $total_tigerpint,
            "total_tigercrystal" => $total_tigercrystal,
            "total_tigerdraught" => $total_tigerdraught,
            "total_tigerquart" => $total_tigerquart,
            "total_tigercrystalquart" => $total_tigercrystalquart,
            "total_today" => $total_today,
            "filter_datas" => $filter_datas,
            "data_vouchers" => $data_vouchers,
        );
        // dd($data_return);
        return view('admin.voucher_list_outlet', $data_return);
    }

    public function show($voucher_id){
        $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->first();

        $voucher_owner_fullnames = array();
        // if($voucher_info->owner_title){
        //     $voucher_owner_fullnames[] = $voucher_info->owner_title;
        // }
        if($voucher_info->owner_name){
            $voucher_owner_fullnames[] = $voucher_info->owner_name;
        }
        // if($voucher_info->owner_last_name){
        //     $voucher_owner_fullnames[] = $voucher_info->owner_last_name;
        // }

        $extend_histories = array();
        $data_extend_histories = \App\VoucherExtend::where("voucher_id", $voucher_id)->get();
        if($data_extend_histories){
            foreach($data_extend_histories as $dt){
                $extend_histories[] = array(
                    "new_expiry_date" => date("d M Y", strtotime($dt->new_expiry_date)),
                    "old_expiry_date" => date("d M Y", strtotime($dt->old_expiry_date))
                );
            }
        }

        $redemptions = array();
        $data_redemptions = \App\VoucherRedeem::where("voucher_id", $voucher_info->voucher_id)->get();
        foreach($data_redemptions as $dt){
            $redemptions[] = array(
                "date" => $dt->date,
                "amount" => $dt->amount,
                "cashier" => $dt->cashier,
                "receipt_no" => $dt->receipt,
                "property" => $dt->property,
                "card_number" => $dt->service,
                "is_void" => ""
            );
        }
        
        $sms_logs = array();
        $data_sms_logs = \App\SmsLog::where("voucher_id", $voucher_info->voucher_id)->get();
        foreach($data_sms_logs as $dt){
            $sms_logs[] = array(
                "date" => $dt->date,
                "mobile" => $dt->mobile,
                "status" => $dt->status
            );
        }

        $email_logs = array();
        /*$data_email_logs = \App\DeliveryHistory::where("voucher_id", $voucher_info->voucher_id)->get();
        foreach($data_email_logs as $dt){
            $email_logs[] = array(
                "email_id" => $dt->email_id,
                "date" => date("d M Y", strtotime($dt->send_date)),
                "type" => $dt->type
            );
        }*/

        $token_qrcode = array(
            "voucher_id" => $voucher_info->voucher_id
        );

        $jwt_qrcode = JWT::encode($token_qrcode, env('JWT_KEY_QRCODE'));
        $order = \App\Order::find($voucher_info->order_id);
        $data_return = array(
            "voucher_info" => array(
                "image" => url('files/occasion-deal/' . $voucher_info->voucher_id),
                "qrcode" => url('qrcode/' . $jwt_qrcode),
                "voucher_id" => $voucher_info->voucher_id,
                "order_id" => $voucher_info->id,
                "status" => \App\Voucher::getVoucherStatus($voucher_info->voucher_id),
                "expiry_date_formatted" => date("d M Y", strtotime($voucher_info->expiry_date)),
                "transaction_date" => date("d M Y", strtotime($order->order_date)),
                "balance" => number_format($voucher_info->balance),
                "voided" => $voucher_info->voided?true:false,
                "expiry_date" => $voucher_info->expiry_date
            ),
            "owner_info" => array(
                "fullname" => implode(" ", $voucher_owner_fullnames),
                "email" => $voucher_info->owner_email,
                "mobile" => $voucher_info->owner_mobile,
                "country" => $voucher_info->owner_country
            ),
            "extend_histories" => $extend_histories,
            "redemptions" => $redemptions,
            "sms_logs" => $sms_logs,
            "email_logs" => $email_logs
        );
        return view('admin.voucher_view', $data_return);
    }

    public function getOwner($voucher_id){

        $data_titles = \App\Term::where("type", "title")->where("status", 1)->get();
        $titles = array();
        foreach($data_titles as $dt){
            $titles[] = array(
                "value" => $dt->id,
                "label" => $dt->name
            );
        }

        $data_countries = \App\Country::get();
        $countries = array();
        foreach($data_countries as $dt_country){
            $countries[] = array(
                "value" => $dt_country->id,
                "label" => $dt_country->country_name,
                "calling_code" => $dt_country->calling_code
            );
        }

        $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->first();

        $mobile = str_replace($voucher_info->owner_calling_code, "", $voucher_info->owner_mobile);

        $data_return = array(
            "titles" => $titles,
            "countries" => $countries,
            "owner_info" => array(
                "title" => $voucher_info->owner_title,
                "first_name" => $voucher_info->owner_name,
                //"last_name" => $voucher_info->owner_last_name,
                "email" => $voucher_info->owner_email,
                "country" => $voucher_info->owner_country_id,
                "mobile" => $mobile
            )
        );

        return response()->json($data_return);
    }

    public function updateOwner($voucher_id, Request $request){

        $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'mobile' => 'required'
        ]);
        
        if(\App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->count()<=0){
            return response()->json([
                "message" => "Voucher not found"
            ], 422);
        }else{
            $voucher = \App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->first();
            //$voucher->owner_title = $request->title;
            $voucher->owner_name = $request->first_name;
            //$voucher->owner_last_name = $request->last_name;
            $voucher->owner_email = $request->email;

            $country_info = \App\Country::find($request->country);

            $voucher->owner_country_id = $country_info->id;
            $voucher->owner_country_code_2 = $country_info->country_code_2;
            $voucher->owner_country_code_3 = $country_info->country_code_3;
            $voucher->owner_country = $country_info->country_name;
            $voucher->owner_calling_code = $country_info->calling_code;
            $voucher->owner_mobile = $request->mobile;

            $voucher->save();

            $voucher_fullnames = array();
            // if($voucher->owner_title){
            //     $voucher_fullnames[] = $voucher->owner_title;
            // }
            if($voucher->owner_name){
                $voucher_fullnames[] = $voucher->owner_name;
            }
            // if($voucher->owner_last_name){
            //     $voucher_fullnames[] = $voucher->owner_last_name;
            // }

            return response()->json([
                "fullname" => implode(" ", $voucher_fullnames),
                "email" => $voucher->owner_email,
                "mobile" => $voucher->owner_mobile,
                "country" => $voucher->owner_country
            ]);
        }
    }

    private function getShortUrl($voucher_id){
        //// shortlink begin
        $username = 'apbuser';
        $password = '^shortIsKin9#%';
        $api_url =  'http://giv.one' . '/add/self/qrcode';

        $token = array("voucher_id" => $voucher_id);
        $jwt = \Firebase\JWT\JWT::encode($token, 'self-serve-2017-secret-key');

        $link = 'https://selfserve.giv.one/qrcode' . '/' . $jwt;
        // Init the CURL session
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, 1);              // This is a POST request
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(     // Data to POST
            'link'      => $link,
            'username'  => $username,
            'password'  => $password
        ));
        // Fetch and return content
        $c_shortlink = curl_exec($ch);
        curl_close($ch);

        $data_shortlink = json_decode($c_shortlink);

        return $data_shortlink;
    }

    public function url($voucher_id){
        $data_shortlink = $this->getShortUrl($voucher_id);

        return response()->json(array(
            "link" => $data_shortlink->shorturl
        ));
    }

    public function resendSms(Request $request, $voucher_id){

        if(\App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->count()<=0){
            return response()->json([
                "message" => "Voucher not found"
            ], 422);
        }else{

            $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->first();

            $error = false;

            if(!$voucher_info){
                $error = 'Voucher not found!';
            }

            if(!$error){
                //// send sms
                $api_key = env('NEXMO_KEY');
                $api_secret = env('NEXMO_SECRET');
                $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
                $sms_text = 'Thank you for your support. View your voucher at '. 
                url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id) .' We look forward to welcoming you back very soon!';

                $to = $voucher_info->owner_calling_code . $voucher_info->owner_mobile;
                //$to = '+62...';  // test

                $message = $client->message()->send([
                    'to' => $to,
                    'from' => 'Tiger',
                    'text' => $sms_text
                ]);

                if($message){
                    //echo "SMS sent!";
                    //$result['message'] = true;
                    //$result['message_text'] = $text_message;
                    $sms_log = new \App\SmsLog;
                    $sms_log->voucher_id = $voucher_info->voucher_id;
                    $sms_log->date = date("Y-m-d H:i:s");
                    $sms_log->mobile = $to;
                    $sms_log->text = $sms_text;
                    $sms_log->status = 'Resend Success';
                    $sms_log->created_at = date("Y-m-d H:i:s");
                    $sms_log->updated_at = date("Y-m-d H:i:s");
                    $sms_log->save(); 
                }else{
                    //echo "Sorry SMS is failed!";
                    //$result['message'] = false;
                    //$result['message_text'] = $text_message;
                    //$result['message_reason'] = "Sorry SMS is failed!";
                    $sms_log = new \App\SmsLog;
                    $sms_log->voucher_id = $voucher_info->voucher_id;
                    $sms_log->date = date("Y-m-d H:i:s");
                    $sms_log->mobile = $to;
                    $sms_log->text = $sms_text;
                    $sms_log->status = 'Resend Failed';
                    $sms_log->created_at = date("Y-m-d H:i:s");
                    $sms_log->updated_at = date("Y-m-d H:i:s");
                    $sms_log->save(); 
                }

            }

            if($error){
                return response()->json(array(
                    "message" => $error
                ), 422);
            }else{
                return response()->json(array(
                    "message" => "Sms sent."
                ));
            }
        
        }
    }

    public function resendEmail(Request $request, $voucher_id){

        if(\App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->count()<=0){
            return response()->json([
                "message" => "Voucher not found"
            ], 422);
        }else{

            try {
                $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->first();

                $error = false;

                if(!$voucher_info){
                    $error = 'Voucher not found!';
                }

                $order_info = \App\Order::find($voucher_info->order_id);
                $user_info = \App\User::find($voucher_info->user_id);

                $email = $user_info->email;
                //$email = "adit@cprvision.com";  // test
                $name = $user_info->first_name;
                $email_from = 'Tiger';
                $email_subject = 'Thank you for your support!';
                $email_tpl = 'emails.purchase';
                $desc = " x Tiger Beer";

                $order_items = \App\OrderItem::where('order_id', $voucher_info->order_id)->get();
                $items = [];
                foreach($order_items as $order_item){
                    $items[] = [
                        'voucher_id' => $voucher_info->voucher_id,
                        'entitlement' => $order_item->total_pint . $desc,
                        'qty' => $order_item->total_pint,
                        'total' => "SGD " . number_format($order_item->total, 2),
                        'expired_date' => '30 September 2020',
                    ];
                }

                $string_token = \Firebase\JWT\JWT::encode(array(
                    "order_id" => $voucher_info->order_id
                ), "email-preview-purchase-tiger");
        
                $data_email = array(
                    "string_token" => $string_token,
                    'name' => $voucher_info->user->first_name,
                    'items' => $items
                );
                
                $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
                    $m->from('no-reply@apb.giv.one', $email_from);
                    $m->bcc(['cprv.notifications@gmail.com']);
                    $m->to($email, $name)->subject($email_subject);
                });

                if($sendEmail){
                    //log report sent
                    $voucher_info->sent = 1;
                    $voucher_info->save();
        
                    $result['email'] = true;
                    $result['data_email'] = $data_email;
                    $result['result_email'] = $sendEmail;
                    //var_dump($result);
        
                    //write to email logs table
                    $mail_log = new \App\EmailLog;
                    $mail_log->voucher_id = $voucher_info->voucher_id;
                    $mail_log->date = date('Y-m-d H:i:s');
                    $mail_log->email_id = $email;
                    $mail_log->type = 'voucher.resend';
                    $mail_log->save();
                } else {
                    //write to email logs table
                    $mail_log = new \App\EmailLog;
                    $mail_log->voucher_id = $voucher_info->voucher_id;
                    $mail_log->date = date('Y-m-d H:i:s');
                    $mail_log->email_id = $email;
                    $mail_log->type = 'resend.failed';
                    $mail_log->save();
                }
                //// send email end

            } catch(Exception $e) {
                echo 'Message: ' .$e->getMessage();
            }

            if($error){
                return response()->json(array(
                    "message" => $error
                ), 422);
            }else{
                return response()->json(array(
                    "message" => "Email sent successfully!"
                ));
            }
        
        }
    }

    public function redeem(Request $request, $voucher_id){

        $this->validate($request, [
            'receipt' => 'required',
            'cashier' => 'required',
            'amount' => 'required'
        ]);

        if(\App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->count()<=0){
            return response()->json([
                "message" => "Voucher not found"
            ], 422);
        }else{

            $voucher_info = \App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->first();

            if($voucher_info->balance>=$request->amount){

                $voucher_redeem = new \App\VoucherRedeem;
                $voucher_redeem->voucher_id = $voucher_id;
                $voucher_redeem->receipt = $request->receipt;
                $voucher_redeem->amount = $request->amount;
                $voucher_redeem->redeem_date = $request->date;
                $voucher_redeem->redeem_type = 'admin';
                $voucher_redeem->save();

                $old_balance = $voucher_info->balance;
                $new_balance = ($old_balance - $request->amount);
                $voucher_info->balance = $new_balance;

                $voucher_info->save();

                $balance = number_format($new_balance, 2);

                return response()->json(array(
                    "balance" => $balance,
                    "message" => "Redeem successfully"
                ));
            }else{
                return response()->json(array(
                    "message" => "Insufficient balance"
                ), 422);
            }
        
        }
    }

    public function extend(Request $request, $voucher_id){

        if(\App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->count()<=0){
            return response()->json([
                "message" => "Voucher not found"
            ], 422);
        }else{

            $voucher = \App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->first();
        
            if(strtotime($voucher->expiry_date) >= strtotime($request->date)){
                return response()->json(array(
                    "message" => "The extend date cannot be lower or equal than expiry date"
                ), 422);
            }

            $voucher_extend = new \App\VoucherExtend;
            $voucher_extend->voucher_id = $voucher_id;
            $voucher_extend->old_expiry_date = $voucher->expiry_date;
            $voucher_extend->new_expiry_date = $request->date;
            $voucher_extend->modified_by = $this->user_id;
            $voucher_extend->save();

            $voucher->expiry_date = $request->date;
            $voucher->voided = 0;
            $voucher->save();

            return response()->json([
                "new_expiry_date" => $voucher->expiry_date,
                "new_expiry_date_formatted" => date("d M Y", strtotime($voucher->expiry_date))
            ]);

        }
    }

    public function void($voucher_id){

        if(\App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->count()<=0){
            return response()->json([
                "message" => "Voucher not found"
            ], 422);
        }else{
            $voucher = \App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->first();

            $voucher_void = new \App\VoucherVoid;
            $voucher_void->voucher_id = $voucher_id;
            $voucher_void->modified_by = $this->user_id;
            $voucher_void->save();

            $voucher->voided = 1;
            $voucher->save();
        }
    }

    public function emailLog(){
        
    }

    public function smsLog(){
        
    }

    public function extendHistory(){
        
    }

    public function redemption($voucher_id){
        $redemptions = array();
        $data_redemptions = \App\VoucherRedeem::where("voucher_id", $voucher_id)->get();
        foreach($data_redemptions as $dt){
            $redemptions[] = array(
                "date" => $dt->date,
                "amount" => $dt->amount,
                "cashier" => $dt->cashier,
                "receipt_no" => $dt->receipt,
                "property" => $dt->property,
                "description" => $dt->service,
                "is_void" => ""
            );
        }
        $data_return = array(
            "redemptions" => $redemptions
        );
        return response()->json($data_return);

    }

    public function updateEventTicketStatus($voucher_id){
        if(\App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->count()<=0){
            return response()->json([
                "message" => "Voucher not found"
            ], 422);
        }else{
            $voucher = \App\Voucher::where("voucher_id", $voucher_id)->where("voided", 0)->first();

            if($voucher->is_event == 0){
                $status = 1;
                $voucher->is_event = $status;
                $voucher->ticket_event_issueddate = date('Y-m-d H:i:s');
                $voucher->ticket_event_issuedby = $this->user_id;
                
            }else{
                $status = 0;
                $voucher->is_event = $status;
                $voucher->ticket_event_issueddate = NULL;
                $voucher->ticket_event_issuedby = NULL;
            }

            $voucher->save();

            $data_return = array(
                "status" => $status
            );
            return response()->json($data_return);
        }
    }

    public function voucherredeem(){
        $data = VoucherRedeem::paginate(12);
        $menu_active = 'voucher-redeem';
        return view('admin.voucherredeem', compact('data', 'menu_active'));
    }

    public function payment(Request $request){

        //$data = ReddotLog::paginate(12);
        DB::enableQueryLog();
        $data_logs = DB::table('reddot_logs')
                    ->join('vouchers', 'vouchers.order_id', '=', 'reddot_logs.order_id')
                    //->join('orders', 'orders.id', '=', 'vouchers.order_id')
                    ->paginate(12);
        //var_dump($data_logs);
        $voucher_id = urlencode($request->input('voucher_id'));
        if (!empty($voucher_id)) {
            $data = ReddotLog::join('vouchers', 'vouchers.order_id', '=', 'reddot_logs.order_id')
            ->where('voucher_id', 'like', '%' . $voucher_id . '%')->paginate(12);
        }else{
            $data = ReddotLog::join('vouchers', 'vouchers.order_id', '=', 'reddot_logs.order_id')
                    ->paginate(12);
        }
        
        //var_dump($data);

        $queries = DB::getQueryLog();
        //dd($queries);
        $menu_active = 'payment';
        return view('admin.payment', compact('data', 'menu_active'));
    }
}
