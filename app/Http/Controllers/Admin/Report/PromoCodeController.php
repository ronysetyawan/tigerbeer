<?php

namespace App\Http\Controllers\Admin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\BaseController;

use Illuminate\Support\Facades\Input;

use DB;

use Excel;

use PDF;

class PromoCodeController extends BaseController
{

    public function __construct(Request $request){
        parent::__construct($request, 'report-promo-code');
    }

    public function index(){

        $data_promocodes = \App\PromoCode::get();
        $promocodes = [];
        foreach($data_promocodes as $dt){
            $type = "";
            if($dt->type=="referral"){
                $type = "Referral";
            }else if($dt->type=="redemption"){
                $type = "Redemption";
            }
            $promocodes[] = [
                "code" => $dt->code,
                "type" => $type,
                "issued_by" => isset($dt->user->first_name)?$dt->user->first_name:'',
                "issued_date" => date("d/m/Y", strtotime($dt->created_at)),
                "used" => $dt->used?true:false,
                "used_on" => $dt->used?date("d/m/Y", strtotime($dt->used_on)):false
            ];
        }

        return view('admin.report.promo_code', array(
            "record" =>  100,//$total_record . ($total_record>1 ? ' records' : ' record'),
            "promocodes" => $promocodes
        ));
    }

    public function downloadExcel(){
        $datas = array();
        
        $datas[] = array(
            'trans_type' => '',
            'voucher_type' => '',
            'trans_date' => '',
            'voucher_id' => '',
            'trans_id' => '',
            'purchaser' => '',
            'purchaser_email' => '',
            'purchaser_mobile' => '',
            'recipient' => '',
            'recipient_email' => '',
            'recipient_mobile' => '',
            'value' => '',
            'amount_paid' => '',
            'redemption_amount' => '',
            'is_void' => '',
            'property' => '',
            'status' => ''
        ); 

        Excel::create('Full Transaction Report', function($excel) use($datas){
            $excel->setTitle('Full Transaction Report');
            $excel->setCreator('E-Gifting')->setCompany('SPD - CPR Vision');
            $excel->setDescription('A demonstration to change the file properties');
            $excel->sheet('Sheet 1', function($sheet) use($datas){
                $sheet->loadView('admin.report.redemption_by_voucher_excel', array(
                    "datas" => $datas
                ));
            });
            $excel->download('xlsx');
        });
    }

    public function downloadPdf(){

        $datas = array();
        
        $datas[] = array(
            'trans_type' => '',
            'voucher_type' => '',
            'trans_date' => '',
            'voucher_id' => '',
            'trans_id' => '',
            'purchaser' => '',
            'purchaser_email' => '',
            'purchaser_mobile' => '',
            'recipient' => '',
            'recipient_email' => '',
            'recipient_mobile' => '',
            'value' => '',
            'amount_paid' => '',
            'redemption_amount' => '',
            'is_void' => '',
            'property' => '',
            'status' => ''
        ); 

        $pdf = PDF::loadView('admin.report.redemption_by_voucher_pdf', array(
            "datas" => $datas
        ))->setOrientation('landscape');
        
        //return $pdf->inline();
        return $pdf->download('invoice.pdf');
    }
}
