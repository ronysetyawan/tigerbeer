<?php

namespace App\Http\Controllers\Admin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\BaseController;

use Illuminate\Support\Facades\Input;

use DB;

use Excel;

use PDF;

class FullTransactionController extends BaseController
{
    public function __construct(Request $request){
        parent::__construct($request, 'report-full-transaction');
    }

    private function _getData(){
        $transactions_data_query = DB::table(DB::raw(
            '
            ((SELECT
            "Purchase" AS trans_type,
            o.order_date as trans_date,
            v.voucher_id,
            v.user_id,
            v.is_event,
            o.id AS trans_id,
            o.name AS purchaser_name,
            o.email AS purchaser_email,
            o.calling_code AS purchaser_calling_code,
            o.mobile AS purchaser_mobile,
            o.total_pint as total_pint,
            o.total as total,
            "" as redemption_pint,
            "" as property
            FROM
            orders AS o 
            INNER JOIN vouchers AS v on o.user_id = v.user_id
            INNER JOIN reddot_logs
            ON o.id = reddot_logs.order_id
            WHERE reddot_logs.result = "successful"
            )
            UNION
            (SELECT
            "Registration" AS trans_type,
            f.created_at as trans_date,
            f.voucher_id,
            f.user_id,
            f.is_event,
            f.id AS trans_id,
            f.owner_name AS purchaser_name,
            f.owner_email AS purchaser_email,
            f.owner_calling_code AS purchaser_calling_code,
            f.owner_mobile AS purchaser_mobile,
            "" as total_pint,
            "" as total,
            "" as redemption_pint,
            "" as property
            FROM
            vouchers AS f
            )
            UNION
            (SELECT
            "Redemption" AS trans_type,
            vr.redeem_date as trans_date,
            v.voucher_id,
            v.user_id,
            v.is_event,
            vr.id AS trans_id,
            v.owner_name AS purchaser_name,
            v.owner_email AS purchaser_email,
            v.owner_calling_code AS purchaser_calling_code,
            v.owner_mobile AS purchaser_mobile,
            "" as total_pint,
            "" as total,
            vr.value as redemption_pint,
            vr.property as property
            FROM
            voucher_redeems AS vr
            LEFT JOIN vouchers AS v ON vr.voucher_id = v.voucher_id)) as tb1'
        ));

        $transactions_data_query->orderBy("trans_date", "desc");

        if(Input::get("start-date") && Input::get("end-date") && strtotime(Input::get("start-date"))<=strtotime(Input::get("end-date"))){
            $transactions_data_query->where('trans_date', '>=' , Input::get("start-date"). " 00:00:00");
            $transactions_data_query->where('trans_date', '<=', Input::get("end-date") . " 59:59:59");
        }
        if(Input::get("type")){
            $transactions_data_query->where('trans_type', Input::get("type"));
        }
        if(Input::get("mobile_number")){
            $transactions_data_query->where('purchaser_mobile', 'like', '%' . trim(Input::get('mobile_number')) . '%');
        }

        return $transactions_data_query;
    }

    public function index(){
        $datas = array();

        $filter_datas = [];

        if(Input::get("start-date") && Input::get("end-date") && strtotime(Input::get("start-date"))<=strtotime(Input::get("end-date"))){
             }
        if(Input::get("type")){
            $filter_datas['type'] = Input::get('type');
        }

        if(Input::get("mobile_number")){
            $filter_datas['mobile_number'] = Input::get('mobile_number');
        }

        $transactions_data_query = $this->_getData();

        $total_record = $transactions_data_query->count();
    
        $transactions_data = $transactions_data_query->paginate(20);

        if($transactions_data){
            foreach($transactions_data as $dt){

                $last_pay = \App\ReddotLog::where('order_id', $dt->trans_id)->first();
                $last_status = '';
                if ($last_pay) {
                    if ($last_pay->result == 'successful') {
                        $last_status = 'Success';
                    } else {
                        $last_pay = 'Failed';
                    }
                }

                $datas[] = array(
                    "trans_type" => $dt->trans_type,
                    "trans_date" => date("d/m/Y", strtotime($dt->trans_date)),
                    "voucher_id" => $dt->voucher_id,
                    "trans_id" => $dt->trans_id,
                    "purchaser" => $dt->purchaser_name,
                    "purchaser_email" => $dt->purchaser_email,
                    "purchaser_mobile" => $dt->purchaser_calling_code . $dt->purchaser_mobile,
                    "total_pint" => $dt->total_pint?number_format($dt->total_pint):"",
                    "total" => $dt->total?("SGD " . number_format($dt->total,2)):"",
                    "redemption_pint" => $dt->redemption_pint?number_format($dt->redemption_pint):"",
                    "property" => $dt->property,
                    "status" => isset(\App\Voucher::getVoucherStatus($dt->voucher_id)['label']) ? \App\Voucher::getVoucherStatus($dt->voucher_id)['label'] : "",
                    "last_status" => $last_status
                );
            }
        }

        $string_params = [];
        if($filter_datas){
            foreach($filter_datas as $key=>$filter_data){
                $string_params[] = ($key . "=" . $filter_data);
            }
        }
        
        $string_param = implode("&", $string_params);
        $string_param = $string_param?("?" . $string_param):"";

        return view('admin.report.full_transaction', array(
            "datas" => $datas,
            "filter_datas" => $filter_datas,
            "total_record" => $total_record,
            "download_link" => [
                "excel" => [
                    "title" => "Excel",
                    "url" => ( url("admin/report/full-transaction/download/excel") . $string_param),
                ]
            ]
        ));
    }

    public function downloadExcel(){
        
        $transactions_data_query = $this->_getData();
    
        $transactions_data = $transactions_data_query->get();

        $datas = [];

        if($transactions_data){
            foreach($transactions_data as $dt){

                $last_pay = \App\ReddotLog::where('order_id', $dt->trans_id)->first();
                $last_status = '';
                if ($last_pay) {
                    if ($last_pay->result == 'successful') {
                        $last_status = 'Success';
                    } else {
                        $last_pay = 'Failed';
                    }
                }

                $datas[] = array(
                    "trans_type" => $dt->trans_type,
                    "trans_date" => date("d/m/Y", strtotime($dt->trans_date)),
                    "voucher_id" => $dt->voucher_id,
                    "trans_id" => $dt->trans_id,
                    "purchaser" => $dt->purchaser_name,
                    "purchaser_email" => $dt->purchaser_email,
                    "purchaser_mobile" => $dt->purchaser_calling_code . $dt->purchaser_mobile,
                    "total_pint" => $dt->total_pint?number_format($dt->total_pint):"",
                    "total" => $dt->total?("SGD " . number_format($dt->total,2)):"",
                    "redemption_pint" => $dt->redemption_pint?number_format($dt->redemption_pint):"",
                    "is_void" => "is void",
                    "property" => $dt->property,
                    "status" => isset(\App\Voucher::getVoucherStatus($dt->voucher_id)['label']) ? \App\Voucher::getVoucherStatus($dt->voucher_id)['label'] : "",
                    "last_status" => $last_status
                );
            }
        }

        Excel::create('Full Transaction Report', function($excel) use($datas){
            $excel->setTitle('Full Transaction Report');
            $excel->setCreator('Erdinger')->setCompany('SPD - CPR Vision');
            $excel->setDescription('Full Transaction Report');
            $excel->sheet('Sheet 1', function($sheet) use($datas){
                $sheet->loadView('admin.report.full_transaction_excel', array(
                    "datas" => $datas
                ));
            });
            $excel->download('xlsx');
        });
    }

    public function downloadPdf(){

        $transactions_data_query = $this->_getData();
    
        $transactions_data = $transactions_data_query->get();

        $datas = [];

        if($transactions_data){
            foreach($transactions_data as $dt){

                $last_pay = \App\ReddotLog::where('order_id', $dt->trans_id)->first();
                $last_status = '';
                if ($last_pay) {
                    if ($last_pay->result == 'successful') {
                        $last_status = 'Success';
                    } else {
                        $last_pay = 'Failed';
                    }
                }

                $datas[] = array(
                    "trans_type" => $dt->trans_type,
                    "trans_date" => date("d/m/Y", strtotime($dt->trans_date)),
                    "voucher_id" => $dt->voucher_id,
                    "trans_id" => $dt->trans_id,
                    "purchaser" => $dt->purchaser_name,
                    "purchaser_email" => $dt->purchaser_email,
                    "purchaser_mobile" => $dt->purchaser_calling_code . $dt->purchaser_mobile,
                    "total_pint" => $dt->total_pint?number_format($dt->total_pint):"",
                    "total" => $dt->total?("SGD " . number_format($dt->total,2)):"",
                    "redemption_pint" => $dt->redemption_pint?number_format($dt->redemption_pint):"",
                    "is_void" => "is void",
                    "property" => $dt->property,
                    "status" => isset(\App\Voucher::getVoucherStatus($dt->voucher_id)['label']) ? \App\Voucher::getVoucherStatus($dt->voucher_id)['label'] : "",
                    "last_status" => $last_status
                );
            }
        }

        $pdf = PDF::loadView('admin.report.full_transaction_pdf', array(
            "datas" => $datas
        ))->setOrientation('landscape');
        
        return $pdf->inline();
        //return $pdf->download('invoice.pdf');
    }
}
