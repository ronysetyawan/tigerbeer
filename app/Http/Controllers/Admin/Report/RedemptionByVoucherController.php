<?php

namespace App\Http\Controllers\Admin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\BaseController;

use Illuminate\Support\Facades\Input;

use DB;

use Excel;

use PDF;

class RedemptionByVoucherController extends BaseController
{

    public function __construct(Request $request){
        parent::__construct($request, 'report-redemption-by-voucher');
    }

    public function index(){

        $datas = array();
        
        $total_record = 0;

        if(Input::get("voucher_id")){
            $data_voucher_redeems = \App\VoucherRedeem::where("voucher_id", Input::get("voucher_id"))->get();
            foreach($data_voucher_redeems as $dt){
                $datas[] = array(
                    "redemption_date" => $dt->redeem_date,
                    "amount" => $dt->amount,
                    "cashier" => $dt->cashier,
                    "receipt_no" => $dt->receipt,
                    "property" => $dt->property,
                    "description" => $dt->service,
                    "is_void" => "",
                );
            }

            $total_record = $data_voucher_redeems->count();
        }

        return view('admin.report.redemption_by_voucher', array(
            "record" => $total_record . ($total_record>1 ? ' records' : ' record'),
            "datas" => $datas
        ));
    }

    public function downloadExcel(){
        $datas = array();
        
        $datas[] = array(
            'trans_type' => '',
            'voucher_type' => '',
            'trans_date' => '',
            'voucher_id' => '',
            'trans_id' => '',
            'purchaser' => '',
            'purchaser_email' => '',
            'purchaser_mobile' => '',
            'recipient' => '',
            'recipient_email' => '',
            'recipient_mobile' => '',
            'value' => '',
            'amount_paid' => '',
            'redemption_amount' => '',
            'is_void' => '',
            'property' => '',
            'status' => ''
        ); 

        Excel::create('Full Transaction Report', function($excel) use($datas){
            $excel->setTitle('Full Transaction Report');
            $excel->setCreator('E-Gifting')->setCompany('SPD - CPR Vision');
            $excel->setDescription('A demonstration to change the file properties');
            $excel->sheet('Sheet 1', function($sheet) use($datas){
                $sheet->loadView('admin.report.redemption_by_voucher_excel', array(
                    "datas" => $datas
                ));
            });
            $excel->download('xlsx');
        });
    }

    public function downloadPdf(){

        $datas = array();
        
        $datas[] = array(
            'trans_type' => '',
            'voucher_type' => '',
            'trans_date' => '',
            'voucher_id' => '',
            'trans_id' => '',
            'purchaser' => '',
            'purchaser_email' => '',
            'purchaser_mobile' => '',
            'recipient' => '',
            'recipient_email' => '',
            'recipient_mobile' => '',
            'value' => '',
            'amount_paid' => '',
            'redemption_amount' => '',
            'is_void' => '',
            'property' => '',
            'status' => ''
        ); 

        $pdf = PDF::loadView('admin.report.redemption_by_voucher_pdf', array(
            "datas" => $datas
        ))->setOrientation('landscape');
        
        //return $pdf->inline();
        return $pdf->download('invoice.pdf');
    }
}
