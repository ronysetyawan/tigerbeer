<?php

namespace App\Http\Controllers\Admin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\BaseController;

use Illuminate\Support\Facades\Input;

use DB;

use Excel;

use PDF;

class PrizeRedemptionController extends BaseController
{

    public function __construct(Request $request){
        parent::__construct($request, 'report-prize-redemption');
    }

    private function _date(){
        $data_redemptions_query = DB::table(DB::raw("(
            SELECT
            pr.redemption_date as redemption_date,
            pr.prize_image AS prize_image,
            pr.prize_name,
            pr.point,
            u.first_name as customer_name,
            1 as redeemed,
            'shipping-product' as `type`,
            cp.address_1,
            cp.address_2,
            cp.postal_code,
            cp.country,
            CONCAT(cp.calling_code, cp.mobile) as mobile,
            cp.email,
            '' as promo_code
            FROM
            customer_prizes AS cp
            INNER JOIN point_redemptions AS pr ON cp.point_redemption_id = pr.id
            INNER JOIN users AS u ON u.id = pr.user_id

            UNION
            
            SELECT
            pr.redemption_date as redemption_date,
            pr.prize_image AS prize_image,
            pr.prize_name,
            pr.point,
            u.first_name as customer_name,
            pc.used AS redeemed,
            'promocode' as `type`,
            '' as address_1,
            '' as address_2,
            '' as postal_code,
            '' as county,
            '' as mobile,
            '' as email,
            pc.code as promo_code
            FROM
            promo_codes AS pc
            INNER JOIN point_redemptions AS pr ON pc.point_redemption_id = pr.id
            INNER JOIN users AS u ON u.id = pr.user_id

        ) as tbl"));

        if(Input::get("type")){
            $data_redemptions_query->where('type', Input::get("type"));
        }

        return $data_redemptions_query;
    }

    public function index(){

        $data_redemptions_query = $this->_date();
        
        $total_record = $data_redemptions_query->count();

        $data_redemptions = $data_redemptions_query->paginate(20);

        $filter_datas = [];

        if(Input::get("type")){
            $filter_datas['type'] = Input::get('type');
        }

        $redemptions = [];

        foreach($data_redemptions as $dt){

            $type = "";
            if($dt->type=='shipping-product'){
                $type = "Shipping Product";
            }else if($dt->type=='promocode'){
                $type = "Promo Code";
            }

            $y = date("Y", strtotime($dt->redemption_date));
            $m = date("m", strtotime($dt->redemption_date));
            
            $redemptions[] = [
                "prize_name" => $dt->prize_name,
                "prize_image" => url("files/prize/deal/" . $y . "/" . $m . "/" . $dt->prize_image),
                "type" => $dt->type,
                "type_string" => $type,
                "customer_name" => $dt->customer_name,
                "point" => $dt->point,
                "date" => date("d/m/Y H:i", strtotime($dt->redemption_date)),
                "address_1" => $dt->address_1,
                "address_2" => $dt->address_2,
                "postal_code" => $dt->postal_code,
                "country" => $dt->country,
                "mobile" => $dt->mobile,
                "email" => $dt->email,
                "promo_code" => $dt->promo_code,
            ];
        }

        $string_params = [];
        if($filter_datas){
            foreach($filter_datas as $key=>$filter_data){
                $string_params[] = ($key . "=" . $filter_data);
            }
        }
        
        $string_param = implode("&", $string_params);
        $string_param = $string_param?("?" . $string_param):"";

        return view('admin.report.prize_redemption', [
            "total_record" =>  $total_record,
            "redemptions" => $redemptions,
            "filter_datas" => $filter_datas,
            "download_link" => [
                "excel" => [
                    "title" => "Excel",
                    "url" => ( url("admin/report/prize-redemption/download/excel") . $string_param),
                ]
            ]
        ]);
    }

    public function downloadExcel(){
        
        $data_redemptions_query = $this->_date();

        $data_redemptions = $data_redemptions_query->get();

        $datas = [];

        foreach($data_redemptions as $dt){

            $type = "";
            if($dt->type=='shipping-product'){
                $type = "Shipping Product";
            }else if($dt->type=='promocode'){
                $type = "Promo Code";
            }

            $y = date("Y", strtotime($dt->redemption_date));
            $m = date("m", strtotime($dt->redemption_date));
            
            $datas[] = [
                "prize_name" => $dt->prize_name,
                "type_string" => $type,
                "customer_name" => $dt->customer_name,
                "point" => $dt->point,
                "date" => date("d/m/Y H:i", strtotime($dt->redemption_date)),
                "address_1" => $dt->address_1,
                "address_2" => $dt->address_2,
                "postal_code" => $dt->postal_code,
                "country" => $dt->country,
                "mobile" => $dt->mobile,
                "email" => $dt->email,
                "promo_code" => $dt->promo_code,
            ];
        }

        Excel::create('Prize Redemption Report', function($excel) use($datas){
            $excel->setTitle('Prize Redemption Report');
            $excel->setCreator('Erdinger')->setCompany('SPD - CPR Vision');
            $excel->setDescription('Prize Redemption Report');
            $excel->sheet('Sheet 1', function($sheet) use($datas){
                $sheet->loadView('admin.report.prize_redemption_excel', array(
                    "datas" => $datas
                ));
            });
            $excel->download('xlsx');
        });
    }

    public function downloadPdf(){

        $datas = array();
        
        $datas[] = array(
            'trans_type' => '',
            'voucher_type' => '',
            'trans_date' => '',
            'voucher_id' => '',
            'trans_id' => '',
            'purchaser' => '',
            'purchaser_email' => '',
            'purchaser_mobile' => '',
            'recipient' => '',
            'recipient_email' => '',
            'recipient_mobile' => '',
            'value' => '',
            'amount_paid' => '',
            'redemption_amount' => '',
            'is_void' => '',
            'property' => '',
            'status' => ''
        ); 

        $pdf = PDF::loadView('admin.report.redemption_by_voucher_pdf', array(
            "datas" => $datas
        ))->setOrientation('landscape');
        
        //return $pdf->inline();
        return $pdf->download('invoice.pdf');
    }
}
