<?php

namespace App\Http\Controllers\Admin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\BaseController;

use Illuminate\Support\Facades\Input;

use DB;

use Excel;

use PDF;
use Auth;

class RedemptionByPropertyController extends BaseController
{

    public function __construct(Request $request){
        parent::__construct($request, 'report-redemption-by-property');
    }

    private function _getData(){
        $voucher_redeems_query = \App\VoucherRedeem::query()->orderBy("redeem_date","desc");
        $voucher_redeems_query->join('properties','properties.id','=','voucher_redeems.property_id');
        $voucher_redeems_query->join('products','products.id','=','voucher_redeems.product_id');

        if(Input::get("start-date") && Input::get("end-date") && strtotime(Input::get("start-date"))<=strtotime(Input::get("start-date"))){
            $voucher_redeems_query->where('redeem_date', '>=' , Input::get("start-date"). " 00:00:00");
            $voucher_redeems_query->where('redeem_date', '<=', Input::get("end-date") . " 59:59:59");
        }
        if(Input::get("property")){
            $voucher_redeems_query->where('property', Input::get("property"));
        }

        $user = Auth::guard('admin')->user();
        if($user->role == 'property'){
            $ids = \App\PropertyUser::where('user_id', $user->id)->pluck('property_id')->toArray();
            $voucher_redeems_query->whereIn('property_id', $ids);
        }

        return $voucher_redeems_query;
    }
    
    public function index(){

        $datas = [];

        $filter_datas = array();

        $voucher_redeems_query = $this->_getData();

        if(Input::get("start-date") && Input::get("end-date") && strtotime(Input::get("start-date"))<=strtotime(Input::get("start-date"))){
            //$filter_datas['property'] = Input::get('property');
        }
        if(Input::get("property")){
            $filter_datas['property'] = Input::get('property');
        }

        $total_record = $voucher_redeems_query->count();

        $data_redeems = $voucher_redeems_query->paginate(20);


        foreach($data_redeems as $dt){
            $owner_names = array();
            if($dt->voucher){
                if($dt->voucher->owner_first_name){
                    $owner_names[] = $dt->voucher->owner_first_name;
                }
                if($dt->voucher->owner_last_name){
                    $owner_names[] = $dt->voucher->owner_last_name;
                }
            }
            $datas[] = array(
                "redeem_date" => date("d/m/Y H:i", strtotime($dt->redeem_date)),
                "property" => $dt->property,
                "zone" => $dt->zone,
                "customer_code" => $dt->customer_code,
                "voucher_id" => $dt->voucher_id,
                "voucher_type" => $dt->name,
                "customer" => $dt->voucher->user->first_name,
                "value" => number_format($dt->value),
            );
        }

        $properties = array();
        $data_properties = \App\Property::orderBy("name","asc")->get();
        
        $user = Auth::guard('admin')->user();
        if($user->role == 'property'){
            $ids = \App\PropertyUser::where('user_id', $user->id)->pluck('property_id')->toArray();
            $data_properties = \App\Property::whereIn('id', $ids)->orderBy("name","asc")->get();
        }
        foreach($data_properties as $dt){
            $properties[] = array(
                "value" => $dt->name,
                "label" => $dt->name,
            );
        }

        $string_params = [];
        if($filter_datas){
            foreach($filter_datas as $key=>$filter_data){
                $string_params[] = ($key . "=" . $filter_data);
            }
        }
        
        $string_param = implode("&", $string_params);
        $string_param = $string_param?("?" . $string_param):"";
        return view('admin.report.redemption_by_property', array(
            "total_record" => $total_record,
            "datas" => $datas,
            "links" => $filter_datas?$data_redeems->appends($filter_datas)->links():$data_redeems->links(),
            "properties" => $properties,
            "filter_datas" => $filter_datas,
            "download_link" => [
                "excel" => [
                    "title" => "Excel",
                    "url" => ( url("admin/report/redemption-by-property/download/excel") . $string_param),
                ]
            ]
        ));
    }

    public function downloadExcel(){
        
        $transactions_data_query = $this->_getData();
    
        $transactions_data = $transactions_data_query->get();

        $datas = [];

        foreach($transactions_data as $dt){
            $owner_names = array();
            if($dt->voucher){
                if($dt->voucher->owner_first_name){
                    $owner_names[] = $dt->voucher->owner_first_name;
                }
                if($dt->voucher->owner_last_name){
                    $owner_names[] = $dt->voucher->owner_last_name;
                }
            }
            $datas[] = array(
                "redeem_date" => date("d/m/Y H:i", strtotime($dt->redeem_date)),
                "property" => $dt->property,
                "zone" => $dt->zone,
                "customer_code" => $dt->customer_code,
                "voucher_id" => $dt->voucher_id,
                "voucher_type" => $dt->name,
                "customer" => $dt->voucher->user->first_name,
                "value" => number_format($dt->value),
            );
        }

        Excel::create('Redemption By Property Report', function($excel) use($datas){
            $excel->setTitle('Redemption By Property Report');
            $excel->setCreator('E-Gifting')->setCompany('SPD - CPR Vision');
            $excel->setDescription('Redemption By Property Report');
            $excel->sheet('Sheet 1', function($sheet) use($datas){
                $sheet->loadView('admin.report.redemption_by_property_excel', array(
                    "datas" => $datas
                ));
            });
            $excel->download('xlsx');
        });
    }

    public function downloadPdf(){

        $datas = array();
        
        $datas[] = array(
            'trans_type' => '',
            'voucher_type' => '',
            'trans_date' => '',
            'voucher_id' => '',
            'trans_id' => '',
            'purchaser' => '',
            'purchaser_email' => '',
            'purchaser_mobile' => '',
            'recipient' => '',
            'recipient_email' => '',
            'recipient_mobile' => '',
            'value' => '',
            'amount_paid' => '',
            'redemption_amount' => '',
            'is_void' => '',
            'property' => '',
            'status' => ''
        ); 

        $pdf = PDF::loadView('admin.report.redemption_by_voucher_pdf', array(
            "datas" => $datas
        ))->setOrientation('landscape');
        
        //return $pdf->inline();
        return $pdf->download('invoice.pdf');
    }
}
