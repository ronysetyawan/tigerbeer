<?php

namespace App\Http\Controllers\Admin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class CustomController extends Controller
{
    public function index(){
        $datas = array();

        $transactions_data_query = DB::table(DB::raw('
            (SELECT
            "Purchase" AS trans_type,
            "" AS voucher_type,
            o.order_date as trans_date,
            v.voucher_id,
            "" as trans_id,
            o.first_name AS purchaser_first_name,
            o.last_name AS purchaser_last_name,
            o.email AS purchaser_email,
            o.mobile AS purchaser_mobile,
            oi.recipient_first_name,
            oi.recipient_last_name,
            oi.recipient_email,
            oi.recipient_mobile,
            "" as value,
            "" as amount_paid,
            "" as redemption_amount
            FROM
            vouchers AS v
            INNER JOIN order_items AS oi ON v.order_item_id = oi.id
            INNER JOIN orders AS o ON oi.order_id = o.id
            UNION
            SELECT
            "Redemption" AS trans_type,
            "" AS voucher_type,
            vr.redeem_date as trans_date,
            v.voucher_id,
            "" as trans_id,
            o.first_name AS purchaser_first_name,
            o.last_name AS purchaser_last_name,
            o.email AS purchaser_email,
            o.mobile AS purchaser_mobile,
            oi.recipient_first_name,
            oi.recipient_last_name,
            oi.recipient_email,
            oi.recipient_mobile,
            "" as value,
            "" as amount_paid,
            "" as redemption_amount
            FROM
            voucher_redeems AS vr
            INNER JOIN vouchers AS v ON vr.voucher_id = v.voucher_id
            INNER JOIN order_items AS oi ON v.order_item_id = oi.id
            INNER JOIN orders AS o ON oi.order_id = o.id) as tb1'
        ));
    
        //$transactions_data_query->select('name', 'email as user_email')
        $transactions_data = $transactions_data_query->get();

        if($transactions_data){
            foreach($transactions_data as $dt){

                $purchasers = array();
                if($dt->purchaser_first_name){
                    $purchasers[] = $dt->purchaser_first_name;
                }
                if($dt->purchaser_last_name){
                    $purchasers[] = $dt->purchaser_last_name;
                }

                $recipients = array();
                if($dt->recipient_first_name){
                    $recipients[] = $dt->recipient_first_name;
                }
                if($dt->recipient_last_name){
                    $recipients[] = $dt->recipient_last_name;
                }

                $datas[] = array(
                    "trans_type" => $dt->trans_type,
                    "voucher_type" => $dt->voucher_type,
                    "trans_date" => "transdate",
                    "voucher_id" => $dt->voucher_id,
                    "trans_id" => "trans id",
                    "purchaser" => implode(" ", $purchasers),
                    "purchaser_email" => $dt->purchaser_email,
                    "purchaser_mobile" => $dt->purchaser_mobile,
                    "recipient" => implode(" ", $recipients),
                    "recipient_email" => $dt->recipient_email,
                    "recipient_mobile" => $dt->recipient_mobile,
                    "value" => "value",
                    "amount_paid" => "amount paid",
                    "redemption_amount" => "redemption amount",
                    "is_void" => "is void",
                    "property" => "property",
                    "status" => "status"
                );
            }
        }

        return view('admin.report.custom', array(
            "datas" => $datas
        ));
    }
}
