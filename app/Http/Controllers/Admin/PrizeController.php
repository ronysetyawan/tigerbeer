<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class PrizeController extends BaseController
{

    public function __construct(Request $request){
        parent::__construct($request, 'prize');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $links_appends = array();

        $filter_datas = array(
            "retailer" => ""
        );

        $query_prizes = \App\Prize::query();
        $query_prizes->orderBy("id", "desc");
        
        $data_prizes = $query_prizes->paginate(20);
        $prizes = array();
        if($data_prizes){
            foreach($data_prizes as $dt_prize){
                $type = "";
                if($dt_prize->type=="shipping-product"){
                    $type = "Shipping Product";
                }else if($dt_prize->type=="promocode"){
                    $type = "Promo Code";
                }
                $prizes[] = array(
                    "id" => $dt_prize->id,
                    "image" => url('files/prize/' . $dt_prize->image),
                    "name" => $dt_prize->name,
                    "point_needed" => $dt_prize->point_needed,
                    "value" => $dt_prize->value,
                    "qty" => $dt_prize->qty,
                    "type" => $type,
                    "sort_order" => $dt_prize->sort_order,
                    "status" => $dt_prize->status?true:false,
                    "status_string" => $dt_prize->status?"Active":"Not Active",
                );
            }
        }

        $data = array(
            "prizes" => $prizes,
            "filter_datas" => $filter_datas,
            "current_url" => $request->fullUrl(),
            "links" => $links_appends?$data_prizes->appends($links_appends)->links():$data_prizes->links(),
        );

        return view('admin.prize_list', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image',
            'name' => 'required'
        ]);
        
        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();
        $filename = date("YmdHis") . "-" . $image->getClientOriginalName();
        Storage::disk('product')->put($filename,  File::get($image));

        $product = new \App\Product;
        $product->image = $filename;
        $product->name = $request->name;
        $product->description = isset($request->description) ? $request->description : '' ;
        $product->pint = $request->pint;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->sort_order = $request->sort_order;
        $product->status = $request->status;
        $product->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Product::find($id);

        $data_return = array(
            "name" => $product->name,
            "description" => $product->description,
			"price" => $product->price,
            "pint" => $product->pint,
            "stock" => $product->stock,
			"status" => $product->status
        );

        return response()->json($data_return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$this->validate($request, [
            'price' => 'required'
        ]);*/

        $product = \App\Product::find($id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->pint = $request->pint;
        $product->stock = $request->stock;
        $product->price = $request->price;
        $product->status = $request->status;
        $product->save();

        return response()->json([
            "name" => $product->brand->name,
            "price" => number_format($product->price, 2),
            "pint" => $product->pint,
            "stock" => $product->stock,
            "sort_order" => $product->sort_order,
            "status" => $product->status?true:false,
            "status_string" => $product->status?"Active":"Not Active"
        ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = \App\Product::find($id);

        if($product->image){
            \Storage::delete("/public/product/" . $product->image);
        }

        $product->delete();
    }

    public function updateStatus(Request $request, $product_id)
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $product = \App\Product::find($product_id);

        $product->status = $request->status?1:0;

        $product->save();

        return response()->json([
            "status" => $product->status?true:false,
            "status_string" => $product->status?"Active":"Not Active",
        ]);
    }
}
