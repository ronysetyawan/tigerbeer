<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Mail;

class TestEmailController extends Controller
{

    public function order(){
        
        $order_id = 331;    //331-18;

        $order_info = \App\Order::find($order_id);

        $user_info = \App\User::find($order_info->user_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $email = $user_info->email;
        $email = "adit.herdityo@gmail.com"; // test
        $name = $user_info->first_name;
        $name = "Dev";  // test
        $email_from = 'Tiger';
        $email_subject = 'Thank you for your support!';
        $email_tpl = 'emails.purchase';

        $order_items = \App\OrderItem::where('order_id', $order_id)->get();
        $items = [];
        foreach($order_items as $order_item){
            $items[] = [
                'voucher_id' => $voucher_info->voucher_id,
                'entitlement' => $order_item->qty . " of " . $order_item->item_name,
                'qty' => $order_item->qty,
                'total' => "SGD " . number_format($order_item->total, 2),
                'expired_date' => '30 September 2020',
            ];
        }

        $string_token = \Firebase\JWT\JWT::encode(array(
            "order_id" => $order_id
        ), "email-preview-tiger");

        $data_email = array(
            "string_token" => $string_token,
            //'image' => asset('assets/web-purchase/image/tiger.jpg'),
            'name' => $voucher_info->user->first_name,
            'items' => $items
        );

        $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
            $m->from('no-reply@apb.giv.one', $email_from);
            $m->cc(['hazel@cprvision.com','zack@cprvision.com']);
            $m->to($email, $name)->subject($email_subject);
        });

        if($sendEmail){
            echo "sent to ".$email;
        }

    }

    public function resend(Request $request){
        
        $order_id = $request->orderid;

        $order_info = \App\Order::find($order_id);

        $user_info = \App\User::find($order_info->user_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $email = $user_info->email;
        $name = $user_info->first_name;
        $email_from = 'Tiger';
        $email_subject = 'Thank you for your support!';
        $email_tpl = 'emails.purchase';

        $order_items = \App\OrderItem::where('order_id', $order_id)->get();
        $items = [];
        foreach($order_items as $order_item){
            $items[] = [
                'voucher_id' => $voucher_info->voucher_id,
                'entitlement' => $order_item->qty . " of " . $order_item->item_name,
                'qty' => $order_item->qty,
                'total' => "SGD " . number_format($order_item->total, 2),
                'expired_date' => '30 September 2020',
            ];
        }

        $string_token = \Firebase\JWT\JWT::encode(array(
            "order_id" => $order_id
        ), "email-preview-tiger");

        $data_email = array(
            "string_token" => $string_token,
            //'image' => asset('assets/web-purchase/image/tiger.jpg'),
            'name' => $voucher_info->user->first_name,
            'items' => $items
        );

        $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
            $m->from('no-reply@apb.giv.one', $email_from);
            //$m->cc(['zack@cprvision.com']);
            $m->to($email, $name)->subject($email_subject);
        });

        if($sendEmail){
            echo "sent to ".$email;

            $voucher_info->sent = 1;
            $voucher_info->sent_date = date('Y-m-d H:i:s');
            $voucher_info->save();
        }

    }

    public function confirmation_delivery(){
        
        $redemption_id = 14;

        $redemption_info = \App\PointRedemption::find($redemption_id); 

        $customer_prize = \App\CustomerPrize::where('point_redemption_id', $redemption_id)->first();

        $user_info = \App\User::find($redemption_info->user_id);

        $email = $user_info->email;
        $name = $user_info->first_name;
        $email_from = 'Tiger';
        $email_subject = 'Pint Redemption!';
        $email_tpl = 'emails.confirmation_delivery';

        $string_token = \Firebase\JWT\JWT::encode(array(
            "redemption_id" => $redemption_id
        ), "email-preview-delivery-oct-fest");

        $addresses = [];
        if($customer_prize->address_1){
            $addresses[] = $customer_prize->address_1;
        }
        if($customer_prize->address_2){
            $addresses[] = $customer_prize->address_2;
        }
        if($customer_prize->postal_code){
            $addresses[] = $customer_prize->postal_code;
        }

        $data_email = array(
            "string_token" => $string_token,
            'image' => asset('assets/web-purchase/image/october-fest.jpg'),
            'name' => $user_info->first_name,
            'prize_id' => $redemption_id,
            'prize' => $redemption_info->prize_name,
            'delivery_address' => implode("<br>", $addresses)
        );

        $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
            $m->from('no-reply@apb.giv.one', $email_subject);
            $m->to($email, $name)->subject($email_subject);
        });

        if($sendEmail){
            echo "berhasil";
        }

    }

    public function testSend()
    {
        $order_id = 0;

        $order_info = \App\Order::find($order_id);

        $user_info = \App\User::find($order_info->user_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        //$email = $user_info->email;
        $email = "adit.herdityo@gmail.com";
        $name = $user_info->first_name;
        $email_from = 'Tiger';
        $email_subject = 'You have successfully purchased Tiger pints!';
        $email_tpl = 'emails.purchase';

        $order_items = \App\OrderItem::where('order_id', $order_id)->get();
        $items = [];
        foreach($order_items as $order_item){
            $items[] = [
                'voucher_id' => $voucher_info->voucher_id,
                'entitlement' => $order_item->qty . " of " .$order_item->item_name,
                'qty' => $order_item->qty,
                'total' => "SGD " . number_format($order_item->total, 2),
                'expired_date' => '30 September 2020',
            ];
        }

        $string_token = \Firebase\JWT\JWT::encode(array(
            "order_id" => $order_id
        ), "email-preview-purchase-oct-fest");

        $data_email = array(
            "string_token" => $string_token,
            'name' => $voucher_info->user->first_name,
            'items' => $items
        );
        
        $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
            $m->from('no-reply@apb.giv.one', $email_from);
            $m->to($email, $name)->subject($email_subject);
        });

        //var_dump($sendEmail);
        if($sendEmail){
            echo "sent to ".$email;
        }
    }

    public function test(){

        $order_id = 331;    //331-18;
        
        $order_info = \App\Order::find($order_id);

        $user_info = \App\User::find($order_info->user_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $email = $user_info->email;
        $email = "adit.herdityo@gmail.com";
        $name = $user_info->first_name;
        $email_from = 'Tiger';
        $email_subject = 'Thank you for your support!';
        $email_tpl = 'emails.purchase';
        $desc = " x Tiger Beer";

        $order_items = \App\OrderItem::where('order_id', $order_id)->get();
        $items = [];
        foreach($order_items as $order_item){
            $items[] = [
                'voucher_id' => $voucher_info->voucher_id,
                'entitlement' => $order_item->total_pint . $desc,
                'qty' => $order_item->total_pint,
                //'total' => "SGD " . number_format($order_item->total, 2),
                'total' => "FREE ",
                'expired_date' => '30th September 2020',
            ];
        }

        $string_token = \Firebase\JWT\JWT::encode(array(
            "order_id" => $order_id
        ), "email-preview-tiger");

        $link = url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id);

        $data_return = array(
            "string_token" => $string_token,
            //'image' => asset('assets/web-purchase/image/october-fest.jpg'),
            'image' => "",
            'name' => $voucher_info->user->first_name,
            'items' => $items,
            'link' => $link
        );

        return view('emails.purchase', $data_return);
    }

    public function testSurvey(){

        $order_id = 18;
        
        $order_info = \App\Order::find($order_id);

        $user_info = \App\User::find($order_info->user_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $email = $user_info->email;
        $email = "adit.herdityo@gmail.com";
        $name = $user_info->first_name;
        $email_from = 'Tiger';
        $email_subject = 'Thank you for your support!';
        $email_tpl = 'emails.survey_complete';
        $desc = " x Tiger Beer";

        $order_items = \App\OrderItem::where('order_id', $order_id)->get();
        $items = [];
        foreach($order_items as $order_item){
            $items[] = [
                'voucher_id' => $voucher_info->voucher_id,
                'entitlement' => $voucher_info->balance . $desc,
                'qty' => $voucher_info->balance,
                //'total' => "SGD " . number_format($order_item->total, 2),
                'total' => "FREE ",
                'expired_date' => '30th September 2020',
            ];
        }

        $string_token = \Firebase\JWT\JWT::encode(array(
            "order_id" => $order_id
        ), "email-preview-tiger");

        $link = url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id);

        $data_return = array(
            "string_token" => $string_token,
            //'image' => asset('assets/web-purchase/image/october-fest.jpg'),
            'image' => "",
            'name' => $voucher_info->user->first_name,
            'items' => $items,
            'link' => $link
        );

        return view($email_tpl, $data_return);
    }

    public function sendSurvey(Request $request){

        $order_id = $request->orderid;
        
        $order_info = \App\Order::find($order_id);

        $user_info = \App\User::find($order_info->user_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $email = $user_info->email;
        //$email = "adit.herdityo@gmail.com";
        $name = $user_info->first_name;
        $email_from = 'Tiger';
        $email_subject = 'Thank you for your participation!';
        $email_tpl = 'emails.survey_complete';
        $desc = " x Tiger Beer";

        $order_items = \App\OrderItem::where('order_id', $order_id)->get();
        $items = [];
        foreach($order_items as $order_item){
            $items[] = [
                'voucher_id' => $voucher_info->voucher_id,
                'entitlement' => $voucher_info->balance . $desc,
                'qty' => $voucher_info->balance,
                //'total' => "SGD " . number_format($order_item->total, 2),
                'total' => "FREE ",
                'expired_date' => '30th September 2020',
            ];
        }

        $string_token = \Firebase\JWT\JWT::encode(array(
            "order_id" => $order_id
        ), "email-preview-tiger");

        $link = url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id);

        $data_email = array(
            "string_token" => $string_token,
            //'image' => asset('assets/web-purchase/image/october-fest.jpg'),
            'image' => "",
            'name' => $voucher_info->user->first_name,
            'items' => $items,
            'link' => $link
        );

        $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
            $m->from('no-reply@apb.giv.one', $email_from);
            //$m->cc(['zack@cprvision.com']);
            $m->bcc(['adit@cprvision.com']);
            $m->to($email, $name)->subject($email_subject);
        });

        if($sendEmail){
            echo "sent survey notification to ".$email;
        }
    }

    public function testNotify(){
        //$order_id = 331;    //331-18;
        
        //$order_info = \App\Order::find($order_id);
        //$user_info = \App\User::find($order_info->user_id);
        $voucher_info = \App\Voucher::where("type", 'ft')->first();
        $user_info = \App\User::find($voucher_info->user_id);
        $order_info = \App\Order::find($voucher_info->order_id);

        $data_return = array(
            'name' => $user_info->first_name,
            'balance' => $voucher_info->balance,
            'date' => date("d/m/Y",strtotime($order_info->order_date))
        );

        return view('emails.notify', $data_return);
    }

    public function sendTestNotify(){
        
        //$voucher_info = \App\Voucher::where("type",'f')->limit(1)->get();
        $order_id = 18;
        $voucher_info = \App\Voucher::where("order_id",$order_id)->first();
        
        if ($voucher_info) {
            //var_dump($voucher_info);
            $user_info = \App\User::find($voucher_info->user_id);

            //$email = $user_info->email;
            $email = "adit@cprvision.com";
            $name = $user_info->first_name;
            $email_from = 'Tiger';
            $email_subject = 'Your contribution was unsuccessful. Show your support again!';
            $email_tpl = 'emails.notify';

            $data_email = array(
                'name' => $name
            );
    
            $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
                $m->from('no-reply@apb.giv.one', $email_from);
                $m->cc(['hazel@cprvision.com','zack@cprvision.com','jiahui_number27@hotmail.com','liewjiahuihazel@gmail.com']);
                $m->bcc('adit.herdityo@gmail.com');
                $m->to($email, $name)->subject($email_subject);
            });
    
            //var_dump($sendEmail);
            if($sendEmail){
                echo "sent to ".$email." <br>";
    
                //$vinfo->type = 'ft';
                //$vinfo->save();
            }
        }

    }

    public function sendNotify(){
        
        $voucher_info = \App\Voucher::where("type",'ft')->limit(10)->get();

        if (count($voucher_info)>0) {
            $i = 0;
            foreach($voucher_info as $vinfo) {

                $user_info = \App\User::find($vinfo->user_id);

                $email = $user_info->email;
                //$email = "adit@cprvision.com";
                $name = $user_info->first_name;
                $email_from = 'Tiger';
                $email_subject = 'Your contribution was unsuccessful. Show your support again!';
                $email_tpl = 'emails.notify';

                $data_email = array(
                    'name' => $name
                );
        
                $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
                    $m->from('no-reply@apb.giv.one', $email_from);
                    $m->bcc('adit@cprvision.com');
                    $m->to($email, $name)->subject($email_subject);
                });
        
                if($sendEmail){
                    echo "sent to ".$email.">> (".$vinfo->voucher_id.") <br>";
        
                    $vinfo->type = 'f';
                    $vinfo->save();

                    $i++;
                }
            }
            echo "Success sent to $i emails <br>";
        } else {
            echo "No more emails to sent.";
        }

    }

}
