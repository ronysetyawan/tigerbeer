<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

class AccountController extends BaseController
{
    public function index(Request $request){

        if(!Auth::guard('customer')->check()){
            return redirect("/");
        }

        $user = Auth::guard('customer')->user();

        $dobs = explode("-", $user->dob);

        $user_info = [
            'name' => $user->first_name,
            'gender' => $user->gender,
            'country' => $user->country_id,
            'mobile' => $user->mobile,
            'email' => $user->email,
            'dob_date' => (int)$dobs[2],
            'dob_month' => (int)$dobs[1],
            'dob_year' => (int)$dobs[0],
            'address_1' => $user->address_1,
            'address_2' => $user->address_2,
            'postal_code' => $user->postal_code,
            'subscribe' => $user->subscribe
        ];

        return view('web_purchase.account', [
            "user_info" => $user_info
        ]);
    }

    public function store(Request $request){
        $user = Auth::guard('customer')->user();

        $user->first_name = $request->name;
        $user->gender = $request->gender;
        $user->email = $request->email;
        $user->dob = $request->dob;
        $user->postal_code = $request->postal_code;
        $user->subscribe = $request->subscribe;
        $user->address_1 = $request->address_1;
        $user->address_2 = $request->address_2;

        $user->save();
    }

    public function destroy(){

        $user = Auth::guard('customer')->user();
        $user->deleted = 1;
        $user->save();

        Auth::guard('customer')->logout();

        return response()->json([
            "redirect" => url('/')
        ]);
    }
}
