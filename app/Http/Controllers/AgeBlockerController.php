<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

class AgeBlockerController extends BaseController
{
    public function index(Request $request){
        
        $months = [];
        $months[] = ["value"=>1, "label"=>"Jan"];
        $months[] = ["value"=>2, "label"=>"Feb"];
        $months[] = ["value"=>3, "label"=>"Mar"];
        $months[] = ["value"=>4, "label"=>"Apr"];
        $months[] = ["value"=>5, "label"=>"May"];
        $months[] = ["value"=>6, "label"=>"Jun"];
        $months[] = ["value"=>7, "label"=>"Jul"];
        $months[] = ["value"=>8, "label"=>"Aug"];
        $months[] = ["value"=>9, "label"=>"Sep"];
        $months[] = ["value"=>10, "label"=>"Oct"];
        $months[] = ["value"=>11, "label"=>"Nov"];
        $months[] = ["value"=>12, "label"=>"Dec"];

        // set customer logout when redirected to this page
        Auth::guard('customer')->logout();

        // forward user to home when age session exists
        // session set to last 1 week
        if ($request->session()->has("age")) {
            return redirect('/');
        }
        
        return view('web_purchase.age_blocker', [
            "months" => $months
        ]); 
    }

    public function store(Request $request){
        $dob = '1970-01-01';
        $request->session()->put("age", $dob);
        return response()->json([
            "redirect" => url("/")
        ]);
        // $this->validate($request, [
        //     'date' => 'required',
        //     'month' => 'required',
        //     'year' => 'required'
        // ]);

        // if($request->date<=0 || $request->month<=0 || $request->year<=0){
        //     return response()->json(['message' => 'Invalid date!'], 422);
        // }else{

        //     $dob = $request->year . "-" . $request->month . "-" . $request->date;

        //     $diff = abs(time() - strtotime($dob));

        //     $years = floor($diff / (365*60*60*24));

        //     if(!checkdate($request->month, $request->date, $request->year)){
        //         return response()->json(['message' => 'Invalid date!'], 422);
        //     }else if($years<19){
        //         return response()->json(['message' => 'You have to be of legal drinking age to participate in this promotion.'], 422);
        //     }else{
        //         $request->session()->put("age", $dob);
        //         return response()->json([
        //             "redirect" => url("/")
        //         ]);
        //     }
        // }
    }

}
