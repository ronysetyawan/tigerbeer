<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use View;

use Route;

use Auth;

class BaseController extends Controller
{
    public function __construct(Request $request) {
        
        $titles = array();
        $titles[] = array("value" => "Mr.", "label" => "Mr.");
        $titles[] = array("value" => "Mrs.", "label" => "Mrs.");
        $titles[] = array("value" => "Ms.", "label" => "Ms.");
        $titles[] = array("value" => "Dr.", "label" => "Dr.");
        $titles[] = array("value" => "Miss.", "label" => "Miss.");

        $route = Route::current()->parameters();
        if(count($route) > 0){
            if(!empty($route['voucher_id']) && !empty($route['code'])){
                $voucher_id = $route['voucher_id'];
                $code = $route['code'];
            } else {
                $voucher_id = '';
                $code = '';
            }
        } else {
            $voucher_id = '';
            $code = '';
        }
        
        if(!$request->segment(1)){
            $isFront = true;
        }else{
            $isFront = false;
        }

        $countries = array();
        $data_countries = \App\Country::get();
        if($data_countries){
            foreach($data_countries as $dt_country){
                $countries[] = array(
                    "country_id" => $dt_country->id,
                    "country_name" => $dt_country->country_name,
                    "calling_code" => $dt_country->calling_code
                );
            }
        }

        $outlets = [];
        $data_outlets = \App\Property::where('status',1)->get();
        if($data_outlets){
            foreach($data_outlets as $dt_outlet){
                if(!isset($outlets[$dt_outlet->zone])){
                    $outlets[$dt_outlet->zone] = [];
                }

                $outlets[$dt_outlet->zone][] = [
                    "name" => $dt_outlet->name,
                    "address1" => $dt_outlet->address1,
                    "address2" => $dt_outlet->address2,
                    "zone" => $dt_outlet->zone
                ];
            }
        }

        $is_logged_in = false;
        if(Auth::guard('customer')->check()){
            $is_logged_in = true;
        }

        View::share('asset_url', asset('assets/web_purchase'));
        View::share('create_url', url("create"));
        View::share('confirmation_url', url("confirmation"));
        View::share('is_front', $isFront);
        View::share('home_url', '/');
        View::share('countries', $countries);
        View::share('outlets', $outlets);
        View::share('is_logged_in', $is_logged_in);
        View::share('base_url', url('voucher/' . $code . '/' . $voucher_id));

    }
}
