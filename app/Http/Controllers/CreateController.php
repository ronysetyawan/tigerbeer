<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CreateController extends BaseController
{
    public function index(Request $request){

        $data_products = \App\Pint::where("status", 1)->orderBy("sort_order", "asc")->get();
        $products = [];
        foreach($data_products as $dt_product){
            $products[] = [
                "id" => $dt_product->id,
                "image" => url('files/pint/' . $dt_product->image),
                "name" => $dt_product->name,
            ];
        }

        return view('web_purchase.create', [
            "products" => $products
        ]); 
    }

}
