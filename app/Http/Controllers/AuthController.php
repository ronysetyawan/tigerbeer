<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use Hash;

class AuthController extends Controller
{
    public function login(){
        return view('admin.login');
    }

    public function store(Request $request){
        $country = $request->country;
        $mobile = $request->mobile;
        $password = $request->pin;

        $user_info = \App\User::where("country_id", $country)->where("mobile", $mobile)->where("status", 1)->where("deleted", 0)->first();

        if ($user_info && Hash::check($password, $user_info->password) && $user_info->type=='customer'
        && Auth::guard('customer')->attempt(['country_id' => $country, 'mobile' => $mobile, 'password' => $password, 'status' => 1, 'deleted' => 0,'type' => 'customer'])) {
            if($request->return_data){

                $dobs = explode("-", $user_info->dob);
                $dob_year = "";
                $dob_month = "";
                $dob_date = "";
                if($dobs){
                    $dob_date = $dobs[2];
                    $dob_month = $dobs[1];
                    $dob_year = $dobs[0];
                }
                
                return response()->json([
                    "name" => $user_info->first_name,
                    "gender" => $user_info->gender,
                    "country" => (int) $user_info->country_id,
                    "mobile" => $user_info->mobile,
                    "email" => $user_info->email,
                    "dob_date" => $dob_date,
                    "dob_month" => $dob_month,
                    "dob_year" => $dob_year,
                    "postal_code" => $user_info->postal_code,
                ]);
            }else{
                return response()->json([
                    "redirect" => url('account')
                ]);
            }
        }else{
            return response()->json([
                "message" => "Login Failed!",
            ], 422);
        }
    }

    public function logout(){
        Auth::guard('customer')->logout();

        return redirect('/'); 
    }
}
