<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Routing\Route;

use View;
use Captcha;
use Validator;
use Illuminate\Support\Facades\Input;

class ResendVoucherController extends BaseController
{

    public function index(Request $request){

        return view('web_purchase.resend_voucher', []);
    }

    public function store(Request $request){


        $user_info = \App\User::where("country_id", $request->country)->where("mobile", $request->mobile)->where("status", 1)->where("deleted", 0)->first();
        
        if(!$user_info){
            return response()->json([
                "message" => "User not found"
            ], 422);
        }else{

            $voucher_info = \App\Voucher::where("user_id", $user_info->id)->first();

            // BEGIN SMS
            try {
                $user_mobile = $voucher_info->owner_mobile;
                $len_user_mobile = strlen($user_mobile);
                $trim_user_mobile = $len_user_mobile - 4;
                $string_mobile = substr($user_mobile,($trim_user_mobile),4);

                //// send sms
                $api_key = env('NEXMO_KEY');
                $api_secret = env('NEXMO_SECRET');
                $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
                $message = $client->message()->send([
                    'to' => $voucher_info->owner_calling_code . $voucher_info->owner_mobile,
                    'from' => 'Erdinger',
                    'text' => 'Hi ' . $voucher_info->user->first_name . '. View your voucher: ' . url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id),
                ]);
                if($message){
                    //echo "SMS sent!";
                    //$result['message'] = true;
                    //$result['message_text'] = $text_message;
                }else{
                    //echo "Sorry SMS is failed!";
                    //$result['message'] = false;
                    //$result['message_text'] = $text_message;
                    //$result['message_reason'] = "Sorry SMS is failed!";
                }
            }catch(Exception $e) {
                //echo 'Message: ' .$e->getMessage();
            }
            // END SMS
        }
    }

}
