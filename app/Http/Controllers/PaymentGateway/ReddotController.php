<?php

namespace App\Http\Controllers\PaymentGateway;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Mail;

class ReddotController extends Controller
{

    protected $voucher_digit = 10;

    public function index(Request $request){

        $order_id = $request->session()->get('order_id');

        $order_info = \App\Order::find($order_id);
        
        $reddot = new \App\PaymentGateway\Reddot(env('REDDOT_SECRET_KEY'), array( //'ccf35d'
            'first_name' => $order_info->name,
            'last_name' => '',
            'amount' => $order_info->total,
            'email' => $order_info->email, 
            'order_number' => $order_info->id,
            'currency_code' => 'USD',
            'merchant_id' => env('REDDOT_MID'), //1000089318
            'merchant_reference' => 'eGift'.time(),
            'notify_url' => $request->root() . "/payment-gateway/reddot/notify",
            'return_url' => $request->root() . "/payment-gateway/reddot/return",
            'cancel_url' => $request->root() . "/payment-gateway/reddot/cancel", 
            'transaction_type' => 'Sale',
            'key' => env('REDDOT_KEY'), //'2bfba6b3b2af0ccf35dcc4f6166d474cb91266e8'
            'feature_voucher_enable' => '0'
        ));
            
        return view('web_purchase.payment_gateway.reddot',array(
            "url" => $reddot->url,
            "fields" => $reddot->getFields(),
            "signature" => $reddot->signature
        ));
        
        return view('web_purchase.payment_gateway.reddot');
    }

    public function cancel(Request $request){
        return redirect('/occasions');
    }

    public function return_(Request $request){
        
        if(\App\PaymentGateway\Reddot::checkingResponseRDP(env('REDDOT_SECRET_KEY'))){ 

            $data = \App\Order::finishOrder($request->session()->get("order_id"), $request);

            /*$reddot_log = new \App\ReddotLog;
            $reddot_log->mid = isset($_GET['mid'])?$_GET['mid']:'';
            $reddot_log->order_number = isset($_GET['order_number'])?$_GET['order_number']:'';
            $reddot_log->result = isset($_GET['result'])?$_GET['result']:'';
            $reddot_log->confirmation_code = isset($_GET['confirmation_code'])?$_GET['confirmation_code']:'';
            $reddot_log->transaction_id = isset($_GET['transaction_id'])?$_GET['transaction_id']:'';
            $reddot_log->authorization_code = isset($_GET['authorization_code'])?$_GET['authorization_code']:'';
            $reddot_log->card_number = isset($_GET['card_number'])?$_GET['card_number']:'';
            $reddot_log->merchant_reference = isset($_GET['merchant_reference'])?$_GET['merchant_reference']:'';
            $reddot_log->currency_code = isset($_GET['currency_code'])?$_GET['currency_code']:'';
            $reddot_log->amount = isset($_GET['amount'])?$_GET['amount']:'';
            $reddot_log->authorized_currency_code = isset($_GET['authorized_currency_code'])?$_GET['authorized_currency_code']:'';
            $reddot_log->authorized_amount = isset($_GET['authorized_amount'])?$_GET['authorized_amount']:'';
            $reddot_log->signature = isset($_GET['signature'])?$_GET['signature']:'';
            $reddot_log->save();*/
            //dd($data);
            $request->session()->put('purchased', $data['result']);
            return redirect('order-success');
        }else{
            return redirect('occasions');
        }

    }

    public function notify($slug, Request $request){
        return redirect($slug . '/product');
    }
}
