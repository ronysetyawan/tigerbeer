<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Validator;

class SurveyController extends Controller
{
    public function storeResult(Request $request)
    {
        try {
            $response = [];

            $validator = Validator::make($request->all(), [
                'hpno' => 'required|max:65',
                'email' => 'required|max:64',
                'firstname' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => false,
                    "message" => $validator->errors(),
                ], 400);
            }

            $mobile = $request->hpno;
            $email = $request->email;
            $name = $request->firstname;

            $this->getProduct($request);
            $user = User::where('mobile', $mobile)->where('email', $email)->first();
            if ($email == 'adit@cprvision.com') {
                $cid = 100;
            } else {
                $cid = !empty($user) ? $user->country_id : 192;
            }
            // cross check voucher is_survey
            $voucher = \App\Voucher::where('owner_mobile', $mobile)->where('is_survey',1)->first();
            if ($voucher) {
                // user already submit a survey
                return response()->json([
                    "status" => "failed",
                    "message" => "Sorry. Current mobile ($mobile) has submitted a survey.",
                    "voucher" => $voucher->voucher_id,
                ], 200);
            }

            $customer = [
                'name' => $name,
                'gender' => " ",
                'country' => $cid,
                'mobile' => $mobile,
                'email' => $email,
                'dob' => " ",
                'agree_newsletter' => 1
            ];

            $request->session()->put("customer", $customer);

            $response['customer'] = $request->session()->get("customer");

            if ($request->session()->has("customer")) {
                \App\Order::confirmOrder();
                if ($request->session()->has("order_id")) {
                    \App\Order::finishOrder($request->session()->get("order_id"), true);
                    return response()->json([
                        "status" => true,
                        "message" => "save data successful",
                        "data" => $response,
                    ], 200);
                }
            }

            return response()->json([
                "status" => false,
                "message" => "save data failed!",
            ], 422);
        } catch (\Exception $e) {
            return response()->json([
                "status" => false,
                "errors" => [$e->getMessage(), $e->getFile(), $e->getLine()],
            ], 500);
        }
    }

    public function storeResultMulti(Request $request)
    {
        try {
            $response = [];

            $data_info = $request->all();
            foreach($data_info as $data) {

                $validator = Validator::make($data, [
                    'hpno' => 'required|max:65',
                    'email' => 'required|max:64',
                    'firstname' => 'required',
                ]);
    
                if ($validator->fails()) {
                    return response()->json([
                        "status" => false,
                        "message" => $validator->errors(),
                    ], 400);
                }
    
                $mobile = $data['hpno'];
                $email = $data['email'];
                $name = $data['firstname'];
    
                $this->getProduct($request);
                $user = User::where('mobile', $mobile)->where('email', $email)->first();
    
                $customer = [
                    'name' => $name,
                    'gender' => " ",
                    'country' => !empty($user) ? $user->country_id : 192,
                    'mobile' => $mobile,
                    'email' => $email,
                    'dob' => " ",
                    'agree_newsletter' => 1
                ];
    
                $request->session()->put("customer", $customer);
    
                $response['customer'] = $request->session()->get("customer");

                if ($request->session()->has("customer")) {
                    \App\Order::confirmOrder();
                    if ($request->session()->has("order_id")) {
                        \App\Order::finishOrder($request->session()->get("order_id"), true);
                        return response()->json([
                            "status" => true,
                            "message" => "save data successful",
                            "data" => $response,
                        ], 200);
                    }
                }

            }

            if ($request->session()->has("customer")) {
                return response()->json([
                    "status" => true,
                    "message" => "save data successful",
                    "data" => $response,
                ], 200);
            }

            return response()->json([
                "status" => false,
                "message" => "save data failed!",
            ], 422);
        } catch (\Exception $e) {
            return response()->json([
                "status" => false,
                "errors" => [$e->getMessage(), $e->getFile(), $e->getLine()],
            ], 500);
        }
    }

    public function storeResultTest(Request $request)
    {
        try {
            $response = [];

            $data_info = $request->all();

            $validator = Validator::make($data_info, [
                'hpno' => 'required|max:65',
                'email' => 'required|max:64',
                'firstname' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => false,
                    "message" => $validator->errors(),
                ], 400);
            }

            $mobile = $request->hpno;
            $email = $request->email;
            $name = $request->firstname;

            $this->getProduct($request);
            $user = User::where('mobile', $mobile)->where('email', $email)->first();
            // cross check voucher is_survey
            $voucher = \App\Voucher::where('owner_mobile', $mobile)->where('is_survey',1)->first();
            if ($voucher) {
                // user already submit a survey
                return response()->json([
                    "status" => "failed",
                    "message" => "Sorry. Current mobile ($mobile) has submitted a survey.",
                    "voucher" => $voucher->voucher_id,
                ], 200);
            }

            $customer = [
                'name' => $name,
                'gender' => " ",
                'country' => !empty($user) ? $user->country_id : 192,
                'mobile' => $mobile,
                'email' => $email,
                'dob' => " ",
                'agree_newsletter' => 1
            ];

            $request->session()->put("customer", $customer);

            $response['customer'] = $request->session()->get("customer");


            if ($request->session()->has("customer")) {
                return response()->json([
                    "status" => true,
                    "message" => "save data successful",
                    "data" => $response,
                ], 200);
            }

            return response()->json([
                "status" => false,
                "message" => "save data failed!",
            ], 422);
        } catch (\Exception $e) {
            return response()->json([
                "status" => false,
                "errors" => [$e->getMessage(), $e->getFile(), $e->getLine()],
            ], 500);
        }
    }

    public function getProduct(Request $request)
    {
        // call bypass function to filled cart with default product
        $this->defaultCart($request);

        if (!$request->session()->has("carts") || !$request->session()->get("carts")) {
            return response()->json([
                "status" => false,
                "message" => "You dont have carts",
            ], 422);
        }

        $setting = \App\Setting::where("type", "global")->where("key", "product_name")->first();
        $product_name = $setting->value;

        $data_carts = $request->session()->get("carts");

        $carts = array();
        $grand_total_pints = 0;
        $grand_total = 0;

        foreach ($data_carts as $pint_id => $cart) {
            $pint = \App\Pint::find($pint_id);

            $total_pints = (int) $cart['qty'] * $pint->pint;
            $subtotal = (int) $cart['qty'] * $pint->price;

            $grand_total_pints += $total_pints;
            $grand_total += $subtotal;

            $carts[$pint_id] = array(
                "image" => url('files/pint/' . $pint->image),
                "name" => $product_name,
                "price" => "SGD " . number_format($pint->price, 2),
                "qty"   => $cart['qty'],
                "total_pints" => $total_pints,
                "subtotal" => "SGD " . number_format($subtotal, 2),
                "id" => $pint_id,
            );
        }

        $data_return = array(
            "carts" => $carts,
            "grand_total_pints" => number_format($grand_total_pints, 0),
            "grand_total" => 'SGD ' . number_format($grand_total, 2),
        );

        return $data_return;
    }

    public function defaultCart(Request $request)
    {
        $request->session()->forget('carts');
        $carts = array();
        $id = 2;
        $carts[$id] = array(
            "product" => 2,
            "pint" => null,
            "qty" => 1
        );
        $request->session()->put('carts', $carts);
    }
}
