<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Config;
use Exception;

class CheckoutController extends BaseController
{
    public $jsonHeader;
    public $secretKey;
    public $urlFirstStep;
    public $urlResult;
    public $mid;

    /*
    public function indexOri(Request $request){
        /// validation
        if(!$request->session()->has("carts") || !$request->session()->get("carts")){
            return response()->json([
                "message" => "You dont have carts",
                "redirect" => url("create")
            ], 422);
        }elseif(!$request->session()->has("customer") || !$request->session()->get("customer")){
            return response()->json([
                "message" => "Customer is not set",
                "redirect" => url("create")
            ], 422);
        }

        $order_id = $request->session()->get('order_id');

        $order_info = \App\Order::find($order_id);

        $is_live = env('APP_ENV') == 'local' ? false : true;
        $merchant_id = '1000089318';
        $key = '2bfba6b3b2af0ccf35dcc4f6166d474cb91266e8';
        $secret_key = 'ccf35d';

        if($is_live){
            $merchant_id = '0000022338';
            $key = '9jjFpbqlJUw3B883Sg28LNUMKuGwOpB8ZYMuedii';
            $secret_key = '7eBYeL5FeyiHb8JZ9lGrgb6cpz1J9yARlvB99lqF9xJsrecYUf7yFpoyC8S8jJxa1SLLMyAhZzeMxqk4aVpwPGyGrR8zPr1Ux0TVF8hDGx3YCmeffZqFtlshgECUKxEo';
        }


        $reddot = new \App\PaymentGateway\Reddot($secret_key, array(
            'first_name' => $order_info->name,
            'amount' => $order_info->total,
            'email' => $order_info->email,
            'order_number' => date("Ymdhis") . '-' . $order_info->id,
            'currency_code' => 'SGD',
            'merchant_id' => $merchant_id,
            'merchant_reference' => 'save-pub-' . time(),
            'notify_url' => url("checkout/notify"),
            'return_url' => url("checkout/return"),
            'cancel_url' => url("create"),
            'transaction_type' => 'Sale',
            'key' => $key,
            'feature_voucher_enable' => '0'
        ), $is_live);

        return view('web_purchase.checkout', [
            "url" => $reddot->url,
            "fields" => $reddot->getFields(),
            "signature" => $reddot->signature
        ]);
    } */

    public function index(Request $request){
        /// validation
        if(!$request->session()->has("carts") || !$request->session()->get("carts")){
            return response()->json([
                "message" => "You dont have carts",
                "redirect" => url("create")
            ], 422);
        }elseif(!$request->session()->has("customer") || !$request->session()->get("customer")){
            return response()->json([
                "message" => "Customer is not set",
                "redirect" => url("create")
            ], 422);
        }

        $this->jsonHeader = ['Content-Type', 'application/json'];
        $this->secretKey = env('APP_ENV') == 'local' ? Config::get('payment.local') : Config::get('payment.live');
        $this->urlFirstStep = env('APP_ENV') == 'local' ? Config::get('payment.url_firststep_local') : Config::get('payment.url_firststep_live');
        $this->urlResult = env('APP_ENV') == 'local' ? Config::get('payment.url_result_local') : Config::get('payment.url_result_live');
        $this->mid = env('APP_ENV') == 'local' ? Config::get('payment.mid_local') : Config::get('payment.mid_live');

        $order_id = $request->session()->get('order_id');

        $order_info = \App\Order::find($order_id);

        $orderid = date("Ymdhis") . '-' . $order_info->id;
        $amount = $order_info->total;

        $status = false;
        $data = [];
        $errors = [];
        $statusCode = 400;

        $params = $this->createFieldSign($orderid, $amount);

        try {
            $client = new Client();
            $request = $client->request('POST', $this->urlFirstStep, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'json' =>$params
            ]);
            $status = true;
            $data = json_decode($request->getBody(), true); //array response
            $statusCode = 200;
            if ($data['response_msg']) {
                Session::put(
                    'payment',
                    $data
                );
            }
        } catch (RequestException $e) {
            $errors =  $e->getMessage();
        }
        /*
        return response()->json([
                'success'   => $status,
                'data'     => Session::get('payment'),
                'errors'     =>$errors
        ], $statusCode);
        */
        
        return view('web_purchase.checkout', [
            "url" => $this->urlFirstStep,
            "fields" => $params,
            "signature" => $params['signature'],
            "success" => $status,
            "data" => Session::get('payment'),
            "errors" =>$errors
        ]);
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'mobile' => 'required',
        ]);

        // if(!$request->session()->has("carts") && !$request->session()->get("carts")){
        //     return response()->json([
        //         "message" => "You dont have carts",
        //         "redirect" => url("occasions")
        //     ], 422);
        // }

        $customer = [
            'name' => $request->name,
            'gender' => " ",
            'country' => $request->country,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'redemtion_code' => $request->redemtion_code,
            'dob' => $request->dob,
            'agree_newsletter' => " ",
            'country_name' => $request->country_name,
            'code' => $request->code,
            'date_format' => $request->date_format,
        ];

        $request->session()->put("customer", $customer);

        $data = \App\Order::confirmOrder();
        if(isset($data['order_id']) && $data['order_id'] > 0 && $data['total'] == 0){
            \App\Order::finishOrder($data['order_id']);
            $voucher = \App\Voucher::where('order_id',$data['order_id'])->first();
            $customer['voucher_id'] = $voucher->voucher_id;
            $request->session()->put("customer", $customer);
            return response()->json([
                "redirect" => url("registration_successful")
            ]);
        }

        return response()->json([
            "customer" => $customer,
            "redirect" => url("checkout")
        ]);
    }

    public function registration_success()
    {
        $customer = Session::get('customer');

        $data_return = array(
            "name" => $customer['name'],
            "voucher_id" => isset($customer['voucher_id']) ? $customer['voucher_id'] : '',
            "country" => $customer['country'],
            "email" => $customer['email'],
            "mobile" => $customer['mobile'],
            "date_format" => $customer['date_format'],
            "country_name" => $customer['country_name'],
            "code" => $customer['code'],
        );
        return view('web_purchase.registration_success',$data_return);
    }

    /* old function */
    public function notify(){
        /*$setting_info = new \App\Setting;
        $setting_info->key = "error_reddot";
        $setting_info->value = $_SERVER['REQUEST_URI'];
        $setting_info->type = "error-reddot";
        return redirect($slug . '/create');*/
    }

    /* old function */
    public function _return(Request $request){
        /*$setting_info = \App\Setting::where("key", "error_reddot")->where("type", "error-reddot")->first();
        if(!$setting_info){
            $setting_info = new \App\Setting;
        }
        $setting_info->key = "error_reddot";
        $setting_info->value = $_SERVER['REQUEST_URI'];
        $setting_info->type = "error-reddot";
        $setting_info->save();*/

        $is_live = env('APP_ENV') == 'local' ? false : true;
        $merchant_id = '1000089318';
        $key = '2bfba6b3b2af0ccf35dcc4f6166d474cb91266e8';
        $secret_key = 'ccf35d';

        if($is_live){
            $merchant_id = '0000022338';
            $key = '9jjFpbqlJUw3B883Sg28LNUMKuGwOpB8ZYMuedii';
            $secret_key = '7eBYeL5FeyiHb8JZ9lGrgb6cpz1J9yARlvB99lqF9xJsrecYUf7yFpoyC8S8jJxa1SLLMyAhZzeMxqk4aVpwPGyGrR8zPr1Ux0TVF8hDGx3YCmeffZqFtlshgECUKxEo';
        }
        
        if(\App\PaymentGateway\Reddot::checkingResponseRDP($secret_key) && $request->result==='Paid'){

            /////////////
            $order_id = $request->session()->get('order_id');
            $order_info = \App\Order::find($order_id);
            $order_items = \App\OrderItem::where("order_id", $order_id)->get();

            $order_info->payment_method = 'reddot';
            $order_info->order_status = 2;

            $order_info->save();
            ////////////////

            $reddot_log = new \App\ReddotLog;
            $reddot_log->mid = isset($_GET['mid'])?$_GET['mid']:'';
            $reddot_log->order_number = isset($_GET['order_number'])?$_GET['order_number']:'';
            $reddot_log->result = isset($_GET['result'])?$_GET['result']:'';
            $reddot_log->confirmation_code = isset($_GET['confirmation_code'])?$_GET['confirmation_code']:'';
            $reddot_log->transaction_id = isset($_GET['transaction_id'])?$_GET['transaction_id']:'';
            $reddot_log->authorization_code = isset($_GET['authorization_code'])?$_GET['authorization_code']:'';
            $reddot_log->card_number = isset($_GET['card_number'])?$_GET['card_number']:'';
            $reddot_log->merchant_reference = isset($_GET['merchant_reference'])?$_GET['merchant_reference']:'';
            $reddot_log->currency_code = isset($_GET['currency_code'])?$_GET['currency_code']:'';
            $reddot_log->amount = isset($_GET['amount'])?$_GET['amount']:'';
            $reddot_log->authorized_currency_code = isset($_GET['authorized_currency_code'])?$_GET['authorized_currency_code']:'';
            $reddot_log->authorized_amount = isset($_GET['authorized_amount'])?$_GET['authorized_amount']:'';
            $reddot_log->signature = isset($_GET['signature'])?$_GET['signature']:'';
            $reddot_log->save();

            \App\Order::finishOrder($order_id);

            return redirect('checkout/success');
        }else{
            return redirect('create');
        }
    }

    /* USE - called after checkoutSuccess */
    public function success(Request $request){
        /* */
        if(!$request->session()->has('order_id')){
            return redirect("confirmation");
        }/* */

        $order_id = $request->session()->get('order_id');

        //$order_id = 336;

        $request->session()->forget('order_id');

        $order_info = \App\Order::find($order_id);
        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();
        $order_items = \App\OrderItem::where("order_id", $order_id)->get();
        $pints = \App\Pint::where("id",2)->first();

        //$y = date("Y", strtotime($order_info->order_date));
        //$m = date("m", strtotime($order_info->order_date));

        $items = [];
        foreach($order_items as $order_item){

            $pint_info = \App\Pint::find($order_item->item_id);
 
            $items[] = [
                "image" => url('files/pint/' . $pints->image),
                'name'  => $order_item->item_name,
                "price" => "SGD " . number_format($order_item->price,2),
                "qty"   => $order_item->qty,
                "pint"  => $order_item->pint,
                "total_pints" => $order_item->total_pint,
                "subtotal" => $order_item->total,
            ];
        }

        return view('web_purchase.order_success', [
            "voucher_id" => $voucher_info->voucher_id,
            "show_discount" => $order_info->discount>0?true:false,
            "discount" => "SGD " . number_format($order_info->discount, 0),
            "grand_total_pints" => number_format($order_info->total_pint, 0),
            "grand_total" => "SGD " . number_format($order_info->total, 2),
            "items" => $items
        ]);
    }

    /* USE - called after checkoutNotify */
    public function failure(Request $request){
        return view('web_purchase.order_failed');
    }

    public function checkoutNotify(Request $request)
    {
        // Merchant’s site URL where a notification will be received once a final result of the payment transaction is acquired.
        var_dump($request);
        $data = json_decode($request->getBody());

        //return redirect('checkout/failure');

        /*
        return response()->json(
            [
                'success'   => false,
                'errors'   => 'Error Payment Gateway',
            ],
            500,
            $this->jsonHeader
        ); */
    }

    /* USE - called after return Reddot payment */
    public function checkoutSuccess(Request $request)
    {
        $this->jsonHeader = ['Content-Type', 'application/json'];
        $this->secretKey = env('APP_ENV') == 'local' ? Config::get('payment.local') : Config::get('payment.live');
        $this->urlFirstStep = env('APP_ENV') == 'local' ? Config::get('payment.url_firststep_local') : Config::get('payment.url_firststep_live');
        $this->urlResult = env('APP_ENV') == 'local' ? Config::get('payment.url_result_local') : Config::get('payment.url_result_live');
        $this->mid = env('APP_ENV') == 'local' ? Config::get('payment.mid_local') : Config::get('payment.mid_live');

        $data = [];
        try {
            $rps = $request->session()->get('payment');
            $resp_array['signature'] = $rps['signature'];
            $payment = new \App\PaymentGateway\ReddotPaymentSignature($this->secretKey, $rps);
            $calculated_signature = $payment->signGeneric();

            $order_id = $request->session()->get('order_id');
            
            if ($calculated_signature != $resp_array['signature']) {
                throw new Exception('signature wrong! invalid response!');
            } else {
                $rp = array(
                    'request_mid' => $this->mid,
                    'transaction_id' => $request->transaction_id
                );
                $payment = new \App\PaymentGateway\ReddotPaymentSignature($this->secretKey, $rp);
                $rp['signature'] = $payment->signGeneric();

                $client = new Client();
                $request = $client->request('POST', $this->urlResult, [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json'
                    ],
                    'json' =>$rp
                ]);
                $status = true;
                $data = json_decode($request->getBody());
                $data->firstStep = $rps;
                $reddot_log = \App\ReddotLog::where('order_number', $data->order_id)->first();
                if (empty($reddot_log)) {
                    $reddot_log = new \App\ReddotLog;
                }
                //var_dump($data);
                $reddot_log->mid = $data->mid;
                $reddot_log->order_id = $order_id;
                $reddot_log->order_number = $data->order_id;
                $reddot_log->result = $data->response_msg;
                $reddot_log->confirmation_code = "";
                $reddot_log->transaction_id = $data->transaction_id;
                $reddot_log->authorization_code = (!empty($data->acquirer_authorization_code)) ? $data->acquirer_authorization_code : '';
                $reddot_log->card_number = "";
                $reddot_log->merchant_reference = "";
                $reddot_log->currency_code = $data->request_ccy;
                $reddot_log->amount = $data->request_amount;
                $reddot_log->authorized_currency_code = $data->authorized_ccy;
                $reddot_log->authorized_amount = $data->authorized_amount;
                $reddot_log->signature = $data->signature;
                $reddot_log->save();

                /* */
                if (trim(strtolower($data->response_msg)) == 'successful') {
                    // Save Order & Voucher
                    \App\Order::finishOrder($order_id);

                    return redirect('checkout/success');
                } else {
                    return redirect('checkout/failure');
                } /* */
                
            }
        } catch (RequestException $e) {
            $errors =  $e->getMessage();
        }

        /* /
        return view('web_purchase.order_success', [
            "voucher_id" => '',
            "show_discount" => $order_info->discount>0?true:false,
            "discount" => "SGD " . number_format($order_info->discount, 0),
            "grand_total_pints" => number_format($order_info->total_pint, 0),
            "grand_total" => "SGD " . number_format($order_info->total, 2),
            "items" => $items
        ]); 
        /* */
    }

    /* REF function */
    public function storePaymentGatewayProperties(Request $request)
    {
        Session::forget('payment');     // init session container

        $status = false;
        $data = [];
        $errors = [];
        $statusCode = 400;

        $params = $this->createFieldSign($request->orderID, $request->amount);

        try {
            $client = new Client();
            $request = $client->request('POST', $this->urlFirstStep, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'json' =>$params
            ]);
            $status = true;
            $data = json_decode($request->getBody(), true); //array response
            $statusCode = 200;
            if ($data['response_msg']) {
                Session::put(
                    'payment',
                    $data
                );
            }
        } catch (RequestException $e) {
            $errors =  $e->getMessage();
        }
        return response()->json([
                'success'   => $status,
                'data'     => Session::get('payment'),
                'errors'     =>$errors
        ], $statusCode);
    }   

    /* USE - called by index */
    public function createFieldSign($orderID, $amount)
    {
        $params['api_mode'] = 'redirection_hosted';
        $params['mid'] = $this->mid;
        $params['order_id'] = $orderID;
        $params['payment_type'] = 'S';
        $params['amount'] = $amount;
        $params['ccy'] = 'SGD';

        $payment = new \App\PaymentGateway\ReddotPaymentSignature($this->secretKey, $params);
        $params['redirect_url'] = url('/checkout-success');
        $params['notify_url'] = url('/checkout-notify');
        $params['back_url'] = url('/confirmation');
        $params['signature'] = $payment->generateSignature();

        return $params;
    }

    //function for testing only
    public function getSignature()
    {
        if (isset($_GET['secret_key']) && isset($_GET['api_mode'])) {
            $params['mid'] = '1000089318';
            $params['order_id'] = 'test-1';
            $params['payment_type'] = 'S';
            $params['amount'] = '1';
            $params['ccy'] = 'SGD';
            $secret_key="ccf35d";

            $fields_for_sign = array('mid', 'order_id', 'payment_type', 'amount', 'ccy');
            $aggregated_field_str = "";
            foreach ($fields_for_sign as $f) {
                $aggregated_field_str .= trim($params[$f]);
            }
            // api_mode = redirection_sop
            if ($_GET['api_mode'] == 'sop') {
                $params['api_mode'] = 'redirection_sop';
                $params['card_no'] = '4598587972447987';
                $params['exp_date'] = '102022';
                $params['cvv2'] = '586';
                $params['payer_name'] = 'Dev Test';
                $params['payer_email'] = 'admin@cprvision.com';

                $aggregated_field_str .= substr($params['card_no'], 0, 6).substr($params['card_no'], -4);
                $aggregated_field_str .= $params['exp_date'];
                $aggregated_field_str .= substr($params['cvv2'], -1);
            }
            // api_mode = redirection_hosted
            if ($_GET['api_mode'] == 'hop') {
                $params['api_mode'] = 'redirection_hosted';

                // it doesn't need payer_id
                // $params['token_mod'] = 0;
                // $params['payer_id'] = 'test-1';
                // $aggregated_field_str .= $params['payer_id'];
            }

            $secret_key="ccf35d"; //secret key test
            $aggregated_field_str .= $_GET['secret_key'];

            $params['redirect_url'] = 'http:\/\/localhost\/rdp\/service\/test-suite\/T_redirection_hosted_single\/redirect_url';
            $params['notify_url'] = 'http:\/\/localhost\/rdp\/notif_server\/payment_api\/notif-url.php';
            $params['back_url'] = 'http:\/\/localhost\/rdp\/service\/test-suite\/T_redirection_hosted_single\/back_url';

            $signature = hash('sha512', $aggregated_field_str);

            $params['signature'] = $signature;

            echo json_encode($params);
        } else {
            echo 'add param secret_key and api_mode = sop/hop';
        }
    }

}
