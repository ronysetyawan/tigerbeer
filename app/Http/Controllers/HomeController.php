<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Routing\Route;

use View;
use Captcha;
use Validator;
use Illuminate\Support\Facades\Input;
use DB;
class HomeController extends BaseController
{

    public function index(Request $request){

        $faqs = array();

        $faqs[] = array(
            "title" => "When and where can I redeem my complimentary drinks?",
            "body" => "<p>Vouchers are valid for redemption from <WGS to advise on campaign duration>. Select your purchased bottle to view all participating bars.</p>
            "
        );
        $faqs[] = array(
            "title" => "What must I do to enjoy my complimentary drinks?",
            "body" => "<p>Scan the QR code on the neck tag of your purchased bottle. Fill in your registration details and redemption code. Your e-voucher will be sent via SMS. Show the e-voucher to redeem 2 complimentary drinks at participating outlets.</p>"
        );
        $faqs[] = array(
            "title" => "What if i did not receive my voucher or I’m unable to redeem my drinks at a participating bar?",
            "body" => "<p>Please contact us using the form below or WGS to advise if any number to call for immediate assistance and we will help.</p>"
        );
        $faqs[] = array(
            "title" => "Can I transfer my drinks to another friend?",
            "body" => "<p>Yes. Vouchers may be used by anyone who has the link. However, each voucher can only be used once.</p>"
        );
        $faqs[] = array(
            "title" => "Is there an expiry date for my drinks?",
            "body" => "<p>WGS to advise on campaign duration.</p> "
        );
        $faqs[] = array(
            "title" => "How do I track the redemption status of my drinks?",
            "body" => "<p>CPR to advise</p> "
        );
        // $faqs[] = array(
        //     "title" => "How long will this initiative be running? ",
        //     "body" => "<p>The initiative will be running from 17 April 2020 to 31 May 2020. During this period, you can make their contributions through <a href='/confirmation'>here</a>.</p> "
        // );
        // $faqs[] = array(
        //     "title" => "How much of my contribution goes to the outlet?",
        //     "body" => "<p>Each week, the contributions are collated and all proceeds will be distributed equally among the participating F&B outlets. The F&B community will receive 100% from each contribution. Any additional transaction fees and charges will be absorbed by Tiger. </p> "
        // );
        // $faqs[] = array(
        //     "title" => "Is there a limit on the number of contributions per person?",
        //     "body" => "<p>There is no limit on the number of times you may make a contribution.</p> "
        // );
        // $faqs[] = array(
        //     "title" => "What are the payment options available? ",
        //     "body" => "<p>Contributions can be made through Visa and Mastercard.</p> "
        // );
        // $faqs[] = array(
        //     "title" => "Are my transactions safe and secure?",
        //     "body" => "Yes, we have created a highly secure platform for our community. Your personal details, including credit card details, are kept safe and connection to the server is encrypted to enhance security and avoids misuse of your personal details."
        // );
        // $faqs[] = array(
        //     "title" => "Is there a limit to the number of drink vouchers Tiger will contribute?",
        //     "body" => "The overall goal is to raise $300,000 in contributions from Singaporeans and we’re looking at giving out 30,000 vouchers or 60,000 beers."
        // );
        // $faqs[] = array(
        //     "title" => "When will I receive my redemption voucher?",
        //     "body" => "You will receive an auto-generated SMS within 15 minutes after the contribution has been made. This SMS will also provide details of the redemption process and a redemption link."
        // );
        // $faqs[] = array(
        //     "title" => "I’ve not received my voucher. Who can I contact?",
        //     "body" => "Please send an email attached with a screenshot of your successful transaction to <u>enquiries@apb.com.sg.</u>"
        // );
        // $faqs[] = array(
        //     "title" => "How long are the vouchers valid for?",
        //     "body" => "The vouchers are valid until 30 September 2020. However, the redemption period may be extended depending on the COVID-19 situation. "
        // );
        // $faqs[] = array(
        //     "title" => "Are the vouchers exchangeable for cash?",
        //     "body" => " The vouchers are not exchangeable for cash."
        // );
        // $faqs[] = array(
        //     "title" => "Are the vouchers transferable?",
        //     "body" => "Yes, anyone with the redemption link can redeem the drinks."
        // );
        // $faqs[] = array(
        //     "title" => "Which outlets are participating in the voucher redemption?",
        //     "body" => "The vouchers can be redeemed at any of the participating coffeeshops, bars, pubs and restaurants. Please refer to our full list of partners <a href='#how-to-participate'>here</a>."
        // );
        // $faqs[] = array(
        //     "title" => "How do I redeem my drink? ",
        //     "body" => "All redemption vouchers will be sent out digitally after the contribution has been made. Each contribution will entitle you to a digital drink voucher redeemable for two Tiger beers when the outlet resumes operations. 
        //     To use, simply access the redemption link, click to select the beer (Tiger or Tiger Crystal while stocks last) that you wish to redeem. Click the ‘Redeem’ button and show it to any staff from the participating outlet. 
        //     "
        // );
        // $faqs[] = array(
        //     "title" => "Can I cancel my contribution?",
        //     "body" => "All contributions made are not refundable."
        // );
        // $faqs[] = array(
        //     "title" => "Does the voucher have to be redeemed in full at once?",
        //     "body" => "You may redeem the desired number of pints and the quantity will be deducted from the voucher. Any remaining balance can be used for future redemption and the original expiry date applies."
        // );
        // $faqs[] = array(
        //     "title" => "What do I do if my voucher has not been delivered?",
        //     "body" => "Please contact us via email at <a href='mailto:enquiries@apb.com.sg'>enquiries@apb.com.sg</a>"
        // );
        /*$faqs[] = array(
            "title" => "How does the point system work?",
            "body" => "For every purchase of 1 x Erdinger pint of beer on the Erdinger online purchase platform, 1 point will be added into your account.<br>
            Accumulate the points to redeem the prizes online during the promotion period."
        );*/
        // $faqs[] = array(
        //     "title" => "Per beer entitles purchaser to one game. Consumers can redeem the game vouchers at the redemption booth during the event itself.",
        //     "body" => "<p>Prizes are:</p>
        //         <ul>
        //             <li>Consolation prizes:
        //                 <ul>
        //                     <li>Grey/brown fedora hats</li>
        //                     <li>Erdinger coasters</li>
        //                     <li>Erdinger note book</li>
        //                     <li>Floral band</li>
        //                     <li>Oktoberfest mug</li>
        //                 </ul>
        //             </li>
        //             <li>Grand prizes:
        //                 <ul>
        //                     <li>1L Stein mugs</li>
        //                     <li>Mini glass</li>
        //                     <li>Chicken hat</li>
        //                     <li>Pretzel float</li>
        //                 </ul>
        //             </li>
        //             <li>Stage challenge game
        //                 <ul>
        //                     <li>3L Erdinger glass</li>
        //                     <li>Karcher vacuum cleaner</li>
        //                 </ul>
        //             </li>
        //             <li>Lucky draw:
        //                 <ul>
        //                     <li>$1000 worth of Hugo styling session + outfit</li>
        //                 </ul>
        //             </li>
        //         </ul>"
        // );
        /*$faqs[] = array(
            "title" => "What are the prizes and how many points are required for each prize? ",
            "body" => "Prizes are:<ul><li>4 points = $6 off for the next Erdinger pint online purchase</li>
                <li>7 points = $12 off for the next Erdinger pint online purchase</li>
                <li>10 points = A 3-litre Erdinger Glass</li> 
                <li>13 points = A 2.5-litre Erdinger Beer Tower</li></ul>"
        );
        $faqs[] = array(
            "title" => "Can I transfer my points to my friends?",
            "body" => "No, the points are not transferrable."
        );
        $faqs[] = array(
            "title" => "Is there an expiry date for the points?",
            "body" => "Yes, points are valid till 30 November 2018, 2359 hours. Any unused balance of the Rewards is not refundable or exchangeable."
        );
        $faqs[] = array(
            "title" => "How can I redeem my prize?",
            "body" => "You may redeem your prize through the voucher page link that is sent to you upon confirmation of your purchase. Your total number of points will be reflected on the link and you may select your preferred prize and desired quantity. Follow the steps and your prizes will be on the way to you. "
        );*/
        // $faqs[] = array(
        //     "title" => "Where can I collect my prize?",
        //     "body" => "3-litre Erdinger glass and Karcher vacuum will be delivered to your preferred address."
        // );
        // $faqs[] = array(
        //     "title" => "When will the prizes be delivered to me?",
        //     "body" => "<p>3-litre Erdinger glass and Karcher vacuum will be delivered by our appointed vendor and they will contact you within 7 working days for the delivery.</p><p>We will collect details and contact number for the grand prize winners for the Hugo experience and arrange a suitable timing with Hugo Boss.</p>"
        // );
        // $faqs[] = array(
        //     "title" => "Can I transfer my points to my friends?",
        //     "body" => "No, the points are not transferrable."
        // );
        /*$faqs[] = array(
            "title" => "Can I change my delivery address after confirming the prize redemption?",
            "body" => "No, you are required to key in the delivery address correctly before confirming the prize redemption. "
        );
        $faqs[] = array(
            "title" => "Do I get rewarded if I have successfully referred someone to purchase Erdinger online? ",
            "body" => "Yes, you will receive a $3 off next Erdinger pint online purchase for successful referral. Successful referral refers to person who has been referred by you and who has successfully purchased the Promotion Products using a unique code issued to them via the Website during the Promotion Period. "
        );
        $faqs[] = array(
            "title" => "How do I refer a friend? ",
            "body" => "You can send the referral link to your friends through the voucher page. Your friend is required to purchase the Promotion Products using a unique code issued to them via the Website during the Promotion Period."
        );
        $faqs[] = array(
            "title" => "How do I know if my friend has successfully purchased Erdinger online? ",
            "body" => "You will receive a SMS when your friend has made a purchase. "
        );
        $faqs[] = array(
            "title" => "What do I do if I do not receive my prize?",
            "body" => "If you do not receive your prize after 7 days, please contact us via email at <a href='mailto:enquiries@apb.com.sg'>enquiries@apb.com.sg</a>"
        );*/

        // count all contributions
        $base = 100000;
        $total = \App\ReddotLog::where('result','successful')->sum('amount');
        $raw_total = number_format($base + $total,0);
        $arr_total = str_split($raw_total);

        $data_return = array(
            "faqs" => $faqs,
            "arr_total" => $arr_total,
            "total_contribution" => $raw_total,
            "contact_url" => url('/contact'),
            "captcha_image" => Captcha::url()
        );
        
        return view('web_purchase.home', $data_return);
    }

    public function getStoreByName(Request $request)
    {
        $name   = $request->name == "" ? null : $request->name;
        $store  = DB::table('properties')
            ->where(function ($store) use ($name){
                $store->where('name','like','%'.$name.'%')
                ->orWhere('address1', 'like', '%' . $name . '%');
            })
            ->where('status',1)
            ->orderBy("name","ASC")
            ->get();

        return response()->json([
            'success'   => true,
            'data'      => $store
        ]);
    }

    public function getStoreByRegion(Request $request){
        if($request->region != ""){
            $store = DB::table('properties')
            ->where('zone', $request->region)
            ->where('status',1)
            ->orderBy("name", "ASC")
            ->get();
        } else {
            $store = DB::table('properties')
            ->where('status',1)
            ->orderBy("name", "ASC")
            ->get();
        }

        return response()->json([
            'success'   => true,
            'data'      => $store
        ]);
    }

    public function captchaImage(Request $request){
        $captcha = Captcha::url();

        $data_return = array(
            "image" => $captcha
        ); 
        return $data_return;
    }

    public function captchaCheck(Request $request){
        if(!Captcha::check($request->captcha)) {
            return response()->json([
                "message" => "Captcha is not correct"
            ], 422);
        }
    }

    public function registration(Request $request){

        $text1 = 'Please confirm that you have read and agree to the Terms & Condition.';
        
        for ($i=1; $i <= 31; $i++) { 
            if($i < 10){
                $dd[] = 0 . $i;
            } else {
                $dd[] = $i;
            }
        }
        
        for ($i=1900; $i <= date('Y'); $i++) { 
            $yyyy[] = $i;
        }

        $data_return = array(
            "text1" => $text1,
            "dd" => $dd,
            "yyyy" => $yyyy,
        );
        
        return view('web_purchase.registration',$data_return);
    }

    public function redemtioncodeCheck(Request $request){
        $redemption_code = DB::table('redemption_code')
        ->where('code',$request->redemtion_code)
        ->first();

        if(!empty($redemption_code)){
            if($redemption_code->status == 1){
                return response()->json([
                    "message" => "Code already used"
                ], 422);
            }
        } else {
            return response()->json([
                "message" => "Code not available"
            ], 422);
        }
    }

    public function recipes(){

        $data = '';

        $data_return = array(
            "data" => $data,
        );
        
        return view('web_purchase.recipes',$data_return);
    }

    public function recipes_details($id = null)
    {
        if($id == 1){
            $INGREDIENTS = '
            <li>2 parts Reyka Vodka</li>
            <li>3/4 Ginger Ale</li>
            <li>1/2 part Lime Juice</li>
            <li>Top with Soda water</li>
            <li>Garnish: Lime Wedge</li>
            ';

            $PREPARATION = '
            <li>Measure and combime all ingredients into a shaker.</li>
            <li>Shake and strain into glass.</li>
            <li>Top with Soda water and garnish with a lime wedge.</li>
            ';
        } else if($id == 2){
            $INGREDIENTS = "
            <li>1 part Sailor Jerry Spiced Rum</li>
            <li>3 parts Cola</li>
            <li>Garnish: Lime</li>";

            $PREPARATION = "
            <li>In a highball glass, measure Sailor Jerry spiced rum.</li>
            <li>Add ice.</li>
            <li>Top with cola and garnish with a lime wheel.</li>
            ";
        } else if($id == 3){
            $INGREDIENTS = "
            <li>1 1/2 parts milagro silver</li>
            <li>2 parts club soda</li>
            <li>2 parts tonic</li>
            <li>Garnish: 2 Dashes of grapefruit or orange bitters</li>
            ";

            $PREPARATION = "
            <li>Fill glass with ice, add Milagro Silver and bitters.</li>
            <li>Top with club soda and tonic.</li>
            <li>Garnish with lime wheel and grapefruit wheel float.</li>
            ";
        } else if($id == 4){
            $INGREDIENTS = "
            <li>50ml Hendrick's Gin</li>
            <li>150ml Tonic Water</li>
            <li>Garnish: 3 thinly sliced rounds of cucumbers</li>";

            $PREPARATION = "
            <li>Combine all ingredients in a highball glass filled with cubed ice.</li>
            <li>Lightly sitr and serve.</li>
            <li>Garnish with 3 thinly sliced rounds of cucumbers.</li>
            ";
        } else if($id == 5){
            $INGREDIENTS = "
            <li>50ml Monkey Shoulder</li>
            <li>120ml Ginger Ale</li>
            <li>Garnish: Orange</li>";

            $PREPARATION = "
            <li>Pour Monkey Shoulder into rock glass.</li>
            <li>Add ice.</li>
            <li>Top up with Ginger Ale and garnish with orange wedge.</li>
            ";
        } else if($id == 6){
            $INGREDIENTS = "
            <li>30ml Glenfiddich 12 Years Old</li>
            <li>90ml Soda Water</li>
            <li>Garnish: Lemon</li>";

            $PREPARATION = "
            <li>Pour Glenfiddich 12 into Highball glass.</li>
            <li>Add ice.</li>
            <li>Top up with soda and garnish with lemon wedge.</li>
            ";
        }

        $data_return = array(
            "INGREDIENTS" => $INGREDIENTS,
            "PREPARATION" => $PREPARATION
        );
        
        return view('web_purchase.recipes_details',$data_return);
    }

    public function checkAge(Request $request){
        $date = date_format(date_add(date_create($request->dob),date_interval_create_from_date_string("1 days")),"Y-m-d");
        $date = strtotime($date);
        $min = strtotime('+18 years', $date);
        if(time() < $min)  {
            return response()->json([
                "message" => "Age must be 18 years and over"
            ], 422);
        }
    }

}
