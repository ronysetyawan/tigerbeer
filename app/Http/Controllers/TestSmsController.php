<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Voucher;

use Mail;

class TestSmsController extends Controller
{
    public function index(){
        
        $order_id = 0;

        $order_info = \App\Order::find($order_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $user_mobile = $voucher_info->owner_mobile;
        $len_user_mobile = strlen($user_mobile);
        $trim_user_mobile = $len_user_mobile - 4;
        $string_mobile = substr($user_mobile,($trim_user_mobile),4);

        //// send sms
        $api_key = env('NEXMO_KEY');
        $api_secret = env('NEXMO_SECRET');
        $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
        $sms_text = 'Thank you for your support. View your voucher at '. 
        url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id) .' We look forward to welcoming you back very soon!';

        $message = $client->message()->send([
            'to' => $voucher_info->owner_calling_code . $voucher_info->owner_mobile,
            'from' => 'Tiger',
            'text' => $sms_text
        ]);
        if($message){
            //echo "SMS sent!";
            //$result['message'] = true;
            //$result['message_text'] = $text_message;
            $sms_log = new \App\SmsLog;
            $sms_log->voucher_id = $voucher_info->voucher_id;
            $sms_log->date = date("Y-m-d H:i:s");
            $sms_log->mobile = $voucher_info->owner_calling_code . $voucher_info->owner_mobile;
            $sms_log->text = $sms_text;
            $sms_log->status = 'Success';
            $sms_log->created_at = date("Y-m-d H:i:s");
            $sms_log->updated_at = date("Y-m-d H:i:s");
            $sms_log->save(); 
        }else{
            //echo "Sorry SMS is failed!";
            //$result['message'] = false;
            //$result['message_text'] = $text_message;
            //$result['message_reason'] = "Sorry SMS is failed!";
            $sms_log = new \App\SmsLog;
            $sms_log->voucher_id = $voucher_info->voucher_id;
            $sms_log->date = date("Y-m-d H:i:s");
            $sms_log->mobile = $voucher_info->owner_calling_code . $voucher_info->owner_mobile;
            $sms_log->text = $sms_text;
            $sms_log->status = 'Failed';
            $sms_log->created_at = date("Y-m-d H:i:s");
            $sms_log->updated_at = date("Y-m-d H:i:s");
            $sms_log->save(); 
        }
    }

    public function resend(Request $request){
        
        $order_id = $request->orderid;

        $order_info = \App\Order::find($order_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $user_mobile = $voucher_info->owner_mobile;
        $len_user_mobile = strlen($user_mobile);
        $trim_user_mobile = $len_user_mobile - 4;
        $string_mobile = substr($user_mobile,($trim_user_mobile),4);

        $to = $voucher_info->owner_calling_code . $voucher_info->owner_mobile;
        //$to = "+6281266446612";
        $link = url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id);
        
        //// send sms
        $api_key = env('NEXMO_KEY');
        $api_secret = env('NEXMO_SECRET');
        $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
        $sms_text = 'Thank you for contributing! With some participating outlets reopening, you can redeem beers starting 19 Jun '. $link;

        $message = $client->message()->send([
            'to' => $to,
            'from' => 'Tiger',
            'text' => $sms_text
        ]);
        if($message){
            echo "SMS sent! to ".$to." (".$voucher_info->owner_name.")";
            //$result['message'] = true;
            //$result['message_text'] = $text_message;
            $sms_log = new \App\SmsLog;
            $sms_log->voucher_id = $voucher_info->voucher_id;
            $sms_log->date = date("Y-m-d H:i:s");
            $sms_log->mobile = $to;
            $sms_log->text = $sms_text;
            $sms_log->status = 'Success';
            $sms_log->created_at = date("Y-m-d H:i:s");
            $sms_log->updated_at = date("Y-m-d H:i:s");
            $sms_log->save(); 
        }else{
            echo "Sorry SMS send failed!";
            //$result['message'] = false;
            //$result['message_text'] = $text_message;
            //$result['message_reason'] = "Sorry SMS is failed!";
            $sms_log = new \App\SmsLog;
            $sms_log->voucher_id = $voucher_info->voucher_id;
            $sms_log->date = date("Y-m-d H:i:s");
            $sms_log->mobile = $to;
            $sms_log->text = $sms_text;
            $sms_log->status = 'Failed';
            $sms_log->created_at = date("Y-m-d H:i:s");
            $sms_log->updated_at = date("Y-m-d H:i:s");
            $sms_log->save(); 
        }
    }

    public function test(){

        $order_id = 100;

        $order_info = \App\Order::find($order_id);

        $voucher_info = \App\Voucher::where("user_id", $order_info->user_id)->first();

        $user_mobile = $voucher_info->owner_mobile;
        $len_user_mobile = strlen($user_mobile);
        $trim_user_mobile = $len_user_mobile - 4;
        $string_mobile = substr($user_mobile,($trim_user_mobile),4);

        $to = $voucher_info->owner_calling_code . $voucher_info->owner_mobile;
        //$to = "+6281266446612";

        //// send sms
        $api_key = env('NEXMO_KEY');
        $api_secret = env('NEXMO_SECRET');
        $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
        $message = $client->message()->send([
            'to' => $to,
            'from' => 'Tiger',
            'text' => 'Thanks for contributing! With some participating outlets reopening, you can redeem beers starting 19 Jun. View your voucher at ' . 
            url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id)
        ]);

        //var_dump($message);
        if($message){
            echo "sent to ".$to;
        }
    }

    public function sendSmsReminderTest(){
        $vouchers = Voucher::where('sms_reminder', 0)
                                    //->whereOr('sms_reminder', 0)
                                    ->where('voided', 0)
                                    ->where('owner_mobile', '<>', '')
                                    ->where('owner_mobile', '<>', '00000000')
                                    ->where('balance','>',0)
                                    ->limit(20)
                                    ->get();


        $vcount = Voucher::where('sms_reminder', 0)
                                    //->whereOr('sms_reminder', 0)
                                    ->where('voided', 0)
                                    ->where('owner_mobile', '<>', '')
                                    ->where('owner_mobile', '<>', '00000000')
                                    ->where('balance','>',0)
                                    ->count();
                                    
                                    dd($vcount);


        if ($vcount > 0) {
            foreach($vouchers as $voucher){
                // SMS REMINDER
                if($voucher->sms_reminder == 0){
                    $user_mobile = $voucher->owner_mobile;
    
                    $url = env('LIVE_URL');
                    $link = $url . '/voucher/' . $voucher_info->code . '/' . $voucher->code;
                    // $link = "http://save-pubs-my.test" . '/voucher/' . $voucher->code;
                    
                    //// send sms
                    $api_key = env('NEXMO_KEY');
                    $api_secret = env('NEXMO_SECRET');
                    $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
                    // $name = $user_info->first_name;
        
                    $to = $voucher->owner_calling_code . $voucher->owner_mobile;
                    $hash = \App\Hash::where('type', 'mobile')->where('key', $user_mobile)->first();
                    $sms_text = 'Thanks for contributing! With some participating outlets reopening, you can redeem beers starting 19 Jun. View your voucher at '. $link;
                    
                    $message = $client->message()->send([
                        'to' => $to,
                        'from' => 'Tiger',
                        'text' => $sms_text
                    ]);
                    if($message){
                        $voucher->sms_reminder = 1;
                        $voucher->sms_reminder_date = date('Y-m-d H:i:s');
                        $voucher->save();
        
                        $sms_log = new \App\SmsLog;
                        $sms_log->voucher_id = $voucher->voucher_id;
                        $sms_log->date = date("Y-m-d H:i:s");
                        $sms_log->mobile = $to;
                        $sms_log->text = $sms_text;
                        $sms_log->status = 'sent.reminder.success';
                        $sms_log->created_at = date("Y-m-d H:i:s");
                        $sms_log->updated_at = date("Y-m-d H:i:s");
                        $sms_log->save(); 

                        echo "Reminder sms success ($to) <br>";
                    }else{
                        $voucher->sms_reminder = -1;
                        $voucher->save();
    
                        $sms_log = new \App\SmsLog;
                        $sms_log->voucher_id = $voucher->voucher_id;
                        $sms_log->date = date("Y-m-d H:i:s");
                        $sms_log->mobile = $to;
                        $sms_log->text = $sms_text;
                        $sms_log->status = 'sent.reminder.failed';
                        $sms_log->created_at = date("Y-m-d H:i:s");
                        $sms_log->updated_at = date("Y-m-d H:i:s");
                        $sms_log->save(); 

                        echo "Reminder sms failed ($to) <br>";
                    }
                }
            }
        }
    }

    // LIVE email & sms reminder
    function sendReminder()
    {
        $vouchers = Voucher::where('sms_reminder', 0)
            //->whereOr('sms_reminder', 0)
            ->where('voided', 0)
            ->where('owner_mobile', '<>', '')
            ->where('owner_mobile', '<>', '00000000')
            ->where('balance','>',0)
            ->limit(20)
            ->get();

        $vcount = Voucher::where('sms_reminder', 0)
                //->whereOr('sms_reminder', 0)
                ->where('voided', 0)
                ->where('owner_mobile', '<>', '')
                ->where('owner_mobile', '<>', '00000000')
                ->where('balance','>',0)
                ->count();

        if ($vcount > 0) {
            foreach($vouchers as $voucher){
                // SMS REMINDER
                if($voucher->sms_reminder == 0){
                    $user_mobile = $voucher->owner_mobile;

                    //$url = env('LIVE_URL');
                    //$link = $url . '/voucher/' . $voucher_info->code . '/' . $voucher->code;
                    $link = url('voucher/' . $voucher->code . '/' . $voucher->voucher_id);

                    //// send sms
                    $api_key = '61f0cb2d';
                    $api_secret = 'd01e159afbeeec55';
                    $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
                    // $name = $user_info->first_name;

                    $to = $voucher->owner_calling_code . $voucher->owner_mobile;
                    //$hash = \App\Hash::where('type', 'mobile')->where('key', $user_mobile)->first();
                    $sms_text = 'Thank you for contributing! With some participating outlets reopening, you can redeem beers starting 19 Jun '. $link;

                    $message = $client->message()->send([
                        'to' => $to,
                        'from' => 'Tiger',
                        'text' => $sms_text
                    ]);
                    if($message){
                        $voucher->sms_reminder = 1;
                        $voucher->sms_reminder_date = date('Y-m-d H:i:s');
                        $voucher->save();

                        $sms_log = new \App\SmsLog;
                        $sms_log->voucher_id = $voucher->voucher_id;
                        $sms_log->date = date("Y-m-d H:i:s");
                        $sms_log->mobile = $to;
                        $sms_log->text = $sms_text;
                        $sms_log->status = 'send.reminder.success';
                        $sms_log->created_at = date("Y-m-d H:i:s");
                        $sms_log->updated_at = date("Y-m-d H:i:s");
                        $sms_log->save(); 

                        echo "Reminder sms send success ($to) <br>";
                    }else{
                        $voucher->sms_reminder = -1;
                        $voucher->save();

                        $sms_log = new \App\SmsLog;
                        $sms_log->voucher_id = $voucher->voucher_id;
                        $sms_log->date = date("Y-m-d H:i:s");
                        $sms_log->mobile = $to;
                        $sms_log->text = $sms_text;
                        $sms_log->status = 'send.reminder.failed';
                        $sms_log->created_at = date("Y-m-d H:i:s");
                        $sms_log->updated_at = date("Y-m-d H:i:s");
                        $sms_log->save(); 

                        echo "Reminder sms send failed ($to) <br>";
                    }
                }
            }
        }

    }

}
