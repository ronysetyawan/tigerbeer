<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use View;

use Auth;


class CartController extends BaseController
{
    public function index(){
        // bypass redirect to thank you
        //return view('web_purchase.thankyou');
        return redirect('thankyou');
    }

    public function indexOld(Request $request){
        // call bypass function to filled cart with default product
        $this->defaultCart($request);

        if(!$request->session()->has("carts") || !$request->session()->get("carts")){
            return redirect('/create');
        }

        $setting = \App\Setting::where("type", "global")->where("key", "product_name")->first();
        $product_name = $setting->value;

        $data_carts = $request->session()->get("carts");
        //var_dump($data_carts);

        $carts = array();
        $grand_total_pints = 0;
        $grand_total = 0;

        foreach($data_carts as $pint_id=>$cart){
            $pint = \App\Pint::find($pint_id);

            $total_pints = (int)$cart['qty'] * $pint->pint;
            $subtotal = (int)$cart['qty'] * $pint->price;

            $grand_total_pints += $total_pints;
            $grand_total += $subtotal;
            //$name = $product_name . " " . $pint->pint . " Entitlements";

            $carts[$pint_id] = array(
                "image" => url('files/pint/' . $pint->image),
                "name" => $product_name,
                "price" => "SGD " . number_format($pint->price,2),
                "qty"   => $cart['qty'],
                "total_pints" => $total_pints,
                "subtotal" => "SGD " . number_format($subtotal, 2),
                "id" => $pint_id,
            );
        }

        $contact_details = [];

        $dobs = explode("-",  $request->session()->get("age"));
        $dob_year = "";
        $dob_month = "";
        $dob_date = "";
        if($dobs){
            $dob_date = $dobs[2];
            $dob_month = $dobs[1];
            $dob_year = $dobs[0];
        }

        $contact_details["dob_date"] = (int)$dob_date;
        $contact_details["dob_month"] = (int)$dob_month;
        $contact_details["dob_year"] = (int)$dob_year;

        $is_logged_in = false;

        if(Auth::guard('customer')->check()){

            $user = Auth::guard('customer')->user();
            
            $dobs = explode("-", $user->dob);
            $dob_year = "";
            $dob_month = "";
            $dob_date = "";
            if($dobs){
                $dob_date = $dobs[2];
                $dob_month = $dobs[1];
                $dob_year = $dobs[0];
            }

            //$contact_details["name"] = $user->first_name;
            $contact_details["gender"] = $user->gender;
            $contact_details["country"] = (int)$user->country_id;
            $contact_details["mobile"] = $user->mobile;
            $contact_details["email"] = $user->email;
            $contact_details["dob_date"] = (int)$dob_date;
            $contact_details["dob_month"] = (int)$dob_month;
            $contact_details["dob_year"] = (int)$dob_year;
            $contact_details["postal_code"] = $user->postal_code;

            $is_logged_in = true;
        }

        $promo_code_applied = 0;
        $discount = 0;
        $ses_promo_code = $request->session()->get("promo_code");
        if($ses_promo_code){
            $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $ses_promo_code['code'])->first();
            if($promo_code){
                $discount = $promo_code->discount;
                $promo_code_applied = 1;
            }
        }

        $grand_total = ($grand_total-$discount);

        $data_return = array(
            "carts" => $carts,
            "product_url" => url("product"),
            "payment_url" => url("payment"),
            "grand_total_pints" => number_format($grand_total_pints, 0),
            "discount" => 'SGD ' . number_format($discount, 2),
            "promo_code_applied" => $promo_code_applied,
            "grand_total" => 'SGD ' . number_format($grand_total, 2),
            "contact_details" => $contact_details,
            "is_logged_in" => $is_logged_in
        );
        
        return view('web_purchase.confirmation', $data_return);
    }

    public function hasCart(Request $request){
        return response()->json([
            "has_cart" => ($request->session()->has("carts") && $request->session()->get("carts"))
        ]);
    }

    public function store(Request $request){

        if ($request->session()->has('carts')) {
            $carts = $request->session()->get('carts');
        }else{
            $carts = array();
        }

        $id = $request->product;

        if(isset($carts[$id])){
            $carts[$id]['qty'] += $request->qty; 
        }else{
            $carts[$id] = array(
                "product" => $request->product,
                "pint" => $request->pint,
                "qty" => $request->qty
            );
        }

        $request->session()->put('carts', $carts);

        return response()->json([
            "message" => "Add to cart success"
        ]);
    }

    public function increaseQty(Request $request, $id){

        if ($request->session()->has('carts')) {
            $carts = $request->session()->get('carts');
        }else{
            $carts = array();
        }

        if(isset($carts[$id])){
            $carts[$id]['qty']++; 
            $request->session()->put('carts', $carts);
            $error = false;
        }else{
            $error = true;
        }

        $pint = \App\Pint::find($id);

        $total_pints = (int)$carts[$id]['qty'] * $pint->pint;
        $subtotal = (int)$carts[$id]['qty'] * $pint->price;

        $grand_total_pints = 0;
        $grand_total = 0;
        
        foreach($carts as $pint_id=>$cart){
            $pint = \App\Pint::find($pint_id);

            $grand_total_pints += (int)$cart['qty'] * $pint->pint;
            $grand_total += (int)$cart['qty'] * $pint->price;
        }

        $discount = 0;
        $ses_promo_code = $request->session()->get("promo_code");
        if($ses_promo_code){
            $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $ses_promo_code['code'])->first();
            if($promo_code){
                $discount = $promo_code->discount;
            }
        }

        $grand_total = ($grand_total-$discount);

        return response()->json([
            "message" => $error? "Failed to update": "Update to cart success",
            "qty" => $carts[$id]['qty'],
            "total_pints" => $total_pints,
            "subtotal" => "SGD " . number_format($subtotal, 2),
            "grand_total_pints" => number_format($grand_total_pints, 0),
            "grand_total" => 'SGD ' . number_format($grand_total, 2),
        ]);
    }

    public function decreaseQty(Request $request, $id){

        if ($request->session()->has('carts')) {
            $carts = $request->session()->get('carts');
        }else{
            $carts = array();
        }

        if(isset($carts[$id])){
            $carts[$id]['qty']--;
            $request->session()->put('carts', $carts);
            $error = false;
        }else{
            $error = true;
        }

        $pint = \App\Pint::find($id);

        $total_pints = (int)$carts[$id]['qty'] * $pint->pint;
        $subtotal = (int)$carts[$id]['qty'] * $pint->price;

        $grand_total_pints = 0;
        $grand_total = 0;
        
        foreach($carts as $pint_id=>$cart){
            $pint = \App\Pint::find($pint_id);

            $grand_total_pints += (int)$cart['qty'] * $pint->pint;
            $grand_total += (int)$cart['qty'] * $pint->price;
        }

        
        $discount = 0;
        $ses_promo_code = $request->session()->get("promo_code");
        if($ses_promo_code){
            $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $ses_promo_code['code'])->first();
            if($promo_code){
                $discount = $promo_code->discount;
            }
        }

        $grand_total = ($grand_total-$discount);

        return response()->json([
            "message" => $error? "Failed to update": "Update to cart success",
            "qty" => $carts[$id]['qty'],
            "total_pints" => $total_pints,
            "subtotal" => "SGD " . number_format($subtotal, 2),
            "grand_total_pints" => number_format($grand_total_pints, 0),
            "grand_total" => 'SGD ' . number_format($grand_total, 2),
        ]);
    }

    public function storePromoCode(Request $request){

        $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $request->code)->first();
        if($promo_code){
            $ses_promo_code = $request->session()->get("promo_code");
            $ses_promo_code = [
                "code" => $promo_code->code
            ];
            $request->session()->put("promo_code", $ses_promo_code);

            $data_carts = $request->session()->get("carts");

            $grand_total = 0;

            foreach($data_carts as $pint_id=>$cart){
                $pint = \App\Pint::find($pint_id);
                $grand_total += (int)$cart['qty'] * $pint->price;
            }

            $grand_total = ($grand_total-$promo_code->discount);

            return response()->json([
                "message" => "Promo Code applied successfully",
                "discount" => 'SGD ' . number_format($promo_code->discount, 2),
                "promo_code_applied" => 1,
                "grand_total" => 'SGD ' . number_format($grand_total, 2),
            ]);
        }else{
            return response()->json([
                "message" => "Promo Code not found"
            ], 422);
        }
    }

    public function destroyPromoCode(Request $request){

        $request->session()->pull("promo_code");

        $data_carts = $request->session()->get("carts");

        $grand_total = 0;

        foreach($data_carts as $pint_id=>$cart){
            $pint = \App\Pint::find($pint_id);
            $grand_total += (int)$cart['qty'] * $pint->price;
        }

        return response()->json([
            "message" => "Promo Code deleted successfully",
            "discount" => 'SGD ' . number_format(0, 2),
            "promo_code_applied" => 0,
            "grand_total" => 'SGD ' . number_format($grand_total, 2),
        ]);
    }

    public function destroy(Request $request, $id){

        if ($request->session()->has('carts')) {
            $carts = $request->session()->get('carts');
        }else{
            $carts = array();
        }

        if(isset($carts[$id])){
            unset($carts[$id]); 
            $request->session()->put('carts', $carts);
            $error = false;
        }else{
            $error = true;
        }

        $grand_total_pints = 0;
        $grand_total = 0;
        
        foreach($carts as $pint_id=>$cart){
            $pint = \App\Pint::find($pint_id);

            $grand_total_pints += (int)$cart['qty'] * $pint->pint;
            $grand_total += (int)$cart['qty'] * $pint->price;
        }

        $discount = 0;
        $ses_promo_code = $request->session()->get("promo_code");
        if($ses_promo_code){
            $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $ses_promo_code['code'])->first();
            if($promo_code){
                $discount = $promo_code->discount;
            }
        }

        $grand_total = ($grand_total-$discount);

        $is_cart_empty = false;
        if(!$request->session()->get("carts")){
           $is_cart_empty = true; 
        }
        
        return response()->json([
            "error" => $error,
            "message" => $error? "Failed to delete item": "Item removed",
            "grand_total_pints" => number_format($grand_total_pints, 0),
            "grand_total" => 'SGD ' . number_format($grand_total, 2),
            "is_cart_empty" => $is_cart_empty
        ]);
    }

    // call by index > /confirmation to bypass add to cart page
    public function defaultCart(Request $request){
        $request->session()->forget('carts');
        $carts = array();
        $id = 2;
        $carts[$id] = array(
            "product" => 2,
            "pint" => null,
            "qty" => 1
        );
        $request->session()->put('carts', $carts);
    }

    public function thankyou(){
        return view('web_purchase.thankyou');
    }
}
