<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Mail;

class ContactController extends BaseController
{
    public function index(Request $request){
        $this->validate($request, [
            'enquiry' => 'required|max:255',
            'name' => 'required|max:255',
            'mobile' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required',
            'country' => 'required',
        ]);

        $country_info = \App\Country::find($request->country);
        
        $data = [
            'enquiry' => $request->enquiry,
            'name' => $request->name,
            'mobile' => $country_info->calling_code . $request->mobile,
            'email' => $request->email,
            'message' => $request->message,
            'ip' => request()->ip(),
            'recipientAlias' => 'Admin Enquiries',
        ];
        $sendEmail = Mail::send('emails.contact', ['data' => $data], function ($m) use ($data) {
            $m->from('no-reply@mail.cprv-sptestserver.com', 'Contact Page Bot');
            $m->to('enquiries@apb.com.sg', 'Admin Enquiries')->subject('Supportourfnb Website Enquiry');
            //$m->to('adit.herdityo@gmail.com', 'Admin Enquiries')->subject('Supportourfnb Website Enquiry');
            $m->bcc(['cprv.notifications@gmail.com','zack@cprvision.com','hazel@cprvision.com']);
            //$m->cc(['marcus@cprvision.com']);
        });

        return array(
            'error' => $sendEmail ? false : true,
        );
        
    }
}
