<?php

namespace App\Http\Controllers\Voucher;

use App\Http\Controllers\Voucher\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Hash;

use Auth;

class AuthController extends BaseController
{
    public function index(Request $request, $_4_last_digit_mobile, $voucher_id){

        $has_password = false;

        $voucher_info = \App\Voucher::where('voucher_id', $voucher_id)->first();
        if($voucher_info->user->password){
            $has_password = true;
        }

        return view('voucher.login',[
            "user_name" => $voucher_info->user->first_name,
            "has_password" => $has_password
        ]);
    }

    public function store(Request $request, $code, $voucher_id){
        $voucher_info = \App\Voucher::where('voucher_id', $voucher_id)->first();

        $password = $request->pin;

        if(Auth::guard('customer')->attempt([
            'id'=> $voucher_info->user_id,
            'password' => $password,
            'status' => 1]) 
            //'type' => 'customer'])
        ) {
            return response()->json([
                "redirect" => url("voucher/" . $code . '/' . $voucher_id)
            ]);
        }else{
            return response()->json([
                "message" => "User not found!"
            ], 422);
        }
    }

    public function setPinStore(Request $request, $code, $voucher_id){
        $voucher_info = \App\Voucher::where('voucher_id', $voucher_id)->first();
        $user_info = \App\User::find($voucher_info->user->id);
        $user_info->password = Hash::make($request->pin);
        $user_info->status = 1;
        $user_info->save();

        Auth::guard('customer')->login($user_info);
        
        return response()->json([
            "redirect" => url("voucher/" . $code . '/' . $voucher_id)
        ]);
    }

    public function logout(Request $request, $code, $voucher_id){
        Auth::guard('customer')->logout();
        
        return redirect("voucher/" . $code . '/' . $voucher_id);
    }

}
