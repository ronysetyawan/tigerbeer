<?php

namespace App\Http\Controllers\Voucher;

use App\Http\Controllers\Voucher\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Auth;

use Mail;

use Storage;

class PrizeController extends BaseController
{
    public function index(Request $request){

        $user_info = Auth::guard("customer")->user();

        $user_points = $user_info->total_points;

        $data_prizes = \App\Prize::where("status", 1)->orderBy("sort_order", "asc")->where("qty", ">=", 1)->get();

        $prizes = [];

        foreach($data_prizes as $dt){
            
            if($user_points >= $dt->point_needed){
                $more_point_needed = 0;
            }else{
                $more_point_needed = $dt->point_needed - $user_points;
            }

            $prizes[] = array(
                "id" => $dt->id,
                "image" => url('files/prize/' . $dt->image),
                "title1" => $dt->name,
                "point_needed" => $dt->point_needed,
                "more_point_needed" => $more_point_needed,
                "type" => $dt->type
            );
        }

        $histories = [];

        $data_point_redemptions = DB::table(DB::raw("(
                SELECT
                pr.prize_image AS image,
                pr.prize_name AS title,
                1 AS redeemed,
                pr.redemption_date as date,
                'shipping-product' as `type`
                FROM
                customer_prizes AS co
                INNER JOIN point_redemptions AS pr ON co.point_redemption_id = pr.id
                WHERE
                pr.user_id = " . $user_info->id . "
                
                UNION
                
                SELECT
                pc.image AS image,
                pc.`code` AS title,
                pc.used AS redeemed,
                pc.created_at as date,
                'promocode' as `type`
                FROM
                promo_codes AS pc
                WHERE
                pc.user_id = " . $user_info->id . "
            ) as tbl"))->get();

        foreach($data_point_redemptions as $dt){
            $redeemed = true;
            if($dt->redeemed==0){
                $redeemed = false;
            }

            $y = date("Y", strtotime($dt->date));
            $m = date("m", strtotime($dt->date));

            $histories[] = [
                "title" => $dt->title,
                "image" => url('files/prize/deal/' . $y . '/' . $m . '/' . $dt->image),
                "redeemed" => $redeemed,
                "type" => $dt->type,
                "purchase_link" => $dt->type=='promocode'?(url("create") . "/?promocode=" . $dt->title):''
            ];
        }

        $countries = array();

        $data_countries = \App\Country::get();
        if($data_countries){
            foreach($data_countries as $dt_country){
                $countries[] = array(
                    "country_id" => $dt_country->id,
                    "country_name" => $dt_country->country_name,
                    "calling_code" => $dt_country->calling_code
                );
            }
        }

        $_user_info = [
            "address_1" => $user_info->address_1,
            "address_2" => $user_info->address_2,
            "postal_code" => $user_info->postal_code,
            "country" => $user_info->country_id,
            "mobile" => $user_info->mobile,
            "email" => $user_info->email
        ];

        return view('voucher.prize',[
            "user_points" => $user_points,
            "prizes" => $prizes,
            "histories" => $histories,
            "countries" => $countries,
            "user_info" => $_user_info
        ]);
    }

    public function store(Request $request){

        $user_info = Auth::guard("customer")->user();

        $user_points = $user_info->total_points;

        $prize_info = \App\Prize::where("status", 1)->where("id", $request->prize_id)->first();

        if(!$prize_info){
            return response()->json([
                "message" => "Prize not found"
            ], 422);
        }

        if($prize_info->qty<=0){
            return response()->json([
                "message" => "Prize not available anymore"
            ], 422);
        }

        if($user_points<$prize_info->point_needed){
            return response()->json([
                "message" => "Point is not enough"
            ], 422);
        }

        $current_date = date("Y-m-d H:i:s");

        $point_redemption = new \App\PointRedemption;
        $point_redemption->user_id = $user_info->id;
        $point_redemption->prize_id = $prize_info->id;
        $point_redemption->prize_name = $prize_info->name;
        $point_redemption->point = $prize_info->point_needed;
        $point_redemption->redemption_date = $current_date;
        $point_redemption->save();

        /// copy image
        $y = date("Y", strtotime($current_date));
        $m = date("m", strtotime($current_date));

        $images_prize_array = explode(".",  $prize_info->image);
        $ext = $images_prize_array[count($images_prize_array)-1];
        $new_prize_image_name = md5(date("YmdHis")) . '-' . $point_redemption->id . '.' . $ext;
        Storage::copy('public/prize/' . $prize_info->image, 'public/prize/deal/' . $y . '/' . $m . '/' . $new_prize_image_name);
        $point_redemption->prize_image = $new_prize_image_name;
        $point_redemption->save();
        ////////// 

        if($prize_info->type=="promocode"){
           
            $promo_code = new \App\PromoCode;

            $string_limit = 5;
            $md5 = md5(time()); 
            $code = substr($md5, strlen($md5)-$string_limit, $string_limit);
            
            $promo_code->user_id = $user_info->id;
            $promo_code->point_redemption_id = $point_redemption->id;
            $promo_code->code = $code;
            $promo_code->discount = $prize_info->value;
            $promo_code->image = $point_redemption->prize_image;
            $promo_code->status = 1;
            $promo_code->type = "redemption";

            $promo_code->save();

            $user_info->total_points = ($user_info->total_points - $prize_info->point_needed);
            
            $user_info->save();

            $prize_info->qty = ($prize_info->qty - 1);
            $prize_info->save();

            $data_promocodes = \App\PromoCode::whereHas('point_redemption', function($q) use($user_info) {
                $q->where("user_id", $user_info->id);
            })->where("used", 0)->get();
    
            $promo_codes = [];
    
            foreach($data_promocodes as $dt_promocode){
                $promo_codes[] = [
                    "image" => url('files/prize/' . $dt_promocode->point_redemption->prize->image),
                    "code" => $dt_promocode->code,
                ];
            }    

            return response()->json([
                "total_point" => $user_info->total_points,
                "promo_codes" => $promo_codes,
                "purchase_link" => url("create") . "/?promocode=" . $promo_code->code,
                "prize_promocode" => $promo_code->code
            ]);

        }else if($prize_info->type=="shipping-product"){

            $country_info = \App\Country::find($request->country);

            $customer_prize = new \App\CustomerPrize;
            $customer_prize->point_redemption_id = $point_redemption->id;
            $customer_prize->address_1 = $request->address_1;
            $customer_prize->address_2 = $request->address_2;
            $customer_prize->postal_code = $request->postal_code;
            $customer_prize->country_id = $country_info->id;
            $customer_prize->country_code_2 = $country_info->country_code_2;
            $customer_prize->country_code_3 = $country_info->country_code_3;
            $customer_prize->country = $country_info->country_name;
            $customer_prize->calling_code = $country_info->calling_code;
            $customer_prize->mobile = $request->mobile;
            $customer_prize->email = $request->email;

            $customer_prize->save();

            $user_info->total_points = ($user_info->total_points - $prize_info->point_needed);
            
            $user_info->save();

            $prize_info->qty = ($prize_info->qty - 1);
            $prize_info->save();


            //// send email ///
            $email = $user_info->email;
            $name = $user_info->first_name;
            $email_from = 'Erdinger Oktoberfest';
            $email_subject = 'Your prize has been successfully issued!';
            $email_tpl = 'emails.confirmation_delivery';

            $string_token = \Firebase\JWT\JWT::encode(array(
                "redemption_id" => $point_redemption->id
            ), "email-preview-delivery-oct-fest");

            $addresses = [];
            if($customer_prize->address_1){
                $addresses[] = $customer_prize->address_1;
            }
            if($customer_prize->address_2){
                $addresses[] = $customer_prize->address_2;
            }
            if($customer_prize->postal_code){
                $addresses[] = $customer_prize->postal_code;
            }

            $data_email = array(
                "string_token" => $string_token,
                'image' => asset('assets/web-purchase/image/october-fest.jpg'),
                'name' => $user_info->first_name,
                'prize_id' => $point_redemption->id,
                'prize' => "1 x " . $point_redemption->prize_name,
                'delivery_address' => implode("<br>", $addresses)
            );

            $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
                $m->from('no-reply@apb.giv.one', $email_from);
                $m->bcc(['cprv.notifications@gmail.com']);
                $m->to($email, $name)->subject($email_subject);
            });
            ////

            return response()->json([
                "total_point" => $user_info->total_points
            ]);
        }
    }
}
