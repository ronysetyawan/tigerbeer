<?php
 
namespace App\Http\Controllers\Voucher;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\Product;

class HomeController extends BaseController
{
    // public function index(Request $request){
    //     //$user_info = Auth::guard("customer")->user();

    //     $code = $request->code;
    //     $vid = $request->voucher_id;

    //     $vouchers = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->where("voided", 0)->with('order')->get();
    //     $voucher_info = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->first();
    //     $user_info = \App\User::where("id",$voucher_info->user_id)->first();

    //     return view('voucher.home',[
    //         "vouchers" => $vouchers,
    //         "grab_code" => $code,
    //         "share_url" => url("/") . "/?ref_id=" . $user_info->id,
    //         "balance" => $voucher_info->balance,
    //         "total_points" => $user_info->total_points,
    //         "user_subscribe" => $user_info->subscribe,
    //         "user_subscribe_first_time" => $user_info->subscribe_first_time
    //     ]);
    // }
    public function index(Request $request){
        $code = $request->code;
        $vid = $request->voucher_id;
        $products = Product::paginate(2);
        $product2 = Product::all();
        $vouchers = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->where("voided", 0)->with('order')->get();
        $voucher_info = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->first();
        $user_info = \App\User::where("id",$voucher_info->user_id)->first();

        // new product
        $product_details = \App\Voucher::join('order_items AS a','a.order_id','=','vouchers.order_id')
                    ->join('product_redemptions AS b','item_id','=','b.id')
                    ->where("code", $code)
                    ->where("voucher_id", $vid)
                    ->first();

        $data_return = array(
            "name" => $user_info->first_name,
            "vouchers" => $vouchers,
            "balance" => $voucher_info->balance,
            "total_pints" => $voucher_info->value,
            "code" => $code,
            "vid" => $vid,
            "products" => $products,
            "product2" => $product2,
            "product_details" => $product_details
            // "product_id" => $request->session()->put('product_id', $request->product_id),
        );

        return view('voucher.home_new', $data_return); 
    }
    public function welcome(Request $request){

        $code = $request->code;
        $vid = $request->voucher_id;

        $vouchers = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->where("voided", 0)->with('order')->get();
        $voucher_info = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->first();
        $user_info = \App\User::where("id",$voucher_info->user_id)->first();

        $data_return = array(
            "name" => $user_info->first_name,
            "vouchers" => $vouchers,
            "date" => date("d/m/Y",strtotime($voucher_info->created_at)),
            "balance" => $voucher_info->balance,
            "total_pints" => $voucher_info->value,
        );

        if ($voucher_info->type == 'f') {
            return view('voucher.notify', $data_return); 
        }

        return view('voucher.welcome', $data_return); 
    }

    public function redirectNotify(Request $request){

        $code = $request->code;
        $vid = $request->voucher_id;

        $vouchers = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->where("voided", 0)->with('order')->get();
        $voucher_info = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->first();

        $data_return = array(
            "name" => $voucher_info->owner_name,
            "date" => date("d/m/Y",strtotime($voucher_info->created_at)),
            "balance" => $voucher_info->balance
        );

        return view('voucher.notify', $data_return); 
    }

}
