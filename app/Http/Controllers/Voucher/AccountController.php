<?php

namespace App\Http\Controllers\Voucher;

use App\Http\Controllers\Voucher\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Auth;

use Route;

use Hash;

class AccountController extends BaseController
{
    public function index(Request $request){

        $user_info = Auth::guard("customer")->user();

        $countries = array();

        $data_countries = \App\Country::get();
        if($data_countries){
            foreach($data_countries as $dt_country){
                $countries[] = array(
                    "country_id" => $dt_country->id,
                    "country_name" => $dt_country->country_name,
                    "calling_code" => $dt_country->calling_code
                );
            }
        }

        $data_dobs = explode("-", $user_info->dob);
        $dobs = [
            "year" => $data_dobs[0],
            "month" => $data_dobs[1],
            "date" => $data_dobs[2]
        ];

        return view('voucher.account', [
            "user_info" => [
                "name" => $user_info->first_name,
                "gender" => $user_info->gender,
                "dobs" => $dobs,
                "country" => (int) $user_info->country_id,
                "mobile" => $user_info->mobile,
                "email" => $user_info->email,
                "address_1" => $user_info->address_1,
                "address_2" => $user_info->address_2,
                "postal_code" => $user_info->postal_code,
                "subscribe" => $user_info->subscribe
            ],
            "countries" => $countries
        ]);
    }

    public function store(Request $request){
        $user = Auth::guard("customer")->user();
        $user->first_name = $request->name;
        $user->dob = $request->dob;
        $user->gender = $request->gender;
        $user->email = $request->email;
        $user->address_1 = $request->address_1;
        $user->address_2 = $request->address_2;
        $user->postal_code = $request->postal_code;
        $user->subscribe = $request->subscribe;
        $user->save();
    }

    public function destroy(Request $request){
        
        $user = Auth::guard('customer')->user();

        if(Hash::check($request->pin, $user->password)) {
            $user->deleted = 1;
            $user->save();

            Auth::guard('customer')->logout();
        }else{
            return response()->json([
                "message" => "User not found"
            ], 422);
        }
    }

    public function subscribeUpdate(Request $request){
        $user = Auth::guard("customer")->user();

        $subscribe_first_time = false;

        if($user->subscribe_first_time==0 && $request->status==1){
            // first
            // add points

            $point_subscribe_info = \App\Setting::where('type', 'global')->where('key', 'point_subscribe')->first();
            $point_subscribe = $point_subscribe_info->value;

            $point = new \App\Point;
            $point->user_id = $user->id;
            $point->order_id = 0;
            $point->type = "subscribe";
            $point->point = $point_subscribe;
            $point->save();

            $user->subscribe_first_time = 1;

            $subscribe_first_time = true;

            $user->total_points = ($user->total_points + $point->point);
            $user->save();
        }

        $user->subscribe = $request->status;
        $user->save();
        
        return response()->json([
            "total_points" => $user->total_points,
            "subscribe_first_time" => $subscribe_first_time
        ]);

    }
}
