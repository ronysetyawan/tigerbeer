<?php

namespace App\Http\Controllers\Voucher;

use App\Http\Controllers\Voucher\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

class OutletController extends BaseController
{
    public function index(Request $request){
        $data_properties = \App\Property::where('status',1)->get();
        
        $properties = [];

        if($data_properties){
            foreach($data_properties as $dt_property){
                if(!isset($properties[$dt_property->zone])){
                    $properties[$dt_property->zone] = [];
                }
                $properties[$dt_property->zone][] = [
                    "name" => $dt_property->name,
                    "address1" => $dt_property->address1,
                    "address2" => $dt_property->address2
                ];
            }
        }

        return view('voucher.outlet', [
            "properties" => $properties
        ]);
    }

}
