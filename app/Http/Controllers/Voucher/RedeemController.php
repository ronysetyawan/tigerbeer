<?php

namespace App\Http\Controllers\Voucher;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;
 
use DB;

use Auth;

class RedeemController extends BaseController
{
    public function index(Request $request){
        // $request->session()->forget('product_id');
        // $user_info = Auth::guard("customer")->user();
        $code = $request->code;
        $vid = $request->voucher_id;
        $product_name = $request->product_name;
        $request->session()->put('product_id', $request->product_id);

        // $voucher_info = \App\Voucher::where("user_id", $user_info->id)->where("voucher_id",$vid)->first();
        $voucher_info = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->first();
        $user_info = \App\User::where("id",$voucher_info->user_id)->first();

        return view('voucher.redeem_new', [
            "type" => $voucher_info->type, 
            "voucher" => $voucher_info,
            "vid" => $vid,
            "total_pint" => $voucher_info->balance,
            "product_name" => $product_name,
            "cancel" => "/voucher"."/".$code."/".$vid,
        ]);
    }

    public function store(Request $request, $code, $voucher_id){

        $code = $request->code;
        $vid = $request->vid;

        // $product_id = $request->$product_id;

        $property_info = \App\Property::where("pin", $request->property_pin)->where("status", 1)->first();

        if(!$property_info){
            return response()->json(array(
                "message" => "Property is not found!"
            ), 422);
        }else{

            // $user_info = Auth::guard("customer")->user();

            //$voucher_info = \App\Voucher::where("user_id", $user_info->id)->first();
            // $voucher_info = \App\Voucher::where("user_id", $user_info->id)->where("voucher_id", $vid)->first();
            $voucher_info = \App\Voucher::where("code", $code)->where("voucher_id", $vid)->first();
            $user_info = \App\User::where("id",$voucher_info->user_id)->first();

            if($voucher_info->balance<$request->pint){
                return response()->json([
                    "message" => "Pint is not enough"
                ], 422);
            }else{
                $voucher_redeem = new \App\VoucherRedeem;
                //$voucher_redeem->voucher_id = $voucher_id;
                $voucher_redeem->voucher_id = $vid;
                $voucher_redeem->product_id = $request->session()->get('product_id');
                $voucher_redeem->receipt = "";  
                $voucher_redeem->value = $request->pint;
                $voucher_redeem->property_id = $property_info->id;
                $voucher_redeem->property = $property_info->name;
                $voucher_redeem->redeem_date = date("Y-m-d H:i:s");
                $voucher_redeem->save();

                $voucher_info->balance = ($voucher_info->balance - $request->pint);
                $voucher_info->save();
                // $request->session()->forget('product_id');

                $voucher_res = '';

                return response()->json([
                    "balance" => $voucher_info->balance
                ]);
            }

        }
    }
}
