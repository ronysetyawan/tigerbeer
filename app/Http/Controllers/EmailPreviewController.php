<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class EmailPreviewController extends Controller
{
    public function index($token){

        try{
            $tokens = \Firebase\JWT\JWT::decode($token, "email-preview-purchase-oct-fest", array('HS256'));

            $order_id = $tokens->order_id;
           
            $order_info = \App\Order::find($order_id);

            $voucher_info = \App\Voucher::where("user_id", $order_info->user->id)->first();

            $order_items = \App\OrderItem::where('order_id', $order_id)->get();
            $items = [];
            foreach($order_items as $order_item){
                $items[] = [
                    'voucher_id' => $voucher_info->voucher_id,
                    'entitlement' => $order_item->qty . " of " . $order_item->item_name,
                    'qty' => $order_item->qty,
                    'total' => "SGD " . number_format($order_item->total, 2),
                    'expired_date' => '9 December 2018',
                ];
            }

            $data = array(
                'image' => asset('assets/web-purchase/image/october-fest.jpg'),
                'name' => $voucher_info->user->first_name,
                'items' => $items
            );

            return view('web_purchase.email_preview.purchase', $data);
        }catch (\Firebase\JWT\SignatureInvalidException $e){
            abort(404);
        }
    }

    public function delivery($token){

        try{
            $tokens = \Firebase\JWT\JWT::decode($token, "email-preview-delivery-oct-fest", array('HS256'));

            $redemption_id = $tokens->redemption_id;

            $redemption_info = \App\PointRedemption::find($redemption_id); 

            $customer_prize = \App\CustomerPrize::where('point_redemption_id', $redemption_id)->first();
           
            $user_info = \App\User::find($redemption_info->user_id);

            $addresses = [];
            if($customer_prize->address_1){
                $addresses[] = $customer_prize->address_1;
            }
            if($customer_prize->address_2){
                $addresses[] = $customer_prize->address_2;
            }
            if($customer_prize->postal_code){
                $addresses[] = $customer_prize->postal_code;
            }

            $data = array(
                'name' => $user_info->first_name,
                'prize_id' => $redemption_id,
                'prize' => "1 x " . $redemption_info->prize_name,
                'delivery_address' => implode("<br>", $addresses)
            );

            return view('web_purchase.email_preview.delivery', $data);

        }catch (\Firebase\JWT\SignatureInvalidException $e){
            abort(404);
        }
    }
}
