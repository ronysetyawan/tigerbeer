<?php
namespace App\PaymentGateway;

class Reddot {

    protected $app;

    public $url;
    protected $secret_key;
    protected $fields = array();
    protected $isLive;
    public $signature;


    public function __construct($secret_key, $fields_ = array(), $isLive = false)
    {
        $this->secret_key = $secret_key;

        if($fields_){
            foreach($fields_ as $key=>$value){
                $this->fields[$key] = $value;
            }
        }

        if ($isLive) {
            $this->url = "https://connect.reddotpayment.com/merchant/cgi-bin-live";
        } else {
            $this->url = "https://test.reddotpayment.com/connect-api/cgi-bin-live";
        }

        $fields = $this->fields;
        
        $array_keys_fields = array_keys($fields);
        sort($array_keys_fields);

        $fields_array = array();

        foreach($array_keys_fields as $key){
            $value = $fields[$key];
            $fields_array[] = $key . "=" . $value;
        }

        $fields_array[] = 'secret_key=' . $this->secret_key;

        $fields_string = implode("&", $fields_array);

        $this->signature = md5($fields_string);
    }

    public function getFields(){
        return $this->fields;
    }
    
    public static function checkingResponseRDP($secret_key){
        if(!isset($_GET['signature'])){
            return false;
        }else{
            $rdp_response  = $_GET;
            $rdp_signature = $_GET['signature'];
            unset($rdp_response ['signature']);
            ksort($rdp_response);
            $string_to_hash = ''; 
            foreach ($rdp_response as $key=>$value) {
                $string_to_hash .= $key. '=' . ($value) . '&';
            }
            $string_to_hash .= 'secret_key=' . $secret_key;
            $merchant_calculated_signature = md5($string_to_hash);

            //comparison
            $is_really_from_rdp = ($merchant_calculated_signature == $rdp_signature);
            return $is_really_from_rdp;
        }
    }
}
