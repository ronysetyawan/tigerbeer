<?php
namespace App\PaymentGateway;

class ReddotPaymentSignature
{
    /**
     * fieldsSign - defined variable fieldSign
     * @var array
     */
    protected $fieldsSign;
    /**
     * SecretKey - defined variable SecretKey
     * @var string
     */
    protected $secretKey;
    /**
     * __construct - required params for this class
     * @param  mixed $secretKey
     * @param  mixed $fieldSign
     * @return void
     */
    public function __construct($secretKey, $fieldsSign)
    {
        $this->secretKey = $secretKey;
        $this->fieldsSign = $fieldsSign;
    }

    /**
     * generateSignature
     * @return string - signature
     */
    public function generateSignature()
    {
        $fieldsSignString = $this->setFieldSigin($this->fieldsSign);
        $fieldsSignString .= $this->secretKey;

        return $this->signatureHash($fieldsSignString);
    }
    /**
     * setFieldSigin
     * set field tarray to sting for signature properties
     * @param  mixed $fields
     * @return string
     */
    protected function setFieldSigin($fields)
    {
        $result = "";
        $fieldsSign = ['mid', 'order_id', 'payment_type', 'amount', 'ccy'];
        foreach ($fieldsSign as $field) {
            $result .= trim($fields[$field]);
        }
        return $result;
    }
    /**
     * signatureHash
     * @param  mixed $fieldSignString
     * @return string
     */
    protected function signatureHash($fieldSignString)
    {
        return  hash('sha512', $fieldSignString);
    }

    /**
     * signGeneric
     * @return string
     */
    public function signGeneric()
    {
        $fieldSign = "";
        unset($this->fieldsSign['signature']);
        $this->recursiveGenericArraySign($this->fieldsSign, $fieldSign);
        $fieldSign .= $this->secretKey;
        return $this->signatureHash($fieldSign);
    }

    /**
     * recursiveGenericArraySign
     * @param  mixed $params
     * @param  mixed $fieldSign
     * @return void
     */
    public function recursiveGenericArraySign(&$params, &$fieldSign)
    {
        //	sort the parameters based on its key
        ksort($params);
        //	Traverse through each component
        //	And generate the concatenated string to sign
        foreach ($params as $v) {
            if (is_array($v)) {
                //	in case of array traverse inside
                //	and build further the string to sign
                $this->recursiveGenericArraySign($v, $fieldSign);
            } else {
                //	Not an array means this is a key=>value map,
                //	Concatenate the value to data to sign
                $fieldSign .= $v;
            }
        }
    }
}
