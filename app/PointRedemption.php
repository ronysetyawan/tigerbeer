<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointRedemption extends Model
{
    public function prize(){
        return $this->belongsTo('App\Prize');
    }
    public function customer_prize(){
        return $this->hasOne('App\CustomerPrize');
    }
    public function promo_code(){
        return $this->hasOne('App\PromoCode');
    }
}
