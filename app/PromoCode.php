<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    public function point_redemption(){
        return $this->belongsTo('App\PointRedemption');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
