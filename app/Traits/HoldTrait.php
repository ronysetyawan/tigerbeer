<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Hold;
use Carbon\Carbon;

trait HoldTrait
{
    public static function releaseHold()
    {
        $holds = \App\Hold::where("status", "on")->where("end_at", '<', Carbon::now())->get();
        foreach($holds as $hold){
            $qty = isset($hold->qty) ? $hold->qty : 1;
            HoldTrait::cancelProductHold($hold->product_id, $qty);
        }
        \App\Hold::where("status", "on")->where("end_at", '<', Carbon::now())->update(['status' => 'off']);
    }
    
    public static function addHold($data)
    {
        $hold = new \App\Hold;
        $hold->order_id = $data['order_id'];
        $hold->product_id = $data['product_id'];
        $hold->qty = $data['qty'];
        $hold->status = 'on';
        $hold->end_at = Carbon::now()->addMinutes('5');
        $hold->save();

        HoldTrait::addProductHold($data['product_id'], $data['qty']);
    }

    public static function updateHoldStatusbyOrderID($order_id, $status)
    {
        $holds = \App\Hold::where("status", "on")->where("order_id", $order_id)->get();
        foreach($holds as $hold){
            $qty = isset($hold->qty) ? $hold->qty : 1;
            HoldTrait::cancelProductHold($hold->product_id, $qty);
            if($status == 'done'){
                HoldTrait::addProductSold($hold->product_id, $qty);
            }
        }
        \App\Hold::where("status", "on")->where("order_id", $order_id)->update(['status' => $status]);
    }

    public static function getHoldStatusbyOrderID($order_id)
    {
        $hold = \App\Hold::where("order_id", $order_id)->get();
        return $hold;
    }

    public static function addProductHold($product_id, $qty)
    {
        $product = \App\Product::find($product_id);
        if($product){
            $product->hold = $product->hold + $qty;
            $product->save();
        }
    }

    public static function cancelProductHold($product_id, $qty)
    {
        $product = \App\Product::find($product_id);
        if($product){
            $product->hold = $product->hold - $qty;
            $product->save();
        }
    }

    public static function addProductSold($product_id, $qty)
    {
        $product = \App\Product::find($product_id);
        if($product){
            $product->sold = $product->sold + $qty;
            $product->save();
        }
    }

}