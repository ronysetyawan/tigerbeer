<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('\App\Libraries\Sms', function ($app) {
            $api_key = env('NEXMO_KEY');
            $api_secret = env('NEXMO_SECRET');

            return new \App\Libraries\Sms($api_key, $api_secret);
        });
    }
}
