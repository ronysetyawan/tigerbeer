<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;


class FutureNotif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'future-notif';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email  and SMS Notification for Voucher with future send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //check SMS setting
        $send_sms = \App\Setting::where("key", 'send_sms')->first();
        $allowSendSMS = filter_var($send_sms->value, FILTER_VALIDATE_BOOLEAN);
        if($allowSendSMS){
            $nexmo = \App\Setting::where("key", 'nexmo')->first();
            $nexmo_data = unserialize($nexmo->value);
        }
        $vouchers = \App\Voucher::where("sent", 0)->get();
        echo "<pre>";
        foreach($vouchers as $key => $voucher){
            //echo $voucher->voucher_id." - ". $voucher->order_item_recipient_id;
            $order_item_recipient = \App\OrderItemRecipient::find($voucher->order_item_recipient_id);
            echo $order_item_recipient->send_type;
            echo "<br/>";
            if(($order_item_recipient->send_type == 'future-send') && (int)strtotime($order_item_recipient->date_send) <= (int)strtotime("now")){
                echo "run!";
                $order_info = \App\Order::find($voucher->order_id);
                $order_item = \App\OrderItem::find($voucher->order_item_id);

                //send SMS
                if($allowSendSMS){
                    try {
                        $text_message = \App\Voucher::getSmsText($voucher->voucher_id);
                        //// send sms
                        $api_key = env('NEXMO_KEY');
                        $api_secret = env('NEXMO_SECRET');
                        $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));  
                        $message = $client->message()->send([
                            'to' => $voucher->owner_mobile,
                            'from' => 'eGifting',
                            'text' => $text_message
                        ]);
                        if($message){
                            //echo "SMS sent!";
                            $result['sms'] = true;
                            $result['message_text'] = $text_message;
                        }else{
                            //echo "Sorry SMS is failed!";
                            $result['message'] = false;
                            $result['message_text'] = $text_message;
                            $result['message_reason'] = "Sorry SMS is failed!";
                        }
                    }catch(Exception $e) {
                        //echo 'Message: ' .$e->getMessage();
                    }
                }else{
                    $result['sms'] = null;
                }
                //end SMS


                //send email
                $email = $voucher->owner_email;
                $name = $voucher->owner_first_name;

                $data_email = array(
                    'first_name_rec' => $voucher->owner_first_name,
                    'last_name_rec' => $voucher->owner_last_name,
                    'owner_email' => $voucher->owner_email,
                    'first_name' => $order_info->first_name,
                    'last_name' => $order_info->last_name,
                    'purchaser_email' => $order_info->email,
                    'mobile' => $voucher->owner_mobile,
                    'view_desktop_url' => url('/email-preview/' . \Firebase\JWT\JWT::encode(array('voucher_id'=>$voucher->voucher_id), env('JWT_KEY_VOUCHER_BROWSER'))),
                    'voucher_id' => $voucher->voucher_id,
                    'product' => $order_item->item_name,
                    // 'image' => $order_item->item_image,
                    'image' => url('files/occasion-deal/'.$voucher->voucher_id),
                    'currency' => $order_info->currency,
                    'amount' => $order_item->amount,
                    'qty'=> $order_item->qty,
                    'expiry_date' => date('j M, Y', strtotime($voucher->expiry_date)),
                    'voucher_link' => isset($data_shortlink) ? $data_shortlink->shorturl : null,
                    'term_url' => null,
                    'style_width' => isset($data_shortlink) ? '25%' : '33%',
                    'message_text' => isset($order_item_recipient->message)? $order_item_recipient->message : ''

                );

                
                $sendEmail = Mail::send('emails.voucher', $data_email, function ($m) use ($email, $name) {
                    $m->from('no-reply@mail.cprv-sptestserver.com', 'eGift Card');
                    $m->to($email, $name)->subject('Order Success!');
                });

                if($sendEmail){
                    //log report sent
                    $voucher->sent = 1;
                    $voucher->save();

                    $result['email'] = true;
                    $result['result'] = $data_email;
                }
                //print_r($data_email);
        }else{
                echo "steady";	
            }
        }
    }
}
