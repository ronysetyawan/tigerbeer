<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

use Session;

use Storage;
use Mail;
use DB;

use App\Traits\HoldTrait;

class Order extends Model
{
    use HoldTrait;

    public function branch(){
        return $this->belongsTo('App\Branch');
    }

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function order_item(){
        return $this->hasMany('App\OrderItem');
    }

    public static function confirmOrder(){

        $customer = Session::get('customer');

        //$carts = Session::get('carts');

        $current_date = date("Y-m-d H:i:s");

        $order = new \App\Order;

        $country_info = \App\Country::find($customer['country']);

        $order->name = $customer['name'];
        $order->gender = $customer['gender'];
        $order->email = $customer['email'];
        $order->mobile = $customer['mobile'];
        $order->dob = $customer['dob'];
        $order->country_id = $country_info->id;
        $order->country_code_2 = $country_info->country_code_2;
        $order->country_code_3 = $country_info->country_code_3;
        $order->country = $country_info->country_name;
        $order->calling_code = $country_info->calling_code;
        ///
        $order->owner_name = $customer['name'];
        $order->owner_gender = $customer['gender'];
        $order->owner_email = $customer['email'];
        $order->owner_mobile = $customer['mobile'];
        $order->owner_dob = $customer['dob'];
        $order->owner_country_id = $country_info->id;
        $order->owner_country_code_2 = $country_info->country_code_2;
        $order->owner_country_code_3 = $country_info->country_code_3;
        $order->owner_country = $country_info->country_name;
        $order->owner_calling_code = $country_info->calling_code;
        ////
        $order->order_status = 1;
        $order->order_date = $current_date;
        $order->payment_method = "";
        $order->agree_newsletter = $customer['agree_newsletter'];
        $order->total_pint = 2;
        $order->save();

        $setting = \App\Setting::where("type", "global")->where("key", "product_name")->first();
        $product_name = $setting->value;

        $y = date("Y", strtotime($current_date));
        $m = date("m", strtotime($current_date));

        $total = 0;
        HoldTrait::releaseHold();
        HoldTrait::updateHoldStatusbyOrderID($order->id, 'cancel');

        $total_pint = 0;
        /*
        foreach($carts as $index=>$cart){

            $pint_id = $index;
            $pint_info = \App\Pint::find($pint_id);

            $order_item = new \App\OrderItem;
            $order_item->order_id = $order->id;
            $order_item->item_id = $pint_info->id;
            $order_item->item_name = $product_name;
            $order_item->qty = $cart['qty'];
            $order_item->pint = $pint_info->pint;
            $order_item->total_pint = ($cart['qty'] * $pint_info->pint);
            $order_item->price = $pint_info->price;
            $order_item->total = ($cart['qty'] * $pint_info->price);
            $order_item->save();

            /// copy image
            $images_array = explode(".",  $pint_info->image);
            $ext = $images_array[count($images_array)-1];
            $new_image_name = md5(date("YmdHis")) . "-" . $order->id . "-" . $order_item->id . '.' . $ext;
            //Storage::copy('public/pint/' . $pint_info->image, 'public/pint/deal/' . $y . '/' . $m . '/' . $new_image_name);
            $order_item->item_image = $new_image_name;
            $order_item->save();
            ////////// 

            $total_pint += $order_item->total_pint;

            $total += $order_item->total;
        } */

        // $product_id = 1;    // fetch the product id
        $qty = 1;
        //old
        // $product_info = \App\Product::find($product_id);

        //new
        $getProductByCode = DB::table('redemption_code')->where('code',$customer['redemtion_code'])->first();
        $product_info = \App\ProductRedemption::find($getProductByCode->product_redemption_id);
        // set order item for 1 free tiger crystal can
        $order_item = new \App\OrderItem;
        $order_item->order_id = $order->id;
        $order_item->item_id = $product_info->id;
        $order_item->item_name = $product_info->name;
        $order_item->qty = $qty;
        $order_item->balance = $qty;
        $order_item->price = 0;

        $order_item->total = ($qty * 0);
        ///
        // $order_item->owner_name = $customer['name'];
        // $order_item->owner_gender = $customer['gender'];
        // $order_item->owner_email = $customer['email'];
        // $order_item->owner_mobile = $customer['mobile'];
        // $order_item->owner_dob = $customer['dob'];
        // $order_item->owner_country_id = $country_info->id;
        // $order_item->owner_country_code_2 = $country_info->country_code_2;
        // $order_item->owner_country_code_3 = $country_info->country_code_3;
        // $order_item->owner_country = $country_info->country_name;
        // $order_item->owner_calling_code = $country_info->calling_code;

        $order_item->save();

        // Hold product
        $hold_data = [
            'order_id' => $order->id,
            'product_id' => $product_info->id,
            'qty' => $qty,
        ];
        HoldTrait::addHold($hold_data);
        // End hold product

        $discount = 0;
        /*
        $ses_promo_code = Session::get("promo_code");
        if($ses_promo_code){
            $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $ses_promo_code['code'])->first();
            if($promo_code){
                $discount = $promo_code->discount;
            }
        } */

        $order->discount = $discount;
        //$total = ($total-$discount);
        $total = 0;

        $order->total = $total;
        $order->save();

        // Not active redemtion code
        DB::table('redemption_code')->where('code',$customer['redemtion_code'])->update(['status'=>1]);

        Session::put("order_id", $order->id);

        return array(
            'order_id' => $order->id,
            'total' => $total
        );
    }

    public static function finishOrder($order_id){

        $order_info = \App\Order::find($order_id);
        //$customer = Session::get('customer');

        $is_new_user = false;
        // check user by mobile
        $user_info = \App\User::where("country_id", $order_info->country_id)
            ->where("mobile", $order_info->mobile)
            ->where("type", "customer")
            ->where("deleted", 0)->first();
        if(!$user_info){

            $is_new_user = true;

            $user_info = new \App\User;
            $user_info->status = 1;
            $user_info->type = "customer";

            $user_info->country_id = $order_info->country_id;
            $user_info->mobile = $order_info->mobile;
            $user_info->email = $order_info->email;
        }

        $user_info->first_name = $order_info->name;        
        $user_info->gender = $order_info->gender;
        $user_info->dob = $order_info->dob;
        // $user_info->subscribe = $customer['agree_newsletter'];

        $user_info->save();

        $order_info->user_id = $user_info->id;
        $order_info->save();

        // release hold success voucher and update sold
        HoldTrait::updateHoldStatusbyOrderID($order_id, 'done');
        HoldTrait::releaseHold();

        $results = array();

        $voucher_sequence_info = \App\Setting::where('type', 'voucher-sequence')->where('key', 'voucher_sequence')->first();
        $voucher_prefix_info = \App\Setting::where('type', 'global')->where('key', 'voucher_prefix')->first();
        $voucher_digit_info = \App\Setting::where('type', 'global')->where('key', 'voucher_digit')->first();
        $point_per_pint_info = \App\Setting::where('type', 'global')->where('key', 'point_per_pint')->first();
        $discount_per_referral_info = \App\Setting::where('type', 'global')->where('key', 'discount_per_referral')->first();
        $image_referral_info = \App\Setting::where('type', 'global')->where('key', 'image_referral')->first();

        $voucher_sequence = $voucher_sequence_info->value;
        $voucher_prefix = $voucher_prefix_info->value;
        $voucher_digit = $voucher_digit_info->value;
        $point_per_pint = $point_per_pint_info->value;
        $discount_per_referral = $discount_per_referral_info->value;
        $image_referral = $image_referral_info->value;

        $voucher_zero_string = "";
        for($i=0; $i<$voucher_digit; $i++){
            $voucher_zero_string .= "0";
        }
        $len_sq = strlen($voucher_sequence);
        $len_zero = strlen($voucher_zero_string);
        $voucher_zero = substr($voucher_zero_string, 0, ($len_zero - $len_sq));
        $voucher = $voucher_prefix . $voucher_zero . $voucher_sequence;


        $voucher_info = \App\Voucher::where("order_id", $order_info->id)->first();
        // if(!$voucher_info){
          $voucher_info = new \App\Voucher;

          $voucher_info->user_id = $order_info->user_id;
          $voucher_info->value = $order_info->total_pint;
          $voucher_info->balance = $order_info->total_pint;
          $voucher_info->code = str_random(4);
          $voucher_info->voucher_id = $voucher;
          $voucher_info->type = '';
          $voucher_info->order_id = (int)$order_info->id;
          $voucher_info->owner_name = $order_info->owner_name;
          $voucher_info->owner_email = $order_info->owner_email;
          $voucher_info->owner_mobile = $order_info->owner_mobile;
          $voucher_info->owner_country_id = $order_info->owner_country_id;
          $voucher_info->owner_country_code_2 = $order_info->owner_country_code_2;
          $voucher_info->owner_country_code_3 = $order_info->owner_country_code_3;
          $voucher_info->owner_country = $order_info->owner_country;
          $voucher_info->owner_calling_code = $order_info->owner_calling_code;
          $voucher_info->owner_dob = $order_info->owner_dob;
          $voucher_info->expiry_date = date('Y-m-d',strtotime('2020-10-30'));
          //$voucher_info->expiry_date = date('Y-m-d',strtotime('+30 days',time()));
          //$voucher_info->status = 1;
        // }else{
        //   $voucher_info->value = ($voucher_info->value + $order_info->total_pint);
        //   $voucher_info->balance = ($voucher_info->balance + $order_info->total_pint);
        // }
        $voucher_info->is_survey = 0;   // flag voucher comes from survey api

        $voucher_info->save();

        $voucher_id = $voucher_info->voucher_id;

        $old_voucher_sequence = (int) $voucher_sequence_info->value;
        $old_voucher_sequence++;
        $voucher_sequence_info->value = $old_voucher_sequence;
        $voucher_sequence_info->save();


       // $result['voucher_id'] = $voucher_id;
        /*
       ////
        $point = new \App\Point;
        $point->user_id = $user_info->id;
        $point->type = "order";
        $point->order_id = $order_info->id;
        $point->point = ($order_info->total_pint * (int)$point_per_pint);
        $point->save();

        $user_info->total_points = ($user_info->total_points + $point->point);
        $user_info->save();
       ///
       */

        /*
        $ses_promo_code = Session::get("promo_code");
        if($ses_promo_code){
            $promo_code = \App\PromoCode::where("status", 1)->where("used", 0)->where("code", $ses_promo_code['code'])->first();
            if($promo_code){
                $promo_code->used = 1;
                $promo_code->used_on = date("Y-m-d H:i:s");
                $promo_code->save();
            }
        } */

        $send_sms = 1;      // turn off when local
        // BEGIN SMS
        if ($send_sms) {
            try {
                $user_mobile = $voucher_info->owner_mobile;
                $len_user_mobile = strlen($user_mobile);
                $trim_user_mobile = $len_user_mobile - 4;
                $string_mobile = substr($user_mobile,($trim_user_mobile),4);

                //// send sms
                $api_key = env('NEXMO_KEY');
                $api_secret = env('NEXMO_SECRET');
                $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($api_key, $api_secret));
                if(explode('/', url()->current())[2] == 'localhost:8000'){
                    $url = url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id);
                } else {
                    $url = app('bitly')->getUrl(url('voucher/' . $voucher_info->code . '/' . $voucher_info->voucher_id));
                }
                $sms_text = 'Congratulations, your registration is successful. View your voucher '.$url.' to redeem your drinks!';

                $message = $client->message()->send([
                    'to' => $voucher_info->owner_calling_code . $voucher_info->owner_mobile,
                    'from' => 'WGS',
                    'text' => $sms_text
                ]);
                if($message){
                    //echo "SMS sent!";
                    //$result['message'] = true;
                    //$result['message_text'] = $text_message;
                    $sms_log = new \App\SmsLog;
                    $sms_log->voucher_id = $voucher_info->voucher_id;
                    $sms_log->date = date("Y-m-d H:i:s");
                    $sms_log->mobile = $voucher_info->owner_calling_code . $voucher_info->owner_mobile;
                    $sms_log->text = $sms_text;
                    $sms_log->status = 'sms.success';
                    $sms_log->created_at = date("Y-m-d H:i:s");
                    $sms_log->updated_at = date("Y-m-d H:i:s");
                    $sms_log->save(); 
                }else{
                    //echo "Sorry SMS is failed!";
                    //$result['message'] = false;
                    //$result['message_text'] = $text_message;
                    //$result['message_reason'] = "Sorry SMS is failed!";
                    $sms_log = new \App\SmsLog;
                    $sms_log->voucher_id = $voucher_info->voucher_id;
                    $sms_log->date = date("Y-m-d H:i:s");
                    $sms_log->mobile = $voucher_info->owner_calling_code . $voucher_info->owner_mobile;
                    $sms_log->text = $sms_text;
                    $sms_log->status = 'sms.failed';
                    $sms_log->created_at = date("Y-m-d H:i:s");
                    $sms_log->updated_at = date("Y-m-d H:i:s");
                    $sms_log->save(); 
                }
            }catch(Exception $e) {
                //echo 'Message: ' .$e->getMessage();
            }
        }
        // END SMS

        //start Email
        //// send email begin

        $email = $user_info->email;
        $name = $user_info->first_name;
        $email_from = 'WGS';
        $email_subject = 'Your registration is successful.';
        $email_tpl = 'emails.purchase';

        $order_items = \App\OrderItem::where('order_id', $order_id)->get();
        $desc = $order_items[0]->item_name;
        $items = [];
        foreach($order_items as $order_item){
            $items[] = [
                'voucher_id' => $voucher_info->voucher_id,
                'entitlement' => $voucher_info->balance .' x '. $desc,
                'qty' => $voucher_info->balance,
                'total' => "SGD " . number_format($order_item->total, 2),
                'expired_date' => '31 October 2020',
            ];
        }

        $string_token = \Firebase\JWT\JWT::encode(array(
            "order_id" => $order_id
        ), "email-preview-purchase-tiger");

        $data_email = array(
            "string_token" => $string_token,
            'name' => $voucher_info->user->first_name,
            'items' => $items
        );
        
        $sendEmail = Mail::send($email_tpl, $data_email, function ($m) use ($email, $name, $email_from, $email_subject) {
            $m->from('enquiries@apb.com.sg', $email_from);
            $m->bcc(['cprv.notifications@gmail.com']);
            $m->to($email, $name)->subject($email_subject);
        });

        if($sendEmail){
            //log report sent
            $voucher_info->sent = 1;
            $voucher_info->save();

            $result['email'] = true;
            $result['data_email'] = $data_email;
            $result['result_email'] = $sendEmail;
            //var_dump($result);

            //write to email logs table
            $mail_log = new \App\EmailLog;
            $mail_log->voucher_id = $voucher_info->voucher_id;
            $mail_log->date = date('Y-m-d H:i:s');
            $mail_log->email_id = $email;
            $mail_log->type = 'voucher.new';
            $mail_log->save();
        } else {
            //write to email logs table
            $mail_log = new \App\EmailLog;
            $mail_log->voucher_id = $voucher_info->voucher_id;
            $mail_log->date = date('Y-m-d H:i:s');
            $mail_log->email_id = $email;
            $mail_log->type = 'send.failed';
            $mail_log->save();
        }
        //// send email end
        //end Email
        $order_info->payment_method = '';
        $order_info->order_status = 2;

        $order_info->save();
       // return array(
        //    'result' => $results,
       // );


        //Session::forget('carts');

        ////////////////
    }

    public static function orderSuccess($order_id){

    }
}
