var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	
	/// ADMIN BEGIN
	mix.scripts([
        /// bower
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/angular/angular.min.js',
		'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
		'bower_components/angular-sanitize/angular-sanitize.min.js',
		'bower_components/angular-loading-bar/build/loading-bar.min.js',
		'bower_components/angular-animate/angular-animate.min.js',
		'bower_components/angular-google-chart/ng-google-chart.min.js',
		//'bower_components/checklist-model/checklist-model.js',
		'bower_components/ng-file-upload/ng-file-upload.min.js',
		'bower_components/ng-file-upload-shim/ng-file-upload.min.js',
		'bower_components/moment/moment.js',
		'bower_components/bootstrap-daterangepicker/daterangepicker.js',
		'bower_components/angular-daterangepicker/js/angular-daterangepicker.min.js',
		///
		'resources/assets/admin/theme/js/custom.js',
		///
		'resources/assets/admin/js/admin.module.js',
		'resources/assets/admin/js/admin.value.js',
		'resources/assets/admin/js/admin.controller.js',
		'resources/assets/admin/js/admin.directive.js',

	], 'public/assets/admin/js/admin.js', './');
	
	/// auth
	mix.scripts([
		'resources/assets/admin/js/auth/auth.controller.js',
		'resources/assets/admin/js/auth/auth.service.js'
	], 'public/assets/admin/js/auth.js', './');
	/// dashboard
	mix.scripts([
		'resources/assets/admin/js/dashboard/dashboard.controller.js',
		'resources/assets/admin/js/dashboard/dashboard.service.js'
	], 'public/assets/admin/js/dashboard.js', './');
	/// voucher
	mix.scripts([
		'resources/assets/admin/js/voucher/voucher_list.controller.js',
		'resources/assets/admin/js/voucher/voucher.service.js'
	], 'public/assets/admin/js/voucher.js', './');
	mix.scripts([
		'resources/assets/admin/js/voucher/voucher_view.controller.js',
		'resources/assets/admin/js/voucher/voucher.service.js'
	], 'public/assets/admin/js/voucher_view.js', './');
	/// pint
	mix.scripts([
		'resources/assets/admin/js/pint/pint.controller.js',
		'resources/assets/admin/js/pint/pint.service.js',
	], 'public/assets/admin/js/pint.js', './');
	/// prize
	mix.scripts([
		'resources/assets/admin/js/prize/prize.controller.js',
		'resources/assets/admin/js/prize/prize.service.js',
	], 'public/assets/admin/js/prize.js', './');
	/// country
	mix.scripts([
		'resources/assets/admin/js/country/country.controller.js',
		'resources/assets/admin/js/country/country.service.js',
	], 'public/assets/admin/js/country.js', './');
	/// property
	mix.scripts([
		'resources/assets/admin/js/property/property.controller.js',
		'resources/assets/admin/js/property/property.service.js',
	], 'public/assets/admin/js/property.js', './');
	/// report full transaction
	mix.scripts([
		'resources/assets/admin/js/report/full_transaction_report.controller.js',
		'resources/assets/admin/js/report/report.service.js',
	], 'public/assets/admin/js/full_transaction_report.js', './');
	/// report redemption by property
	mix.scripts([
		'resources/assets/admin/js/report/redemption_by_property_report.controller.js',
    ], 'public/assets/admin/js/redemption_by_property_report.js', './');
	/// report prize redemption
	mix.scripts([
		'resources/assets/admin/js/report/prize-redemption.controller.js',
	], 'public/assets/admin/js/prize-redemption.js', './');
	mix.styles([
		'bower_components/font-awesome/css/font-awesome.css',
		'bower_components/angular-loading-bar/build/loading-bar.min.css',
		'bower_components/bootstrap-daterangepicker/daterangepicker.css',
		
		'resources/assets/admin/css/theme/bootstrap.min.css',
		'resources/assets/admin/css/theme/bootstrap-override.css',
		'resources/assets/admin/css/theme/weather-icons.min.css',
		'resources/assets/admin/css/theme/jquery-ui-1.10.3.css',
		'resources/assets/admin/css/theme/animate.min.css',
		'resources/assets/admin/css/theme/animate.delay.css',
		'resources/assets/admin/css/theme/toggles.css',
		'resources/assets/admin/css/theme/pace.css',

		'resources/assets/admin/css/admin.css',
		'resources/assets/admin/css/custom.css',
	], 'public/assets/admin/css/admin.css', './');

	mix.copy([
		'bower_components/bootstrap/fonts',
		'bower_components/font-awesome/fonts',
	], 'public/assets/admin/fonts');
	
	mix.copy([
		'resources/assets/admin/image'
	], 'public/assets/admin/image');
	/// ADMIN END

	/// VOUCHER BEGIN
	mix.scripts([
        /// bower
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/angular/angular.min.js',
		'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
		'bower_components/angular-sanitize/angular-sanitize.min.js',
		'bower_components/angular-loading-bar/build/loading-bar.min.js',
		'bower_components/angular-animate/angular-animate.min.js',
		///
		'resources/assets/voucher/js/voucher.module.js',
		'resources/assets/voucher/js/voucher.controller.js',
		'resources/assets/voucher/js/voucher.directive.js',
	], 'public/assets/voucher/js/voucher.js', './');

	mix.styles([
		'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/bootstrap/dist/css/bootstrap.css',
		'bower_components/angular-loading-bar/build/loading-bar.min.css',
		'bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css',
		'resources/assets/voucher/css/voucher.css',
	], 'public/assets/voucher/css/voucher.css', './');

	mix.scripts([
		'resources/assets/voucher/js/home/home.controller.js',
		'resources/assets/voucher/js/home/home.service.js',
	], 'public/assets/voucher/js/home.js', './');

	mix.scripts([
		'bower_components/jqueryui-datepicker/datepicker.js',
		'resources/assets/voucher/js/account/account.controller.js',
		'resources/assets/voucher/js/account/account.service.js',
	], 'public/assets/voucher/js/account.js', './');

	mix.scripts([
		'resources/assets/voucher/js/prize/prize.controller.js',
		'resources/assets/voucher/js/prize/prize.service.js',
	], 'public/assets/voucher/js/prize.js', './');

	mix.scripts([
		'resources/assets/voucher/js/redeem/redeem.controller.js',
		'resources/assets/voucher/js/redeem/redeem.service.js',
	], 'public/assets/voucher/js/redeem.js', './');

	mix.scripts([
		'resources/assets/voucher/js/auth/auth.controller.js',
		'resources/assets/voucher/js/auth/auth.service.js',
	], 'public/assets/voucher/js/auth.js', './');

	mix.copy([
		'bower_components/bootstrap/fonts',
		'bower_components/font-awesome/fonts',
	], 'public/assets/voucher/fonts');
	
	mix.copy([
		'resources/assets/voucher/image'
	], 'public/assets/voucher/image');

	mix.copy([
		'bower_components/jquery-ui/themes/smoothness/images/ui-icons_222222_256x240.png'
	], 'public/assets/voucher/image/ui-icons_222222_256x240.png');
	/// VOUCHER END

	/// WEB PURCHASE BEGIN
	mix.scripts([
        /// bower
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/angular/angular.min.js',
		'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
		'bower_components/angular-sanitize/angular-sanitize.min.js',
		'bower_components/angular-loading-bar/build/loading-bar.min.js',
		'bower_components/angular-animate/angular-animate.min.js',
		'bower_components/angular-touch/angular-touch.min.js',
		'bower_components/jquery.scrollTo/jquery.scrollTo.min.js',
		'bower_components/jquery.localScroll/jquery.localScroll.min.js',
		'bower_components/jqueryui-datepicker/datepicker.js',
		///
		'resources/assets/web_purchase/js/web-purchase.module.js',
		'resources/assets/web_purchase/js/web-purchase.value.js',
		'resources/assets/web_purchase/js/web-purchase.controller.js',
		'resources/assets/web_purchase/js/web-purchase.directive.js',
		'resources/assets/web_purchase/js/auth/auth.service.js',

	], 'public/assets/web-purchase/js/web-purchase.js', './');

	mix.styles([
		'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/bootstrap/dist/css/bootstrap.css',
		'bower_components/angular-loading-bar/build/loading-bar.min.css',
		'bower_components/slick-carousel/slick/slick.css',
		'bower_components/slick-carousel/slick/slick-theme.css',
		'bower_components/owl.carousel/dist/assets/owl.carousel.css',
		'bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css',
		'resources/assets/web_purchase/css/web_purchase.css',
	], 'public/assets/web-purchase/css/web-purchase.css', './');

	mix.copy([
		'bower_components/bootstrap/fonts',
		'bower_components/font-awesome/fonts',
		'resources/assets/web_purchase/fonts',
    ], 'public/assets/web-purchase/fonts');

	mix.scripts([
		'bower_components/owl.carousel/dist/owl.carousel.min.js',
		'resources/assets/web_purchase/js/product/product.controller.js',
		'resources/assets/web_purchase/js/product/product.directive.js'
	], 'public/assets/web-purchase/js/product.js', './');

	mix.styles([
        'bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
	], 'public/assets/web-purchase/css/product.css', './');

	mix.scripts([
		'resources/assets/web_purchase/js/confirmation/confirmation.controller.js',
		'resources/assets/web_purchase/js/cart/cart.service.js',
		'resources/assets/web_purchase/js/checkout/checkout.service.js',
	], 'public/assets/web-purchase/js/confirmation.js', './');

	mix.scripts([
		'resources/assets/web_purchase/js/create/create.controller.js',
		'resources/assets/web_purchase/js/cart/cart.service.js',
	], 'public/assets/web-purchase/js/create.js', './');

	mix.scripts([
		'resources/assets/web_purchase/js/age-blocker/age-blocker.controller.js',
		'resources/assets/web_purchase/js/age-blocker/age-blocker.service.js',
	], 'public/assets/web-purchase/js/age-blocker.js', './');

	mix.scripts([
		'resources/assets/web_purchase/js/order/order.controller.js',
	], 'public/assets/web-purchase/js/order.js', './');

	mix.scripts([
		'resources/assets/web_purchase/js/home/home.controller.js',
		'resources/assets/web_purchase/js/home/home.service.js',
	], 'public/assets/web-purchase/js/home.js', './');

	mix.scripts([
		'resources/assets/web_purchase/js/account/account.controller.js',
		'resources/assets/web_purchase/js/account/account.service.js',
	], 'public/assets/web-purchase/js/account.js', './');

	mix.scripts([
		'resources/assets/web_purchase/js/resend-voucher/resend-voucher.controller.js',
		'resources/assets/web_purchase/js/resend-voucher/resend-voucher.service.js',
	], 'public/assets/web-purchase/js/resend-voucher.js', './');

	mix.copy([
		'resources/assets/web_purchase/image'
	], 'public/assets/web-purchase/image');

	mix.copy([
		'bower_components/jquery-ui/themes/smoothness/images/ui-icons_222222_256x240.png'
	], 'public/assets/web-purchase/image/ui-icons_222222_256x240.png');

	mix.copy([
		'resources/assets/web_purchase/term.pdf'
	], 'public/assets/web-purchase/term.pdf');

	mix.copy([
		'resources/assets/web_purchase/privacy-policy.pdf'
	], 'public/assets/web-purchase/privacy-policy.pdf');
	/// WEB PURCHASE END
});
