@extends('web_purchase.layout')

@section('content')
@section('style')
<style>
    .modal-content{
        border-radius: 11px;
    }

    .modal-header{
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }

    .modal-footer{
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
    }

    @media screen and (max-width: 480px){
        #home {
            background-repeat: no-repeat;
            background-position: center;
        }

        body #contact {
            background-image: url({{ asset('assets/web-purchase/image/age-mobile.png') }});
        }

        .no-padding-left{
            padding-left: 0px !important;
        }

        .redeem {
            height: 45px;
            width: 45px;
            font-size: 25px;
            text-align: center;
            border: 1px solid #000000;
            width: 36px;
            height: 36px;
        }
        .redeem::-webkit-inner-spin-button,
        .redeem::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        section.contact .form-control{
            height: 36px;
        }

    }

    @media screen and (min-width: 480px){
        .redeem {
            height: 45px;
            width: 45px;
            font-size: 25px;
            text-align: center;
            border: 1px solid #000000;
            width: 50px;
            height: 50px;
            position: relative;
            z-index: 1;
        }
        .redeem::-webkit-inner-spin-button,
        .redeem::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    }

    .form-group{
        margin-bottom: 0px;
    }

    @media screen and (max-width: 992px){
        .recipes-ipad{
        display: block;
        }

        .recipes-ipad-desktop{
        display: none;
        }
    }
</style>    
@endsection
<div class="home" home-scroll>
    

    <div class="home" ng-controller="HomeController" ng-init="countries = {{ json_encode($countries) }};">

        <section class="opening" id="home">
            <div class="container">
            <div class="row recipes-desktop">
                <div class="col-md-4 col-sm-6 col-sm-offset-1 text-center text-white">
                    <h3><b>ENJOY 2 <br> COMPLIMENTARY <br> DRINKS <br> ON US AT SUNSET<b></h3>
                    <!-- <img class="img-responsive" id="mbl-sp" src="{{ asset('assets/web-purchase/image/ENJOY.png') }}"> -->
                </div>
            </div>
            </div>
        </section>

        <section id="reg" class="contact">

            <div class="container">
                <div class="row recipes-desktop">
                    <div class="col-xs-12" style="padding-bottom: 15px;text-align: center;">
                        <h1 style="color: #FCE8C2;font-size: 32px;padding-bottom: 15px;">REGISTRATION</h1>
                        <h4 style="color:#FCE8C2;font-size: 14px;">Please fill in your details to redeem 2 complimentary drinks at our participating bars!</h4>
                    </div>
                </div>
                <div class="row recipes-mobile">
                    <div class="col-xs-12" style="padding-bottom: 25px;padding-top: 25px;">
                        <h1 style="color: #FCE8C2;text-align: center;font-size: 32px;">REGISTRATION</h1>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-offset-2 col-sm-12">
                    
                        <div class="col-md-12">
        
                            <div class="form-group" ng-class="{'has-error': errors_registration.hasOwnProperty('mobile')}">
                                <div class="col-md-5 no-padding-left">
                                    <div class="form-group" ng-class="{'has-error': errors_registration.hasOwnProperty('name')}">
                                        <input type="text" class="form-control" ng-model="form_registration.name" placeholder="Name" maxlength="180"/>
                                        <div class="help-block" id="help-block" style="color:#fff;"g-if="errors_registration.hasOwnProperty('name')" ng-cloak>@{{errors_registration.name}}</div>
                                    </div>
                                </div>
                                <div class="col-md-3 no-padding-left">
                                    <select class="form-control" ng-model="form_registration.country" ng-options="country.country_id as country.country_name for country in countries"></select>
                                </div>
                                <div class="col-md-4 mbl-phone no-padding-left">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1" style="color: #F58337;font-weight: bold;">@{{ form_registration_calling_code }}</span>
                                        <input type="number" class="form-control" aria-describedby="basic-addon1" ng-model="form_registration.mobile">
                                    </div>
                                    <div class="help-block" id="help-block1" style="color:#fff;"g-if="errors_registration.hasOwnProperty('mobile')" ng-cloak>@{{errors_registration.mobile}}</div>
                                </div>
                            </div>
                        
                            <div class="col-md-7 no-padding-left">
                                <div class="form-group" ng-class="{'has-error': errors_registration.hasOwnProperty('email')}" style="margin-bottom: 0px;">
                                    <input type="email" class="form-control" ng-model="form_registration.email" placeholder="Email Address"/>
                                    <div class="help-block" id="help-block2" style="color:#fff;"g-if="errors_registration.hasOwnProperty('email')" ng-cloak>@{{errors_registration.email}}</div>
                                </div>
                            </div>

                            <div class="col-md-5 no-padding-left">
                                <!-- <div class="form-group" ng-class="{'has-error': errors_registration.hasOwnProperty('dob')}">
                                    <input type="text" class="form-control" date="form_registration.dob" placeholder="Date of Birth" dob-picker-ui max-year="2018" total-year="80" readonly />
                                </div> -->
                                <div class="col-md-4 col-xs-4 no-padding-left">
                                    <select class="form-control redeem" style="width: 70px;" ng-model="form_registration.dob1">
                                        <option value="">DD</option>
                                        @foreach ($dd as $item)
                                            <option value="{{ $item }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 col-xs-4 no-padding-left">
                                    <select class="form-control redeem" style="width: 70px;" ng-model="form_registration.dob2">
                                        <option value="">MM</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-xs-4 no-padding-left">
                                    <select class="form-control redeem" style="width: 88px;" ng-model="form_registration.dob3">
                                        <option value="">YYYY</option>
                                        @foreach ($yyyy as $y)
                                            <option value="{{ $y }}">{{ $y }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="help-block" id="help-block3" style="color:#fff;"g-if="errors_registration.hasOwnProperty('dob')" ng-cloak>@{{errors_registration.dob}}</div>
                            </div>

                            <div class="col-md-8 no-padding-left" style="padding-bottom: 15px;">
                                <div class="form-group" ng-class="{'has-error': errors_registration.hasOwnProperty('redemtion_code')}">
                                    <label style="color: #FCE8C2;display: block;">Redemption Code</label>
                                    <!-- <input type="text" class="form-control redemtion_code" ng-model="form_registration.redemtion_code" maxlength="1" /> -->
                                    <div class="col-md-2 col-xs-2 no-padding-left">
                                        <input class="form-control redeem" ng-model="form_registration.redemtion_code1" id="codeBox1" type="text" maxlength="1" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)"/>
                                    </div>
                                    <div class="col-md-2 col-xs-2 no-padding-left">
                                        <input class="form-control redeem" ng-model="form_registration.redemtion_code2" id="codeBox2" type="text" maxlength="1" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)"/>
                                    </div>
                                    <div class="col-md-2 col-xs-2 no-padding-left">
                                        <input class="form-control redeem" ng-model="form_registration.redemtion_code3" id="codeBox3" type="text" maxlength="1" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)"/>
                                    </div>
                                    <div class="col-md-2 col-xs-2 no-padding-left">
                                        <input class="form-control redeem" ng-model="form_registration.redemtion_code4" id="codeBox4" type="text" maxlength="1" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)"/>
                                    </div>
                                    <div class="col-md-2 col-xs-2 no-padding-left">
                                        <input class="form-control redeem" ng-model="form_registration.redemtion_code5" id="codeBox5" type="text" maxlength="1" onkeyup="onKeyUpEvent(5, event)" onfocus="onFocusEvent(5)"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-xs-12 no-padding-left" style="padding-bottom: 15px;">
                                <div class="checkbox">
                                    <label style="color: #FCE8C2;"><input type="checkbox" id="text1" name="text1" value="1" ng-change="EnableDisableReg()" ng-model="checked">{!! $text1 !!}</label>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding-bottom: 15px;">
                                <label style="color: white;" id="help-block4" g-if="errors_registration.hasOwnProperty('redemtion_code')" ng-cloak>@{{errors_registration.redemtion_code}}</label>
                            </div>
                            <div class="col-md-12 col-md-offset-5 recipes-desktop">
                                <button class="btn btn-danger btn-lg show-support-btn" id="click" ng-click="submitFormRegistration()" style="margin-top: 0px;outline: none;">SUBMIT</button>
                            </div>
                            <div class="col-xs-12 recipes-mobile text-center">
                                <button class="btn btn-danger btn-lg show-support-btn" ng-click="submitFormRegistration()" style="margin-top: 0px;outline: none;">SUBMIT</button>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-12 recipes-desktop">
                        <div class="col-md-1 col-md-1 col-md-offset-3">
                            <img class="img-responsive" id="prizes1" style="visibility: hidden;" src="{{ asset('assets/web-purchase/image/Group 152@2x.png') }}">
                        </div>
                    </div>   -->
                    <div class="col-md-9 col-md-offset-2 recipes-desktop recipes-ipad-desktop" style="margin-top: 5%;">
                        <div class="col-md-2">
                            <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 40px;">
                            <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="img-responsive" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 32px;">
                            <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="img-responsive" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 40px;">
                            <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="img-responsive" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                        </div>
                        <div class="col-md-2">
                            <a href="https://www.milagrotequila.com/" target="_blank"><img class="img-responsive" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 27px;">
                            <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="img-responsive" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                        </div>
                    </div>  
                    <div class="col-xs-12 recipes-mobile recipes-ipad" style="margin-top: 5%;">
                        <div class="container">
                            <div class="col-xs-4" style="margin-top: 6%;">
                                <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 66px">
                                <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="img-responsive" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 59px">
                                <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="img-responsive" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                            </div>
                        </div>
                        <div class="container" style="margin-top: 6%;">
                            <div class="col-xs-4" style="top: 45px">
                                <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="img-responsive" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 9px">
                                <a href="https://www.milagrotequila.com/" target="_blank"> <img class="img-responsive" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 35px">
                                <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="img-responsive" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-6 footer-left text-white" style="top: 60px;">
                &copy; 2020 William Grant & Sons. <br> All Rights Reserved.
            </div>
            <div class="col-md-6 footer-right text-white" style="top: 60px;">
                <ul class="list-inline footer-menu">
                    <li><a href="{{ url('privacy-policy') }}" target="_blank" style="color: #fff;">Privacy Policy</a></li>
                    <li><a href="{{ url('terms-and-conditions') }}" target="_blank" style="color: #fff;">Terms of Use</a></li>
                </ul>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/home.js') }}"></script>
<script>
    $(function(){
        document.getElementById('click').click();
        document.getElementById('help-block').style.visibility = 'hidden';
        document.getElementById('help-block1').style.visibility = 'hidden';
        document.getElementById('help-block2').style.visibility = 'hidden';
        document.getElementById('help-block3').style.visibility = 'hidden';
        document.getElementById('help-block4').style.display = 'none';

        document.getElementById('help-block').style.marginTop = '-14px';
        document.getElementById('help-block1').style.marginTop = '-14px';
        document.getElementById('help-block2').style.marginTop = '-14px';
        document.getElementById('help-block3').style.marginBottom = '-14px';
    })
    function getCodeBoxElement(index) {
      return document.getElementById('codeBox' + index);
    }
    function onKeyUpEvent(index, event) {
      const eventCode = event.which || event.keyCode;
      if (getCodeBoxElement(index).value.length === 1) {
        if (index !== 6) {
          getCodeBoxElement(index+ 1).focus();
        } else {
          getCodeBoxElement(index).blur();
        }
      }
      if (eventCode === 8 && index !== 1) {
        getCodeBoxElement(index - 1).focus();
      }
    }
    function onFocusEvent(index) {
      for (item = 1; item < index; item++) {
        const currentElement = getCodeBoxElement(item);
        if (!currentElement.value) {
            currentElement.focus();
            break;
        }
      }
    }
</script>
@endsection
