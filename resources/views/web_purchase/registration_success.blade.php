@extends('web_purchase.layout')
@section('style')
    <style>
        @media screen and (max-width: 480px){
            .here_btn{
                bottom: 3px;
            }

            .prizes-img{
                width: 100%;
            }
        }

        @media screen and (max-width: 992px) and (min-width:481px){
            .recipes-ipad{
                display: block;
            }

            .recipes-ipad-desktop{
                display: none;
            }
        }
    </style>    
@endsection
@section('content')

<div class="home" home-scroll>
    

    <div class="home" ng-controller="HomeController" ng-init="countries = {{ json_encode($countries) }};">

        <section id="reg_success" class="reg_success">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="color: #FBEDCE;text-align: center;font-weight: bold;">REGISTRATION SUCCESSFUL</h3>
                        <h4 style="color: #FBEDCE;text-align: center;font-weight: medium;font-size: 16px;">Thank you for raising your glass in support! <br> A confirmation email will be sent to you shortly.</h4>
                    </div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                    
                        <div class="col-md-12">
        
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color: #f58139;color: #FBEDCE;border-top-left-radius: 10px;border-top-right-radius: 10px;font-weight: bold;font-size: 16px;">REGISTRATION ID: {{$voucher_id}}</div>
                                <div class="panel-body" style="background-color: #fff;color: orange;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                                    <ul style="list-style:none;margin-left: -35px;">
                                        <li>{{$name}}</li>
                                        <li>{{$country_name}}</li>
                                        <li>{{$email}}</li>
                                        <li>{{$code}} {{$mobile}}</li>
                                        <li>{{$date_format}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-2 col-sm-12" style="margin-bottom: 2%;">
                        <div class="col-md-12">
                            <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/Banner-success.png') }}">
                            <p class="here recipes-desktop">
                                Here are some amazing <br> sunset drink recipes you <br> can try at home.
                            </p>
                            <p class="here recipes-mobile">
                                Here are some amazing sunset drink <br> recipes you can try at home.
                            </p>
                            <a href="{{ url('recipes') }}" class="btn here_btn" style="font-weight: bold;color: #FBEDCE;">LEARN MORE</a>
                        </div>  
                    </div>
                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                        <div class="col-md-12">
                            <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/banner-succes2.png') }}">
                            <p class="here recipes-desktop">
                                Find out where you can <br> redeem your drinks
                            </p>
                            <p class="here recipes-mobile">
                                Find out where you can redeem <br> your drinks
                            </p>
                            <a href="{{ url('/#how-to-participate') }}" class="btn here_btn" style="font-weight: bold;color: #FBEDCE;">LEARN MORE</a>
                        </div>  
                    </div>
                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                        <div class="col-md-12">
                            <!-- <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/buy.png') }}"> -->
                            <br><h4 style="color:#FBEDCE;text-align: center;margin-top: 0px; font-weight: medium;font-size: 16px;">
                                Buy a bottle at these participating online retailers and enjoy another 2 complimentary drinks
                            </h4>
                        </div>  
                    </div>  
                    <div class="col-md-8 col-md-offset-2 recipes-desktop recipes-ipad-desktop">
                        <div class="col-md-2">
                            <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 40px;">
                            <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="img-responsive" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 32px;">
                            <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="img-responsive" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 40px;">
                            <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="img-responsive" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                        </div>
                        <div class="col-md-2">
                            <a href="https://www.milagrotequila.com/" target="_blank"><img class="img-responsive" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                        </div>
                        <div class="col-md-2" style="top: 27px;">
                            <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="img-responsive" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                        </div>
                    </div> 
                    <div class="col-xs-12 recipes-mobile recipes-ipad">
                        <div class="container">
                            <div class="col-xs-4" style="margin-top: 6%;">
                                <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="prizes-img" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 66px">
                                <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="prizes-img" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 59px">
                                <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="prizes-img" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                            </div>
                        </div>
                        <div class="container" style="margin-top: 6%;">
                            <div class="col-xs-4" style="top: 45px">
                                <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="prizes-img" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 9px">
                                <a href="https://www.milagrotequila.com/" target="_blank"> <img class="prizes-img" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 35px">
                                <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="prizes-img" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-6 footer-left text-white" style="top: 60px;">
                &copy; 2020 William Grant & Sons. <br> All Rights Reserved.
            </div>
            <div class="col-md-6 footer-right text-white" style="top: 60px;">
                <ul class="list-inline footer-menu">
                    <li><a href="{{ url('privacy-policy') }}" target="_blank" style="color: #fff;">Privacy Policy</a></li>
                    <li><a href="{{ url('terms-and-conditions') }}" target="_blank" style="color: #fff;">Terms of Use</a></li>
                </ul>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/home.js') }}"></script>
@endsection
