@extends('web_purchase.layout')

@section('content')
<section class="section section-order-failed">
    <div class="container">

        <div class="order-failed-intro">
            <h2 class="text-left">Your payment is failed.</h2> 
            <p>Sorry the payment process or service is unavailable at the moment. Please try again in a few minutes.<br>
            Please <a href="{{ url('/') }}#contact">contact us</a> if you need any assistance.</p>
        </div>

        <div class="panel panel-danger panel-order-failed-items">
        </div>

        <div style="margin-top=300px;">&nbsp;</div>

        <div class="next-step">
            <p><strong>What would you like to do next?</strong></p>
            <ul class="list-inline">
                <li><a href="{{ url('/') }}">Back to home</a></li>
                <!-- <li><a href="{{ url('create') }}">Purchase another voucher</a></li> -->
                <li><a href="{{ url('confirmation') }}">Give support</a></li>
            </ul>
        </div>

    </div>
</section>
@endsection
