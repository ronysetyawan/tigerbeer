<!DOCTYPE html>
<html ng-app="CPRV-OctFestApp">
    <head>

      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
      <meta property="og:title" content="Support Our Local F&B!">
      <meta property="og:type" content="article">
      <meta property="og:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
      <meta property="og:description" content="Support Our Local F&B.">
      
      <meta name="twitter:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
      <meta itemprop="image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
      
      <meta name="description" content="Support Our Local F&B.">

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-17"></script>
      <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-109851240-19');
      </script>

      <!-- Facebook Pixel Code -->
      <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window,document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2226828927336271'); 
          fbq('track', 'PageView');
          </script>
          <noscript>
          <img height="1" width="1" src="https://www.facebook.com/tr?id=2226828927336271&ev=PageView &noscript=1"/>
      </noscript>
      <!-- End Facebook Pixel Code -->

      <?php /*
      <link rel="shortcut icon" href="assets/images/favicon.ico">
      */?>

      <title>Tiger #SunsetSocial</title>

      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i|Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet"> 

      <style>
        html{
          height: 100%;
        }
      </style>

      <link href="{{ asset('assets/web-purchase/css/web-purchase.css') }}" rel="stylesheet" type="text/css" />

      <style>
        @media screen and (min-width: 993px){
          body .panel-body{
            background-color: #efe9e9;
            color: #ee5424;
          }

          .recipes-ipad-desktop{
            margin-top: 4%;
          }
        }

        @media screen and (max-width: 480px){
          .mbl{
            margin-bottom: 10%;
          }

          .panel-default{
            border-bottom: #fff;
          }
        }

        @media screen and (max-width: 992px) and (min-width:481px){
          body.age-blocker{
            background-image: url("{{ asset('assets/web-purchase/image/age-mobile.png') }}");
          }

          .recipes-ipad{
            display: block;
          }

          .recipes-ipad-desktop{
            display: none;
          }

          .panel-default{
            border-bottom: #fff;
          }

          .logo-mobile{
            width: 100px;
          }
        }
      </style>
    </head>

    <body class="age-blocker">
        <div class="container" ng-controller="AgeBlockerController">
          <div class="row">
            <div class="col-xs-12 mbl" style="margin-top: 4%;">
              <a href="{{ url('/') }}"><img class="logo-mobile" src="{{ asset('assets/web-purchase/image/new-logo.png') }}" /></a>
            </div>

            <div class="col-md-6 col-md-offset-3" style="margin-top: 4%;">
              <div class="panel panel-default">
                <div class="panel-body">
                  <h4><b>ARE YOU OF LEGAL DRINKING AGE?</b></h4>
                  <h5><b>You must be 21 years old and above to enter this site.</b></h5>
                  <div class="col-md-6" style="margin-bottom: 3%;">
                    <a href="#" class="btn btn-md btn-danger" ng-click="submit()" style="width: 130px;">Yes, I am</a>
                  </div>
                  <div class="col-md-6" style="margin-bottom: 3%;">
                    <a href="#" class="btn btn-md btn-danger" ng-click="reload()" style="width: 130px;">No, I'm not</a>
                  </div>
                  <h5><input type="checkbox" id="text1" name="text1" value="1" ng-change="EnableDisable()" ng-model="checked"> By entering this site you agree to our terms and conditions and <a href="{{ url('privacy-policy') }}" style="color: #e95d74;">privacy policy.</a></h5>
                </div>
              </div>
            </div>

            <div class="col-xs-12 brand-mobile recipes-ipad-desktop">
              <div class="container">
                <div class="row">
                  <div class="col-sm-2">
                    <img id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152@2x.png') }}">
                  </div>
                  <div class="col-sm-2" style="top: 53px;">
                    <img id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151@2x.png') }}">
                  </div>
                  <div class="col-sm-2" style="top: 43px;">
                    <img id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147@2x.png') }}">
                  </div>
                  <div class="col-sm-2" style="top: 53px;">
                    <img id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148@2x.png') }}">
                  </div>
                  <div class="col-sm-2" style="top: 17px;">
                    <img id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149@2x.png') }}">
                  </div>
                  <div class="col-sm-2" style="top: 33px;">
                    <img id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150@2x.png') }}">
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

        <script src="{{ asset('assets/web-purchase/js/web-purchase.js') }}"></script>
        <script src="{{ asset('assets/web-purchase/js/age-blocker.js') }}"></script>
    </body>

</html>