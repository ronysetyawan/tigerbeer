@extends('web_purchase.layout_success')

@section('content')
<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="padding-top: 17%; padding-bottom: 70%">
                <h1 class="text-center" style="padding-bottom: 10%"><strong>Thank you for your support.</strong></h1>
                <p class="text-center">
                    Tiger Support Our Local F&B initiative has concluded. <br><br>
                    Follow us @TigerBeer.SG on Facebook for new updates. <br>
                    If you experienced any issues with your voucher(s) purchased previously, <br>please contact us at 
                    <a href="mailto:enquiries@apb.com.sg">enquiries@apb.com.sg</a> 
                </p>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
@endsection

