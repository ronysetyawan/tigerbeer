@extends('web_purchase.layout')

@section('content')

<section class="section account" ng-controller="AccountCtrl"
    ng-init="
      countries = {{ json_encode($countries) }};
      form.name = '{{ $user_info['name'] }}';
      form.gender = '{{ $user_info['gender'] }}';
      form.country = {{ $user_info['country'] }};
      form.mobile = '{{ $user_info['mobile'] }}';
      form.email = '{{ $user_info['email'] }}';
      form.address_1 = '{{ $user_info['address_1'] }}';
      form.address_2 = '{{ $user_info['address_2'] }}';
      form.postal_code = '{{ $user_info['postal_code'] }}';
      form.subscribe = {{ $user_info['subscribe']?'true':'false' }};
      dobs.date = {{ $user_info['dob_date'] }};
      dobs.month = {{ $user_info['dob_month'] }};
      dobs.year = {{ $user_info['dob_year'] }};
    ">
  <div class="container">
    <p class="text-right"><a href="" class="text-danger" 
    button-confirmation confirmation-title="Delete of user account." 
    confirmation-description="All the redeemable products under the account will be permanently removed.<br>Are you sure you want to remove your account?" 
    text-yes="Yes, I'm sure."
    text-no="Cancel"
    btn-yes-class="btn btn-more-padding-y text-uppercase btn-danger btn-sm"
    btn-no-class="btn btn-more-padding-y text-uppercase btn-primary btn-sm"
    size="lg"
    confirmation-function="deleteAccount()">Delete account</a></p>
    <uib-accordion close-others="true">
      <div uib-accordion-group class="panel-danger" heading="Tell us more about your self" is-open="true">
          <h3>Update your contact information</h3>
          
          <div class="row">
              <div class="col-md-4">
                  <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('name')}">
                      <label>Name <span class="required">*</span></label>
                      <input class="form-control" ng-model="form.name" />
                      <div class="help-block" ng-if="errors.hasOwnProperty('name')" ng-cloak>@{{errors.name}}</div>
                  </div>
              </div>
              
              <div class="col-md-4">
                  <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('gender')}">
                      <label>Gender <span class="required">*</span></label>
                      <select class="form-control" ng-model="form.gender">
                          <option value="">-- select --</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                      </select>
                      <div class="help-block" ng-if="errors.hasOwnProperty('gender')" ng-cloak>@{{errors.gender}}</div>
                  </div>
              </div>
              
              <div class="col-md-4">
                  <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('dob')}">
                      <label>Date of Birth <span class="required">*</span></label>
                      <input type="text" class="form-control" date="form.dob" placeholder="Date of Birth" dob-picker-ui max-year="2018" total-year="80" readonly />
                      <div class="help-block" ng-if="errors.hasOwnProperty('dob')" ng-cloak>@{{errors.dob}}</div>
                  </div>
              </div>

          </div>
          
          <div class="row">
            <div class="col-md-4">
                  <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('email')}">
                      <label>Email <span class="required">*</span></label>
                      <input class="form-control" type="email" ng-model="form.email" />
                      <div class="help-block" ng-if="errors.hasOwnProperty('email')" ng-cloak>@{{errors.email}}</div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('mobile')}">
                      <label>Mobile <span class="required">*</span></label>
                      <div class="input-group">
                          <div class="input-group-addon">@{{ calling_code }}</div>
                          <input class="form-control" ng-model="form.mobile" type="text" readonly>
                      </div>
                      <div class="help-block" ng-if="errors.hasOwnProperty('mobile')" ng-cloak>@{{errors.mobile}}</div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('country')}">
                      <label>Country <span class="required">*</span></label>
                      <select class="form-control" ng-model="form.country" ng-options="country.country_id as country.country_name for country in countries" disabled></select>
                      <div class="help-block" ng-if="errors.hasOwnProperty('country')" ng-cloak>@{{errors.country}}</div>
                  </div>
              </div>

          </div>

          <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Address Line 1</label>
                    <input class="form-control" type="text" ng-model="form.address_1" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Address Line 2</label>
                    <input class="form-control" type="text" ng-model="form.address_2" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('postal_code')}">
                    <label>Postal Code <span class="required">*</span></label>
                    <input class="form-control" type="text" ng-model="form.postal_code" />
                    <div class="help-block" ng-if="errors.hasOwnProperty('postal_code')" ng-cloak>@{{errors.postal_code}}</div>
                </div>
            </div>
          </div>
      </div>
  </uib-accordion>

    <div class="checkbox">
      <label>
        <input type="checkbox" ng-model="form.subscribe"> I would like to receive invites, vouchers and promotions from Asia Pacific Breweries (Singapore) Pte Ltd and their partner outlets
      </label>
    </div>

    <button class="btn btn-primary btn-more-padding-y text-uppercase" ng-click="submit()">Update</button>
          

  </div>
</section>

@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/account.js') }}"></script>
@endsection
