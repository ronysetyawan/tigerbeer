<!DOCTYPE html>
<html ng-app="CPRV-OctFestApp">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta property="og:title" content="Support Our Local F&B!">
        <meta property="og:type" content="article">
        <meta property="og:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        <meta property="og:description" content="Support Our Local F&B.">
        
        <meta name="twitter:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        <meta itemprop="image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        
        <meta name="description" content="Support Our Local F&B.">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Tiger #SunsetSocial</title>

        <!-- App CSS -->
        <link href="{{ asset('assets/web-purchase/css/web-purchase.css') }}" rel="stylesheet" type="text/css" />

        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet"> 

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-19"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109851240-19');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2226828927336271');
        fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2226828927336271&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->


        <style type="text/css">
            html, body {
                height: 100%;
            }
            #wrap {
                min-height: 100%;
                height: auto !important;
                height: 100%;
                margin: 0 auto -100px;
            }
            .logo-success {
                background-image: url(../assets/web-purchase/image/bg-container-fluid.png);
                border-bottom: 1px solid #eee;
            }
            .footer{
                height: 100px;
                padding-top:40px;
                padding-bottom: 40px;
                background-color:#17284d;
                color: #fff;
            }
        </style>

        @yield('style')

    </head>

    <body ng-controller="MainCtrl">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=366775843451872&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <header>
            <div class="logo-success">
                <a class="" href="{{$home_url}}">
                    <img src="<?php echo asset("assets/web-purchase/image/logo.png");?>" alt="Tiger" style="display:block; border:none; outline:none; text-decoration:none;" border="0" width="123.96" height="50">
                </a>
            </div>
        </header>
        
        @yield('header')

        @yield('content')

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 footer-left">
                    &copy; 2020 Tiger Beer. All Rights Reserved.
                    </div>
                    <div class="col-md-6 footer-right">
                        <ul class="list-inline footer-menu">
                            <li><a href="{{ url('privacy-policy') }}" target="_blank">Privacy Policy</a></li>
                            <li><a href="{{ url('terms-and-conditions') }}" target="_blank">Terms of Use</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
                                
        <script src="{{ asset('assets/web-purchase/js/web-purchase.js') }}"></script>

        @yield('script')

        <script type="text/ng-template" id="loginModal.html">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">Log In Details</h3>
            </div>
            <div class="modal-body" id="modal-body" ng-init="countries = {{ json_encode($countries) }}">
                <div class="row">
                    <div class="col-md-8">
                        <label>Mobile <span class="required">*</span></label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" ng-model="form_login.country" ng-options="country.country_id as country.country_name for country in countries"></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-addon">@{{ calling_code }}</div>
                                    <input class="form-control" ng-model="form_login.mobile" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>PIN <span class="required">*</span></label>
                            <input class="form-control" ng-model="form_login.pin" type="password">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary text-uppercase" ng-click="login()">Log In</button>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/ng-template" id="outletsModal.html">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">Participating Outlets</h3>
            </div>
            <div class="modal-body" id="modal-body">
                <ul class="list-inline list-zone">
                    @foreach($outlets as $zone=>$outlet)
                    <li><a href="#{{ $zone }}">{{ $zone }}</a></li>
                    @endforeach
                </ul>
                <hr>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address 1</th>
                                <th>Address 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($outlets as $zone=>$outlet)
                                <tr id="{{ $zone }}"><td colspan="4" class="text-uppercase"><strong>ZONE: {{ $zone }}</strong></td></tr>
                                @foreach($outlet as $_outlet)
                                <tr>
                                    <td>{{ $_outlet['name'] }}</td>
                                    <td>{{ $_outlet['address1'] }}</td>
                                    <td>{{ $_outlet['address2'] }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" ng-click="close()">Close</button>
            </div>
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics Old-->
        <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-13"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109851240-13');
        </script> -->

         <!-- Global site tag (gtag.js) - Google Analytics New-->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-19"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109851240-19');
        </script>

               
    </body>
</html>
