
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>You have received an eGift Card</title>
<!--[if gte mso 9]>
<style>
ul li {
    text-indent: -1em; /* Normalise space between bullets and text */
}
</style>
<![endif]-->

<style type="text/css">
/* BOILERPLATE STYLES */
#outlook a {
  padding: 0;
}
.ReadMsgBody {
  width: 100%;
}
.ExternalClass {
  width: 100%;
}
.ExternalClass, .ExternalClass span, .ExternalClass td, .ExternalClass div {
  line-height: 100%;
}
body, table, td, a {
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
}
table, td {
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}
img {
  -ms-interpolation-mode: bicubic;
}
body {
  height: 100% !important;
  margin: 0 auto !important;
  padding: 0 !important;
}
img {
  border: 0;
  -ms-interpolation-mode: bicubic;
  outline: none;
  text-decoration: none;
}
a:link {
  text-decoration: none !important;
  text-decoration: none;
  color: #000000;
}
/* LINK STYLES */
a[x-apple-data-detectors] {
  color: inherit !important;
  text-decoration: none !important;
  font-size: inherit !important;
  font-family: inherit !important;
  font-weight: inherit !important;
  line-height: inherit !important;
}
.appleLink a {
  color: #363636 !important;
  text-decoration: none !important;
}

/*IPAD STYLES*/
@media only screen and (max-width: 640px) {
a[href^="tel"], a[href^="sms"] {
  text-decoration: none;
  color: #0a8cce; /* or whatever your want */
  pointer-events: none;
  cursor: default;
}
.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
  text-decoration: default;
  color: #0a8cce !important;
  pointer-events: auto;
  cursor: default;
}
*[class=devicewidth] {
  width: 100% !important;
  height: auto !important;
  text-align: center!important;
  align: center!important;
}
*[class=devicewidthinner] {
  width: 90%!important;
  height: auto!important;
  text-align: center!important;
}
img[class=imgresponsive] {
  width: 100%!important;
  height: auto!important;
}
*[class=mobile-hide] {
  display: none!important;
}
td[class=aheight] {
  height: auto!important;
}
/*---------FOR THIS TEMPLATE ONLY---------*/
*[class=f14] {
  font-size: 14px!important;
  line-height: 20px!important;
  text-align: center;
}
*[class=f21] {
  font-size: 21px!important;
  line-height: 25px!important;
  text-align: center;
}
*[class=devicewidthinner2] {
  width: 70%!important;
  height: auto!important;
  text-align: center!important;
}
.c1 {
  max-width: 54px;
}
.c2 {
  max-width: 47px;
}
.c3 {
  max-width: 100px;
  height:auto!important;
}
#borderHide {
  border: none;
  border: none !important;
}
.space {
  text-align: center;
  text-align: center!important;
  padding-left: 10px!important;
  padding-right: 10px!important;
  align: center!important;
}
#bottom {
  padding-top: 15px!important;
}
#top {
  padding-bottom: 15px!important;
}
#middle {
  vertical-align: baseline!important;
}
/*---------FOR THIS TEMPLATE ONLY---------*/

}

/*IPHONE STYLES*/
@media only screen and (max-width: 480px) {
*[class=rowline] {
  display: block;
}
/*---------FOR THIS TEMPLATE ONLY---------*/
*[class=f14] {
  font-size: 14px!important;
  line-height: 20px!important;
  text-align: center;
}
*[class=f21] {
  font-size: 21px!important;
  line-height: 25px!important;
  text-align: center;
}
#borderHide {
  border: none;
  border: none !important;
}
.space {
  text-align: center;
  text-align: center!important;
  padding-left: 10px!important;
  padding-right: 10px!important;
  align: center!important;
}
#bottom {
  padding-top: 15px!important;
}
#top {
  padding-bottom: 15px!important;
}
/*---------FOR THIS TEMPLATE ONLY---------*/
}
</style>
<script type="text/javascript">
 window.onload = function() { <?php /* $print */ ?> }
</script>
</head>
<body bgcolor="#f2f2f2">
<!--Start of Subject-->
<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="display:none !important; visibility:hidden; mso-hide:all; font-size:1px; color:#ffffff; line-height:1px; max-height:0px; max-width:0px; opacity:0; overflow:hidden;"><span style="display:none !important; visibility:hidden; mso-hide:all; font-size:1px; color:#ffffff; line-height:1px; max-height:0px; max-width:0px; opacity:0; overflow:hidden;"></span></td>
    </tr>
  </tbody>
</table>
<!-- End of Subject--> 

<!-- Center Container -->
<table width="100%" style="table-layout: fixed;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="center"><!-- Nested Content--> 
        <!-- Start of Preheader -->
        
        
        <!-- End of Preheader --> 
        
        <!-- Start of Header-->
        
        <table width="100%" bgcolor="#D4D4D4" cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed;">
          <tbody>
            <tr>
              <td align="center"><table width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidthinner" style="width:580px">
                  <tbody>
                    <tr>
                      <td align="center"><table width="82" align="center" border="0" cellpadding="0" cellspacing="0" style="width:82px" class="devicewidth">
                          <tbody>
                            <tr>
                              <td align="center" style="padding:20px 0;"><img src="https://cprv-egifting.com/assets/images/soneva/Soneva.png" alt="Soneva Logo" border="0" width="160" height="87" style="width:160px; height:87px; display:block; border:none; outline:none; text-decoration:none;"></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        
        <!-- End of Header --> 
        
        <!-- Start of Top linning -->
        
        <table width="100%" bgcolor="#D4D4D4" cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed; padding-top: 10px;">
          <tbody>
            <tr>
              <td align="center"><table width="600" bgcolor="#63666A" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="width:600px;">
                  <tbody>
                    <tr>
                      <td bgcolor="#63666A" height="6" valign="bottom"></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        
        <!-- End of Top linning -->
        
        <table bgcolor="#D4D4D4" width="100%" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;">
          <tbody>
            <tr>
              <td width="100%" align="center"><!-- Title -->
                
                <table align="center" bgcolor="#EDEDED" border="0" cellpadding="0" cellspacing="0" width="600" class="devicewidth" style="width:600px">
                  <tbody>
                    <tr>
                      <td align="center"><table align="center" bgcolor="#EDEDED" border="0" cellpadding="0" cellspacing="0" width="460" class="devicewidth" style="width:460px">
                          <tbody>
                            <!-- Text -->
                            <tr>
                              <td align="center" style="font-family: Helvetica, arial, sans-serif; color:#63666A; mso-line-height-rule:exactly; padding: 41px 20px 21px 20px; text-align:center; font-size: 15px; letter-spacing: 0.2em;">You have received an eGift Card from {{ $first_name }}</td>
                            </tr>
                            <!-- Text -->
                            <tr>
                              <td align="center"><table align="center" bgcolor="#EDEDED" border="0" cellpadding="0" cellspacing="0" width="460" class="devicewidth" style="width:460px">
                                  <tbody>
                                    <tr>
                                      <td align="center" style="font-family: Helvetica, arial, sans-serif; color:#63666A; mso-line-height-rule:exactly; padding: 5px 20px 25px 20px; text-align:center; font-size: 12px; line-height:20px;"> Dear
                                        {{ $first_name_rec }},<br /><br />
                                        {{ $first_name }} wishes you wonderful travel memories with your loved ones and Soneva. You may redeem this Soneva eGift Card for your stay or any of the amazing experiences at Soneva. Simply book a stay and present this eGift Card together with an official ID to your Mr/Ms <a style="color:#63666A; text-decoration:none;">Friday</a> upon arrival at Soneva.<br /><br />
                                        For inspired ideas on your next vacation, simply browse our experiences at 
                                        <a href="http://www.soneva.com/flipbook/sfr-experiences-brochure/" target="_blank" style="outline:none; text-decoration: none; color:#996017;">Soneva Fushi</a>, 
                                        <a href="http://www.soneva.com/flipbook/sjr-experiences-brochure/" target="_blank" style="outline:none; text-decoration: none; color:#996017;">Soneva Jani</a>, 
                                        <a href="http://www.soneva.com/flipbook/sia-experiences-brochure/" target="_blank" style="outline:none; text-decoration: none; color:#996017;">Soneva in Aqua</a> and 
                                        <a href="http://www.soneva.com/flipbook/skr-experiences-brochure/" target="_blank" style="outline:none; text-decoration: none; color:#996017;">Soneva Kiri</a>.</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- End of Text --> 
                            <!-- E-gift card -->
                            <?php if(isset($voucher_list)) { ?>

                              <?php foreach ($voucher_list as $index => $item) { ?>
                                <?php foreach ($item as $key => $vc) { ?>
                                  <tr>
                                    <td align="center" style="padding-top:10px;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="400" class="devicewidthinner" style="width:400px; align-content:center; background-color:#ffffff;">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="top"><table width="390" align="center" border="0" cellpadding="0" cellspacing="0" style="width:390px;" class="devicewidthinner">
                                                <tbody>
                                                  <tr>
                                                    <td align="left" valign="top" style="font-family: Helvetica, arial, sans-serif; color:#63666A; mso-line-height-rule:exactly; padding: 5px 0; text-align:left; font-size: 12px; ">Item <?= $key+1?></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr>
                                            <td align="center" valign="top"><table width="390" align="center" border="0" cellpadding="0" cellspacing="0" style="width:390px;" class="devicewidthinner">
                                                <tbody>
                                                  <tr>
                                                    <td align="center" valign="top"><a href="#" target="_blank"><img src="<?= $image ?>" alt="" border="0" height="auto" width="390" style="width:390px; height:auto; display:block; border:none; outline:none; text-decoration:none;" class="imgresponsive"></a></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr align="center" valign="top">
                                            <td valign="top" align="center" ><table bgcolor="#ffffff" width="390" align="center" border="0" cellpadding="0" cellspacing="0" style="width:390px;" class="devicewidthinner">
                                                <tbody>
                                                  <tr>
                                                    <td align="center" valign="top" bgcolor="#ffffff" class="devicewidth" style="padding: 10px 15px 20px 15px;"><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">Voucher ID:</td>
                                                          </tr>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#000000; padding: 0 25px 0 25px; text-align:left;" class="f21"><?= $vc['voucherid'] ?></td>
                                                          </tr>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">
                                                            <?= isset($occasion) && $occasion === 'Experience' ? 'Experience' : 'Value' ?>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <?php if (isset($isComplimentaryNight) && $isComplimentaryNight === true) { ?>
                                                              <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                <?= $no_of_night_balance ?>   <?= ($voucher_prefix_type == 'complimentary') ? 'Complimentary Night(s)' : 'Night(s)' ?>
                                                                  
                                                              </td>
                                                            <?php }else{ ?>
                                                              <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                <?= $currency ?> <?= number_format($amount) ?>
                                                              </td>
                                                            <?php } ?>
                                                          </tr>

                                                          <?php if (isset($isComplimentaryNight) && $isComplimentaryNight === true) { ?>
                                                            <?php if (isset($property) && $property !== '') { ?>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">
                                                                Property
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                  <?= isset($property) ? $property : '-' ?>
                                                                </td>
                                                              </tr>
                                                            <?php } ?>

                                                            <?php if (isset($inclusions) && $inclusions !== '') { ?>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">
                                                                Inclusions
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                  <?= isset($inclusions) ? $inclusions : '-' ?>
                                                                </td>
                                                              </tr>
                                                            <?php } ?>                                                      
                                                          <?php } ?>

                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">Expiry Date</td>
                                                          </tr>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21"><?= date('F j, Y', $expiry_date) ?></td>
                                                          </tr>
                                                        </tbody>
                                                      </table></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          
                                        </tbody>
                                      </table></td>
                                  </tr>
                                <?php } ?>
                              <?php } ?>
                            
                            <?php }else{ ?>
                                  <tr>
                                    <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="400" class="devicewidthinner" style="width:400px; align-content:center;">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="top"><table width="390" align="center" border="0" cellpadding="0" cellspacing="0" style="width:390px;" class="devicewidthinner">
                                                <tbody>
                                                  <tr>
                                                    <td align="center" valign="top"><a href="#" target="_blank"><img src="<?= $image ?>" alt="" border="0" height="auto" width="390" style="width:390px; height:auto; display:block; border:none; outline:none; text-decoration:none;" class="imgresponsive"></a></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr align="center" valign="top">
                                            <td valign="top" align="center" ><table bgcolor="#ffffff" width="390" align="center" border="0" cellpadding="0" cellspacing="0" style="width:390px;" class="devicewidthinner">
                                                <tbody>
                                                  <tr>
                                                    <td align="center" valign="top" bgcolor="#ffffff" class="devicewidth" style="padding: 10px 15px 20px 15px;"><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">Voucher ID:</td>
                                                          </tr>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#000000; padding: 0 25px 0 25px; text-align:left;" class="f21"><?= $voucher_id ?></td>
                                                          </tr>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">
                                                            <?= isset($occasion) && $occasion === 'Experience' ? 'Experience' : 'Value' ?>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <?php if (isset($isComplimentaryNight) && $isComplimentaryNight === true) { ?>
                                                              <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                <?= $no_of_night_balance ?>   <?= ($voucher_prefix_type == 'complimentary') ? 'Complimentary Night(s)' : 'Night(s)' ?>
                                                                  
                                                              </td>
                                                            <?php }else{ ?>
                                                              <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                <?= $currency ?> <?= number_format($amount) ?>
                                                              </td>
                                                            <?php } ?>
                                                          </tr>

                                                          <?php if (isset($isComplimentaryNight) && $isComplimentaryNight === true) { ?>
                                                            <?php if (isset($property) && $property !== '') { ?>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">
                                                                Property
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                  <?= isset($property) ? $property : '-' ?>
                                                                </td>
                                                              </tr>
                                                            <?php } ?>

                                                            <?php if (isset($inclusions) && $inclusions !== '') { ?>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">
                                                                Inclusions
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21">
                                                                  <?= isset($inclusions) ? $inclusions : '-' ?>
                                                                </td>
                                                              </tr>
                                                            <?php } ?>                                                      
                                                          <?php } ?>

                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:11px; line-height:14px; color:#000000; padding: 10px 25px 0 25px; text-align:left;" class="f14">Expiry Date</td>
                                                          </tr>
                                                          <tr>
                                                            <td align="center" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size:16px; font-weight:bold; line-height:20px; color:#202020; padding: 0 25px 0 25px; text-align:left;" class="f21"><?= $expiry_date ?></td>
                                                          </tr>
                                                        </tbody>
                                                      </table></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          
                                        </tbody>
                                      </table></td>
                                  </tr>
                            <?php } ?>

                            <!--End of E-gift card --> 
                            <?php if (isset($message) && $message !== '') { ?>
                            <tr>
                              <td align="center"><table bgcolor="#EDEDED" align="center" width="560" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="40" style="font-size:1px; line-height:40px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            
                            <tr>
                              <td align="center"><table bgcolor="#f5f5f5" align="center" border="0" cellpadding="0" cellspacing="0" width="560" class="devicewidthinner" style="width:560px; border:none; margin:0; table-layout: fixed;">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="35" style="font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif; font-size: 12px; color:#63666A; text-align: center; padding: 0 20px 0 20px; font-weight:bold;">Message from
                                        {{ $first_name }}
                                        :</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table bgcolor="#EDEDED" align="center"  width="560" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table bgcolor="#f5f5f5" align="center" border="0" cellpadding="0" cellspacing="0" width="560px" class="devicewidthinner" style="width:560px; border:none; table-layout: fixed;">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="50" style="font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif; font-size: 12px; color:#63666A; text-align: center; padding: 0 20px 5px 20px;"><?= $message ?> </td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            
                            <?php } ?>
                            <tr>
                              <td align="center"><table bgcolor="#EDEDED" align="center" width="560" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="20" style="font-size:1px; line-height:20px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>

                            <tr>
                              <td align="center"><table align="center" bgcolor="#EDEDED" border="0" cellpadding="0" cellspacing="0" width="460" class="devicewidth" style="width:460px">
                                  <tbody>
                                    <tr>
                                      <td align="center" style="font-family: Helvetica, arial, sans-serif; color:#63666A; mso-line-height-rule:exactly; text-align:center; font-size: 12px; line-height:20px;">
                                        Thank {{ $first_name }} by clicking <a href="mailto:{{ $email }}?subject=Thanks for your eGift Card!" target="_blank" style="outline:none; text-decoration: none; color:#996017;">here</a>.  </td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table bgcolor="#EDEDED" align="center" width="560" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="40" style="font-size:1px; line-height:40px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table>
                
                <!-- End of Title --></td>
            </tr>
          </tbody>
        </table>
       
        
        <!-- Start of Content -->
        
        <table align="center" bgcolor="#D4D4D4" width="100%" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed; border:none; margin:0 auto;">
          <tbody>
            <tr>
              <td width="100%" align="center"><table bgcolor="#ffffff" align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="devicewidth" style="width:600px; table-layout:fixed;">
                  <tbody>
                    <tr>
                      <td align="center"><table bgcolor="#ffffff" align="center" border="0" cellpadding="0" cellspacing="0" width="560" class="devicewidthinner" style="width:560px; table-layout:fixed;">
                          <tbody>
                            <tr>
                              <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="560px" class="devicewidth" style="width:520px; table-layout:fixed;">
                                  <tbody>
                                    <tr>
                                      <td align="center" class="devicewidthinner" style="font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif; font-size: 16px; line-height:17px; color:#996017; text-align: center; padding:35px 15px 35px 15px;"> HOW TO REDEEM</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="560px" class="devicewidth" style="width:560px; border:none; margin:0 auto; table-layout: fixed;">
                                  <tbody>
                                    <tr>
                                      <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="510px" class="devicewidth" style="width:510px; border:none; table-layout:fixed;">
                                          <tbody>
                                            <tr>
                                              <td align="center"><table align="left" border="0" cellpadding="0" cellspacing="0" width="253px" class="devicewidth" style="width:253px; ">
                                                  <tbody>
                                                    <!-- seperator -->
                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="10" style="font-size:1px; line-height:10px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- end of seperator --> 
                                                    
                                                    <!-- image -->
                                                    <tr>
                                                      <td height="43" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="54px" class="devicewidth" style="width:54px;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center"><img src="https://cprv-egifting.com/assets/images/ico-book.jpg" alt="Book a stay" style="display:block; width:50px; height: 41px; border:none;" class="imgresponsive c1" height="41" width="50" border="0"></td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- End of image --> 
                                                    
                                                    <!-- seperator -->
                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="15" style="font-size:1px; line-height:15px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- end of seperator --> 
                                                    
                                                    <!-- text -->
                                                    <tr>
                                                      <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="120px" class="devicewidth" style="width:120px; max-width:120px;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" class="devicewidthinner" style="font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif; font-size: 12px; line-height:17px; color:#63666A; text-align: center;">Book a stay via our website, your preferred travel partner or with our reservations team at <a href="mailto:reservations@soneva.com" target="_blank" style="outline:none; text-decoration: none; color:#996017;">reservations@soneva.com</a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- End of text --> 
                                                    
                                                    <!-- seperator -->
                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="30" style="font-size:1px; line-height:30px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- end of seperator -->
                                                    
                                                  </tbody>
                                                </table>
                                                
                                                <!--[if mso]></td><td><![endif]--> 
                                                <!-- 1st column end --> 
                                                
                                                <!-- 2nd column-->
                                                
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="253px" class="devicewidth" style="width:253px; border-left: 1px solid #d4d4d4;" id="borderHide">
                                                  <tbody>
                                                    <!-- seperator -->
                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="10" style="font-size:1px; line-height:10px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- end of seperator --> 
                                                    
                                                    <!-- image -->
                                                    <tr>
                                                      <td height="43" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="47px" class="devicewidth" style="width:47px;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center"><img src="https://cprv-egifting.com/assets/images/ico-checkin.jpg" alt="Check in" style="display:block; width:50px; height: 41px; border:none;" class="imgresponsive c2" height="41" width="50" border="0"></td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- End of image --> 
                                                    
                                                    <!-- seperator -->
                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="15" style="font-size:1px; line-height:15px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- end of seperator --> 
                                                    
                                                    <!-- text -->
                                                    <tr>
                                                      <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="120" class="devicewidth" style="width:120px; max-width:120px;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" class="devicewidthinner" style="font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif; font-size: 12px; line-height:17px; color:#63666A; text-align: center;">Present your Soneva eGift Card with an official ID to your Mr/Ms Fr&#8203;iday upon check in</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- End of text --> 
                                                    
                                                    <!-- seperator -->
                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="30" style="font-size:1px; line-height:30px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    <!-- end of seperator -->
                                                    
                                                  </tbody>
                                                </table>
                                                
                                                <!-- End of 2nd Column --> 
                                                
                                                <!--[if mso]></td><td><![endif]--> 
                                                <!-- Spacing end --> 
                                                
                                                <!-- 3rd column-->
                                                
                                                <!-- <table align="left" border="0" cellpadding="0" cellspacing="0" width="169px" class="devicewidth" style="width:169px;">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="10" style="font-size:1px; line-height:10px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>

                                                    <tr>
                                                      <td height="120" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="120" class="devicewidth" style="width:120px;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center"><img src="qrcode/{{ $voucher_id }}" alt="QR code" style="display:block; width:120px; height: 120px; border:none;" class="c3" height="auto" width="120" border="0"></td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>

                                                    <tr>
                                                      <td align="center"><table width="160" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" height="30" style="font-size:1px; line-height:30px;">&nbsp;</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                    
                                                  </tbody>
                                                </table> -->
                                                
                                                <!--[if mso]></td><td><![endif]--> 
                                                <!-- 3rd column end --></td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        
        <!-- end of Content--> 
        
        <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#D4D4D4">
          <tbody>
            <tr>
              <td align="center"><table class="devicewidth" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="width:600px;">
                  <tbody>
                    <tr>
                      <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="560px" class="devicewidth" style="width:560px;table-layout: fixed;">
                          <tbody>
                          <!-- seperator -->
                            <tr>
                              <td align="center"><table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" style="width:560px;">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="40" style="font-size:1px; line-height:40px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- end of seperator -->
                            
                            <!-- Table row 1-->
                            <tr>
                              <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" width="560" style="width:560px;">
                                  <tbody>
                                    <tr>
                                      <td align="center" class="space" height="34" bgcolor="#d4d4d4" style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color: #63666A; text-align: left; padding-left:20px; font-weight:bold;">Need Help?</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- End of Table row 1--> 
                            
                            <!-- seperator -->
                            <tr>
                              <td align="center"><table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" style="width:560px;">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="2" style="font-size:1px; line-height:2px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- end of seperator --> 
                            
                            <!-- Table row 2-->
                            <tr>
                              <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="560" class="devicewidth" style="width:560px; border:none;">
                                  <tbody>
                                    <tr>
                                      <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="560" style="width:560px;">
                                          <tbody>
                                            <tr>
                                              <td align="center" width="100%"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="560" style="width:560px;">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"><!-- start of left column -->
                                                        
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="279" style="width:279px;">
                                                          <tbody>
                                                            <tr>
                                                              <td width="100%" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" width="279" style="width:279px;">
                                                                  <tbody>
                                                                    <tr>
                                                                      <td align="center" id="bottom1" class="space" height="34" bgcolor="#f5f5f5" style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color:#63666A; text-align: left; padding-left:20px; font-weight:bold;">Reservations</td>
                                                                    </tr>
                                                                  </tbody>
                                                                </table></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                        
                                                        <!--[if mso]></td><td><![endif]--><!-- end of left column --><!-- start of right column -->
                                                        
                                                        <table align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="279" style="width:279px;">
                                                          <tbody>
                                                            <tr>
                                                              <td width="100%" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" width="279" style="width:279px;">
                                                                  <tbody>
                                                                    <tr>
                                                                      <td align="center" id="top" class="space" height="34" bgcolor="#f5f5f5"  style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color:#63666A; text-align: left; padding-left:20px; "><a style="color:#63666A; text-decoration:none;">+91 124 4511000</a></td>
                                                                    </tr>
                                                                  </tbody>
                                                                </table></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                        
                                                        <!-- end of right column --></td>
                                                    </tr>
                                                  </tbody>
                                                </table></td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- End of Table row 2 --> 
                            
                            <!-- seperator -->
                            <tr>
                              <td align="center"><table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" style="width:560px;">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="2" style="font-size:1px; line-height:2px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- end of seperator --> 
                            
                            <!-- Table row 2-->
                            <tr>
                              <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="560" class="devicewidth" style="width:560px; border:none;">
                                  <tbody>
                                    <tr>
                                      <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="560" style="width:560px;">
                                          <tbody>
                                            <tr>
                                              <td align="center" width="100%"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="560" style="width:560px;">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"><!-- start of left column -->
                                                        
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="279" style="width:279px;">
                                                          <tbody>
                                                            <tr>
                                                              <td width="100%" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" width="279" style="width:279px;">
                                                                  <tbody>
                                                                    <tr>
                                                                      <td align="center" id="bottom1" class="space" height="65" bgcolor="#f5f5f5" style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color:#63666A; text-align: left; padding-left:20px; font-weight:bold;">Toll-Free Reservations<br class="mobile-hide" />
                                                                        &nbsp;</td>
                                                                    </tr>
                                                                  </tbody>
                                                                </table></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                        
                                                        <!--[if mso]></td><td><![endif]--><!-- end of left column --><!-- start of right column -->
                                                        
                                                        <table align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="279" style="width:279px;">
                                                          <tbody>
                                                            <tr>
                                                              <td width="100%" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" width="279" style="width:279px;">
                                                                  <tbody>
                                                                    <tr>
                                                                      <td align="center" id="top" class="space" height="65" bgcolor="#f5f5f5" style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color:#63666A; text-align: left; padding-left:20px;"><a style="color:#63666A; text-decoration:none;">China: 400 1209 100</a><br/>
                                                                        <a style="color:#63666A; text-decoration:none;">Thailand: 180 001 2068</a><br/>
                                                                        <a style="color:#63666A; text-decoration:none;">UK: 0 800 048 8044</a></td>
                                                                    </tr>
                                                                  </tbody>
                                                                </table></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                        
                                                        <!-- end of right column --></td>
                                                    </tr>
                                                  </tbody>
                                                </table></td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- End of Table row 2 --> 
                            
                            <!-- seperator -->
                            <tr>
                              <td align="center"><table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" style="width:560px;">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="2" style="font-size:1px; line-height:2px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- end of seperator --> 
                            
                            <!-- Table row 3 -->
                            
                            <tr>
                              <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="560" style="width:560px;">
                                  <tbody>
                                    <tr>
                                      <td align="center" width="100%"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="560" style="width:560px;">
                                          <tbody>
                                            <tr>
                                              <td align="center"><!-- start of left column -->
                                                
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="279" style="width:279px;">
                                                  <tbody>
                                                    <tr>
                                                      <td width="100%" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" width="279" style="width:279px;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" id="bottom" class="space" height="34" bgcolor="#f5f5f5" style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color:#71787f; text-align: left; padding-left:20px; font-weight:bold;">Email</td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                                
                                                <!--[if mso]></td><td><![endif]--><!-- end of left column --><!-- start of right column -->
                                                
                                                <table align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth" width="279" style="width:279px;">
                                                  <tbody>
                                                    <tr>
                                                      <td width="100%" align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner" width="279" style="width:279px;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="center" id="top" class="space" height="34" bgcolor="#f5f5f5"  style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color:#996017; text-align: left; padding-left:20px; "><a href="mailto:sonevaegifting@soneva.com" style="text-decoration:none; color:#996017;">sonevaegifting@soneva.com</a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table></td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                                
                                                <!-- end of right column --></td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- End of Table row 3 --> 
                            
                            <!-- seperator -->
                            <tr>
                              <td align="center"><table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" style="width:560px;">
                                  <tbody>
                                    <tr>
                                      <td align="center" height="40" style="font-size:1px; line-height:40px;">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- end of seperator -->
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        
        <!-- Start of TC -->
        
        <table bgcolor="#D4D4D4" width="100%" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;">
          <tbody>
            <tr>
              <td align="center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="devicewidth" style="width:600px;">
                  <tbody>
                    <tr>
                      <td><table bgcolor="ffffff" valign="bottom" align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="devicewidth" style="width:600px;">
                          <tbody>
                            <tr>
                              <td align="center"><img src="https://cprv-egifting.com/assets/images/soneva/tnc.jpg" alt="Terms & Conditions" border="0" width="600" height="157" style="width:600px; height:157px; display:block; border:none; outline:none; text-decoration:none;" class="imgresponsive"></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        
        <!-- end of TC--> 
        
        <!-- Start of Terms -->
        
        <table width="100%" bgcolor="#D4D4D4" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="right-image" style="table-layout: fixed;">
          <tbody>
            <tr>
              <td align="center"><table bgcolor="#f5f5f5" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="width:600px;">
                  <tbody>
                    <tr>
                      <td width="100%" align="center"><table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="width:600px;">
                          <tbody>
                            <tr>
                              <td align="center"><!-- start of right column -->
                                
                                <table width="600" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width:600px;">
                                  <tbody>
                                    <tr>
                                      <td align="center"><table width="560" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width:560px;">
                                          <tbody>
                                            <!-- title -->
                                            <tr>
                                              <td colspan="4" style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color: #282828; text-align:left; line-height: 24px;">&nbsp;</td>
                                            </tr>
                                            <!-- end of title --> 
                                            <?php if(isset($email_tnc) && $email_tnc !== '') { ?>
                                              <!-- Spacing -->
                                              <tr>
                                                <td width="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                <td width="auto" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                <td width="40" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                <td width="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                              </tr>
                                              <!-- /Spacing --> 

                                              <tr>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;" valign="top">&nbsp;</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;"><?= $email_tnc ?></td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                            <?php }else{ ?>
                                              <!-- Spacing -->
                                              <tr>
                                                <td width="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                <td width="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                <td width="auto" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                <td width="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                              </tr>
                                              <!-- /Spacing --> 
                                              <!-- content -->
                                              <tr>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;" valign="top">&nbsp;</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;" valign="top">1</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva eGift Vouchers are non-transferable and can only be redeemed by the recipient indicated on the Soneva eGift Voucher at the time of voucher purchase.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <!-- end of content --> 
                                              <!-- Spacing -->
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">2</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva eGift Vouchers are redeemable for in-house guests against their total hotel stay at the time of redemption.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">3</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva eGift Vouchers may also be redeemed for walk-in guests that are not staying in-house for hotel experiences including food and beverage at selected restaurants.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">4</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">The local exchange rate will be used to calculate the voucher value.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">5</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva eGift Vouchers may only be used for a single transaction; the full value must be redeemed at one time.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">6</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva eGift Vouchers may be combined with other forms of payment. Charges in excess of the voucher's stored value may be settled with cash or major credit cards accepted by Soneva.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">7</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva eGift Vouchers are not exchangeable for cash and have no value unless redeemed.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">8</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva is not responsible for lost or stolen Soneva eGift Vouchers.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">9</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva eGift Vouchers cannot be sold, are not transferable and cannot be scalped, auctioned, raffled, pledged or promoted as an incentive or reward by any third party as an inducement for any person or other entity to enter into any commercial or other arrangements with that third party. If a Soneva eGift Voucher is obtained through any of these methods, it will not be honoured and will be deemed invalid.</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">10</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Redemption of a Soneva eGift Voucher is subject to the resort’s availability, specific room category availability and ‘blackout’ periods (including school holidays and public holidays).</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                                <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">11</td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;"><table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                      <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">Soneva may amend these terms & conditions from time to time without prior notice. Any amendment will be effective immediately.</td>
                                                    </tr>
                                                  </table></td>
                                                <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              </tr>
                                            <?php } ?>
                                            <tr>
                                              <td height="30" valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              <td valign="top" style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                              <td style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #63666A; text-align:left; line-height: 16px;">&nbsp;</td>
                                            </tr>
                                            <!-- /Spacing --> 
                                            <!-- read more --> 
                                            <!-- end of read more -->
                                          </tbody>
                                        </table></td>
                                    </tr>
                                  </tbody>
                                </table>
                                
                                <!-- end of right column --></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        
        <!-- end of Terms--> 
        
        <!-- Start of copyright -->
        
        <table bgcolor="#D4D4D4" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
          <tbody>
            <tr>
              <td align="center"><table bgcolor="#ffffff" width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" style="width:600px;">
                  <tbody>
                    <tr>
                      <td style="font-size:12px; line-height:12px; padding: 10px 0 30px 0; font-family: Helvetica, arial, sans-serif; color:#63666A;" align="center" height="12">© 2017 Soneva. All Rights Reserved.</td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        
        <!-- End of coptright --></td>
    </tr>
  </tbody>
</table>
<!-- End Center Container -->

</body>
</html>
