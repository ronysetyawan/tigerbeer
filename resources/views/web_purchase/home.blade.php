@extends('web_purchase.layout')

@section('content')
@section('style')
<style>
  .footer-left{
    top: 60px;
  }

    .footer-right{
    top: 60px;
  }

  /* 1920 x 1080 */
  @media screen and (width: 1920px) and (height: 1080px){
    #home {
      background-image: url("{{ asset('assets/web-purchase/image/new-banner.png') }}");
      background-repeat: no-repeat;
      background-size: cover;
      height: 100%;
      padding-top: 365px;
      padding-bottom: 50px;
    }

    .home section .enjoy {
      margin-top: -130px;
      margin-bottom: 160px;
      margin-left: -470px;
      font-size: 32px;
      line-height: 1.2;
    }

    #prizes {
      background-image: url("{{ asset('assets/web-purchase/image/BG_Register.png') }}");
      background-repeat: no-repeat;
      background-size: cover;
      height: 100%;
      padding-top: 110px;
      padding-bottom: 125px;
    }

    #how-to-participate {
      background-image: url("{{ asset('assets/web-purchase/image/BG_Where_to_Redeem.png') }}");
      background-size: cover;
      padding-top: 60px;
      padding-bottom: 320px;
      margin-top: -3px;
    }

    #recipes {
      background-image: url("{{ asset('assets/web-purchase/image/recipes.png') }}");
      background-repeat: no-repeat;
      background-size: cover;
      height: 100%;
      padding-top: 214px;
      padding-bottom: 331px;
      margin-top: -6px;
    }

    .home section.faq {
      background-image: url("{{ asset('assets/web-purchase/image/faq-new.png') }}");
      background-repeat: no-repeat;
      background-size: cover;
      padding-top: 100px;
      padding-bottom: 280px;
    }

    body #contact {
      background-image: url("{{ asset('assets/web-purchase/image/contact-us-new.png') }}");
      background-repeat: no-repeat;
      background-size: cover;
      padding-top: 280px;
      padding-bottom: 194px;
      margin-top: -4px;
    }

    .footer-left{
      top: 130px;
    }

    .footer-right{
      top: 130px;
    }
  }


  @media screen and (min-width: 1152px){
    .carousel-control.left {
      left: -88px;
    }

    .carousel-control.right {
      right: -88px;
    }
  }

  /* ipad pro */
  @media screen and (width: 1024px) and (height: 1366px){
    .slider-img{
      width: 85%;
    }
    .carousel-control.left {
      left: -80px;
    }
    .carousel-control.right {
      right: -38px;
    }
  }

  @media screen and (max-width: 480px){
    #home {
      background-repeat: no-repeat;
      background-position: center;
    }

    .recipes-ipad{
      display: none;
    }

    .slider{
      display: block;
    }

    .rec{
      display: block;
    }

    .prizes-img{
      width: 100%;
    }
  }

  @media screen and (max-width: 992px) and (min-width:481px){
    #home{
      background-image: url("{{ asset('assets/web-purchase/image/Banner-mobile.png') }}");
      background-repeat: no-repeat;
      background-position: center;
      margin-top: 50px;
    }

    .recipes-ipad{
      display: block;
    }

    .recipes-ipad-desktop{
      display: none;
    }

    .ipad{
      margin-left: 38px;
    }

    .prizes-img{
      width: 50%;
    }
  }

  .panel-title{
    color: #F9E3BF;
  }

  .help-block{
    color: #F58337;
  }

  ::placeholder {
    color: #F58337;
    opacity: 1;
  }

  .form-group {
    margin-bottom: 8px;
  }
</style>    
@endsection
<div class="home" home-scroll>
    

<div class="home" ng-controller="HomeController"
  ng-init="countries = {{ json_encode($countries) }};"
>

  
  <section class="opening" id="home">
    <div class="container home-mobile">
      <div class="row">
        <!-- <div class="col-md-6">
        </div> -->
        <div class="col-md-4 col-sm-6 col-sm-offset-1 text-center text-white  recipes-desktop recipes-ipad-desktop">
          <!-- <h3><strong>Product Short Description</strong></h3> -->
          <h3 class="enjoy"><b>ENJOY 2 <br> COMPLIMENTARY <br> DRINKS <br> ON US AT SUNSET<b></h3>
          <!-- <img class="img-responsive" id="mbl-sp" src="{{ asset('assets/web-purchase/image/ENJOY.png') }}"> -->

          <!-- <p class="custom-p">We’ve all had great times at our favourite 
            F&B spots so now Tiger is starting this initiative to show our support back to our F&B community!
            <br><br>
            Every $10 contribution will go directly to your local F&B outlet. 
            In return, we will give you <b>2x Tiger FREE</b> at participating outlets*
          </p> -->

          <!-- <a href="{{ url('create') }}" class="btn btn-warning text-upercase btn-lg btn-more-padding-y">Show Your Support</a> -->
          <!-- <a href="{{ url('confirmation') }}" class="btn btn-warning text-upercase btn-lg show-support-btn">Contribute Now</a> -->
          
          <!-- <br><br>
          <div class="terms" style="padding-top: 20px;">
            <small><a href="{{ url('terms-and-conditions') }}" class="text-white" target="_blank">*Terms & Conditions</a> apply</small>
          </div> -->
        </div>
      </div>
    </div>
  </section>

  <section class="reward" id="prizes">
    <div class="container">
      <div class="row desktop">
   
        <div class="col-xs-12 text-center">
          <div class="container">
            <div class="row">
              <br>
              <p style="color: #754c6a;font-size: medium;"><b>
                Thank you for your bottle purchase. <br> 
                Here's 2 complimentary sunset drinks on us to help <br> the bars we love during these tough times.</b>
              </p>
              <p style="color: #754c6a;font-size: medium;"><b>
                Let's raise our glasses to celebrate our awesome <br> 
                community coming together in support of our local bars!</b>
              </p><br>
              <a href="{{ url('registration') }}" class="btn btn-warning btn-lg show-support-btn" style="background-color: #ff8c55;color: #fff;">REGISTER TO REDEEM</a>
            </div>
          </div>
        </div>

        <div class="col-xs-12 recipes-desktop recipes-ipad-desktop" style="margin-bottom: 5%;">
          <div class="container">
              <div class="row">
                  <div class="col-sm-2 col-xs-4">
                      <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                  </div>
                  <div class="col-sm-2 col-xs-4" style="top: 53px;">
                      <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                  </div>
                  <div class="col-sm-2 col-xs-4" style="top: 43px;">
                      <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                  </div>
                  <div class="col-sm-2 col-xs-4" style="top: 53px;">
                      <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                  </div>
                  <div class="col-sm-2 col-xs-4" style="top: 17px;">
                      <a href="https://www.milagrotequila.com/" target="_blank"><img id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                  </div>
                  <div class="col-sm-2 col-xs-4" style="top: 33px;">
                      <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                  </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row mobile" style="margin-top: 50px;">
        <div class="col-md-12 col-md-offset-1 text-center" style="color: #ed5751;">
          <h4><b>ENJOY 2 <br> COMPLIMENTARY <br> SUNSET DRINKS <br> ON US<b></h4>
        </div>
        <div class="col-md-12 col-md-offset-1" style="margin-top: 50px;">
          <!-- <img class="img-responsive how-it-work-img" src="{{ asset('assets/web-purchase/image/thanks.png') }}"> -->
          <p class="text-center" style="color: #754c6a;font-size: medium;"><b>
            Thank you for your bottle purchase. <br> 
            Here's 2 complimentary sunset drinks on us to help the bars we love during these tough times.</b>
          </p>
          <p class="text-center" style="color: #754c6a;font-size: medium;"><b>
            Let's raise our glasses to celebrate our awesome <br> 
            community coming together in support of our local bars!</b>
          </p>
        </div>
        <div class="col-md-12 text-center" style="margin-top: 50px;">
          <a href="{{ url('registration') }}" class="btn btn-warning btn-lg show-support-btn" style="background-color: #ff8c55;color: #fff;">REGISTER TO REDEEM</a>
        </div>
        <div class="col-xs-12 recipes-mobile recipes-ipad rec" style="margin-top: 5%;">
          <div class="container">
              <div class="col-xs-4" style="margin-top: 6%;">
                  <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="prizes-img" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
              </div>
              <div class="col-xs-4" style="top: 66px">
                  <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="prizes-img" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
              </div>
              <div class="col-xs-4" style="top: 59px">
                  <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="prizes-img" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
              </div>
          </div>
          <div class="container" style="margin-top: 6%;">
              <div class="col-xs-4" style="top: 45px">
                  <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="prizes-img" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
              </div>
              <div class="col-xs-4" style="top: 9px">
                  <a href="https://www.milagrotequila.com/" target="_blank"><img class="prizes-img" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
              </div>
              <div class="col-xs-4" style="top: 35px">
                  <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="prizes-img" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
              </div>
          </div>
      </div>  
      </div>
      <div class="col-xs-12 recipes-mobile recipes-ipad" style="text-align-last: center;">
        <div class="container">
            <div class="col-xs-4" style="margin-top: 6%;">
                <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="prizes-img" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
            </div>
            <div class="col-xs-4" style="top: 66px">
                <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="prizes-img" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
            </div>
            <div class="col-xs-4" style="top: 59px">
                <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="prizes-img" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
            </div>
        </div>
        <div class="container" style="margin-top: 6%;">
            <div class="col-xs-4" style="top: 45px">
                <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="prizes-img" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
            </div>
            <div class="col-xs-4" style="top: 9px">
                <a href="https://www.milagrotequila.com/" target="_blank"><img class="prizes-img" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
            </div>
            <div class="col-xs-4" style="top: 35px">
                <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="prizes-img" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
            </div>
        </div>
    </div>  
    </div>
  </section>

  <section class="how-to-participate" id="how-to-participate">
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-md-offset-3  support" style="padding-bottom: 20px; padding-top: 70px">
          <div class="row">
            <div class="col-md-12 col-sm-12">
            <!-- <img class="img-responsive" src="{{ asset('assets/web-purchase/image/WHERE TO REDEEM.png') }}"><br>
            <img class="img-responsive" src="{{ asset('assets/web-purchase/image/Raise a glass.png') }}"> -->
              <h3 style="color: #FCE8C2;text-align: center;"><strong>WHERE TO REDEEM YOUR DRINKS?</strong></h3>
              <h5 style="color: #FCE8C2;text-align: center;">Raise a glass to your favourite local bars! We have partnered with loads of the places our community calls home to help give back to those who gave us so many awesome moments.</h5>
            </div>
            </div>
            <div class="row" style="margin-top: 20px;">
              <div class="col-md-2 col-sm-3 col-xs-3 col-md-offset-2 recipes-desktop recipes-ipad-desktop">
                <div class="dropdown" ng-init="getRegion('')" style="margin-left: 50%;">
                  <button id="dLabel" type="button" class="btn btn-outline-warning btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FCE8C2;background-color: transparent;border-color: #FCE8C2;width: 264px;">
                    <span ng-if="selectedRegion == ''">Choose Your Purchased Bottle</span>
                    <span ng-if="selectedRegion != ''">@{{selectedRegion}}</span>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dLabel" style="min-width: 264px;">
                    <li ng-repeat="list in regionList" ng-click="getRegion(list.name)"><a href="javascript:void(0);">@{{ list.name }}</a></li>
                  </ul>
                </div>
                <!-- <input type="text" class="form-control" placeholder="Search" aria-label="" aria-describedby="basic-addon1"> -->
              </div>
              <div class="col-xs-12 text-center recipes-mobile recipes-ipad">
                <div class="container">
                  <div class="row">
                    <div class="dropdown" ng-init="getRegion('')">
                      <button id="dLabel" type="button" class="btn btn-outline-warning btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FCE8C2;background-color: transparent;border-color: #FCE8C2;width: 264px;">
                        <span ng-if="selectedRegion == ''">Choose Your Purchased Bottle</span>
                        <span ng-if="selectedRegion != ''">@{{selectedRegion}}</span>
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dLabel" style="min-width: 264px;">
                        <li ng-repeat="list in regionList" ng-click="getRegion(list.name)"><a href="javascript:void(0);">@{{ list.name }}</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- <input type="text" class="form-control" placeholder="Search" aria-label="" aria-describedby="basic-addon1"> -->
              </div>
            </div>
        </div>
        <div class="col-md-9 col-md-offset-2 store-list">
          <div class="store-container">
            <div ng-if="$index % 3 == 0" ng-repeat="value in regionData" class="row">
              <div class="col-md-4 col-sm-4 col-xs-12 text-white" style="margin-bottom:16px;color: #FCE8C2;">
                <p class="p-15px"><strong>@{{regionData[$index].name}}</strong></p>
                <p style="font-weight: 100;">@{{regionData[$index].address1}}</p>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12 text-white" style="margin-bottom:16px;color: #FCE8C2;">
                <p class="p-15px"><strong>@{{regionData[$index + 1].name}}</strong></p>
                <p style="font-weight: 100;">@{{regionData[$index + 1].address1}}</p>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12 text-white" style="margin-bottom:16px;color: #FCE8C2;">
                <p class="p-15px"><strong>@{{regionData[$index + 2].name}}</strong></p>
                <p style="font-weight: 100;">@{{regionData[$index + 2].address1}}</p>
              </div>
            </div>
            {{-- <div class="col-md-4 col-sm-6 col-xs-12 text-white" style="margin-bottom:16px;" ng-repeat="(key, value) in regionData">
            <p><strong>@{{value.name}}</strong></p>
              <p>@{{value.address1}}</p>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="recipes" id="recipes">
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-md-offset-3  support recipes-desktop" style="padding-bottom: 20px; padding-top: 20px;margin-left: 20%;">
          <div class="row">
            <div class="col-md-12 col-sm-12 recipes-ipad-desktop">
              <!-- <img class="img-responsive" src="{{ asset('assets/web-purchase/image/recipes-title.png') }}"><br>
              <img class="img-responsive" src="{{ asset('assets/web-purchase/image/recipes1.png') }}"> -->
              <h3 style="color: #F9E3BF;text-align: center;"><strong>RECIPES</strong></h3>
              <h5 style="color: #F9E3BF;text-align: center;">Need some more inspiration for awesome drinks you can make at home? Here's great list of bartender-approved recipes!</h5>
            </div>
          </div>
        </div>
        <div class="col-md-12 recipes-mobile recipes-ipad rec">
          <h3 style="color: #F9E3BF;text-align: center;"><strong>RECIPES</strong></h3>
          <h5 style="color: #F9E3BF;text-align: center;">Need some more inspiration for awesome drinks you can make at home? Here's great list of bartender-approved recipes!</h5>
        </div>
        <div id="carousel-example-generic" class="carousel slide recipes-desktop recipes-ipad-desktop" data-ride="carousel" style="margin-top: 110px;">
        
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="col-md-4">
                <a href="{{ url('recipes') }}/1"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 397@2x.png') }}" style="height: 265px;"></a>
              </div>
              <div class="col-md-4">
                <a href="{{ url('recipes') }}/2"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 381@2x.png') }}" style="height: 265px;"></a>
              </div>
              <div class="col-md-4">
                <a href="{{ url('recipes') }}/3"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 382@2x.png') }}" style="height: 265px;"></a>
              </div>
            </div>
            
            <div class="item">
              <div class="col-md-4">
                <a href="{{ url('recipes') }}/4"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 402@2x.png') }}" style="height: 265px;"></a>
              </div>
              <div class="col-md-4">
                <a href="{{ url('recipes') }}/5"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 401@2x.png') }}" style="height: 265px;"></a>
              </div>
              <div class="col-md-4">
                <a href="{{ url('recipes') }}/6"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 400@2x.png') }}" style="height: 265px;"></a>
              </div>
            </div>
          </div>
        
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="background-image: none;color: #F9E3BF;">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="background-image: none;color: #F9E3BF;">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

        <div id="carousel-example-generic-mobile" class="carousel slide recipes-mobile recipes-ipad slider" data-ride="carousel" style="margin-top: 30px;">
        
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="col-md-4" style="text-align: -webkit-center;">
                <a href="{{ url('recipes') }}/1"><img src="{{ asset('assets/web-purchase/image/Group 397@2x.png') }}" style="width:80%;"></a>
              </div>
            </div>
            
            <div class="item">
              <div class="col-md-4" style="text-align: -webkit-center;">
                <a href="{{ url('recipes') }}/2"><img src="{{ asset('assets/web-purchase/image/Group 381@2x.png') }}" style="width:80%;"></a>
              </div>
            </div>

            <div class="item">
              <div class="col-md-4" style="text-align: -webkit-center;">
                <a href="{{ url('recipes') }}/3"><img src="{{ asset('assets/web-purchase/image/Group 382@2x.png') }}" style="width:80%;"></a>
              </div>
            </div>
            
            <div class="item">
              <div class="col-md-4" style="text-align: -webkit-center;">
                <a href="{{ url('recipes') }}/4"><img src="{{ asset('assets/web-purchase/image/Group 402@2x.png') }}" style="width:80%;"></a>
              </div>
            </div>

            <div class="item">
              <div class="col-md-4" style="text-align: -webkit-center;">
                <a href="{{ url('recipes') }}/5"><img src="{{ asset('assets/web-purchase/image/Group 401@2x.png') }}" style="width:80%;"></a>
              </div>
            </div>

            <div class="item">
              <div class="col-md-4" style="text-align: -webkit-center;">
                <a href="{{ url('recipes') }}/6"><img src="{{ asset('assets/web-purchase/image/Group 400@2x.png') }}" style="width:80%;"></a>
              </div>
            </div>
          </div>
        
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="prev" style="background-image: none;color: #F9E3BF;">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="next" style="background-image: none;color: #F9E3BF;">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <div class="col-xs-12 recipes-desktop text-center" style="margin-top: 85px;">
          <div class="container">
            <div class="row">
              <a href="{{ url('recipes') }}" class="btn btn-md btn-warning show-support-btn" style="color: white;">SEE MORE RECIPES</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 recipes-mobile text-center" style="margin-top: 85px;">
          <div class="container">
            <div class="row">
              <a href="{{ url('recipes') }}" class="btn btn-md btn-warning show-support-btn" style="color: white;">SEE MORE RECIPES</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="faq" id="faqs">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 support text-center" style="padding-top: 50px">
          <!-- <img class="" src="{{ asset('assets/web-purchase/image/faq.png') }}"> -->
          <h3 class="text-center" style="color: #F9E3BF;"><strong>FREQUENTLY ASKED <br> QUESTIONS</strong></h3>
        </div>
        <!-- <h3 class="text-center text-white"><strong>FAQs</strong></h3> -->
        
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 support">
          <uib-accordion close-others="true" ng-init="faqs = {{ json_encode($faqs) }}">
            <div uib-accordion-group class="panel-default" ng-repeat="faq in faqs">
              <uib-accordion-heading>
                @{{ faq.title }} 
                <i class="pull-right glyphicon glyphicon-chevron-down recipes-desktop recipes-ipad-desktop" style="color: #F9E3BF;margin-right: -36px;"></i>
                <i class="pull-right glyphicon glyphicon-chevron-down recipes-mobile recipes-ipad" style="color: #F9E3BF;"></i>
              </uib-accordion-heading>
              <div ng-bind-html="faq.body" class="faq-body" style="color: #F9E3BF;"></div>
            </div>
          </uib-accordion>
        </div>
      </div>
    </div>
  </section>

  <!-- CONTACT -->
  <section id="contact" class="contact">

      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 support text-center">
            <h3 class="text-center" style="color: #F9E3BF;"><strong>LET'S GET IN TOUCH!</strong></h3>
          </div>
        </div>
      </div>
        <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-sm-12">
            <!--<div class="col-md-6">

              
            </div> -->
            <div class="col-xs-12 no-padding-left" ng-if="errors_contact.hasOwnProperty('name')" ng-if="errors_contact.hasOwnProperty('mobile')" ng-if="errors_contact.hasOwnProperty('email')" ng-if="errors_contact.hasOwnProperty('enquiry')" ng-if="errors_contact.hasOwnProperty('message')" ng-if="errors_contact.hasOwnProperty('captcha')">
              <div class="panel panel-default no-padding-left">
                <div class="panel-body" style="background-color: #fff;">
                  <div class="help-block" ng-if="errors_contact.hasOwnProperty('name')" ng-cloak><i class="fa fa-exclamation" style="color: red;"></i> @{{errors_contact.name}}</div>
                  <div class="help-block" ng-if="errors_contact.hasOwnProperty('mobile')" ng-cloak><i class="fa fa-exclamation" style="color: red;"></i> @{{errors_contact.mobile}}</div>
                  <div class="help-block" ng-if="errors_contact.hasOwnProperty('email')" ng-cloak><i class="fa fa-exclamation" style="color: red;"></i> @{{errors_contact.email}}</div>
                  <div class="help-block" ng-if="errors_contact.hasOwnProperty('enquiry')" ng-cloak><i class="fa fa-exclamation" style="color: red;"></i> @{{errors_contact.enquiry}}</div>
                  <div class="help-block" ng-if="errors_contact.hasOwnProperty('message')" ng-cloak><i class="fa fa-exclamation" style="color: red;"></i> @{{errors_contact.message}}</div>
                  <div class="help-block" ng-if="errors_contact.hasOwnProperty('captcha')" ng-cloak><i class="fa fa-exclamation" style="color: red;"></i> @{{errors_contact.captcha}}</div>
                </div>
              </div>
            </div>
            <div class="col-md-12">

              <div class="form-group" ng-class="{'has-error': errors_contact.hasOwnProperty('mobile')}">
                <div class="row">
                  <div class="col-md-5 no-padding-left">
                    <div class="form-group" ng-class="{'has-error': errors_contact.hasOwnProperty('name')}">
                      <input type="text" class="form-control" ng-model="form_contact.name" placeholder="Name"/>
                      <!-- <div class="help-block" ng-if="errors_contact.hasOwnProperty('name')" ng-cloak>@{{errors_contact.name}}</div> -->
                    </div>
                  </div>
                  <div class="col-md-3 no-padding-left">
                    <select class="form-control" ng-model="form_contact.country" ng-options="country.country_id as country.country_name for country in countries"></select>
                  </div>
                  <div class="col-md-4 mbl-phone no-padding-left">
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1" style="color: #F58337;font-weight: bold;">@{{ form_contact_calling_code }}</span>
                      <input type="text" class="form-control" aria-describedby="basic-addon1" ng-model="form_contact.mobile">
                    </div>
                  </div>
                </div>
                <!-- <div class="help-block" ng-if="errors_contact.hasOwnProperty('mobile')" ng-cloak>@{{errors_contact.mobile}}</div>-->
              </div>
              
              <div class="row">
                <div class="col-md-6 no-padding-left" style="margin-bottom: 8px;">
                  <div class="form-group" ng-class="{'has-error': errors_contact.hasOwnProperty('email')}">
                    <input type="email" class="form-control" ng-model="form_contact.email" placeholder="Email Address *"/>
                    <!-- <div class="help-block" ng-if="errors_contact.hasOwnProperty('email')" ng-cloak>@{{errors_contact.email}}</div> -->
                  </div>
                </div>
                <div class="col-md-6 no-padding-left" style="margin-bottom: 8px;">
                  <div class="form-group" ng-class="{'has-error': errors_contact.hasOwnProperty('enquiry')}">
                    <select class="form-control" name="enquiry" ng-model="form_contact.enquiry">
                      <option value="">Subject</option>
                      <option value="Voucher Enquiries">Voucher Enquiries</option>
                      <option value="Media Enquiries">Media Enquiries</option>
                      <option value="General Enquiries">General Enquiries</option>
                    </select>
                    <!-- <div class="help-block" ng-if="errors_contact.hasOwnProperty('enquiry')" ng-cloak>@{{errors_contact.enquiry}}</div> -->
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 no-padding-left" style="margin-bottom: 8px;">
                  <div class="form-group" ng-class="{'has-error': errors_contact.hasOwnProperty('message')}">
                    <textarea name="message" class="form-control msg-textarea" rows="4" ng-model="form_contact.message" placeholder="Message"></textarea>
                    <!-- <div class="help-block" ng-if="errors_contact.hasOwnProperty('message')" ng-cloak>@{{errors_contact.message}}</div> -->
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-4 no-padding-left">
                  <div class="form-group" ng-class="{'has-error': errors_contact.hasOwnProperty('captcha')}">
                    <input type="text" class="form-control" ng-model="form_contact.captcha" placeholder="Verification">
                  </div>
                </div>

                <div class="col-xs-3 recipes-desktop no-padding-left">
                  <img src="@{{ captchaimage }}" id="recaptcha" style="margin-top: 0px;width: fit-content;">
                  <!-- <div class="help-block" ng-if="errors_contact.hasOwnProperty('captcha')" ng-cloak>@{{errors_contact.captcha}}</div> -->
                </div>

                <!-- <div class="col-xs-2 recipes-desktop">
                  <span><input type="button" ng-click="getCaptcha()" class="btn btn-primary btn-reload-captcha" value="Refresh Code" id="btn-reload" style="margin-top: 0px;"/></span>
                </div> -->

                <div class="col-xs-3 recipes-desktop ipad">
                  <button class="btn btn-danger btn-lg" ng-click="submitFormContact()" style="margin-top: 0px;outline: none;">Submit</button>
                </div>

                <div class="col-xs-3 recipes-mobile" style="margin-left: -21px;">
                  <img src="@{{ captchaimage }}" id="recaptcha" style="margin-top: 0px;width: 107px;">
                </div>

                <div class="col-xs-3 recipes-mobile" style="margin-left: 34px;">
                  <button class="btn btn-danger btn-lg" ng-click="submitFormContact()" style="margin-top: 0px;outline: none;">Submit</button>
                </div>
              </div>
            </div>
          </div>  
          <div class="col-md-12 recipes-desktop recipes-ipad-desktop">
            <div class="container">
              <div class="row">
                <div class="col-sm-2">
                  <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152@2x.png') }}"></a>
                </div>
                <div class="col-sm-2" style="top: 53px;">
                  <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151@2x.png') }}"></a>
                </div>
                <div class="col-sm-2" style="top: 43px;">
                  <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147@2x.png') }}"></a>
                </div>
                <div class="col-sm-2" style="top: 53px;">
                  <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148@2x.png') }}"></a>
                </div>
                <div class="col-sm-2" style="top: 17px;">
                  <a href="https://www.milagrotequila.com/" target="_blank"><img id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149@2x.png') }}"></a>
                </div>
                <div class="col-sm-2" style="top: 33px;">
                  <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150@2x.png') }}"></a>
                </div>
              </div>
            </div>
          </div> 
          <div class="col-xs-12 recipes-mobile recipes-ipad rec">
            <div class="container">
                <div class="col-xs-4" style="margin-top: 6%;">
                    <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="prizes-img" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                </div>
                <div class="col-xs-4" style="top: 66px">
                    <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="prizes-img" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                </div>
                <div class="col-xs-4" style="top: 59px">
                    <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="prizes-img" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                </div>
            </div>
            <div class="container" style="margin-top: 6%;">
                <div class="col-xs-4" style="top: 45px">
                    <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="prizes-img" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                </div>
                <div class="col-xs-4" style="top: 9px">
                    <a href="https://www.milagrotequila.com/" target="_blank"><img class="prizes-img" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                </div>
                <div class="col-xs-4" style="top: 35px">
                    <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="prizes-img" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                </div>
            </div>
        </div>  
      </div>
    </div>
    <div class="col-md-6 footer-left text-white">
      &copy; 2020 William Grant & Sons. <br> All Rights Reserved.
    </div>
    <div class="col-md-6 footer-right text-white">
        <ul class="list-inline footer-menu">
            <li><a href="{{ url('privacy-policy') }}" target="_blank">Privacy Policy</a></li>
            <li><a href="{{ url('terms-and-conditions') }}" target="_blank">Terms of Use</a></li>
        </ul>
    </div>
  </section>
</div>

</div>
@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/home.js') }}"></script>
@endsection
