@extends('web_purchase.layout')

@section('content')
@section('style')
<style>
  @media screen and (max-width: 480px){
    #reg_header {
      background-repeat: no-repeat;
      background-position: center;
    }

    body #reg_success {
        padding-top: 25px;
    }

    .prizes-img{
      width: 100%;
    }
  }

  @media screen and (max-width: 992px) and (min-width:481px){
    #reg_header{
        background-image: url("{{ asset('assets/web-purchase/image/Banner-mobile.png') }}");
        background-repeat: no-repeat;
        background-position: center;
        margin-top: 49px;
        background-size: cover;
    }

    .recipes-ipad{
      display: block;
    }

    .recipes-ipad-desktop{
      display: none;
    }

    .ipad{
      margin-left: 38px;
    }
  }
</style>    
@endsection
<div class="home" home-scroll>
    

    <div class="home" ng-controller="HomeController" ng-init="countries = {{ json_encode($countries) }};">

        <section class="opening" id="reg_header">
            <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-sm-offset-1 text-center text-white recipes-desktop">
                    <h3 style="visibility: hidden;"><b>ENJOY 2 <br> COMPLIMENTARY <br> DRINKS <br> ON US AT SUNSET<b></h3>
                    <!-- <img class="img-responsive" id="mbl-sp" src="{{ asset('assets/web-purchase/image/ENJOY.png') }}"> -->
                </div>
            </div>
            </div>
        </section>

        <section id="reg_success" class="reg_success reg_success_mobile">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="color: #F9E3BF;text-align: center;font-weight: bold;">DRINK RECIPES</h3>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1 col-xs-6 recipes-desktop recipes-ipad-desktop" style="margin-bottom: 2%;margin-left: 120px;">
                        <a href="{{ url('recipes') }}/1"><img style="width: 100%;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 397@2x.png') }}"></a> 
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-desktop recipes-ipad-desktop" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/2"><img style="width: 100%;height: 192px;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 381@2x.png') }}"></a>
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-desktop recipes-ipad-desktop" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/3"><img style="width: 100%;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 382@2x.png') }}"></a>
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-xs-6 recipes-desktop recipes-ipad-desktop" style="margin-bottom: 2%;margin-left: 120px;">
                        <a href="{{ url('recipes') }}/4"><img style="width: 100%;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 402@2x.png') }}"></a> 
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-desktop recipes-ipad-desktop" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/5"><img style="width: 100%;height: 192px;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 401@2x.png') }}"></a>
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-desktop recipes-ipad-desktop" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/6"><img style="width: 220px;margin-left: -9px;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 400@2x.png') }}"></a>
                    </div>

                    
                    <div class="col-md-3 col-md-offset-1 col-xs-6 recipes-mobile recipes-ipad" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/1"><img style="width: 100%;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 397@2x.png') }}"></a> 
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-mobile recipes-ipad" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/2"><img style="width: 100%;margin-bottom: 4%;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 381@2x.png') }}"></a>
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-mobile recipes-ipad" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/3"><img style="width: 100%;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 382@2x.png') }}"></a>
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-xs-6 recipes-mobile recipes-ipad" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/4"><img style="width: 100%;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 402@2x.png') }}"></a> 
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-mobile recipes-ipad" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/5"><img style="width: 105%;margin-left: -5px;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 401@2x.png') }}"></a>
                    </div>
                    <div class="col-md-3 col-xs-6 recipes-mobile recipes-ipad" style="margin-bottom: 2%;">
                        <a href="{{ url('recipes') }}/6"><img style="width: 105%;margin-left: -5px;" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 400@2x.png') }}"></a>
                    </div>

                    <div class="col-xs-12 recipes-desktop recipes-ipad-desktop" style="margin-top: 2%;">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-2 col-xs-4">
                                    <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 53px;">
                                    <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 43px;">
                                    <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 53px;">
                                    <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 17px;">
                                    <a href="https://www.milagrotequila.com/" target="_blank"><img id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 33px;">
                                    <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="col-xs-12 recipes-mobile recipes-ipad">
                        <div class="container">
                            <div class="col-xs-4" style="margin-top: 6%;">
                                <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="prizes-img" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 46px">
                                <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="prizes-img" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 41px">
                                <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="prizes-img" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                            </div>
                        </div>
                        <div class="container" style="margin-top: 6%;">
                            <div class="col-xs-4" style="top: 45px">
                                <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="prizes-img" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 9px">
                                <a href="https://www.milagrotequila.com/" target="_blank"><img class="prizes-img" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 35px">
                                <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="prizes-img" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 footer-left text-white" style="top: 60px;">
                &copy; 2020 William Grant & Sons. <br> All Rights Reserved.
            </div>
            <div class="col-md-6 footer-right text-white" style="top: 60px;">
                <ul class="list-inline footer-menu">
                    <li><a href="{{ url('privacy-policy') }}" target="_blank" style="color: #fff;">Privacy Policy</a></li>
                    <li><a href="{{ url('terms-and-conditions') }}" target="_blank" style="color: #fff;">Terms of Use</a></li>
                </ul>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/home.js') }}"></script>
@endsection
