@extends('web_purchase.layout')

@section('content')
<section class="section cart" ng-controller="CheckoutCtrl" ng-init="recipient_details = {{ json_encode($recipient_details) }}; titles = {{json_encode($titles)}}; countries = {{ json_encode($countries) }}">
    <div class="container">

        <div class="steps">
            <ul>
                <li>Your Detail</li><li>
                Gift Message</li><li>
                Recipient</li><li>
                Order Summary</li><li>
                Payment</li><li>
                Order Completed</li>
            </ul>
        </div>

        <div class="section-checkout section-checkout-sender" ng-if="pagePosition==1" ng-cloak>
            <h3>Your Details</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group" ng-init="form_sender.title = '{{ isset($sender['title'])?$sender['title']:'' }}'">
                                <label>Title</label>
                                <select class="form-control" ng-model="form_sender.title">
                                    <option ng-repeat="title in titles" value="@{{ title.value }}">@{{ title.label }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" ng-init="form_sender.first_name = '{{ isset($sender['first_name'])?$sender['first_name']:'' }}'">
                                <label>First Name<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_sender.first_name">
                                <label class="error-form" ng-if="errorSender.hasOwnProperty('first_name')">@{{errorSender.first_name}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" ng-init="form_sender.last_name = '{{ isset($sender['last_name'])?$sender['last_name']:'' }}'">
                                <label>Last Name<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_sender.last_name">
                                <label class="error-form" ng-if="errorSender.hasOwnProperty('last_name')">@{{errorSender.last_name}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group" ng-init="form_sender.email = '{{ isset($sender['email'])?$sender['email']:'' }}'">
                                <label>Email<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_sender.email">
                                <label class="error-form" ng-if="errorSender.hasOwnProperty('email')">@{{errorSender.email}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" ng-init="form_sender.confirm_email = '{{ isset($sender['confirm_email'])?$sender['confirm_email']:'' }}'">
                                <label>Confirm Email<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_sender.confirm_email">
                                <label class="error-form" ng-if="errorSender.hasOwnProperty('confirm_email')">@{{errorSender.confirm_email}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" ng-init="form_sender.country = '{{ isset($sender['country'])?$sender['country']:'' }}'">
                                <label>Country<span class="required">*</span></label>
                                <select class="form-control" ng-model="form_sender.country" ng-init="countries = {{ json_encode($countries) }}">
                                    <option ng-repeat="country in countries" value="@{{ country.value }}">@{{ country.label }}</option>
                                </select>
                                <label class="error-form" ng-if="errorSender.hasOwnProperty('country')">@{{errorSender.country}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group" ng-init="form_sender.mobile = '{{ isset($sender['mobile'])?$sender['mobile']:'' }}'">
                                <label>Mobile<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_sender.mobile">
                                <label class="error-form" ng-if="errorSender.hasOwnProperty('mobile')">@{{errorSender.mobile}}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-checkout section-checkout-message" ng-if="pagePosition==2" ng-cloak>
            <h3>Enter Recipient List</h3>
              <table class="table table-striped" ng-if="recipients.length > 0">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>Country</th>
                    <th>Mobile</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="recipient in recipients">
                    <td>@{{recipient.title}}</td>
                    <td>@{{recipient.first_name}}</td>
                    <td>@{{recipient.last_name}}</td>
                    <td>@{{recipient.email}}</td>
                    <td>@{{recipient.country_name}}</td>
                    <td>@{{recipient.mobile}}</td>
                    <td>
                        <button class="btn btn-primary" ng-click="editRecipient($index)">Edit</button>
                        <button class="btn btn-primary del-recipient" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" confirmation-function="deleteRecipient($index)">Remove</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            <div class="alert alert-success" ng-if="isSuccesUpdateRecipient">
              <strong>Successfully updated!</strong>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Title</label>
                                <select class="form-control" ng-model="form_recipient.title" ng-required="true">
                                    <option ng-repeat="title in titles" value="@{{ title.value }}">@{{ title.label }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>First Name<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_recipient.first_name" ng-required="true">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Last Name<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_recipient.last_name" ng-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Email<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_recipient.email" ng-required="true">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Confirm Email<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_recipient.email_confirm" ng-required="true">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Country<span class="required">*</span></label>
                                <select class="form-control" ng-init="countries = {{ json_encode($countries) }}" ng-model="form_recipient.country" ng-required="true">
                                    <option ng-repeat="country in countries" value="@{{ country.value }}">@{{ country.label }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Mobile<span class="required">*</span></label>
                                <input class="form-control" ng-model="form_recipient.mobile" ng-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary" ng-click="addRecipientToList(indexToUpdate)">@{{ textAddRecipient }}</button>
                                <button class="btn btn-primary" ng-click="resetRecipient()" ng-if="isUpdateRecipient">Cancel</button>
                            </div>
                        </div>
                    </div> 

                </div>
            </div>
            
        </div>

        
                
        <div class="section-checkout section-checkout-recipient" ng-if="pagePosition==3" ng-cloak>
            <h3>Recipient Details</h3>
            
            <div ng-repeat="(key, cart) in recipient_details">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <!--start content-->
                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    <ul class="list-inline">
                                        <li><img src="@{{cart.product_image}}" class="img-responsive"></li>
                                        <li>
                                            <ul class="list-unstyled">
                                                <li>@{{cart.product_title}}</li>
                                                <li>@{{cart.product_price}}</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Recipient<sup class="required">*</sup></label>
                                            <select class="form-control" ng-required="true"
                                                ng-options="recipient.first_name + ' ' + recipient.last_name for recipient in recipients track by recipient.created" 
                                                ng-change="selectRecipient(key)" 
        -                                       ng-model="form_recipient_detail[key].recipient">
                                                <option value="">--Select Recipient--</option>
                                            </select>
                                           <label class="error-form" ng-if="errorPrompt[key].indexOf('recipient') >= 0">Error! This field is required!</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Can not find?</label></br>
                                            <button class="btn btn-primary" ng-click="addRecipientModal(key)">Add New Recipient</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" ng-if="selected_recipient[key].name || selected_recipient[key].email || selected_recipient[key].mobile">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span ng-if="selected_recipient[key].name" ng-init="form_recipient_detail[key].recipient.name">@{{ selected_recipient[key].name }}<br/></span>
                                            <span ng-if="selected_recipient[key].email" ng-init="form_recipient_detail[key].recipient.email">@{{ selected_recipient[key].email }}<br/></span>
                                            <span ng-if="selected_recipient[key].mobile" ng-init="form_recipient_detail[key].recipient.mobile">@{{ selected_recipient[key].mobile }}<br/></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Gift Message</label>
                                            <textarea class="form-control" ng-model="form_recipient_detail[key].message"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label>When would you like to send this eGift card?<sup class="required">*</sup></label>
                                           <ul class="list-unstyled">
                                               <li>
                                                    <select class="form-control" ng-model="form_recipient_detail[key].send_type" 
                                                        ng-change="selectSendType(key)">
                                                        <option value="">--Select Send Type--</option>
                                                        <option value="send-now">Send Now</option>
                                                        <option value="future-send">Send at a Future Date</option>
                                                    </select>
                                               </li>
                                               <li ng-if="form_recipient_detail[key].send_type == 'future-send'">
                                                   <div class="input-group" ng-init="open_date_send_picker[key].opened = false">
                                                       <input type="text" class="form-control" uib-datepicker-popup="dd-MMMM-yyyy" ng-model="form_recipient_detail[key].date_send" 
                                                            datepicker-options="dateOptions" 
                                                            is-open="open_date_send_picker[key].opened" close-text="Close" alt-input-formats="altInputFormats" />
                                                       <span class="input-group-btn">
                                                           <button type="button" class="btn btn-default" ng-click="openSendDatePicker(key)"><i class="glyphicon glyphicon-calendar"></i></button>
                                                       </span>
                                                   </div>
                                               </li>
                                           </ul>
                                           <label class="error-form" ng-if="errorPrompt[key].indexOf('send_type') >= 0">Error! This field is required!</label>
                                       </div>
                                   </div>
                               </div>

                            </div>
                        </div>
                        
                        <!-- end content -->
                    </div>
                </div>

            </div>
            <label class="mandatory"><sup class="required">*</sup> Fields are mandatory</label>
        </div>

        

        <div class="section-checkout section-checkout-review" ng-if="pagePosition==4" ng-cloak>
            <h3>Order Summary</h3>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-4">Item</div>
                        <div class="col-md-4">Recipient</div>
                        <div class="col-md-2">Send Date</div>
                        <div class="col-md-2">Amount</div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row" ng-repeat="summary in summaries">
                        <div class="hidden-md hidden-lg">Item</div>
                        <div class="col-xs-8 col-md-4">
                            <ul class="list-inline">
                                <li><img class="img-responsive" ng-src="@{{ summary.product_image }}"></li>
                                <li>
                                    <ul class="list-unstyled item-title">
                                        <li class="title">@{{ summary.product_title }}</li>
                                        <li><a href="" button-email-preview key="@{{ summary.key }}">Preview Email</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="hidden-md hidden-lg">Recipient</div>
                        <div class="col-xs-8 col-md-4">
                            <ul class="list-unstyled">
                                <li>@{{ summary.recipient_fullname }}</li>
                                <li>@{{ summary.recipient_email }}</li>
                                <li>@{{ summary.recipient_mobile }}</li>
                            </ul>
                        </div>
                        <div class="hidden-md hidden-lg">Send Date</div>
                        <div class="col-xs-8 col-md-2">@{{ summary.date_send | date:'dd-MMMM-yyyy' }}</div>
                        <div class="hidden-md hidden-lg">Sub Total</div>
                        <div class="col-xs-8 col-md-2">@{{ summary.product_amount }}</div>
                    </div>
                </div>
                <div class="panel-footer">
                    <strong>Grand Total (@{{ total_item }}): @{{ total_subtotal }}</strong>
                </div>
            </div>
        </div>

        <div class="section-checkout section-checkout-review" ng-if="pagePosition==5" ng-cloak>
            You are redirecting to payment gateway...
        </div>

        <payment-confirmation url="{{ url('payment-gateway/reddot') }}" ng-if="pagePosition>4"></payment-confirmation>

        <ul class="list-inline" ng-if="pagePosition<=4">
            <li><button class="btn btn-primary" ng-click="continue()" >Continue</button></li>
            <li><button class="btn btn-primary" ng-click="back()">Back</button></li>
        </ul>

    </div>
</section>
@endsection
@section('script')
<script src="{{ asset('assets/web_purchase/js/multi-card-checkout.js') }}"></script>


@endsection
