@extends('web_purchase.layout')

@section('content')

<section class="section product" ng-controller="CreateController">
    <div class="container">

        <div class="row steps">
            <div class="col-xs-3 text-center step1 active">
                <div class="bullet"></div>
                <span>Select Promotion</span>
            </div>
            <div class="col-xs-6 text-center step2">
                <div class="bullet"></div>
                <span>Confirmation</span>
            </div>
            <div class="col-xs-3 text-center step3">
                <div class="bullet"></div>
                <span>Payment</span>
            </div>
        </div>


        <uib-accordion close-others="true" ng-init="products = {{ json_encode($products) }}; confirmation_url = '{{ $confirmation_url }}'" hidden>
            <div uib-accordion-group class="panel-danger " heading="Select Promotion" is-open="true" is-disabled="false">
                
                <div class="row">
                    <div class="col-md-4 col-sm-12" ng-repeat="product in products" ng-init="qtys[$index] = 1">
                        <div class="product-item" ng-class="{selected: $index == selectedProductIndex}">
                            <span class="marked">
                                <span class="glyphicon glyphicon-ok"></span>
                            </span>
                            <p>
                                <a ng-click="selectProduct($index)"> 
                                    <img class="img-responsive" src="@{{ product.image }}"> 
                                </a>
                            </p>
                            <div class="form-group detail" ng-show="$index == selectedProductIndex">

                                <div class="alert" ng-class="alert.type" role="alert" ng-if="alert.type">
                                    <button type="button" class="close" aria-label="Close" ng-click="closeAlert()"><span aria-hidden="true">&times;</span></button>
                                    @{{ alert.message }}
                                </div>

                                <label>Select No. of Vouchers</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button ng-click="decreaseQty($index)" type="button" class="btn btn-primary">-</button>
                                    </span>
                                    <input class="form-control text-center" ng-model="qtys[$index]" type="text">
                                    <span class="input-group-btn">
                                        <button ng-click="increaseQty($index)" type="button" class="btn btn-primary">+</button>
                                    </span>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button class="btn btn-primary text-uppercase btn-block" ng-click="addToCart($index)">Add to Cart</button>
                                    </div>
                                    <div class="col-xs-6">
                                        <button class="btn btn-danger text-uppercase btn-block" ng-click="checkOut($index)">Check Out</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .product-item -->
                    </div>
                </div><!-- .row -->

            </div>
        </uib-accordion>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-10">
                <button class="btn btn-primary text-uppercase btn-more-padding-y btn-block" ng-click="next('{{ url('confirmation') }}')">View Cart</button>
            </div>
        </div>

    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/web-purchase/js/create.js') }}"></script>
@endsection