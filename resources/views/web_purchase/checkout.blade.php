@extends('web_purchase.layout_success')

@section('content')

<section class="section"> 
    <div class="container">

        <div class="row steps">
            <!-- <div class="col-xs-3 text-center step1">
                <div class="bullet"></div>
                <span>Select Promotion</span>
            </div> -->
            <div class="col-xs-6 text-center step1">
                <div class="bullet"></div>
                <span>Confirmation</span>
            </div>
            <div class="col-xs-3 text-center step3 active">
                <div class="bullet"></div>
                <span>Payment</span>
            </div>
        </div>

        <p class="text-center">Please wait...</p>
        <p class="text-center">We are redirecting you to the payment gateway</p>
        <form name="reddotPaymentform" id="reddotPaymentform" action='{{$url}}' METHOD='POST'>
            @foreach($fields as $key=>$value)
                <input type="hidden" name="<?php echo $key;?>" value="{{$value}}">
            @endforeach
            <input type="hidden" name="signature" value="{{$signature}}">           
        </form>
    </div>
</section>
@endsection
@section('script')
<script language="javascript">

    setTimeout(function(){
        //document.getElementById("reddotPaymentform").submit(); 
        //console.log('<?php echo $data['payment_url']; ?>');
        window.location.href = '<?php echo $data['payment_url']; ?>';

        /*
        window.open(
            '<?php echo $data['payment_url']; ?>',
            //"_blank" // <- This is what makes it open in a new window.
        ); */
    }, 2000);
    
</script>
@endsection
