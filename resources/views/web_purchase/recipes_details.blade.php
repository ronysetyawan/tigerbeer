@extends('web_purchase.layout')

@section('content')
@section('style')
<style>
  @media screen and (min-width: 1152px){
    .carousel-control.left {
      left: -100px;
    }

    .carousel-control.right {
      right: -100px;
    }
  }

  /* ipad pro */
  @media screen and (width: 1024px) and (height: 1366px){
    .slider-img{
      width: 85%;
    }
    .glyphicon.glyphicon-chevron-right{
      margin-right: -48px;
    }
    .carousel-control.left {
      left: -80px;
    }
    .carousel-control.right {
      right: -38px;
    }
  }

  @media screen and (max-width: 480px){
    .recipes-banner {
        margin-top: 50px;
    }

    .prizes-img{
      width: 100%;
    }
  }

  @media screen and (max-width: 992px) and (min-width:481px){
    .recipes-ipad{
      display: block;
    }

    .recipes-ipad-desktop{
      display: none;
    }

    .center{
      text-align: center;
    }
  }
</style>    
@endsection
<div class="home" home-scroll>
    

    <div class="home" ng-controller="HomeController" ng-init="countries = {{ json_encode($countries) }};">

        <img class="recipes-banner" src="{{ asset('assets/web-purchase/image/recipes-banner.png') }}" style="width: 100%;">
        <section class="opening" id="">
            <div class="container">
            <div class="row">
                <!-- <div class="col-md-3 col-sm-6 text-center text-white">
                <img class="img-responsive" id="mbl-sp" src="{{ asset('assets/web-purchase/image/Hendricks-text.png') }}">
                </div> -->
            </div>
            </div>
        </section>

        <section id="reg_success" class="reg_success">

            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-1 col-xs-12">
                        <!-- <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/INGREDIENTS@2x.png') }}">
                        <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/50ml.png') }}"> -->
                        <h4 style="color: #fff;text-align: center;font-weight: bold;font-size: 32px;">INGREDIENTS</h4><br>
                        <ul style="list-style:none;color: #fff;font-size: 16px;" class="center">
                            {!! $INGREDIENTS !!}
                        </ul>
                    </div>
                    <div class="col-md-2 col-md-offset-1 col-sm-2 recipes-desktop recipes-ipad-desktop">
                        <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/Line.png') }}" style="width: 1%;">
                    </div>
                    <div class="col-md-4 col-xs-12" style="margin-bottom: 5%;">
                        <!-- <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/PREPARATION@2x.png') }}">
                        <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/combine.png') }}"> -->
                        <h4 style="color: #fff;text-align: center;font-weight: bold;font-size: 32px;">PREPARATION</h4><br>
                        <ol style="color: #fff;font-size: 16px;" class="center">
                            {!! $PREPARATION !!}
                        </ol>
                    </div>
                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide recipes-desktop recipes-ipad-desktop" data-ride="carousel" style="margin-top: 110px;">
        
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active">
                                <div class="col-md-4">
                                  <a href="{{ url('recipes') }}/1"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 397@2x.png') }}" style="width:100%;height: 265px;"></a>
                                </div>
                                <div class="col-md-4">
                                  <a href="{{ url('recipes') }}/2"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 381@2x.png') }}" style="width:100%;height: 265px;"></a>
                                </div>
                                <div class="col-md-4">
                                  <a href="{{ url('recipes') }}/3"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 382@2x.png') }}" style="width:100%;height: 265px;"></a>
                                </div>
                              </div>
                              
                              <div class="item">
                                <div class="col-md-4">
                                  <a href="{{ url('recipes') }}/4"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 402@2x.png') }}" style="width:100%;height: 265px;"></a>
                                </div>
                                <div class="col-md-4">
                                  <a href="{{ url('recipes') }}/5"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 401@2x.png') }}" style="width:100%;height: 265px;"></a>
                                </div>
                                <div class="col-md-4">
                                  <a href="{{ url('recipes') }}/6"><img class="slider-img" src="{{ asset('assets/web-purchase/image/Group 400@2x.png') }}" style="width:100%;height: 265px;"></a>
                                </div>
                              </div>
                            </div>
                          
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="background-image: none;">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="background-image: none;">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 40px;">
                        <div id="carousel-example-generic-mobile" class="carousel slide recipes-mobile recipes-ipad" data-ride="carousel">
        
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active">
                                <div class="col-xs-12" style="text-align: -webkit-center;">
                                  <a href="{{ url('recipes') }}/1"><img src="{{ asset('assets/web-purchase/image/Group 397@2x.png') }}" style="width:80%;"></a>
                                </div>
                              </div>
                              
                              <div class="item">
                                <div class="col-xs-12" style="text-align: -webkit-center;">
                                  <a href="{{ url('recipes') }}/2"><img src="{{ asset('assets/web-purchase/image/Group 381@2x.png') }}" style="width:80%;"></a>
                                </div>
                              </div>
                  
                              <div class="item">
                                <div class="col-xs-12" style="text-align: -webkit-center;">
                                  <a href="{{ url('recipes') }}/3"><img src="{{ asset('assets/web-purchase/image/Group 382@2x.png') }}" style="width:80%;"></a>
                                </div>
                              </div>
                              
                              <div class="item">
                                <div class="col-xs-12" style="text-align: -webkit-center;">
                                  <a href="{{ url('recipes') }}/4"><img src="{{ asset('assets/web-purchase/image/Group 402@2x.png') }}" style="width:80%;"></a>
                                </div>
                              </div>
                  
                              <div class="item">
                                <div class="col-xs-12" style="text-align: -webkit-center;">
                                  <a href="{{ url('recipes') }}/5"><img src="{{ asset('assets/web-purchase/image/Group 401@2x.png') }}" style="width:80%;"></a>
                                </div>
                              </div>
                  
                              <div class="item">
                                <div class="col-xs-12" style="text-align: -webkit-center;">
                                  <a href="{{ url('recipes') }}/6"><img src="{{ asset('assets/web-purchase/image/Group 400@2x.png') }}" style="width:80%;"></a>
                                </div>
                              </div>
                            </div>
                          
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="prev" style="background-image: none;">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="next" style="background-image: none;">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-12 recipes-desktop recipes-ipad-desktop" style="margin-top: 2%;">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-2 col-xs-4">
                                    <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 53px;">
                                    <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 43px;">
                                    <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 53px;">
                                    <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 17px;">
                                    <a href="https://www.milagrotequila.com/" target="_blank"><img id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                                </div>
                                <div class="col-sm-2 col-xs-4" style="top: 33px;">
                                    <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="col-xs-12 recipes-mobile recipes-ipad">
                        <div class="container">
                            <div class="col-xs-4" style="margin-top: 6%;">
                                <a href="https://www.glenfiddich.com/explore/serves/" target="_blank"><img class="prizes-img" id="prizes1" src="{{ asset('assets/web-purchase/image/Group 152 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 45px">
                                <a href="https://www.hendricksgin.com/cocktail/gin-and-tonic/" target="_blank"><img class="prizes-img" id="prizes2" src="{{ asset('assets/web-purchase/image/Group 151 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 37px">
                                <a href="https://www.monkeyshoulder.com/cocktails/ginger-monkey/" target="_blank"><img class="prizes-img" id="prizes3" src="{{ asset('assets/web-purchase/image/Group 147 white.png') }}"></a>
                            </div>
                        </div>
                        <div class="container" style="margin-top: 6%;">
                            <div class="col-xs-4" style="top: 45px">
                                <a href="http://www.reykavodka.com/recipes?verified=true" target="_blank"><img class="prizes-img" id="prizes4" src="{{ asset('assets/web-purchase/image/Group 148 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 9px">
                                <a href="https://www.milagrotequila.com/" target="_blank"><img class="prizes-img" id="prizes5" src="{{ asset('assets/web-purchase/image/Group 149 white.png') }}"></a>
                            </div>
                            <div class="col-xs-4" style="top: 35px">
                                <a href="https://sailorjerry.com/en/rum-cocktail-recipes/sailor-jerry-spiced-rum-and-cola/" target="_blank"><img class="prizes-img" id="prizes6" src="{{ asset('assets/web-purchase/image/Group 150 white.png') }}"></a>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-6 footer-left text-white" style="top: 60px;">
                &copy; 2020 William Grant & Sons. <br> All Rights Reserved.
            </div>
            <div class="col-md-6 footer-right text-white" style="top: 60px;">
                <ul class="list-inline footer-menu">
                    <li><a href="{{ url('privacy-policy') }}" target="_blank" style="color: #fff;">Privacy Policy</a></li>
                    <li><a href="{{ url('terms-and-conditions') }}" target="_blank" style="color: #fff;">Terms of Use</a></li>
                </ul>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/home.js') }}"></script>
@endsection
