@extends('web_purchase.layout_success')

@section('content')

<section class="section cart" ng-controller="ConfirmationCtrl" ng-init="carts = {{ json_encode($carts) }}; 
grand_total_pints = {{ $grand_total_pints }}; 
promo_code_applied = {{ $promo_code_applied }}; discount = '{{ $discount }}'; 
grand_total = '{{ $grand_total }}'; countries = {{ json_encode($countries) }}; create_url='{{ $create_url }}'">
    <div class="container">

        <div class="row steps">
            <!-- <div class="col-xs-3 text-center step1">
                <div class="bullet"></div>
                <span>Select Promotion</span>
            </div> -->
            <div class="col-xs-6 text-center step1 active">
                <div class="bullet"></div>
                <span>Confirmation</span>
            </div>
            <div class="col-xs-6 text-center step3">
                <div class="bullet"></div>
                <span>Payment</span>
            </div>
        </div>

        <h1>Please confirm your support</h1>

        <div class="table-responsive hidden-xs">
            <table class="table table-striped table-cart">
                <thead>
                    <tr>
                        <th colspan="2">Voucher</th>
                        <th style="text-align: right">Contribution</th>
                        <th style="text-align: right">Quantity</th>
                        <th style="text-align: right">Entitlements</th>
                        <th style="text-align: right">Total Contribution</th>
                        <th style="width:50px"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="cart in carts">
                        <td width="1"><img style="width:100px" ng-src="@{{cart.image}}"></td>
                        <td>@{{cart.name}}</td>
                        <td style="text-align: right">@{{cart.price}}</td>
                        <td>
                            <div class="input-group pull-right">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-primary" ng-disabled="cart.qty === 1" ng-click="decreaseQty(cart.id)">-</button>
                                </span>
                                <input type="number" class="form-control text-center" value="@{{cart.qty}}" readonly>
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-primary" ng-disabled="cart.qty === 998" ng-click="increaseQty(cart.id)">+</button>
                                </span>
                            </div>
                        </td>
                        <td style="text-align: right">@{{ cart.total_pints }}</td>
                        <td style="text-align: right">@{{ cart.subtotal }}</td>
                        <td class="text-center">
                            <!-- <a href="" class="text-danger" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" confirmation-function="delete(cart.id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a> -->
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" rowspan="3">To support, click "Contribute Now" and you will be redirected to the payment page</td>
                        <!-- <td style="text-align: right" class="title-total">Discount:</td>
                        <td colspan="2" style="text-align: right">- @{{ discount }}</td>
                        <td class="text-center">
                            <a ng-if="promo_code_applied" class="text-danger" href="" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete the promo code?" confirmation-function="deletePromoCode()">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td> -->
                    </tr>
                    <tr>
                        <td style="text-align: right" class="title-total">Total Entitlements:</td>
                        <td colspan="2" style="text-align: right">@{{ grand_total_pints }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="text-align: right" class="title-total">Grand Total:</td>
                        <td colspan="2" style="text-align: right">@{{ grand_total }}</td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="hidden-sm hidden-md hidden-lg">
            <div ng-repeat="cart in carts">
                <ul class="list-unstyled list-carts">
                    <li>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="cart-label">
                                    <label>Voucher</label>
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="cart-value">
                                    <p><img style="width:100px" ng-src="@{{cart.image}}"></p>
                                    <p>@{{cart.name}}</p>
                                    <!-- <a href="" class="text-danger" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" confirmation-function="delete(cart.id)">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </a> -->
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="cart-label">
                                    <label>Price</label>
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="cart-value">@{{cart.price}}</div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="cart-label">
                                    <label>Quantity</label>
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="cart-value">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary" ng-disabled="cart.qty === 1" ng-click="decreaseQty(cart.id)">-</button>
                                        </span>
                                        <input type="number" class="form-control text-center" value="@{{cart.qty}}" readonly>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary" ng-disabled="cart.qty === 998" ng-click="increaseQty(cart.id)">+</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="cart-label">
                                    <label>Total Entitlements</label>
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="cart-value">@{{ cart.total_pints }}</div>
                            </div>
                        </div>
                    </li>
                        <li>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="cart-label">
                                    <label>Total Contribution</label>
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="cart-value">@{{ cart.subtotal }}</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <p>To support, click "Contribute Now" and you will be redirected to the payment page</p>
            <!-- <div class="row">
                <div class="col-xs-4 text-right"><label>Discount:</label></div>
                <div class="col-xs-8 text-right"><span ng-if="promo_code_applied"><a class="text-danger" href="" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete the promo code?" confirmation-function="deletePromoCode()">
                                <i class="fa fa-trash-o" aria-hidden="true"></i> 
                            </a></span> - @{{ discount }}
                    
                </div>
            </div> -->
            <div class="row">
                <div class="col-xs-4 text-right"><label>Total Entitlements:</label></div>
                <div class="col-xs-8 text-right">@{{ grand_total_pints }}</div>
            </div>
            <div class="row">
                <div class="col-xs-4 text-right"><label>Grand Total:</label></div>
                <div class="col-xs-8 text-right">@{{ grand_total }}</div>
            </div>

        </div>

        <!--
        <div class="promo-code-container">
            <div class="row">
                <div class="col-md-4 col-md-offset-8">
                    <div class="alert @{{ promo_code_alert.type }}" role="alert" ng-if="promo_code_alert.type">
                        <button type="button" class="close" aria-label="Close" ng-click="closePromoCodeAlert()"><span aria-hidden="true">&times;</span></button>
                        @{{ promo_code_alert.message }}
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter Promo Code" ng-model="form_promo_code.code">
                            <span class="input-group-btn">
                                <button class="btn btn-danger" type="button" ng-click="applyPromoCode()">Apply</button>
                            </span>
                        </div>
                    </div>
                    <p class="text-right"><a class="btn btn-more-padding-y text-uppercase btn-default" href="{{ $create_url }}">Add More Vouchers</a></p>
                </div>
            </div>
        </div> -->

        @if(!$is_logged_in)
        <!--
        <uib-accordion close-others="true" ng-if="!isLoggedIn">
            <div uib-accordion-group class="panel-danger" heading="Do you have an account?" is-open="true">
                <div class="radio">
                    <label>
                        <input type="radio" ng-model="account_type" value="new" >
                        No, I'm new.
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" ng-model="account_type" value="returning">
                        Yes, I have an account.
                    </label>
                </div>
                <div uib-collapse="account_type=='new'">
                    <div class="alert alert-danger" role="alert" ng-if="login_error">
                        <strong>Error!</strong> @{{ login_error }}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Mobile <span class="required">*</span></label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" ng-model="form_login.country" ng-options="country.country_id as country.country_name for country in countries"></select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">@{{ form_login_calling_code }}</div>
                                        <input class="form-control" ng-model="form_login.mobile" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>PIN <span class="required">*</span></label>
                                <input class="form-control" ng-model="form_login.pin" type="password">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <button type="button" class="btn btn-primary btn-block text-uppercase" ng-click="login()">Log In</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </uib-accordion>
        -->
        @endif

        <uib-accordion close-others="true">
            <div uib-accordion-group class="panel-danger" heading="Tell us more about yourself" is-open="true"
                ng-init="
                    @if(isset($contact_details['name']))form_checkout.name = '{{ $contact_details['name'] }}';@endif
                    {{-- @if(isset($contact_details['gender']))form_checkout.gender = '{{ $contact_details['gender'] }}';@endif --}}
                    @if(isset($contact_details['country']))form_checkout.country = {{ $contact_details['country'] }};@endif
                    @if(isset($contact_details['mobile']))form_checkout.mobile = '{{ $contact_details['mobile'] }}';@endif
                    @if(isset($contact_details['email']))form_checkout.email = '{{ $contact_details['email'] }}';@endif
                    {{-- @if(isset($contact_details['dob_date']))dobs.dob_date = {{ $contact_details['dob_date'] }};@endif
                    @if(isset($contact_details['dob_month']))dobs.dob_month = {{ $contact_details['dob_month'] }};@endif
                    @if(isset($contact_details['dob_year']))dobs.dob_year = {{ $contact_details['dob_year'] }};@endif --}}
                "
            >
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" ng-class="{'has-error': errors_checkout.hasOwnProperty('name')}">
                            <label>Name <span class="required">*</span></label>
                            <input class="form-control" ng-model="form_checkout.name" />
                            <div class="help-block" ng-if="errors_checkout.hasOwnProperty('name')" ng-cloak>@{{errors_checkout.name}}</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" ng-class="{'has-error': errors_checkout.hasOwnProperty('email')}">
                            <label>Email <span class="required">*</span></label>
                            <input class="form-control" type="email" ng-model="form_checkout.email" />
                            <div class="help-block" ng-if="errors_checkout.hasOwnProperty('email')" ng-cloak>@{{errors_checkout.email}}</div>
                        </div>
                    </div>
                    {{-- <div class="col-md-4">
                        <div class="form-group" ng-class="{'has-error': errors_checkout.hasOwnProperty('gender')}">
                            <label>Gender <span class="required">*</span></label>
                            <select class="form-control" ng-model="form_checkout.gender">
                                <option value="">-- select --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                            <div class="help-block" ng-if="errors_checkout.hasOwnProperty('gender')" ng-cloak>@{{errors_checkout.gender}}</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" ng-class="{'has-error': errors_checkout.hasOwnProperty('dob')}">
                            <label>Date of Birth <span class="required">*</span></label>
                            <input type="text" class="form-control" date="form_checkout.dob" placeholder="Date of Birth" dob-picker-ui max-year="2018" total-year="80" min-age="18" error="error_dob.data" readonly />
                            <div class="help-block" ng-if="errors_checkout.hasOwnProperty('dob')" ng-cloak>@{{errors_checkout.dob}}</div>
                        </div>
                    </div> --}}
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" ng-class="{'has-error': errors_checkout.hasOwnProperty('country')}">
                            <label>Country <span class="required">*</span></label>
                            <select class="form-control" ng-model="form_checkout.country" ng-options="country.country_id as country.country_name for country in countries"></select>
                            <div class="help-block" ng-if="errors_checkout.hasOwnProperty('country')" ng-cloak>@{{errors_checkout.country}}</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" ng-class="{'has-error': errors_checkout.hasOwnProperty('mobile')}">
                            <label>Mobile <span class="required">*</span></label>
                            <div class="input-group">
                                <div class="input-group-addon">@{{ form_checkout_calling_code }}</div>
                                <input class="form-control" ng-model="form_checkout.mobile" type="tel" ng-min="0" onkeypress="return isNumber(event)">
                            </div>
                            <div class="help-block" ng-if="errors_checkout.hasOwnProperty('mobile')" ng-cloak>@{{errors_checkout.mobile}}</div>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group" ng-class="{'has-error': errors_checkout.hasOwnProperty('confirm_mobile')}">
                            <label>Confirm Mobile <span class="required">*</span></label>
                            <div class="input-group">
                                <div class="input-group-addon">@{{ form_checkout_calling_code }}</div>
                                <input class="form-control" ng-model="form_checkout.confirm_mobile" type="text">
                            </div>
                            <div class="help-block" ng-if="errors_checkout.hasOwnProperty('confirm_mobile')" ng-cloak>@{{errors_checkout.confirm_mobile}}</div>
                        </div>
                    </div>-->
                </div>

                <div class="row">
                    <div class="col-md-12">
                    <p style="color:red;">Please confirm that your mobile number is correct and accurate as your free vouchers will be sent to the registered number.</p>
                    </div>
                </div>
            </div>
        </uib-accordion>
        
        
        <div class="confirm-cart">
            <div class="row">
                <div class="col-md-12">
                    <div class="checkbox">
                        <label><input type="checkbox" ng-model="agree_terms" id="agree_terms">
                        I acknowledge that I have read and understood the <a href="{{ url('terms-and-conditions') }}" target="_blank">Terms and Conditions</a> and in particular, I consent to the collection, use and disclosure of my personal data by Asia Pacific Breweries (Singapore) Pte Ltd for the following purposes:
                        <ol type="i">
                            <li>to provide services related to this campaign, provide technical assistance, assist you in a transaction; and/ or </li>
                            <li>to conduct marketing research, user profile and for analytics purposes</li>
                        </ol>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" ng-model="agree_newsletter" id="agree_newsletter">
                        I would like to receive information about the goods and services which may be provided by Asia Pacific Breweries (Singapore) Pte Ltd and their partner outlets, including (but not limited to) offers, promotions and information about new goods and services, via newsletters, emails and/or text messages.
                        </label>
                    </div>
                </div>
                <div class="col-md-6 left">
                    
                </div>
                <div class="col-md-6 right">
                    <button class="btn btn-primary text-uppercase btn-more-padding-y" ng-click="checkout(form_checkout)">Contribute Now</button>
                </div>
            </div>
        </div>


    </div>
</section>
@endsection
@section('script')
    <script>
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    </script>

<script src="{{ asset('assets/web-purchase/js/confirmation.js') }}"></script>
@endsection
