@extends('web_purchase.layout_success')

@section('content')
<section class="section section-order-success">
    <div class="container">

        <div class="order-success-intro">
            <h2 class="text-left">Your payment is successful</h2> 
            <p>A confirmation email has been sent to your email address, and the voucher link has been sent to the registered mobile number.<br>
            Please <a href="{{ url('/') }}#contact">contact us</a> if you did not receive any notification.</p>
        </div>
        
        <div class="panel panel-danger panel-order-success-items">
            <div class="panel-heading">{{ $voucher_id }}</div>
            <div class="panel-body panel-success">

                <div class="table-responsive hidden-xs">
                    <table class="table table-striped table-cart">
                        <thead>
                            <tr>
                                <th colspan="2">Voucher</th>
                                <th style="text-align: right">Contribution</th>
                                <th style="text-align: right">Quantity</th>
                                <th style="text-align: right">Entitlements</th>
                                <th style="text-align: right">Total Contribution</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                            <tr>
                                <td width="1"><img style="width:100px" src=" {{ $item['image'] }}"></td>
                                <td>{{ $item['name'] }}</td>
                                <td style="text-align: right">{{ $item['price'] }}</td>
                                <td style="text-align: right">{{ $item['qty'] }}</td>
                                <td style="text-align: right">{{ $item['total_pints'] }}</td>
                                <td style="text-align: right">{{ $item['subtotal'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @if ($show_discount)
                            <tr>
                                <td colspan="4"></td>
                                <td style="text-align: right" class="title-total">Discount:</td>
                                <td style="text-align: right">- {{ $discount }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td colspan="4"></td>
                                <td style="text-align: right" class="title-total">Total Entitlements:</td>
                                <td style="text-align: right">{{ $grand_total_pints }}</td>
                            </tr>
                            <tr>
                                <td colspan="4"></td>
                                <td style="text-align: right" class="title-total">Grand Total:</td>
                                <td style="text-align: right">{{ $grand_total }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>


                <div class="hidden-sm hidden-md hidden-lg">
                    @foreach($items as $item)
                    <div>
                        <ul class="list-unstyled list-carts">
                            <li>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="cart-label">
                                            <label>Voucher</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="cart-value">
                                            <p><img style="width:100px" ng-src="{{ $item['image'] }}"></p>
                                            {{ $item['name'] }}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="cart-label">
                                            <label>Contribution</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="cart-value">{{ $item['price'] }}</div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="cart-label">
                                            <label>Quantity</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="cart-value">{{ $item['qty'] }}</div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="cart-label">
                                            <label>Total Entitlements</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="cart-value">{{ $item['total_pints'] }}</div>
                                    </div>
                                </div>
                            </li>
                                <li>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="cart-label">
                                            <label>Total Contribution</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="cart-value">{{ $item['subtotal'] }}</div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    @endforeach
                    @if ($show_discount)
                    <div class="row">
                        <div class="col-xs-4 text-right"><label>Discount:</label></div>
                        <div class="col-xs-8 text-right">{{ $discount }}</div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-4 text-right"><label>Total Entitlements:</label></div>
                        <div class="col-xs-8 text-right">{{ $grand_total_pints }}</div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 text-right"><label>Grand Total:</label></div>
                        <div class="col-xs-8 text-right">{{ $grand_total }}</div>
                    </div>

                </div>


            </div>
        </div>

        <div class="next-step">
            <p><strong>What would you like to do next?</strong></p>
            <ul class="list-inline">
                <li><a href="{{ url('/') }}">Back to home</a></li>
                <!-- <li><a href="{{ url('create') }}">Purchase another voucher</a></li> -->
                <li><a href="{{ url('confirmation') }}">Give another support</a></li>
            </ul>
        </div>

    </div>
</section>
@endsection
