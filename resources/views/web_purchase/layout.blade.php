<!DOCTYPE html>
<html ng-app="CPRV-OctFestApp">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta property="og:title" content="Support Our Local F&B!">
        <meta property="og:type" content="article">
        <meta property="og:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        <meta property="og:description" content="Support Our Local F&B.">
        
        <meta name="twitter:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        <meta itemprop="image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        
        <meta name="description" content="Support Our Local F&B.">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Tiger #SunsetSocial</title>
        
        <!-- App CSS -->
        <link href="{{ asset('assets/web-purchase/css/web-purchase.css') }}" rel="stylesheet" type="text/css" />

        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics New-->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-19"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109851240-19');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2226828927336271');
        fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2226828927336271&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->


        @yield('style')
        <style>
            .navbar-default .navbar-right .btn {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            .home section.reward .btn {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            .btn-warning {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            button{
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            body #contact {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            label {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            body #reg_success {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }
        </style>

    </head>

    <body ng-controller="MainCtrl">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=366775843451872&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{-- <a class="navbar-brand" href="{{$home_url}}">
                    <img src="{{ asset('assets/web-purchase/image/logo.png') }}" alt="Logo">
                </a> --}}
                <a class="navbar-brand new-logo" href="{{$home_url}}"><img src="{{ asset('assets/web-purchase/image/new-logo.png') }}" alt="Logo"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="navbar-right" ng-init="
                        @if($is_logged_in)
                        is_logged_in = true;
                        @else
                        is_logged_in = true;
                        @endif;">
                        <ul class="nav navbar-nav" local-scroll>
                            <!-- <li><a href="<?php echo !$is_front?$home_url:'';?>#home">The Program</a></li> -->
                            <li><a href="<?php echo !$is_front?$home_url:'';?>#how-to-participate">WHERE TO REDEEM</a></li>
                            <li><a href="<?php echo !$is_front?$home_url:'';?>#recipes">RECIPES</a></li>
                            <li><a href="<?php echo !$is_front?$home_url:'';?>#faqs">FAQ</a></li>
                            <li><a href="<?php echo !$is_front?$home_url:'';?>#contact">GET IN TOUCH</a></li>
                            <li><a href="{{ url('registration') }}" class="hidden-lg hidden-md">REGISTER NOW</a></li>
                            <!-- <li><a href="" ng-click="showOutlets()">Participating Outlets</a></li> -->
                            <li class="separator"></li>
                            <!-- <li class="hidden-lg hidden-md hidden-sm" ng-if="!is_logged_in"><a ng-click="login()">User Login</a></li> -->
                            <!-- <li class="hidden-lg hidden-md hidden-sm" ng-if="is_logged_in"><a href="{{ url('account') }}">Account</a></li> -->
                        </ul>
                        <!-- <button ng-click="login()" class="btn text-uppercase btn-login hidden-xs" ng-if="!is_logged_in">User Login</button> -->
                        <!-- <a href="{{ url('account') }}" class="btn text-uppercase btn-login hidden-xs" ng-if="is_logged_in">Account</button> -->
                        <!-- <a href="{{ url('resend-voucher') }}" class="btn btn-danger text-uppercase btn-resend hidden-xs" style="margin:-4px;">Resend Voucher</a> -->
                        <!-- <a href="{{ url('create') }}" class="btn btn-warning btn-product hidden-xs btn-customheader">Show Your Support</a> -->
                        <!-- <a href="{{ url('confirmation') }}" class="btn btn-warning btn-product hidden-xs btn-customheader show-support-btn">Contribute Now</a> -->
                        <a href="{{ url('registration') }}" class="btn btn-danger btn-product hidden-xs btn-customheader show-support-btn">Register Now</a>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        </header>
        
        @yield('header')

        @yield('content')

        <!-- <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 footer-left">
                    &copy; 2020 Tiger Beer. All Rights Reserved.
                    </div>
                    <div class="col-md-6 footer-right">
                        <ul class="list-inline footer-menu">
                            <li><a href="{{ url('privacy-policy') }}" target="_blank">Privacy Policy</a></li>
                            <li><a href="{{ url('terms-and-conditions') }}" target="_blank">Terms of Use</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer> -->
        <script src="{{ asset('assets/web-purchase/js/web-purchase.js') }}"></script>

        @yield('script')

        <script type="text/ng-template" id="loginModal.html">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">Log In Details</h3>
            </div>
            <div class="modal-body" id="modal-body" ng-init="countries = {{ json_encode($countries) }}">
                <div class="row">
                    <div class="col-md-8">
                        <label>Mobile <span class="required">*</span></label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" ng-model="form_login.country" ng-options="country.country_id as country.country_name for country in countries"></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-addon">@{{ calling_code }}</div>
                                    <input class="form-control" ng-model="form_login.mobile" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>PIN <span class="required">*</span></label>
                            <input class="form-control" ng-model="form_login.pin" type="password">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary text-uppercase" ng-click="login()">Log In</button>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/ng-template" id="outletsModal.html">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">Participating Outlets</h3>
            </div>
            <div class="modal-body" id="modal-body">
                <ul class="list-inline list-zone">
                    @foreach($outlets as $zone=>$outlet)
                    <li><a href="#{{ $zone }}">{{ $zone }}</a></li>
                    @endforeach
                </ul>
                <hr>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address 1</th>
                                <th>Address 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($outlets as $zone=>$outlet)
                                <tr id="{{ $zone }}"><td colspan="4" class="text-uppercase"><strong>ZONE: {{ $zone }}</strong></td></tr>
                                @foreach($outlet as $_outlet)
                                <tr>
                                    <td>{{ $_outlet['name'] }}</td>
                                    <td>{{ $_outlet['address1'] }}</td>
                                    <td>{{ $_outlet['address2'] }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" ng-click="close()">Close</button>
            </div>
        </script>

        <script type="text/javascript">
            $('.navbar-nav>li>a').on('click', function(){
                $('.navbar-collapse').collapse('hide');
            });
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics New-->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-19"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109851240-19');
        </script>

        
                        
    </body>
</html>
