@extends('web_purchase.layout')

@section('content')

<section class="section resend-voucher" ng-controller="ResendVoucherCtrl" ng-init="countries = {{ json_encode($countries) }};">
  <div class="container">
    
    <div class="row">
      <div class="col-md-8">
        <div class="form-resend">
          <h1 class="h3">RESEND YOUR VOUCHER</h1>
          <p>Enter your mobile number below to resend your voucher.</p>
          <div class="alert" ng-class="{'alert-success': alert.type=='success', 'alert-danger': alert.type=='error'}" role="alert" ng-if="alert.type">
            <button type="button" class="close" aria-label="Close" ng-click="closeAlert()"><span aria-hidden="true">&times;</span></button>
            <span ng-cloak>@{{ alert.message }}</span>
          </div>
          <form>
            <div class="form-inline">
              <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('country')}">
                  <select class="form-control" ng-model="form.country" ng-options="country.country_id as country.country_name for country in countries"></select>
                  <div class="help-block" ng-if="errors.hasOwnProperty('country')" ng-cloak>@{{errors.country}}</div>
              </div>
              <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('mobile')}">
                  <div class="input-group">
                      <div class="input-group-addon" ng-cloak>@{{ calling_code }}</div>
                      <input class="form-control" ng-model="form.mobile" type="text">
                  </div>
                  <div class="help-block" ng-if="errors.hasOwnProperty('mobile')" ng-cloak>@{{errors.mobile}}</div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary text-uppercase btn-more-padding-y" ng-click="submit()">Resend Your Voucher</button>
          </form>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default panel-how-to-redeem">
          <div class="panel-body">
            <h3>How to Redeem</h3>
            <ul class="list-unstyled">
              <li>
                <div>STEP 1</div>
                Select the number of pints that you wish to redeem
              </li>
              <li>
                <div>STEP 2</div>
                Pass your phone to the bartender for redemption
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/resend-voucher.js') }}"></script>
@endsection
