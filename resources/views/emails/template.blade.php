<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>It is time to enjoy your Pints!</title>
  <!--[if gte mso 9]>
<style>
ul li {
    text-indent: -1em; /* Normalise space between bullets and text */
    white-space: normal;
    display: inline-block;
}

</style>
<![endif]-->

  <!--[if mso]>
<style type="text/css">
body, table, td {font-family: Arial, sans-serif !important;}
</style>
<![endif]-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">
    /* BOILERPLATE STYLES */
    a.btn-facebook:link,
    a.btn-facebook:visited,
    a.btn-facebook:hover,
    a.btn-facebook:active {
      color: #FFF !important;
      font-size: 16px !important;
    }

    .btn-facebook {
      background-color: #4064AC !important;
      color: #fff !important;
      padding: 10px 30px !important;
      border-radius: 5px !important;
    }

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass,
    .ExternalClass span,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    table,
    td,
    a {
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      -ms-interpolation-mode: bicubic;
    }

    body {
      height: 100% !important;
      margin: 0 !important;
      padding: 0 !important;
      width: 100% !important;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
    }

    /* LINK STYLES */
    a[x-apple-data-detectors] {
      color: inherit !important;
      text-decoration: none !important;
      font-size: inherit !important;
      font-family: inherit !important;
      font-weight: inherit !important;
      line-height: inherit !important;
    }

    .appleLink a {
      color: #01aef0 !important;
      text-decoration: none !important;
    }

    a:link,
    a:visited,
    a:hover,
    a:active {
      color: #01aef0;
      text-decoration: none;
    }

    /*IPAD STYLES*/
    @media only screen and (max-width: 640px) {

      a[href^="tel"],
      a[href^="sms"] {
        text-decoration: none;
        color: #0a8cce;
        /* or whatever your want */
        pointer-events: none;
        cursor: default;
      }

      td[class=pheader] {
        font-size: 9px !important;
      }

      .mobile_link a[href^="tel"],
      .mobile_link a[href^="sms"] {
        text-decoration: default;
        color: #0a8cce !important;
        pointer-events: auto;
        cursor: default;
      }

      *[class=devicewidth] {
        width: 100% !important;
        height: auto !important;
        text-align: center !important;
        align: center !important;
      }

      *[class=devicewidthinner] {
        width: 90% !important;
        height: auto !important;
        text-align: center !important;
      }

      img[class=imgresponsive] {
        width: 100% !important;
        height: auto !important;
      }

      table[class=tblresponsive] {
        width: 35% !important;
        height: auto !important;
      }

      *[class=mobile-hide] {
        display: none !important;
      }

      td[class=txtcenter] {
        text-align: center !important;
      }

      td[class=aheight] {
        height: auto !important;
      }

      .center {
        text-align: center !important;
        align: center !important;
      }

      .mobhide {
        display: none !important;
      }
    }

    /*IPHONE STYLES*/
    @media only screen and (max-width: 480px) {
      *[class=rowline] {
        display: block;
      }

      td[class=pheader] {
        font-size: 9px !important;
      }
    }
  </style>
</head>

<body bgcolor="#eeeeee">

  <!-- force gmail app width -->
  <table class="mobile-hide" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
    <tbody>
      <tr>
        <td align="center">
          <table class="devicewidth" style="width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td cellpadding="0" cellspacing="0" border="0" style=" line-height:10px;" height="10px; min-width: 600px;"><img src="emailrec_files/blank.gif" style="max-height:10px; min-height:10px; display:block; width:600px; min-width:600px;" alt="" width="600px" height="10px"></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- end gmail app fix -->

  <!-- Center Container -->
  <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td align="center">
          <!-- Start of Preheader -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
            <tbody>
              <tr>
                <td align="center">
                  <table class="devicewidth" style="width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
                    <tbody>
                      <tr>
                        <!-- <td style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:10px; line-height:14px; color:#646d73; padding:20px 0 0 0; text-align:center;" class="devicewidth" align="center">
                        Please <a href="<?php //echo url("email-preview/purchase/" . $string_token);
                                        ?>" target="_blank" style="color:#d71f36; text-decoration:none;">click here</a> if you have problems viewing this email </td> -->
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <!-- End of Preheader -->

          <!-- Start of Header -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
            <tbody>
              <tr>
                <td align="center">
                  <table class="devicewidth" style="width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
                    <tbody>
                      <tr>
                        <td style="padding: 20px 0;" align="center">
                          <!-- logo -->

                          <table class="devicewidth" style="width:257px" width="257" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
                            <tbody>
                              <tr>
                                <!-- <td align="center"><a href="https://www.apbsingapore.com.sg/other-beers/erdinger/" target="_blank"> <img src="<?php echo asset("assets/web-purchase/image/logo.png"); ?>" alt="Erdinger Logo" style="display:block; border:none; outline:none; text-decoration:none;" border="0" width="100"> </a></td> -->
                              </tr>
                            </tbody>
                          </table>

                          <!-- end of logo -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <!-- End of Header -->

          <!-- Start of Content -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
            <tbody>
              <tr>
                <td width="100%" align="center">
                  <table class="devicewidth" style="width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#0A265C" align="center">
                    <tbody>
                      <tr>
                        <td style="font-family: 'Helvetica Neue', sans-serif; font-size: 16px; line-height:17px; color:#ffffff; text-align:center; -webkit-text-size-adjust:none; letter-spacing:0.1em; padding:15px 10px;">
                          Thank you for your support!
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <!-- end of Content-->

          <!-- Start of Banner -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
            <tbody>
              <tr>
                <td align="center">
                  <table class="devicewidth" style="width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                    <tbody>
                      <tr>
                        <td style="padding:0 0 30px 0;" align="center">
                          <table class="devicewidth" style="width:600px" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                              <tr>
                                <td align="center"><img src="https://supportourfnb.tigerbeer.com.sg/assets/web-purchase/image/EmailBanner600x325.jpg" alt="Thank you for your support!" style="width:600px; height:320px; display:block; border:none; outline:none; text-decoration:none;" class="imgresponsive" width="600" height="200" border="0"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <!-- End of Banner-->

          <!-- Start of Content -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
            <tbody>
              <tr>
                <td width="100%" align="center">
                  <table class="devicewidth" style="width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                    <tbody>
                      <tr>
                        <td align="center">
                          <table class="devicewidthinner" style="width:500px;" width="500" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                              <tr>
                                <td colspan="3" style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none;"> Hi <?php echo $name; ?>,<br>
                                  <br>
                                  Thank you! You have successfully helped <br>more than 440 local F&B outlets!<br>
                                  Enjoy your FREE Tiger Entitlements below once Circuit Breaker is over
                                  <br></td>
                              </tr>
                              <tr>
                                <td>
                                  <hr size="3" color="#eeeeee">
                                </td>
                              </tr>
                              <tr>
                                <td align="center">
                                  <table class="devicewidthinner" style="width:500px;" width="500" cellspacing="0" cellpadding="0" border="0" align="center">
                                    <tbody>
                                      <?php foreach ($items as $item) { ?>
                                        <tr>
                                          <td style="padding:7px 0;" align="center">
                                            <table class="devicewidth" style="width:25%;" width="25%" cellspacing="0" cellpadding="0" border="0" align="left">
                                              <tbody>
                                                <tr>
                                                  <td style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none; padding:10px 0;"><strong>Promo ID:</strong><br>
                                                    <?php echo $item['voucher_id']; ?>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="devicewidth" style="width:25%;" width="25%" cellspacing="0" cellpadding="0" border="0" align="left">
                                              <tbody>
                                                <tr>
                                                  <td style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none; padding:10px 0;"><strong>Entitlement:</strong><br>
                                                    <?php echo $item['entitlement']; ?> </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="devicewidth" style="width:25%;" width="25%" cellspacing="0" cellpadding="0" border="0" align="left">
                                              <tbody>
                                                <tr>
                                                  <td style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none; padding:10px 0;"><strong>Total:</strong><br>
                                                    <?php echo $item['total']; ?> </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="devicewidth" style="width:23%;" width="23%" cellspacing="0" cellpadding="0" border="0" align="left">
                                              <tbody>
                                                <tr>
                                                  <td style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none; padding:10px 0;"><strong>Last Redemption Date:</strong><br>
                                                    <?php echo $item['expired_date']; ?> </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      <?php } ?>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3" style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none;">
                                  <br>
                                  Share with friends that you have just contributed towards supporting our local F&B community!
                                  <br>
                                </td>
                              </tr>
                              <!--
                              <tr colspan="3" style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none;">
                                <td>
                                  <br>
                                  <br>
                                  <a target="_blank" href="https://www.facebook.com/TigerBeer.SG/" style="background-color: #3b5998 !important; color: #fff !important; padding: 10px 30px !important; border-radius: 5px !important;"> 
                                    <img src="<?php //echo asset("assets/web-purchase/image/fb_icon.png"); ?>" width="16" height="16" border=0>
                                    Like</a>&nbsp;
                                  <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://supportourfnb.tigerbeer.com.sg/" style="background-color: #3b5998 !important; color: #fff !important; padding: 10px 30px !important; border-radius: 5px !important;">Share</a>
                                  <br>
                                  <br>
                                </td>
                              </tr> -->

                              <tr>
                                <td style="padding:20px 0px">
                                <table border="0" cellspacing="0" cellpadding="0" align="center">
                                  <tr>
                                    <td  bgcolor="#4064AC" style="padding:10px 30px; border-radius:5px" align="center" class="btn-facebook"><a href="https://www.facebook.com/TigerBeer.SG/" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;">
                                    <img src="<?php echo asset("assets/web-purchase/image/fb_icon.png"); ?>" width="16" height="16" border=0>
                                    Like</a></td> 
                                    <td>&nbsp;&nbsp;</td>
                                    <td bgcolor="#4064AC" style="padding:10px 30px; border-radius:5px" align="center" class="btn-facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=https://supportourfnb.tigerbeer.com.sg/" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; display: inline-block;">Share</a></td>
                                  </tr>
                                </table>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <hr size="3" color="#eeeeee">
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3" style="font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none;"><br>
                                  <br>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <!-- end of Content-->

          <!-- Start of Button -->
          <!-- end of Button-->

          <!-- Start of Content -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
            <tbody>
              <tr>
                <td width="100%" align="center">
                  <table class="devicewidth" style="width:600px; background-color:#fff;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
                    <tbody>
                      <tr>
                        <td align="center">
                          <table class="devicewidthinner" style="width:500px;" width="500" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                              <tr>
                                <td style="font-family: 'Helvetica Neue', sans-serif; font-size: 11px; line-height:17px; color:#393939; text-align:center; -webkit-text-size-adjust:none; padding: 5px 0 35px 0; white-space:normal;">
                                  By participating in this promotion, you agree to be bound by the <a href="<?php echo url("terms-and-conditions"); ?>" target="_blank" style="color:#0A265C; text-decoration:underline; font-weight:bold;">Terms &amp; Conditions</a>. </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <!-- end of Content-->

          <!-- Start of seperator -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
            <tbody>
              <tr>
                <td align="center">
                  <table class="devicewidth" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
                    <tbody>
                      <tr>
                        <td style="font-size:1px; line-height:1px;" height="20" align="center">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <!-- End of seperator -->

          <!-- Start of copyright -->

          <table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
            <tbody>
              <tr>
                <td width="100%" align="center">
                  <table class="devicewidth" style="width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
                    <tbody>
                      <tr>
                        <td align="center">
                          <table class="devicewidth" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                              <tr>
                                <td style="font-family: 'Helvetica Neue', sans-serif; font-size: 10px; line-height:14px; color: #393939; -webkit-text-size-adjust:none; text-align:center; padding: 0 0 20px 0;" align="center">© 2020 Copyright Asia Pacific Breweries Singapore Pte Ltd. All rights reserved.</td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>

          <!-- end of copyright-->
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End Center Container -->



</body>

</html>