<!DOCTYPE html>
<html ng-app="CPRV-OctFest-Admin">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Administration</title>

        <!-- App CSS -->
        <link href="{{ asset('assets/admin/css/admin.css') }}" rel="stylesheet" type="text/css" />

        @yield('style')

    </head>

    <body class="signin">

        <section ng-controller="LoginCtrl">
            
            <div class="panel panel-signin">
                <div class="panel-body">
                    <div class="logo text-center">
                        <img src="{{ url('assets/web-purchase/image/new-logo.png') }}" alt="Logo" >
                    </div>          
                    <h4 class="text-center mb5"></h4>
                    <p class="text-center">Welcome to WGS admin portal</p>
                    <form>
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control" placeholder="Username" ng-model="form_login.username">
                        </div><!-- input-group -->
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password" ng-model="form_login.password">
                        </div><!-- input-group -->
                        
                        <div class="clearfix">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success" ng-click="login()">Sign In <i class="fa fa-angle-right ml5"></i></button>
                            </div>
                        </div>                      
                    </form>
                </div><!-- panel-body -->
            </div><!-- panel -->
            
        </section>

        <script src="{{ asset('assets/admin/js/admin.js') }}"></script>
        <script src="{{ asset('assets/admin/js/auth.js') }}"></script>

    </body>
</html>







