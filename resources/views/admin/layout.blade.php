<!DOCTYPE html>
<html ng-app="CPRV-OctFest-Admin">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Administration</title>

        <!-- App CSS -->
        <link href="{{ asset('assets/admin/css/admin.css') }}" rel="stylesheet" type="text/css" />

        @yield('style')

    </head>

    <body ng-controller="MainCtrl">

    <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a href="{{ url('admin/') }}" class="logo">
                        <img style="height: 27px; width: auto;" src="{{ asset('assets/web-purchase/image/new-logo.png') }}" alt="" /> 
                    </a>
                    <div class="pull-right">
                        <a href="" class="menu-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div><!-- header-left -->
                
                <div class="header-right">
                    
                    <div class="pull-right">
                    
                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              Welcome {{ $user_name }} <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <!--
                                <li><a href="{{ url('admin/my-profile') }}"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                                <li><a href="{{ url('admin/change-password') }}"><i class="fa fa-lock"></i> Change Password</a></li>
                                <li class="divider"></li> -->
                                <li><a href="{{ url('admin/logout') }}"><i class="glyphicon glyphicon-log-out"></i>Log Out</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        
                    </div><!-- pull-right -->
                    
                </div><!-- header-right -->
                
            </div><!-- headerwrapper -->
        </header>
        
        <section>
            <div class="mainwrapper">
                <div class="leftpanel"> 
                    <h5 class="leftpanel-title">Navigation</h5>
                    @if($user_role == 'property')
                    <ul class="nav nav-pills nav-stacked">
                        <li
                        @if($menu_active=='voucher-outlet')
                            class="active"
                        @endif
                        ><a href="{{ url('admin/voucher-outlet') }}"><i class="fa fa-tag"></i> <span>Vouchers</span></a></li>
                        <!--
                        <li
                        @if($menu_active=='report-redemption-by-property')
                            class="active"
                        @endif>
                            <a href="{{ url('admin/report/redemption-by-property') }}"><i class="fa fa-home"></i> <span>Redemption by Property</span></a></li>
                        -->
                    </ul>
                    @else
                    <ul class="nav nav-pills nav-stacked">
                        <li
                        @if($menu_active=='dashboard')
                            class="active"
                        @endif
                        >
                        <a href="{{ url('admin/') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li
                        @if($menu_active=='voucher')
                            class="active"
                        @endif
                        >
                        <a href="{{ url('admin/voucher') }}"><i class="fa fa-tag"></i> <span>Vouchers</span></a></li>
                        <li
                        @if($menu_active=='voucher-redeem')
                            class="active"
                        @endif
                        >
                        <a href="{{ url('admin/voucher-redeem') }}"><i class="fa fa-tag"></i> <span>Vouchers Redeem</span></a></li>
                        <li
                        @if($menu_active=='payment')
                            class="active"
                        @endif
                        >
                        <a href="{{ url('admin/payment') }}"><i class="fa fa-money"></i> <span>Payments</span></a></li>
                    @if($user_role == 'admin')
                        <li
                        @if($menu_active=='pint')
                            class="active"
                        @endif
                        ><a href="{{ url('admin/pint') }}"><i class="fa fa-beer"></i> <span>Pints</span></a></li>
                    @endif
                        {{-- <li
                        @if($menu_active=='prize')
                            class="active"
                        @endif
                        ><a href="{{ url('admin/prize') }}"><i class="fa fa-trophy"></i> <span>Prize</span></a></li> --}}
                        <li class="parent
                        @if(
                            $menu_active=='report-full-transaction' || 
                            $menu_active=='report-redemption-by-voucher' || 
                            $menu_active=='report-redemption-by-property' ||
                            $menu_active=='report-prize-redemption' || 
                            $menu_active=='report-promo-code'
                        )
                            active
                        @endif
                        "><a href=""><i class="fa fa-book"></i> <span>Reports</span></a>
                            <ul class="children">
                                <li
                                @if($menu_active=='report-full-transaction')
                                    class="active"
                                @endif
                                ><a href="{{ url('admin/report/full-transaction') }}">Full Transaction</a></li>
                                <li
                                @if($menu_active=='report-redemption-by-voucher')
                                    class="active"
                                @endif
                                ><a href="{{ url('admin/report/redemption-by-property') }}">Redemption by Property</a></li>
                                {{-- <li
                                @if($menu_active=='report-redemption-by-property')
                                    class="active"
                                @endif
                                ><a href="{{ url('admin/report/redemption-by-property') }}">Redemption by Property</a></li>
                            @if($user_role == 'admin')    
                                <li
                                @if($menu_active=='report-prize-redemption')
                                    class="active"
                                @endif
                                ><a href="{{ url('admin/report/prize-redemption') }}">Prize Redemption</a></li>
                            @endif
                                {{-- <li
                                @if($menu_active=='report-promo-code')
                                    class="active"
                                @endif
                                ><a href="{{ url('admin/report/promo-code') }}">Promo Code</a></li> --}}
                            </ul>
                        </li>
                    @if($user_role == 'admin')
                        <li class="parent 
                        @if(
                            $menu_active=='setting-country' || 
                            $menu_active=='setting-property'
                        )
                            active
                        @endif
                        "><a href=""><i class="fa fa-gear"></i> <span>Settings</span></a>
                            <ul class="children">
                                <li
                                @if($menu_active=='setting-country')
                                    class="active"
                                @endif
                                ><a href="{{ url('admin/setting/country') }}">Countries</a></li>
                                <li
                                @if($menu_active=='setting-property')
                                    class="active"
                                @endif
                                ><a href="{{ url('admin/setting/property') }}">Properties</a></li>
                            </ul>
                        </li>
                    @endif
                    </ul>
                    @endif
                </div><!-- leftpanel -->


                @yield('content') 

                   
            </div><!-- mainwrapper -->
        </section>
                                
        <script src="{{ asset('assets/admin/js/admin.js') }}"></script>

        @yield('script')

    </body>
</html>