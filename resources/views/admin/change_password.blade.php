@extends('admin.layout')

@section('content')

<section class="section">


<div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-lock"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="{{ url('admin') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li>Change Password</li>
                                </ul>
                                <h4>Change Password</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">

       
                <form method="post" action="{{ url('admin/change-password') }}">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <div class="row">
                        <div class="col-sm-6">
                            @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                            @endif

                            @if(count($errors->all())>=1)
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="form-group">
                                <label>New Password<span class="required">*</span></label>
                                <input type="password" class="form-control" name="new_password">
                            </div>
                            <div class="form-group">
                                <label>New Password Confirm<span class="required">*</span></label>
                                <input type="password" class="form-control" name="new_password_confirm">
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Old Password<span class="required">*</span></label>
                                <input type="password" class="form-control" name="old_password">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

  
    </div>
</section>

@endsection