<table>
    <thead>
        <tr>
            <th>Transaction Type</th>
            <th>Transactio Date</th>
            <th>Voucher</th>
            <th>Transaction ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile No.</th>
            <th>Total Pint</th>
            <th>Total</th>
            <th>Redemption Pint</th>
            <th>Property</th>
            <th>Status</th>
            <th>Payment</th>
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
        <tr>
            <td>{{ $data['trans_type'] }}</td>
            <td>{{ $data['trans_date'] }}</td>
            <td>{{ $data['voucher_id'] }}</td>
            <td>{{ $data['trans_id'] }}</td>
            <td>{{ $data['purchaser'] }}</td>
            <td>{{ $data['purchaser_email'] }}</td>
            <td>{{ $data['purchaser_mobile'] }}</td>
            <td class="text-right">{{ $data['total_pint'] }}</td>
            <td class="text-right">{{ $data['total'] }}</td>
            <td class="text-right">{{ $data['redemption_pint'] }}</td>
            <td>{{ $data['property'] }}</td>
            <td>{{ $data['status'] }}</td>
            <td>{{ $data['last_status'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>