<table>
    <thead>
        <tr>
            <th>Transaction Type</th>
            <th>Voucher Type</th>
            <th>Transaction Date</th>
            <th>Voucher Number</th>
            <th>Transaction ID</th>
            <th>Purchaser</th>
            <th>Purchaser's Email</th>
            <th>Purchaser's Mobile No.</th>
            <th>Recipient</th>
            <th>Recipient's Email</th>
            <th>Recipient's Mobile No.</th>
            <th>Value</th>
            <th>Amount Paid</th>
            <th>Redemption Amount</th>
            <th>Is Void</th>
            <th>Property</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
        <tr>
            <td>{{ $data['trans_type'] }}</td>
            <td>{{ $data['voucher_type'] }}</td>
            <td>{{ $data['trans_date'] }}</td>
            <td>{{ $data['voucher_id'] }}</td>
            <td>{{ $data['trans_id'] }}</td>
            <td>{{ $data['purchaser'] }}</td>
            <td>{{ $data['purchaser_email'] }}</td>
            <td>{{ $data['purchaser_mobile'] }}</td>
            <td>{{ $data['recipient'] }}</td>
            <td>{{ $data['recipient_email'] }}</td>
            <td>{{ $data['recipient_mobile'] }}</td>
            <td>{{ $data['value'] }}</td>
            <td>{{ $data['amount_paid'] }}</td>
            <td>{{ $data['redemption_amount'] }}</td>
            <td>{{ $data['is_void'] }}</td>
            <td>{{ $data['property'] }}</td>
            <td>{{ $data['status'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>