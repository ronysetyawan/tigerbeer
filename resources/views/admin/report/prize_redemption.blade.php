@extends('admin.layout')

@section('content')

<section class="section" ng-controller="PrizeRedemptionCtrl" ng-init="redemptions = {{ json_encode($redemptions) }}">
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Prize Redemption</li>
                    </ul>
                    <h4>Prize Redemption</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
        <h5 class="lg-title mb5">Filter</h5>
            
        <form class="form-inline mb15" action="{{ url('admin/report/prize-redemption') }}">
            <ul class="list-inline">
                <li>
                    <select class="form-control input-sm" name="type">
                        <option value="">All Type</option>
                        <option value="shipping-product"
                            @if(isset($filter_datas['type']) && $filter_datas['type']=='shipping-product')
                            selected='selected'
                            @endif
                        >Shipping Product</option>
                        <option value="promocode"
                            @if(isset($filter_datas['type']) && $filter_datas['type']=='promocode')
                            selected='selected'
                            @endif
                        >Promo Code</option>
                    </select>
                </li>
                <li>
                    <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                </li>
                <li>
                    <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Download <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            @foreach($download_link as $link)
                            <li><a href="{{ $link['url'] }}">{{ $link['title'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            </ul>
        </form>
        
            <div class="panel panel-default" ng-if="redemptions.length<=0" ng-cloak>
                <div class="panel-body">
                There is no data
                </div>
            </div>

            <div class="table-responsive" ng-if="redemptions.length>=1" ng-cloak>
                <table class="table table-dark table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Customer Name</th>
                            <th colspan="2">Prize</th>
                            <th class="text-right">Point</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="redemption in redemptions">
                            <td>@{{ redemption.date }}</td>
                            <td>@{{ redemption.customer_name }}</td>
                            <td><img style="max-width:100px;" src="@{{ redemption.prize_image }}"></td>
                            <td>@{{ redemption.prize_name }}</td>
                            <td class="text-right">@{{ redemption.point }}</td> 
                            <td><button class="btn btn-primary btn-xs" ng-click="viewDetail($index)">Detail</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><!-- contentpanel -->    
    </div>
</section>
@endsection

@section('script')
<script src="{{ asset('assets/admin/js/prize-redemption.js') }}"></script>

<script type="text/ng-template" id="modalDetail.html">
    <form>
        <div class="modal-header">
            <button aria-hidden="true" class="close" type="button" ng-click="close()">×</button>
            <h4 class="modal-title">Prize Detail</h4>
        </div>
        <div class="modal-body">
            <p><strong>Prize Type</strong><br>@{{ redemption.type_string }}</p>
            <div ng-if="redemption.type=='shipping-product'">
                <p><strong>Address 1</strong><br>@{{ redemption.address_1 }}</p>
                <p><strong>Address 2</strong><br>@{{ redemption.address_2 }}</p>
                <p><strong>Postal Code</strong><br>@{{ redemption.postal_code }}</p>
                <p><strong>Country</strong><br>@{{ redemption.country }}</p>
                <p><strong>Mobile No.</strong><br>@{{ redemption.mobile }}</p>
                <p><strong>Email</strong><br>@{{ redemption.email }}</p>
            <div>
            <div ng-if="redemption.type=='promocode'">
                <p><strong>Promo Code</strong><br>@{{ redemption.promo_code }}</p>
            </div>
        </div>
    </form>
</script>
@endsection