@extends('admin.layout')

@section('content')

<section class="section" ng-controller="RedemptionByPropertyReportCtrl">
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Redemption By Property</li>
                    </ul>
                    <h4>Redemption By Property</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            <h5 class="lg-title mb5">Filter</h5>
            <form action="{{ url('admin/report/redemption-by-property') }}">
                <ul class="list-inline">
                    {{-- <li>
                        <input date-range-picker class="form-control input-sm date-picker" type="text" ng-model="filter_date.date"
                        options="{eventHandlers: {'apply.daterangepicker': applyDateRange}}" />
                    </li> --}}
                    <li>
                        <select class="form-control input-sm" name="property">
                            <option value="">All</option>
                            @foreach($properties as $property)
                            <option value="{{ $property['value'] }}"
                                @if(isset($filter_datas['property']) && $property['value']==$filter_datas['property'])
                                    selected="seelcted"
                                @endif
                            >{{ $property['label'] }}</option>
                            @endforeach
                        </select>
                    </li>    
                    <li>
                        <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                    </li>
                    <li>
                        <div class="btn-group btn-group-sm" style="margin-bottom: 0px">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Download <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                @foreach($download_link as $link)
                                <li><a href="{{ $link['url'] }}">{{ $link['title'] }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                </ul>
                <input type="hidden" name="date-from" value="" id="filter-date-from">
                <input type="hidden" name="date-start" value="" id="filter-date-start">
                {{-- <input type="hidden" name="property" value="" id="filter-property"> --}}
            </form>

            <div class="alert alert-info">Found {{ $total_record }} record(s).</div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Redeem Date</th>
                            <th>Property</th>
                            <th>Zone</th>
                            <th>Customer Code</th>
                            <th>Voucher ID</th>
                            <th>Voucher Type</th>
                            <th>Customer</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datas as $data)
                        <tr>
                            <td>{{ $data['redeem_date'] }}</td>
                            <td>{{ $data['property'] }}</td>
                            <td>{{ $data['zone'] }}</td>
                            <td>{{ $data['customer_code'] }}</td>
                            <td>{{ $data['voucher_id'] }}</td>
                            <td>{{ $data['voucher_type'] }}</td>
                            <td>{{ $data['customer'] }}</td>
                            <td>{{ $data['value'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $links }}
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/redemption_by_property_report.js') }}"></script>
@endsection