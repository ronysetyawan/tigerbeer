<table>
    <thead>
        <tr>
            <th>Prize Type</th>
            <th>Redemption Date</th>
            <th>Customer Name</th>
            <th>Prize</th>
            <th>Point</th>
            <th>Address 1</th>
            <th>Address 2</th>
            <th>Postal Code</th>
            <th>Country</th>
            <th>Mobile No.</th>
            <th>Email</th>
            <th>Promo Code</th>
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
        <tr>
            <td>{{ $data['type_string'] }}</td>
            <td>{{ $data['date'] }}</td>
            <td>{{ $data['customer_name'] }}</td>
            <td>{{ $data['prize_name'] }}</td>
            <td>{{ $data['point'] }}</td>
            <td>{{ $data['address_1'] }}</td>
            <td>{{ $data['address_2'] }}</td>
            <td>{{ $data['postal_code'] }}</td>
            <td>{{ $data['country'] }}</td>
            <td>{{ $data['mobile'] }}</td>
            <td>{{ $data['email'] }}</td>
            <td>{{ $data['promo_code'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>