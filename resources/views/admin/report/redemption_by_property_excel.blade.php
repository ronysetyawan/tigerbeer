<table>
    <thead>
        <tr>
            <th>Redeem Date</th>
            <th>Property</th>
            <th>Zone</th>
            <th>Customer Code</th>
            <th>Voucher ID</th>
            <th>Voucher Type</th>
            <th>Customer</th>
            <th>Quantity</th>
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
            <tr>
                <td>{{ $data['redeem_date'] }}</td>
                <td>{{ $data['property'] }}</td>
                <td>{{ $data['zone'] }}</td>
                <td>{{ $data['customer_code'] }}</td>
                <td>{{ $data['voucher_id'] }}</td>
                <td>{{ $data['voucher_type'] }}</td>
                <td>{{ $data['customer'] }}</td>
                <td>{{ $data['value'] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>