@extends('admin.layout')

@section('content')

<section class="section" ng-controller="VoucherListCtrl">
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Redemption By Voucher</li>
                    </ul>
                    <h4>Redemption By Voucher</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            <h5 class="lg-title mb5">Filter</h5>
            <p>Please enter the voucher id, customer name or customer mobile number</p>
            <form class="form-inline mb15" action="{{ url('admin/report/prize-redemption') }}">
                <ul class="list-inline">
                    <li>
                        <input type="text" class="form-control input-sm" placeholder="Keyword">
                    </li>
                    <li>
                        <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                    </li>
                </ul>
            </form>
            <div class="alert alert-info">Found {{ $record }}.</div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Redemption Date</th>
                            <th>Amount</th>
                            <th>Cashier</th>
                            <th>Receipt No</th>
                            <th>Property</th>
                            <th>Description</th>
                            <th>Is Void</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($datas)>=1)
                            @foreach($datas as $data)
                            <tr>
                                <td>{{ $data['redemption_date'] }}</td>
                                <td>{{ $data['amount'] }}</td>
                                <td>{{ $data['cashier'] }}</td>
                                <td>{{ $data['receipt_no'] }}</td>
                                <td>{{ $data['property'] }}</td>
                                <td>{{ $data['description'] }}</td>
                                <td>{{ $data['is_void'] }}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">There is no data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>    
        </div>
    </div>
</section>
@endsection