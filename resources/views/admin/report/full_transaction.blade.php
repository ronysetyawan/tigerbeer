@extends('admin.layout')

@section('content')

<section class="section" ng-controller="FullTransactionReportCtrl">
   
<div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('admin') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Full Transaction Report</li>
                    </ul>
                    <h4>Full Transaction Report</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
                    
        <div class="contentpanel">
    
            <h5 class="lg-title mb5">Filter</h5>
            <form action="{{ url('admin/report/full-transaction') }}">
                <ul class="list-inline">
                    <li>
                        <input placeholder="Date range" date-range-picker class="form-control date-picker input-sm" type="text" ng-model="form_filter.date" readonly />
                    </li>
                    <li>
                        <select class="form-control input-sm" name="type">
                            <option value="">All Type</option>
                            <option value="Registration"
                                @if(isset($filter_datas['type']) && $filter_datas['type']=='Registration')
                                selected="selected"
                                @endif
                            >Registration</option>
                            <option value="Redemption"
                                @if(isset($filter_datas['type']) && $filter_datas['type']=='Redemption')
                                selected="selected"
                                @endif
                            >Redemption</option>
                        </select>
                    </li>
                    <li>
                        <input value="@if(isset($filter_datas['mobile_number'])) {{ $filter_datas['mobile_number'] }} @endif" placeholder="Mobile no." name="mobile_number" type="text" class="form-control input-sm">
                    </li>
                    <li>
                        <button class="btn btn-primary btn-sm">Filter</button>
                    </li>
                    <li>
                        <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Download <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                @foreach($download_link as $link)
                                <li><a href="{{ $link['url'] }}">{{ $link['title'] }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                </ul>
                
                <input type="hidden" name="date-from" value="" id="filter-date-from">
                <input type="hidden" name="date-start" value="" id="filter-date-start">
            </form>

            <div class="alert alert-info">Found {{ $total_record }} record(s).</div>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-dark">
                    <thead>
                        <tr>
                            <th>Trans. Type</th>
                            <th>Trans. Date</th>
                            <th>Voucher</th>
                            <th>Trans. ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No.</th>
                            <th class="text-right">Total Pint</th>
                            <th class="text-right">Total</th>
                            <th class="text-right">Redemption Pint</th>
                            <th>Outlet</th>
                            <th>Status</th>
                            <!-- <th>Payment</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datas as $data)
                        <tr>
                            <td>{{ $data['trans_type'] }}</td>
                            <td>{{ $data['trans_date'] }}</td>
                            <td>{{ $data['voucher_id'] }}</td>
                            <td>{{ $data['trans_id'] }}</td>
                            <td>{{ $data['purchaser'] }}</td>
                            <td>{{ $data['purchaser_email'] }}</td>
                            <td>{{ $data['purchaser_mobile'] }}</td>
                            <td class="text-right">{{ $data['total_pint'] }}</td>
                            <td class="text-right">{{ $data['total'] }}</td>
                            <td class="text-right">{{ $data['redemption_pint'] }}</td>
                            <td>{{ $data['property'] }}</td>
                            <td>{{ $data['status'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- contentpanel -->                
    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/full_transaction_report.js') }}"></script>
@endsection
