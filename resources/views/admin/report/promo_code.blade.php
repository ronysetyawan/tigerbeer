@extends('admin.layout')

@section('content')

<section class="section" ng-controller="FullTransactionReportCtrl">

<div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Promo Code</li>
                    </ul>
                    <h4>Promo Code</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
                    
        <div class="contentpanel" ng-init="promocodes = {{ json_encode($promocodes) }}">

            <div class="panel panel-default" ng-if="promocodes.length<=0">
                <div class="panel-body">
                There is no data
                </div>
            </div>
            <div class="table-responsive" ng-if="promocodes.length>=1">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Type</th>
                            <th>Issued By</th>
                            <th>Issued Date</th>
                            <th>Status</th>
                            <th>Used On</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($promocodes as $promocode)
                        <tr>
                            <td>{{ $promocode['code'] }}</td>
                            <td>{{ $promocode['type'] }}</td>
                            <td>{{ $promocode['issued_by'] }}</td>
                            <td>{{ $promocode['issued_date'] }}</td>
                            <td>
                                @if($promocode['used'])
                                <span class="label label-danger">Used</span>
                                @else
                                <span class="label label-success">Available</span>
                                @endif
                            </td>
                            <td>{{ $promocode['used_on'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
                  
            </div><!-- contentpanel -->
                    
                    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/full_transaction_report.js') }}"></script>
@endsection
