@extends('admin.layout')

@section('content')

<section class="section" ng-controller="PrizeListCtrl" ng-init="current_url = '{{ $current_url }}'; prizes = {{ json_encode($prizes) }}">
    
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-trophy"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('admin') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Prize</li>
                    </ul>
                    <h4>Prize</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        
        <div class="contentpanel">

            <p class="text-right"><button class="btn btn-primary btn-xs" ng-click="create()">Create New Prize</button></p>
            
            <div class="table-responsive">
                <table class="table table-dark table-bordered">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Point Needed</th>
                            <th>Value</th>
                            <th>Qty</th>
                            <th>Type</th>
                            <th>Sort Order</th>
                            <th>Status</th>
                            <th style="width:1px" colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-if="prizes.length>=1" ng-repeat="prize in prizes" ng-cloak>
                            <td><img src="@{{ prize.image }}" style="width: 220px; height: auto;"></td>
                            <td>@{{ prize.name }}</td>
                            <td>@{{ prize.point_needed }}</td>
                            <td>@{{ prize.value }}</td>
                            <td>@{{ prize.qty }}</td>
                            <td>@{{ prize.type }}</td>
                            <td>@{{ prize.sort_order }}</td>
                            <td>
                                <button class="btn btn-xs"
                                    ng-class="{'btn-success': prize.status, 'btn-danger': !prize.status}" 
                                    button-confirmation 
                                    confirmation-title="Confirmation" 
                                    confirmation-description="Are you sure want to change the data?" 
                                    confirmation-function="toggleStatus($index)">@{{ prize.status_string }}</button>
                            </td>
                            <td class="text-right">
                                <button class="btn btn-default btn-xs" ng-click="edit($index)" title="Edit"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger btn-xs" title="Delete" 
                                button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" 
                                confirmation-function="delete($index)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{ $links }}
        </div><!-- contentpanel -->                    
    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/prize.js') }}"></script>

<script type="text/ng-template" id="modalEdit.html">
    <form>
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">@{{ title }}</h4>
        </div>
        <div class="modal-body" ng-init="current_url = '{{ $current_url }}'">
            @{{ errorImage }}
            <p ng-if="form.image_string"><img ng-src="@{{ form.image_string }}" style="width: 120px; height: auto;"></p>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('image') && !isEdit}">
                <label>Image</label>
                <input type="file" ngf-select ng-model="form.image" accept="image/*" ngf-model-invalid="errorImage">
                <div class="help-block" ng-if="errors.hasOwnProperty('image') && !isEdit" ng-cloak>@{{errors.image}}</div>
            </div>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('name')}">
                <label>Name <span class="required">*</span></label>
                <input type="text" class="form-control" ng-model="form.name">
                <div class="help-block" ng-if="errors.hasOwnProperty('name')" ng-cloak>@{{errors.name}}</div>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" ng-model="form.description"></textarea>
            </div>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('pint')}">
                <label>Pints <span class="required">*</span></label>
                <input type="number" class="form-control" ng-model="form.pint">
                <div class="help-block" ng-if="errors.hasOwnProperty('pint')" ng-cloak>@{{errors.pint}}</div>
            </div>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('price')}">
                <label>Price <span class="required">*</span></label>
                <input type="number" class="form-control" ng-model="form.price">
                <div class="help-block" ng-if="errors.hasOwnProperty('price')" ng-cloak>@{{errors.price}}</div>
            </div>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('stock')}">
                <label>Stock <span class="required">*</span></label>
                <input type="number" class="form-control" ng-model="form.stock">
                <div class="help-block" ng-if="errors.hasOwnProperty('stock')" ng-cloak>@{{errors.stock}}</div>
            </div>
            <div class="form-group">
                <label>Sort Order</label>
                <input type="number" class="form-control" ng-model="form.sort_order">
            </div>
            <div class="form-group">
                <label>Status</label>
                <div>
                    <label class="radio-inline">
                        <input type="radio" ng-model="form.status" ng-value="1"> Active
                    </label>
                    <label class="radio-inline">
                        <input type="radio" ng-model="form.status" ng-value="0"> Not Active
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" ng-click="save()">Save</button>
        </div>
    </form>
</script>

<script type="text/ng-template" id="modalImages.html">
    <div class="modal-header">
        <h3 class="modal-title">Images</h3>
    </div>
    <div class="modal-body">

        <div class="table-responsive">
            <table class="table table-list table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Sort Order</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="image in images">
                        <td><img src="@{{ image.image }}" style="width: 100px; height: auto;"></td>
                        <td><input type="number" class="form-control" style="width: 50px;" ng-model="images[$index].sort_order"></td>
                        <td>
                            <button class="btn btn-xs btn-success" ng-if="image.status" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to change the data?" confirmation-function="toggleStatus($index)">@{{ image.status_string }}</button>
                            <button class="btn btn-xs btn-danger" ng-if="!image.status" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to change the data?" confirmation-function="toggleStatus($index)">@{{ image.status_string }}</button>
                        </td>
                        <td><button class="btn btn-xs btn-danger" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" confirmation-function="delete($index)">Delete</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-info" ngf-select="uploadFile($file, $invalidFiles)"><i class="fa fa-plus"></i> Add New Image</button>
        <button type="submit" class="btn btn-primary" ng-click="save()"><i class="fa fa-pencil"></i> Save</button>
        <button type="submit" class="btn btn-default" ng-click="close()">Close</button>
    </div>
</script>
@endsection
