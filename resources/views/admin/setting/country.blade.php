@extends('admin.layout')

@section('content')

<section class="section" ng-controller="CountryListCtrl" ng-init="current_url = '{{ $current_url }}'; countries = {{ json_encode($countries) }}">
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-gear"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('admin') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Country</li>
                    </ul>
                    <h4>Country</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        
        <div class="contentpanel">

            <h5 class="lg-title mb5">Filter</h5>
            <div class="clearfix">
                <div class="pull-left">
                    <form method="get" action="{{ url('admin/setting/country') }}">
                        <ul class="list-inline">
                            <li>
                                <input type="text" class="form-control input-sm" 
                                placeholder="Enter country name" 
                                name="keyword"
                                value="{{ isset($filter_datas['keyword'])?$filter_datas['keyword']:'' }}"
                                >
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="pull-right">
                    <button class="btn btn-primary btn-sm" ng-click="create()">Create New Country</button>
                </div>
            </div>
            
            <div class="panel panel-default" ng-if="countries.length<=0" ng-cloak>
                <div class="panel-body">
                There is no data
                </div>
            </div>

            <div class="alert alert-info" ng-if="countries.length>=1" ng-cloak>Found {{ $total_record }} record(s).</div>

            <div class="table-responsive" ng-if="countries.length>=1" ng-cloak>
                <table class="table table-striped table-dark table-bordered">
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Code 2</th>
                            <th>Code 3</th>
                            <th>Calling Code</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="country in countries">
                            <td>@{{ country.name }}</td>
                            <td>@{{ country.code_2 }}</td>
                            <td>@{{ country.code_3 }}</td>
                            <td>@{{ country.calling_code }}</td>
                            <td>
                                <button class="btn btn-xs btn-success" ng-if="country.status" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to change the data?" confirmation-function="toggleStatus($index)">@{{ country.status_string }}</button>
                                <button class="btn btn-xs btn-danger" ng-if="!country.status" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to change the data?" confirmation-function="toggleStatus($index)">@{{ country.status_string }}</button>
                            </td>
                            <td class="text-right">
                                <button class="btn btn-default btn-xs" ng-click="edit($index)"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger btn-xs" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" confirmation-function="delete($index)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>    
                    </tbody>
                </table>
            </div>

            {{ $links }}
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/country.js') }}"></script>

<script type="text/ng-template" id="modalEdit.html">
    <div class="modal-header">
        <h3 class="modal-title">@{{ title }}</h3>
    </div>
    <div class="modal-body" ng-init="current_url = '{{ $current_url }}'">
    
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('name')}">
            <label>Name<span class="required">*</span></label>
            <input type="text" class="form-control" ng-model="form.name">
            <div class="help-block" ng-if="errors.hasOwnProperty('name')" ng-cloak>@{{errors.name}}</div>
        </div>
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('code2')}">
            <label>Code 2<span class="required">*</span></label>
            <input type="text" class="form-control" ng-model="form.code2">
            <div class="help-block" ng-if="errors.hasOwnProperty('code2')" ng-cloak>@{{errors.code2}}</div>
        </div>
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('code3')}">
            <label>Code 3<span class="required">*</span></label>
            <input type="text" class="form-control" ng-model="form.code3">
            <div class="help-block" ng-if="errors.hasOwnProperty('code3')" ng-cloak>@{{errors.code3}}</div>
        </div>
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('calling_code')}">
            <label>Calling Code<span class="required">*</span></label>
            <input type="text" class="form-control" ng-model="form.calling_code">
            <div class="help-block" ng-if="errors.hasOwnProperty('calling_code')" ng-cloak>@{{errors.calling_code}}</div>
        </div>
        <div class="form-group">
            <label>Status</label>
            <div>
                <label class="radio-inline">
                    <input type="radio" ng-model="form.status" ng-value="1"> Aktif
                </label>
                <label class="radio-inline">
                    <input type="radio" ng-model="form.status" ng-value="0"> Not Active
                </label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary" ng-click="save()">Save</button>
    </div>
</script>
@endsection