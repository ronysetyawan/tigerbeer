@extends('admin.layout')

@section('content')

<section class="section" ng-controller="PropertyListCtrl" ng-init="current_url = '{{ $current_url }}'; properties = {{ json_encode($properties) }}">
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-gear"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('admin') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Property</li>
                    </ul>
                    <h4>Property</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        
        <div class="contentpanel">
            <h5 class="lg-title mb5">Filter</h5>
            <div class="clearfix">
                <div class="pull-left">
                    <form method="get" action="{{ url('admin/setting/property') }}">
                        <ul class="list-inline">
                            <li>
                                <input type="text" class="form-control input-sm" placeholder="Enter property name or address" name="keyword"
                                value="{{ isset($filter_datas['keyword'])?$filter_datas['keyword']:'' }}"
                                >
                            </li>
                            <li>
                                <select class="form-control input-sm" name="zone">
                                    <option value="">All Zone</option>
                                    @foreach($zones as $zone)
                                    <option value="{{ $zone }}"
                                        @if(isset($filter_datas['zone']) && $filter_datas['zone'] == $zone)
                                        selected="selected"
                                        @endif
                                    >{{ $zone }}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="pull-right">
                    <button class="btn btn-primary btn-sm" ng-click="create()">Create New Property</button>
                </div>
            </div>
            
            <div class="panel panel-default" ng-if="properties.length<=0" ng-cloak>
                <div class="panel-body">
                There is no data
                </div>
            </div>

            <div class="alert alert-info" ng-if="properties.length>=1" ng-cloak>Found {{ $total_record }} record(s).</div>

            <div class="table-responsive" ng-if="properties.length>=1" ng-cloak>
                <table class="table table-striped table-bordered table-dark" >
                    <thead>
                        <tr>
                            <th>Property Name</th>
                            <th>Address 1</th>
                            <th>Address 2</th>
                            <th>Zone</th>
                            <th>Pin</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="property in properties">
                            <td>@{{ property.name }}</td>
                            <td>@{{ property.address_1 }}</td>
                            <td>@{{ property.address_2 }}</td>
                            <td>@{{ property.zone }}</td>
                            <td>@{{ property.pin }}</td>
                            <td>
                                <button class="btn btn-xs btn-success" ng-if="property.status" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to change the data?" confirmation-function="toggleStatus($index)">@{{ property.status_string }}</button>
                                <button class="btn btn-xs btn-danger" ng-if="!property.status" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to change the data?" confirmation-function="toggleStatus($index)">@{{ property.status_string }}</button>
                            </td>
                            <td class="text-right">
                                <button class="btn btn-default btn-xs" ng-click="edit($index)"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger btn-xs" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" confirmation-function="delete($index)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>    
                    </tbody>
                </table>
            </div>

            {{ $links }}
        
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/property.js') }}"></script>

<script type="text/ng-template" id="modalEdit.html">
    <div class="modal-header">
        <h3 class="modal-title">@{{ title }}</h3>
    </div>
    <div class="modal-body" ng-init="current_url = '{{ $current_url }}'">
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('name')}">
            <label>Name<span class="required">*</span></label>
            <input type="text" class="form-control" ng-model="form.name">
            <div class="help-block" ng-if="errors.hasOwnProperty('name')" ng-cloak>@{{errors.name}}</div>
        </div>
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('address1')}">
            <label>Address 1<span class="required">*</span></label>
            <input type="text" class="form-control" ng-model="form.address1">
            <div class="help-block" ng-if="errors.hasOwnProperty('address1')" ng-cloak>@{{errors.address1}}</div>
        </div>
        <div class="form-group">
            <label>Address 2</label>
            <input type="text" class="form-control" ng-model="form.address2">
        </div>
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('zone')}">
            <label>Zone<span class="required">*</span></label>
            <select class="form-control input-sm" ng-model="form.zone">
                <option value="">-- select zone --</option>
                @foreach($zones as $zone)
                <option value="{{ $zone }}"
                    @if(isset($filter_datas['zone']) && $filter_datas['zone'] == $zone)
                    selected="selected"
                    @endif
                >{{ $zone }}</option>
                @endforeach
            </select>
            <div class="help-block" ng-if="errors.hasOwnProperty('zone')" ng-cloak>@{{errors.zone}}</div>
        </div>
        <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('pin')}">
            <label>Pin<span class="required">*</span></label>
            <input type="text" class="form-control" ng-model="form.pin" min="11111" max="99999">
            <div class="help-block" ng-if="errors.hasOwnProperty('pin')" ng-cloak>@{{errors.pin}}</div>
        </div>
        <div class="form-group">
            <label>Status</label>
            <div>
                <label class="radio-inline">
                    <input type="radio" ng-model="form.status" ng-value="1"> Active
                </label>
                <label class="radio-inline">
                    <input type="radio" ng-model="form.status" ng-value="0"> Not Active
                </label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary" ng-click="save()">Save</button>
    </div>
</script>
@endsection