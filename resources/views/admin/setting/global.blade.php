@extends('admin.layout')

@section('content')

<section class="section" ng-controller="VoucherListCtrl">
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-gear" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Global Setting</li>
                    </ul>
                    <h4>Global Setting</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            <form method="post" action="{{ url('admin/setting/global') }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Voucher Prefix</label>
                            <input type="text" class="form-control" name="form[voucher_prefix]" value="{{ $global_settings['voucher_prefix'] }}">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</section>

@endsection