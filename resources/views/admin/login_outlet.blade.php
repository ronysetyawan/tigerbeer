<!DOCTYPE html>
<html ng-app="CPRV-OctFest-Admin">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Tiger</title>

        <!-- App CSS -->
        <link href="{{ asset('assets/admin/css/admin.css') }}" rel="stylesheet" type="text/css" />

        @yield('style')

    </head>

    <body class="signin">

        <section ng-controller="LoginCtrl">
            
            <div class="panel panel-signin">
                <div class="panel-body">
                    <div class="logo text-center">
                        <img src="{{ url('assets/web-purchase/image/logo.png') }}" alt="Tiger Logo" >
                    </div>          
                    <h4 class="text-center mb5">Support Our F&B 2020</h4>
                    <p class="text-center">Welcome to admin portal <br>欢迎光临管理版面</p>
                    <form>
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="password" class="form-control" placeholder="Outlet Pin 员工密码" ng-model="form_pin.input_pin" id="input_1">
                        </div>
                        
                        <div class="clearfix">
                            <div class="text-center">
                                <!-- <button type="submit" class="btn btn-success" ng-click="login()">Sign In 登入<i class="fa fa-angle-right ml5"></i></button> -->
                                <button type="submit" class="btn btn-success" ng-click="login_outlet()">Sign In 登入</button>
                            </div>
                        </div>                      
                    </form>
                </div><!-- panel-body -->
            </div><!-- panel -->
            
        </section>

        <script src="{{ asset('assets/admin/js/admin.js') }}"></script>
        <script src="{{ asset('assets/admin/js/auth.js') }}"></script>

    </body>
</html>







