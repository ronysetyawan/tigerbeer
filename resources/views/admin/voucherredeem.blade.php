@extends('admin.layout')

@section('content')
<div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-tag"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li><a href="">Pages</a></li>
                                    <li>Voucher Redeem</li>
                                </ul>
                                <h4>Voucher Redeem</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
                    <div class="table-responsive" >
                    <table class="table table-striped">
                      <thead>
                          <tr>
                            <th>Voucher ID</th>
                            <th>Value</th>
                            <th>Property</th>
                            <th>Redeem Date</th>
                          </tr>
                      </thead>
                      <tbody>
                      @foreach($data as $d)
                        <tr>
                          <td>{{ $d->voucher_id}}</td>
                          <td>{{ $d->value}}</td>
                          <td>{{ $d->property}}</td>
                          <td>{{ $d->redeem_date}}</td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                    <div class="text-center">
                      <div class="pagination">
                        <ul>{{ $data->links() }}</ul>
                      </div>
                    </div>
                  </div>
                  </div>
                    <!-- contentpanel -->
                    
                    </div>
@endsection

<!-- @section('script')
<script src="{{ asset('assets/admin/js/voucher_view.js') }}"></script>
@endsection -->