@extends('admin.layout')

@section('content')

<section class="section" ng-controller="DashboardCtrl">


<div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li><a href="">Pages</a></li>
                                    <li>Dashboard</li>
                                </ul>
                                <h4>Dashboard</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
                    
                        <div class="row row-stat">
                            <div class="col-md-4">
                                <div class="panel panel-success-alt noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-gift"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Total Profiles Registered</h5>
                                            <h1 class="mt5" ng-cloak>@{{ widget.purchased }}</h1>
                                        </div><!-- media-body -->  
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-3 -->
                            
                            <div class="col-md-4">
                                <div class="panel panel-primary noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-dropbox"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Total Quantity</h5>
                                            <h1 class="mt5">@{{ widget.pint_purchased }}</h1>
                                        </div><!-- media-body -->                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-3 -->

                            <div class="col-md-4">
                                <div class="panel panel-warning noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" data-placement="left" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-dollar"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Total Redeemed</h5>
                                            <h1 class="mt5" ng-cloak>@{{ widget.pint_redeemed }}</h1>
                                        </div><!-- media-body -->
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-3 -->

                        </div><!-- row -->

                        <div class="row row-stat">

                            <!-- <div class="col-md-6">
                                <div class="panel panel-info noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" data-placement="left" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div>
                                        <div class="panel-icon"><i class="fa fa-dollar"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Total Transaction</h5>
                                            <h1 class="mt5" ng-cloak>@{{ widget.transaction }}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <!--<div class="col-md-6">
                                <div class="panel panel-danger noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" data-placement="left" title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div>
                                        <div class="panel-icon"><i class="fa fa-gift"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Total Contributors</h5>
                                            <h1 class="mt5" ng-cloak>@{{ widget.contributors }}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>
        

        <!--
        <h4 class="text-uppercase">Transactions</h4>
        <div google-chart chart="chart.transaction" style="height:600px; width:100%;"></div>
        
        <h4 class="text-uppercase">Revenue</h4>
        <div google-chart chart="chart.revenue" style="height:600px; width:100%;"></div>

        <h4 class="text-uppercase">Average Transactions</h4>
        <div google-chart chart="chart.average_transaction" style="height:600px; width:100%;"></div>
        -->

     </div><!-- contentpanel -->
                    
                    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/dashboard.js') }}"></script>
@endsection