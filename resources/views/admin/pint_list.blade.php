@extends('admin.layout')

@section('content')

<section class="section" ng-controller="PintListCtrl" ng-init="current_url = '{{ $current_url }}'">
    
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-beer" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('admin') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Pints</li>
                    </ul>
                    <h4>Pints</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
                    
        <div class="contentpanel" ng-init="pints = {{ json_encode($pints) }}">

            <p class="text-right"><button class="btn btn-primary btn-xs" ng-click="create()">Add New Pint</button></p>

            <div class="table-responsive">
                <table class="table table-dark table-bordered">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Sort Order</th>
                            <th>Status</th>
                            <th style="width:1px" colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-if="pints.length>=1" ng-repeat="pint in pints" ng-cloak>
                            <td><img src="@{{ pint.image }}" style="width: 220px; height: auto;"></td>
                            <td>@{{ pint.name }}</td>
                            <td>@{{ pint.price }}</td>
                            <td>@{{ pint.sort_order }}</td>
                            <td>
                                <button class="btn btn-xs"
                                    ng-class="{'btn-success': pint.status, 'btn-danger': !pint.status}" 
                                    button-confirmation 
                                    confirmation-title="Confirmation" 
                                    confirmation-description="Are you sure want to change the data?" 
                                    confirmation-function="toggleStatus($index)">@{{ pint.status_string }}</button>
                            </td>
                            <td class="text-right" colspan="2">
                                <button class="btn btn-default btn-xs" ng-click="edit($index)" title="Edit"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger btn-xs" title="Delete" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to delete?" confirmation-function="delete($index)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        <tr ng-if="pints.length<=0">
                            <td colspan="7">There is no data</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        
            {{ $links }}

        </div><!-- contentpanel -->
                    
    </div>
        
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/pint.js') }}"></script>

<script type="text/ng-template" id="modalEdit.html">
    <form>
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">@{{ title }}</h4>
        </div>
        <div class="modal-body" ng-init="current_url = '{{ $current_url }}'">
            @{{ errorImage }}
            <p ng-if="form.image_string"><img ng-src="@{{ form.image_string }}" style="width: 120px; height: auto;"></p>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('image') && !isEdit}">
                <label>Image</label>
                <input type="file" ngf-select ng-model="form.image" accept="image/*" ngf-model-invalid="errorImage">
                <div class="help-block" ng-if="errors.hasOwnProperty('image') && !isEdit" ng-cloak>@{{errors.image}}</div>
            </div>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('pint')}">
                <label>Pints <span class="required">*</span></label>
                <input type="number" class="form-control" ng-model="form.pint">
                <div class="help-block" ng-if="errors.hasOwnProperty('pint')" ng-cloak>@{{errors.pint}}</div>
            </div>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('price')}">
                <label>Price <span class="required">*</span></label>
                <input type="number" class="form-control" ng-model="form.price">
                <div class="help-block" ng-if="errors.hasOwnProperty('price')" ng-cloak>@{{errors.price}}</div>
            </div>
            <div class="form-group">
                <label>Sort Order</label>
                <input type="number" class="form-control" ng-model="form.sort_order">
            </div>
            <div class="form-group">
                <label>Status</label>
                <div>
                    <label class="radio-inline">
                        <input type="radio" ng-model="form.status" ng-value="1"> Active
                    </label>
                    <label class="radio-inline">
                        <input type="radio" ng-model="form.status" ng-value="0"> Not Active
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" ng-click="save()">Save</button>
        </div>
    </form>
</script>
@endsection
