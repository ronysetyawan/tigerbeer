@extends('admin.layout')
@section('style')
    <style>
         input[type="date"]:before {
                content: attr(placeholder) !important;
                color: #aaa;
                margin-right: 0.5em;
            }
            input[type="date"]:focus:before,
            input[type="date"]:valid:before {
                content: "";
            }
    </style>
@endsection

@section('content')

<section class="section" ng-controller="VoucherListCtrl" ng-init="vouchers = {{ json_encode($vouchers) }}; data_vouchers = {{ json_encode($data_vouchers) }} ">


    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-tag"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb"> 
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li><a href="">Pages</a></li>
                        <li>Vouchers</li>
                    </ul>
                    <h4>Vouchers ({{ $prop_name }})</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->

            
        
        <div class="contentpanel">

            <!-- <form method="get" action="{{ url('admin/voucher') }}">
                <label>FILTER</label>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="form-control" name="mobile_number" placeholder="Mobile Number" value="{{ $filter_datas['mobile_number'] }}" ng-model="form_search.mobile_number">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="form-control" name="voucher_id" placeholder="Voucher ID" value="{{ $filter_datas['voucher_id'] }}" ng-model="form_search.voucher_id">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="form-control" name="name" placeholder="Customer Name" value="{{ $filter_datas['name'] }}" ng-model="form_search.name">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="form-control" name="email" placeholder="Email" value="{{ $filter_datas['email'] }}" ng-model="form_search.email">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" >Filter</button>
                <a href="{{ url('admin/voucher') }}" class="btn btn-default">Reset</a>
            </form> -->
            <div class="row row-stat">
                <div class="col-md-6">
                    <div class="panel panel-success-alt noborder">
                        <div class="panel-heading noborder" style="background-color: #EF8A1E">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div><!-- panel-btns -->
                            <div class="panel-icon"><i class="fa fa-users"></i></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">TOTAL BEERS REDEEMED:<br>累计兑换总 </h5>
                                <!-- <h1 class="mt5" ng-cloak>@{{ user.total }}</h1> -->
                                <h1 class="mt5" ng-cloak>{{ $total }}</h1>
                            </div><!-- media-body -->  
                        </div><!-- panel-body -->
                    </div><!-- panel -->
                </div><!-- col-md-3 -->
                
                <div class="col-md-6">
                    <div class="panel panel-dark noborder">
                        <div class="panel-heading noborder" style="background-color: #204770">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div>
                            {{-- <div class="panel-icon"><i class="fa fa-dropbox"></i></div> --}}
                            <div class="panel-icon"><img width="60px" height="60px" src="{{  asset('assets/voucher/image/Tiger Pint.jpg') }}" alt=""></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">TOTAL TIGER PINT BOTTLE REDEMEED</h5>
                                <h1 class="mt5">{{ $total_tigerpint }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- row -->
            <div class="row row-stat">
                <div class="col-md-6">
                    <div class="panel panel-dark noborder">
                        <div class="panel-heading noborder" style="background-color: #204770">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div>
                            {{-- <div class="panel-icon"><i class="fa fa-dropbox"></i></div> --}}
                            <div class="panel-icon"><img width="60px" height="60px" src="{{  asset('assets/voucher/image/Tiger Crystal Pint.png') }}" alt=""></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">TOTAL TIGER CRYSTAL PINT BOTTLE REDEEMED</h5>
                                <h1 class="mt5">{{ $total_tigercrystal }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="panel panel-dark noborder">
                        <div class="panel-heading noborder" style="background-color: #204770">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div>
                            {{-- <div class="panel-icon"><i class="fa fa-dropbox"></i></div> --}}
                            <div class="panel-icon"><img width="60px" height="60px" src="{{  asset('assets/voucher/image/Draught 500ml.jpg') }}" alt=""></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">TOTAL TIGER DRAUGHT 500ML REDEEMED</h5>
                                <h1 class="mt5">{{ $total_tigerdraught }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- row -->
            <div class="row row-stat">
                <div class="col-md-6">
                    <div class="panel panel-dark noborder">
                        <div class="panel-heading noborder" style="background-color: #204770">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div>
                            {{-- <div class="panel-icon"><i class="fa fa-dropbox"></i></div> --}}
                            <div class="panel-icon"><img width="60px" height="60px" src="{{  asset('assets/voucher/image/Tiger Quart edited.jpg') }}" alt=""></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">TOTAL TIGER QUART BOTTLE REDEEMED</h5>
                                <h1 class="mt5">{{ $total_tigerquart }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="panel panel-dark noborder">
                        <div class="panel-heading noborder" style="background-color: #204770">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div>
                            {{-- <div class="panel-icon"><i class="fa fa-dropbox"></i></div> --}}
                            <div class="panel-icon"><img width="60px" height="60px" src="{{  asset('assets/voucher/image/Tiger Crystal Quart.jpg') }}" alt=""></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">TOTAL TIGER CRYSTAL QUART BOTTLE REDEEMED</h5>
                                <h1 class="mt5">{{ $total_tigercrystalquart }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- row -->

            <hr>
            <form method="get" action="{{ url('admin/voucher-outlet') }}">
                <label>FILTER</label>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="search" placeholder="Voucher ID/Customer ID">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            {{-- <input type="date"  value="2020-06-01" min="2020-06-01" onfocus="(this.value='date')" onblur="(this.type='text')" name="from" class="form-control" placeholder="FROM DATE">   --}}
                            <input type="date" value="2020-06-01" min="2020-06-01" name="from" class="form-control input-sm" placeholder="From">  
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group"> 
                            {{-- <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="to" class="form-control" placeholder="TO DATE"> --}}
                            <input type="date" value="2020-06-01" name="to" class="form-control input-sm" placeholder="To">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group"> 
                            <button type="submit" class="btn btn-primary" >Filter</button>
                <a href="{{ url('admin/voucher-outlet') }}" class="btn btn-default">Reset</a>
                        </div>
                    </div>
                </div>
                
            </form>

            <div class="table-responsive" >
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Transactional Date/Time 兑换日期/时间</th>
                            <th>Voucher ID 礼券编号</th>
                            <th>Customer ID 顾客编号 </th>
                            <th>Voucher Type</th>
                            <th>Quantity</th>
                            <!-- <th class="text-right">Action 行动</th> -->
                            <th style="width:1px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-if="vouchers.length>=1" ng-repeat="voucher in vouchers" class="voucher-row voucher-row-@{{$index}}" ng-cloak>
                            <td>@{{voucher.redeem_date}}</td>
                            <td>@{{voucher.voucher_id}}</td>
                            <td>@{{voucher.user_id}}</td>
                            <!-- <td><span class="label @{{voucher.status.class}}">@{{voucher.status.label}}</span></td> -->
                            <td>@{{voucher.product_name}}</td>
                            <td>@{{voucher.qty_redeem}}</td>
                            <!--
                            <td class="text-right"><a class="btn btn-xs btn-primary" href="/admin/voucher/@{{voucher.voucher_id}}">Detail</a></td> -->

                            <td class="text-right">
                                <div class="btn-group">
                                   
                                    <!-- <button class="btn btn-xs btn-primary" ng-disabled="voucher.voided == 1 || voucher.balance <= 0" ng-click="addNote($index)">Comment</button> -->

                                    <!-- <button class="btn btn-xs btn-info" ng-click="addNote($index)">Add Notes</button>
                                    <a href="" class="btn btn-xs btn-danger <?php echo ($user_role=='staff')?'hidden':'' ?>" ng-disabled="voucher.voided == 1 || voucher.balance <= 0" ng-hide="$user_role=='staff'" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to Void this voucher?" confirmation-function="void($index)">Void</a> -->
                                </div>
                                <!-- <div class="btn-group">
                                    <a class="btn btn-xs btn-success" href="/admin/voucher/@{{voucher.voucher_id}}">Detail</a>
                                </div> -->
                            </td>
                        </tr>
                        <tr ng-if="vouchers.length<=0" ng-cloak><td colspan="9" class="text-center">There is no data</td></tr>
                    </tbody>
                </table>
            </div>
           
        </div><!-- contentpanel -->
                    
    </div>
    
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/voucher.js') }}"></script>
@endsection
