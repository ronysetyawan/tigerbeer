@extends('admin.layout')

@section('content')

<section class="section section-voucher-detail" ng-controller="VoucherViewCtrl" ng-init="voucher_id = '{{ $voucher_info['voucher_id'] }}'">
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-tag"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Voucher</li>
                    </ul>
                    <h4>Voucher</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
                    
        <div class="contentpanel">
            <div class="row" ng-init="voucher_info = {{ json_encode($voucher_info) }}">
                <div class="col-md-4">
                    <p><strong>Voucher ID:</strong><br>@{{ voucher_info.voucher_id }}</p>
                </div>
                <div class="col-md-4">
                    <p><strong>Mobile Number:</strong><br>@{{ owner_info.mobile }}</p>
                </div>
                <div class="col-md-4">
                    <p><strong>Status:</strong><br>@{{ voucher_info.status.label }}</p>
                </div>
                <div class="col-md-4">
                    <p><strong>Expiry Date:</strong><br>@{{ voucher_info.expiry_date_formatted }}</p>
                </div>
                <div class="col-md-4">
                    <p><strong>Transaction Date:</strong><br>@{{ voucher_info.transaction_date }}</p>
                </div>
                <div class="col-md-4">
                    <p><strong>Pint Balance:</strong><br>@{{ voucher_info.balance }}</p>
                </div>
            </div>

            <h4>Owner Detail</h4>
            <ul class="list-unstyled list-form" ng-init="owner_info = {{ json_encode($owner_info) }}">
                <li><strong>Name:</strong> @{{ owner_info.fullname }}</li>
                <li><strong>Email:</strong> @{{ owner_info.email }}</li>
                <li><strong>Country:</strong> @{{ owner_info.country }}</li>
                <li><strong>Address 1:</strong> @{{ owner_info.country }}</li>
                <li><strong>Address 2:</strong> @{{ owner_info.country }}</li>
                <li><strong>Postal Code:</strong> @{{ owner_info.country }}</li>
            </ul>

       

        <hr>
        <div ng-if="!voucher_info.voided">
            <ul class="list-inline">
            @if($user_role == 'admin')
                <li><button class="btn btn-primary" ng-click="redeem()">Redeem Voucher</button></li>
                <li><button class="btn btn-primary" ng-click="editOwner()">Edit Owner</button></li>
                <li><div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Resend Voucher
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to resend Email to Owner?" confirmation-function="resendEmail()">Resend Email</a></li>
                        <li><a href="" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to resend SMS to Owner?" confirmation-function="resendSmsOwner()">Resend SMS</a></li>
                        <!-- <li><a href="" button-confirmation confirmation-title="Confirmation" confirmation-description="Are you sure want to resend Email &  SMS to Owner?" confirmation-function="resendEmailSms()">Resend Email & SMS</a></li> -->
                    </ul>
                </div></li>
            @endif
                <li><a class="btn btn-default" href="{{ url('/admin/voucher') }}">Back</a></li>
            </ul>
        </div>
        <hr>

        <h4>Transaction History</h4>
        <div class="panel panel-default" ng-init="redemptions = {{ json_encode($redemptions) }}">
            
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Total Pint</th>
                                <th>Subtotal</th>
                                <th>Discount</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="redemption in redemptions">
                                <td>@{{ redemption.date }}</td>
                                <td>@{{ redemption.amount }}</td>
                                <td>@{{ redemption.cashier }}</td>
                                <td>@{{ redemption.receipt_no }}</td>
                                <td>@{{ redemption.property }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <h4>Redemption History</h4>
        <div class="panel panel-default" ng-init="redemptions = {{ json_encode($redemptions) }}">
            
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="redemption in redemptions">
                                <td>@{{ redemption.date }}</td>
                                <td>@{{ redemption.property }}</td>
                                <td>@{{ redemption.value }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/voucher_view.js') }}"></script>
@endsection
