@extends('admin.layout')

@section('content')
<div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-tag"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li><a href="">Pages</a></li>
                                    <li>Payment</li>
                                </ul>
                                <h4>Payment</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">

                  
        <form method="get" action="{{ url('admin/payment') }}">
            <label>FILTER</label>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input class="form-control" name="voucher_id" placeholder="Voucher ID" value="">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" >Filter</button>
            <a href="{{ url('admin/payment') }}" class="btn btn-default">Reset</a>
        </form>

                  <hr>
                  
                  <div class="table-responsive" >
                    <table class="table table-striped">
                      <thead>
                          <tr>
                            <th>Order Number</th>
                            <th>Voucher ID</th>
                            <th>Confirmation Code</th>
                            <th>Transaction ID</th>
                            <th>Authorization Code</th> 
                            <th>Card Number</th>
                            <th>Amount</th>
                            <th>Currency</th>
                          </tr>
                      </thead>
                      <tbody>
                      @foreach($data as $d)
                        <tr>
                          <td>{{ $d->order_number }}</td>
                          <td>{{ $d->voucher_id }}</td>
                          <td>{{ $d->confirmation_code }}</td>
                          <td>{{ $d->transaction_id }}</td>
                          <td>{{ $d->authorization_code }}</td>
                          <td>{{ $d->card_number }}</td>
                          <td>{{ $d->amount }}</td>
                          <td>{{ $d->currency_code }}</td>

                          <!-- <td>{{ $d['order_number']}}</td>
                          <td>{{ $d['voucher_id']}}</td>
                          <td>{{ $d['confirmation_code']}}</td>
                          <td>{{ $d['transaction_id']}}</td>
                          <td>{{ $d['authorization_code']}}</td>
                          <td>{{ $d['card_number']}}</td>
                          <td>{{ $d['amount']}}</td>
                          <td>{{ $d['currency_code']}}</td>  -->
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                    <div class="text-center">
                      <div class="pagination">
                        <ul>{{ $data->links() }}</ul>
                      </div>
                    </div>
                  </div>
                
                </div><!-- contentpanel -->
                    
                    </div>
@endsection

<!-- @section('script')
<script src="{{ asset('assets/admin/js/voucher_view.js') }}"></script>
@endsection -->