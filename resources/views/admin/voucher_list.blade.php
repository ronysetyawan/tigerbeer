@extends('admin.layout')

@section('content')

<section class="section" ng-controller="VoucherListCtrl" ng-init="vouchers = {{ json_encode($vouchers) }}; data_vouchers = {{ json_encode($data_vouchers) }} ">


<div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-tag"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li><a href="">Pages</a></li>
                                    <li>Voucher</li>
                                </ul>
                                <h4>Voucher</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">


        <form method="get" action="{{ url('admin/voucher') }}">
            <label>FILTER</label>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input class="form-control" name="voucher_id" placeholder="Voucher ID" value="{{ $filter_datas['voucher_id'] }}" ng-model="form_search.voucher_id">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input class="form-control" name="name" placeholder="Customer Name" value="{{ $filter_datas['name'] }}" ng-model="form_search.name">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input class="form-control" name="email" placeholder="Email" value="{{ $filter_datas['email'] }}" ng-model="form_search.email">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input class="form-control" name="mobile_number" placeholder="Mobile Number" value="{{ $filter_datas['mobile_number'] }}" ng-model="form_search.mobile_number">
                    </div>
                </div>      
            </div>
            <button type="submit" class="btn btn-primary" >Filter</button>
            <a href="{{ url('admin/voucher') }}" class="btn btn-default">Reset</a>
        </form>

        <hr>

        <div class="table-responsive" >
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Voucher ID</th>
                        <th>Pint Balance</th>
                        <th>Transaction Date</th>
                        <th>Expiry Date</th>
                        <th>Status</th>
                        <th>Sender</th>
                        <th>Item Name</th>
                        <th>Recipient</th>
                        <th>Property Name</th>
                        <th class="text-right">Action</th>
                        <th style="width:1px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-if="vouchers.length>=1" ng-repeat="voucher in vouchers" class="voucher-row voucher-row-@{{$index}}" ng-cloak>
                        <td>@{{voucher.voucher_id}}</td>
                        <td>@{{voucher.balance}}</td>
                        <td>@{{voucher.created_date}}</td>
                        <td>@{{voucher.expiry_date}}</td>
                        <td>@{{voucher.status.label}}</td>
                        <td>@{{voucher.sender}} </td>
                        <td>@{{voucher.item_name}}</td>
                        <td>@{{voucher.owner}} </td>
                        <td>@{{voucher.property}}</td>
                        <td class="text-right"><a class="btn btn-xs btn-primary" href="/admin/voucher/@{{voucher.voucher_id}}">Detail</a></td>
                    </tr>
                    <tr ng-if="vouchers.length<=0" ng-cloak><td colspan="9" class="text-center">There is no data</td></tr>
                </tbody>
            </table>
        </div>
           
     </div><!-- contentpanel -->
                    
                    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('assets/admin/js/voucher.js') }}"></script>
@endsection
