@extends('voucher.layout')

@section('content')

<div class="img-head">
    <img class="img-responsive" src="{{ asset('assets/voucher/image/RedemptionBanner605x251.png') }}" alt="Tiger" />
</div>
<div class="main-container redeem-container"  ng-controller="RedeemCtrl" ng-init="base_url = '{{ $base_url }}'; vid = '{{ $vid }}'; balance= {{ $total_pint }};">
    <div ng-if="!success">
        <h2 class="text-center" style="margin-bottom: 2px; margin-top: 2px; font-size: 20px;">
            You have <span style="color: #EF8A1E">@{{ balance }}</span> Entitlements for redemption.
        </h2>
        
        <p class="text-center">Select the quantity of <strong>{{$product_name}}</strong> you would like to redeem.</p>
        <div class="input-group input-group-lg qty-counter">
            <span class="input-group-btn">
                <button class="btn" type="button" ng-click="decreaseQty()">-</button>
            </span>
            <input type="text" onkeypress='validate(event)' class="form-control" readonly value="@{{ qty }}">
            <span class="input-group-btn">
                <button class="btn" type="button" ng-click="increaseQty()">+</button>
            </span>
        </div>

        <!-- <a type="button" class="btn btn-default btn-block text-uppercase" href="{{ $base_url }}">Cancel</a>  -->


        <hr>
        <h2 class="text-center">For staff use only</h2>

        <p class="text-center">Show this to the staff to complete redemption.</p>

        <div class="form-group list-input-pin-container">
            <ul class="list-inline list-input-pin text-center">
                <li><input type="text" maxlength="1" ng-model="form_pin.input_1" id="input_1" move-next-on-maxlength next="#input_2"></li><li>
                    <input type="text" maxlength="1" onkeypress='validate(event)' ng-model="form_pin.input_2" id="input_2" move-next-on-maxlength next="#input_3"></li><li>
                        <input type="text" maxlength="1" onkeypress='validate(event)' ng-model="form_pin.input_3" id="input_3" move-next-on-maxlength next="#input_4"></li><li>
                            <input type="text" maxlength="1" onkeypress='validate(event)' ng-model="form_pin.input_4" id="input_4" move-next-on-maxlength next="#input_5"></li><li>
                                <input type="text" maxlength="1" onkeypress='validate(event)' ng-model="form_pin.input_5" id="input_5" move-next-on-maxlength next=".btn-primary"></li>
            </ul>
            <div class="alert alert-danger" role="alert" ng-if="error_property">@{{ error_property }}</div>
        </div>
        <button type="submit" class="btn btn-primary btn-block text-uppercase" style="background-color: #EF8A1E; border-color: #ef8a1e;" ng-click="redeem()">REDEEM NOW</button>
        <a type="button" class="btn btn-default btn-block text-uppercase" style="background-color: #EF8A1E; border-color: #ef8a1e;" href="{{ $cancel }}">BACK</a> 

    </div>

    <div class="redeem-success" ng-if="success">
        <h2 class="mt-0 mb-0 text-center page-title">Redemption Successful!</h2>
        <div class="text-center">
            <span class="icon-success"></span>
        </div>
        <button type="button" class="btn btn-default btn-block btn-lg" ng-click="backSuccess()">Back</button>
    </div>

</div>
@endsection
@section('script')
<script>
    function validate(evt) {
        var theEvent = evt || window.event;

        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
        // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
<script src="{{ asset('assets/voucher/js/redeem.js') }}"></script>
@endsection
