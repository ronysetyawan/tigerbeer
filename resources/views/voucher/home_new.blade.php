@extends('web_purchase.layout')
@section('style')
    <style>
        body #reg_success{
            background-image: url("{{ url('/assets/web-purchase/image/age-mobile.png') }}");
        }

        @media screen and (max-width: 480px){
            .here_btn{
                bottom: 3px;
            }
        }
    </style>    
@endsection
@section('content')

<div class="home" home-scroll>
    

    <div class="home" ng-controller="HomeController" ng-init="base_url = '{{ $base_url }}'">

        <section id="reg_success" class="reg_success">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12" style="margin-bottom: 2%;">
                        <div class="col-md-12" style="margin-bottom: 2%;">
                            <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image').'/'. $product_details->image }}">
                        </div>
                        <div class="col-md-12">
                            <form action="/voucher/{{$code}}/{{$vid}}/redeem">
                                <input type="hidden" name="product_id" value="{{$product_details->item_id}}">
                                <input type="hidden" name="product_name" value="{{$product_details->name}}">
                                <button type="submit" class="btn btn-sm btn-block" style="background-color: #f26947;color: #fff;font-size: large;font-weight: bold;border: 0;">REDEEM</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-2 col-sm-12" style="margin-bottom: 2%;">
                        <div class="col-md-12">
                            <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/Banner-success.png') }}">
                            <p class="here recipes-desktop">
                                Here are some amazing <br> sunset drink recipes you <br> can try at home.
                            </p>
                            <p class="here recipes-mobile">
                                Here are some amazing sunset drink <br> recipes you can try at home.
                            </p>
                            <a href="{{ url('recipes') }}" class="btn here_btn">LEARN MORE</a>
                        </div>  
                    </div>
                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                        <div class="col-md-12">
                            <img class="img-responsive" id="prizes1" src="{{ asset('assets/web-purchase/image/banner-succes2.png') }}">
                            <p class="here recipes-desktop">
                                Find out where you can <br> redeem your drinks
                            </p>
                            <p class="here recipes-mobile">
                                Find out where you can redeem <br> your drinks
                            </p>
                            <a href="{{ url('/#how-to-participate') }}" class="btn here_btn">LEARN MORE</a>
                        </div>  
                    </div>  
                </div>
            </div>
            <div class="col-md-6 footer-left text-white" style="top: 60px;">
                &copy; 2020 William Grant & Sons. All Rights Reserved.
            </div>
            <div class="col-md-6 footer-right text-white" style="top: 60px;">
                <ul class="list-inline footer-menu">
                    <li><a href="{{ url('privacy-policy') }}" target="_blank" style="color: #fff;">Privacy Policy</a></li>
                    <li><a href="{{ url('terms-and-conditions') }}" target="_blank" style="color: #fff;">Terms of Use</a></li>
                </ul>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/home.js') }}"></script>
@endsection
