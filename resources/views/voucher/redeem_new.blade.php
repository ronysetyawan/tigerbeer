@extends('web_purchase.layout')
@section('content')
@section('style')
    <style>
        body #reg_success{
            background-image: url("{{ url('/assets/web-purchase/image/age-mobile.png') }}");
            padding-bottom: 300px;
        }

        .redeem {
            height: 45px;
            width: 45px;
            font-size: 25px;
            text-align: center;
            border-radius: 8px;
        }
        .redeem::-webkit-inner-spin-button,
        .redeem::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        @media screen and (min-width: 768px){
            .col-xs-2 {
                width: 10%;
            }
            .col-xs-offset-2 {
                margin-left: 35%;
            }
        }

        @media screen and (min-width: 480px)and (max-width: 767px){
            .col-xs-2 {
                width: 12%;
            }
            .col-xs-offset-2 {
                margin-left: 35%;
            }

            .redeem {
                height: 35px;
                width: 35px;
                font-size: 25px;
                text-align: center;
                border-radius: 8px;
            }
            .redeem::-webkit-inner-spin-button,
            .redeem::-webkit-outer-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
        }
    </style>    
@endsection

<div class="home" home-scroll>
    

    <div class="home" ng-controller="RedeemCtrl" ng-init="base_url = '{{ $base_url }}'; vid = '{{ $vid }}'; balance= {{ $total_pint }};">

        <section id="reg_success" class="reg_success">

            <div class="container" ng-if="!success">
                <div class="row">
                    <div class="col-xs-12" style="margin-bottom: 2%;margin-top: 6%;">
                        <p class="text-center" style="color:#fff;font-weight: bold;">
                            Hello {{ $voucher->owner_name }},
                            <br>
                            You can redeem {{ $total_pint }} free drinks.
                            <br><br>
                            Redemption period:
                            <br>
                            From now till 31 Oct 2020.</p>
                        <div class="input-group input-group-lg qty-counter" style="margin-bottom: 4%;margin-top: 4%;">
                            <span class="input-group-btn">
                                <button class="btn form-control" type="button" ng-click="decreaseQty()" style="border-top-right-radius: 0px !important;border-bottom-right-radius: 0px !important;">-</button>
                            </span>
                            <input type="text" onkeypress='validate(event)' class="form-control text-center" readonly value="@{{ qty }}" style="border-left: white;border-right: white;">
                            <span class="input-group-btn">
                                <button class="btn form-control" type="button" ng-click="increaseQty()" style="border-top-left-radius: 0px !important;border-bottom-left-radius: 0px !important;">+</button>
                            </span>
                        </div>
                        <p class="text-center" style="color:#fff;font-weight: bold;">PLEASE SHOW YOUR PHONE TO THE STAFF TO REDEEM </p>
                        <br>
                        <div class="col-xs-12" style="text-align: -webkit-center;margin-bottom: 10%;">
                            <div class="form-group">
                                <div class="col-md-3 col-xs-3 no-padding-left">
                                    <input class="form-control redeem" ng-model="form_pin.input_1" id="codeBox1" type="text" maxlength="1" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)"/>
                                </div>
                                <div class="col-md-3 col-xs-3 no-padding-left">
                                    <input class="form-control redeem" ng-model="form_pin.input_2" id="codeBox2" type="text" maxlength="1" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)"/>
                                </div>
                                <div class="col-md-3 col-xs-3 no-padding-left">
                                    <input class="form-control redeem" ng-model="form_pin.input_3" id="codeBox3" type="text" maxlength="1" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)"/>
                                </div>
                                <div class="col-md-3 col-xs-3 no-padding-left">
                                    <input class="form-control redeem" ng-model="form_pin.input_4" id="codeBox4" type="text" maxlength="1" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)"/>
                                </div>
                            </div>
                        </div>
                        <div ng-if="error_property" class="panel panel-default" style="margin-top: 50px;width: 70%;border-radius: 10px;margin-left: 12%;height: 10px;">
                            <div class="panel-body" style="background-color: #fff;border-radius: 10px;">
                                <center><label g-if="error_property" ng-cloak style="color: #F58337;"><i class="fa fa-exclamation" style="color: red;"></i> @{{error_property}}</label></center>
                            </div>
                        </div>
                        <br><br>
                        <button type="submit" class="btn btn-default btn-block text-uppercase" style="background-color: #f26947;color: #fff;font-size: large;font-weight: bold;border: 0;" ng-click="redeem()">REDEEM</button>
                        <a type="button" class="btn btn-default btn-block text-uppercase" style="background-color: #f26947;color: #fff;font-size: large;font-weight: bold;outline: none;" href="{{ $cancel }}">BACK</a> 
                    </div>
                </div>
            </div>

            <div class="container" ng-if="success">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="color: #d8c7a7;text-align: center;">Redemption Successful!</h3>
                        <div class="text-center">
                            <span class="icon-success"></span>
                        </div>
                        <button type="button" class="btn btn-default btn-block btn-lg" ng-click="backSuccess()">Back</button>
                    </div>
                </div>
            </div>

            <div class="col-md-6 footer-left text-white" style="top: 60px;">
                &copy; 2020 William Grant & Sons. All Rights Reserved.
            </div>
            <div class="col-md-6 footer-right text-white" style="top: 60px;">
                <ul class="list-inline footer-menu">
                    <li><a href="{{ url('privacy-policy') }}" target="_blank" style="color: #fff;">Privacy Policy</a></li>
                    <li><a href="{{ url('terms-and-conditions') }}" target="_blank" style="color: #fff;">Terms of Use</a></li>
                </ul>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/web-purchase/js/home.js') }}"></script>
<script>
    function getCodeBoxElement(index) {
      return document.getElementById('codeBox' + index);
    }
    function onKeyUpEvent(index, event) {
      const eventCode = event.which || event.keyCode;
      if (getCodeBoxElement(index).value.length === 1) {
        if (index !== 5) {
          getCodeBoxElement(index+ 1).focus();
        } else {
          getCodeBoxElement(index).blur();
        }
      }
      if (eventCode === 8 && index !== 1) {
        getCodeBoxElement(index - 1).focus();
      }
    }
    function onFocusEvent(index) {
      for (item = 1; item < index; item++) {
        const currentElement = getCodeBoxElement(item);
        if (!currentElement.value) {
            currentElement.focus();
            break;
        }
      }
    }
</script>
<script src="{{ asset('assets/voucher/js/redeem.js') }}"></script>
@endsection
