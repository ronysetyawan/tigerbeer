@extends('voucher.layout')
<style>
    .btn-grey {
  color: #D3D3D3;
  background-color: #D3D3D3;
  border-color: #D3D3D3;
  font-weight: bold;
  letter-spacing: 0.05em;
  min-width: 120px;
}

.btn-grey {
  color: #3a3a3a;
  background-color: #D3D3D3;
  border-color: #D3D3D3;
  border-radius: 5;
}

.btn-grey:hover,
.btn-grey:active,
.btn-grey:focus,
.btn-grey.active {
  background: #D3D3D3;
  color: #3a3a3a;
  border-color: #D3D3D3;
  
}

</style>
@section('body')
class="welcome"
@endsection

@section('content')

<div class="img-head">
    <img class="img-responsive" src="{{ asset('assets/voucher/image/RedemptionBanner605x251.png') }}" alt="Tiger" />
</div>

<div class="main-container welcome-container">
    <!-- <div class="home-top">
        <div class="row">
            <div class="col-xs-12">
            </div>
        </div>
    </div> -->
 
    <div class="text-center" style="margin-top:30px;">
        <strong>
        Hello {{ $name }}, <br>
        You have <span style="color:orangered;"><strong>{{ $total_pints }}</strong></span> free beers to be redeemed. <br>
        <br>
        Good News! Most F&B businesses will resume operations and reopen for dine-in. <br>Redemption will be available starting 19th June 2020 and it has been extended to 31st October 2020.<br>
        <br>
        Stay Safe and Enjoy Responsibly! <br><br>
        <!-- <a href="javascript:void(0);" class="btn btn-grey btn-disabled">REDEMPTION STARTS 19 JUN 2020</a> -->
        <a href="{{ url()->current().'/dash' }}" class="btn btn-warning">REDEEM NOW</a>
        </strong>
    </div>

    <div class="text-center" style="margin-top:30px;">
        <p><a href="{{ url()->current().'/outlet' }}" target="_blank">Click here</a> 
            for list of participating outlets.
        </p>
    </div>

    <div class="text-center" style="margin-top:30px;">
        <p>Encountered an issue? Please contact us at <br> 
            <a href="mailto:enquiries@apb.com.sg">enquiries@apb.com.sg</a>
        </p>
    </div>

</div>
@endsection
@section('script')
<script src="{{ asset('assets/voucher/js/home.js') }}"></script>
@endsection
