@extends('voucher.layout')

@section('content')

<div class="main-container login-container" ng-controller="AuthCtrl">
   
    <h2>Hi {{ $user_name }}!</h2>

    @if($has_password)
    <p class="subtitle">Enter your Login PIN</p>
    @else
    <p class="subtitle">To begin, set your login PIN</p>
    @endif
    
    <ul class="list-inline list-input-pin">
        <li><input type="tel" class="form_pin_input_1" maxlength="1" ng-model="form_pin.input_1"></li><li>
        <input type="tel" class="form_pin_input_2" maxlength="1" ng-model="form_pin.input_2"></li><li>
        <input type="tel" class="form_pin_input_3" maxlength="1" ng-model="form_pin.input_3"></li><li>
        <input type="tel" class="form_pin_input_4" maxlength="1" ng-model="form_pin.input_4"></li><li>
        <input type="tel" class="form_pin_input_5"  maxlength="1" ng-model="form_pin.input_5"></li>
    </ul>
    <div class="alert alert-danger" role="alert" ng-if="error">@{{ error }}</div>
    @if($has_password)
    <button class="btn btn-primary btn-block btn-lg" ng-click="login()">Login</button>
    <!-- <p class="forgot-password"><a href="#">Forgot Your PIN?</a></p> -->
    @else
    <button class="btn btn-primary btn-block btn-lg" ng-click="setPin()">Login</button>
    @endif

</div>
@endsection
@section('script')
<script src="{{ asset('assets/voucher/js/auth.js') }}"></script>
@endsection
