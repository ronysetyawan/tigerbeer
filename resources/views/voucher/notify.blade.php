@extends('voucher.layout')
<style>
    .btn-grey {
  color: #D3D3D3;
  background-color: #D3D3D3;
  border-color: #D3D3D3;
  font-weight: bold;
  letter-spacing: 0.05em;
  min-width: 120px;
}

.btn-grey {
  color: #3a3a3a;
  background-color: #D3D3D3;
  border-color: #D3D3D3;
  border-radius: 5;
}

.btn-grey:hover,
.btn-grey:active,
.btn-grey:focus,
.btn-grey.active {
  background: #D3D3D3;
  color: #3a3a3a;
  border-color: #D3D3D3;
  
}

</style>
@section('body')
class="welcome"
@endsection

@section('content')

<div class="img-head">
    <img class="img-responsive" src="{{ asset('assets/voucher/image/RedemptionBanner605x251.png') }}" alt="Tiger" />
</div>

<div class="main-container welcome-container">
    <!-- <div class="home-top">
        <div class="row">
            <div class="col-xs-12">
            </div>
        </div>
    </div> -->
 
    <div class="text-center" style="margin-top:30px;">
        <strong>
        Hello {{ $name }}, <br>
        Unfortunately your contribution was unsuccessful as there were errors with your payment. <br>
        <br>
        if you would like to show your support and contribute again, please click on the button below. <br>
        <br>
        <a href="https://supportourfnb.tigerbeer.com.sg" target="_blank" class="btn btn-warning">Show Your Support</a>
        </strong>
    </div>
    

    <div class="text-center" style="margin-top:30px;">
        <p>Encountered an issue? Please contact us at <br> 
            <a href="mailto:enquiries@apb.com.sg">enquiries@apb.com.sg</a>
        </p>
    </div>

</div>
@endsection
@section('script')
@endsection
