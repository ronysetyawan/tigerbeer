@extends('voucher.layout')

@section('content')

<div class="main-container prize-container" ng-controller="PrizeCtrl" ng-init="base_url = '{{ $base_url }}'; 
histories = {{ json_encode($histories) }};
total_point={{ $user_points }}; countries={{ json_encode($countries) }}; ">

    <div ng-if="!prize_type_shipping_product_selected && !prize_shipping_product_success && !prize_promocode_success">
        <a id="prize-selection"></a>
        <h1 class="mt-0  page-title text-center" style="margin-bottom: 10px;">You have <span ng-cloak>@{{ total_point }}</span> point(s)</h1>
        <p class="text-center text-scroll"><a href="#prizes">Click here to scroll down to view your prizes</a></p>
        
        <div class="prizes" ng-init="prizes.data = {{ json_encode($prizes) }}">
            <ul class="list-unstyled list-prizes">
                <li ng-repeat="prize in prizes.data">
                    <div class="row">
                        <div class="col-xs-6">
                            <img class="img-responsive" src="@{{ prize.image }}"  ng-cloak />
                        </div>
                        <div class="col-xs-6">
                            <span class="title1" ng-cloak>@{{ prize.title1 }}<span ng-if="prize.type=='shipping-product'">*</span></span>
                            <span class="title2" ng-cloak>(@{{ prize.point_needed }} points required)</span>
                            <button class="btn btn-primary btn-block text-uppercase" ng-if="prize.more_point_needed<=0" ng-click="selectPrize($index)" ng-cloak>Select</button>
                            <button class="btn btn-default btn-block text-uppercase" ng-if="prize.more_point_needed>=1" disabled ng-cloak>@{{ prize.more_point_needed }} more points</button>
                            <a href="{{ url('create') }}" class="btn btn-default btn-block text-uppercase btn-sm btn-buy-more" ng-if="prize.more_point_needed>=1" ng-cloak>Buy More</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <p class="note-free">* Free delivery to your doorstep!</p>
        <p><a href="{{ $base_url }}" class="btn btn-default text-uppercase btn-block">Back</a></p>
        
        <a id="prizes"></a>
        <h2 class="h1 page-title history-title text-center text-uppercase" ng-class="{'prize-empty':histories.length<=0}">Your Prizes</h2>
        <p class="text-center" ng-if="histories.length<=0" ng-cloak>You have not selected any prizes yet.</p>
        <a href="#prize-selection" class="btn btn-primary btn-block text-uppercase btn-select-prize-now" ng-if="histories.length<=0" ng-cloak>Select Now</a>
        <div class="promocodes" ng-if="histories.length>=1">
            <ul class="list-unstyled list-histories">
                <li ng-repeat="history in histories" ng-class="{'has-redeemed': history.redeemed}">
                    <div class="row">
                        <div class="col-xs-6">
                            <img class="img-responsive" src="@{{ history.image }}" ng-cloak />
                        </div>
                        <div class="col-xs-6">
                            <span class="title text-uppercase" ng-if="history.type=='shipping-product'" ng-cloak>@{{ history.title }}</span>
                            <span class="title text-uppercase" ng-if="history.type=='promocode'">Promo Code:</span>
                            <span class="code" ng-if="history.type=='promocode'" ng-cloak>@{{ history.title }}</span>
                            <span class="redeemed" ng-if="history.redeemed" ng-cloak>(REDEEMED)</span>
                            <a href="@{{ history.purchase_link }}" class="btn btn-default btn-block text-uppercase btn-use-now btn-sm" 
                            ng-if="history.type=='promocode' && !history.redeemed">Buy Now</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </div>

    <div class="" ng-if="prize_type_shipping_product_selected && !prize_shipping_product_success"
        ng-init="
        form_delivery.address_1='{{ $user_info['address_1'] }}'; 
        form_delivery.address_2='{{ $user_info['address_2'] }}'; 
        form_delivery.postal_code='{{ $user_info['postal_code'] }}'; 
        form_delivery.email='{{ $user_info['email'] }}'; 
        form_delivery.country={{ $user_info['country'] }}; 
        form_delivery.mobile='{{ $user_info['mobile'] }}'">
        <p class="text-center">Your item will be delivered FREE to your doorstep within 7 working days</p>
        <p class="text-center">Please confirm your delivery address below.</p>
        <div class="form-group" ng-class="{'has-error': errors_delivery.hasOwnProperty('address_1')}">
            <input type="text" class="form-control" placeholder="Address Line 1 *" ng-model="form_delivery.address_1" />
            <div class="help-block" ng-if="errors_delivery.hasOwnProperty('address_1')" ng-cloak>@{{errors_delivery.address_1}}</div>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Address Line 2" ng-model="form_delivery.address_2" />
        </div>
        <div class="form-group" ng-class="{'has-error': errors_delivery.hasOwnProperty('postal_code')}">
            <input type="text" class="form-control" placeholder="Postal Code *" ng-model="form_delivery.postal_code" />
            <div class="help-block" ng-if="errors_delivery.hasOwnProperty('postal_code')" ng-cloak>@{{errors_delivery.postal_code}}</div>
        </div>
        <div class="form-group">
            <select class="form-control" ng-model="form_delivery.country" ng-options="country.country_id as country.country_name for country in countries"></select>
        </div>
        <div class="form-group" ng-class="{'has-error': errors_delivery.hasOwnProperty('mobile')}">
            <div class="input-group">
                <span class="input-group-addon">@{{ form_delivery_calling_code }}</span>
                <input type="text" class="form-control" placeholder="Mobile number *" ng-model="form_delivery.mobile">
            </div>
            <div class="help-block" ng-if="errors_delivery.hasOwnProperty('mobile')" ng-cloak>@{{errors_delivery.mobile}}</div>
        </div>
        <div class="form-group" ng-class="{'has-error': errors_delivery.hasOwnProperty('email')}">
            <input type="email" class="form-control" placeholder="Email *" ng-model="form_delivery.email" />
            <div class="help-block" ng-if="errors_delivery.hasOwnProperty('email')" ng-cloak>@{{errors_delivery.email}}</div>
        </div>

        <ul class="list-unstyled list-buttons">
            <li><button class="btn btn-primary text-uppercase btn-block" ng-click="submitDelivery()">Submit</button></li>
            <li><button class="btn btn-default text-uppercase btn-block" ng-click="cancelDelivery()">cancel</button></li>
        </ul>

    </div>

    <div class="prize-success-issued" ng-if="prize_promocode_success">
        <h2 class="mt-0 page-title text-center">Prize Successfully<br>Issued!</h3>
        
        <div class="image-container">
            <img src="@{{ prize_promocode.image }}" class="img-responsive" />
        </div>

        <p class="text-center">Promo Code: @{{ prize_promocode.code }}</p>
        <p class="text-center">Use this promo code for the next purchase.</p>
        <p class="text-center">You may also refer to the promo code at Prize Selection.</p>

        <ul class="list-unstyled list-buttons">
            <li><a href="" class="btn btn-primary text-uppercase btn-block" ng-href="@{{ prize_promocode.purchase_link }}">Purchase Now</a></li>
            <li><a href="" class="btn btn-default text-uppercase btn-block" ng-click="back();">Back</a></li>
        </ul>

    </div>

    <div class="prize-success-issued product-shipping" ng-if="prize_shipping_product_success">
        <h2 class="mt-0 page-title text-center">Prize Successfully<br>Issued!</h3>
        
        <p class="text-center">An confirmation email has been sent to your registered email.</p>

        <p class="text-center">Your prize will be deliver to your doorstep within 7 working days by our fulfillment partner, ninjavan.</p>

        <div class="image-container">
            <img src="@{{ prize_shipping_product.image }}" class="img-responsive" />
        </div>

        <button href="" class="btn btn-default text-uppercase btn-block" ng-click="back();">Back</a>

    </div>

</div>
@endsection
@section('script')
<script src="{{ asset('assets/voucher/js/prize.js') }}"></script>
@endsection
