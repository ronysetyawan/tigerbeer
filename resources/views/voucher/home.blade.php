@extends('voucher.layout')
<style>
    .btn-grey {
  color: #D3D3D3;
  background-color: #D3D3D3;
  border-color: #D3D3D3;
  font-weight: bold;
  letter-spacing: 0.05em;
  min-width: 120px;
}

.btn-grey {
  color: #3a3a3a;
  background-color: #D3D3D3;
  border-color: #D3D3D3;
  border-radius: 5;
}

.btn-grey:hover,
.btn-grey:active,
.btn-grey:focus,
.btn-grey.active {
  background: #D3D3D3;
  color: #3a3a3a;
  border-color: #D3D3D3;
  
}

</style>
@section('body')
class="welcome"
@endsection

@section('content')

<div class="img-head">
    <img class="img-responsive" src="{{ asset('assets/voucher/image/RedemptionBanner605x251.png') }}" alt="Tiger" />
</div>
<div class="main-container welcome-container" style="padding-bottom: 20px;">
    <!-- <div class="home-top">
        <div class="row">
            <div class="col-xs-12">
            </div>
        </div>
    </div> -->
 
    <div class="text-center" style="margin-top:30px;">
        <strong>
        Hello {{ $name }}, <br>
        You can redeem <span style="color:orangered;"><strong>{{ $balance }}</strong></span> free beers. 
        </strong>
        <br><br>
        <div class="text-center">
            <p><a href="{{ url()->current().'/outlet' }}" target="_blank">Click here</a> 
                for list of participating outlets. Kindly note that some outlets may not have resumed operations.
            </p>
        </div>
        <br>
        <select class="form-control" onchange="location = this.value;">
            <option value="" disabled selected>Select the type of beer to be redeemed.</option>
            @foreach ($product2 as $p)
                <option value="/voucher/{{$code}}/{{$vid}}/redeem?product_id={{$p->id}}&product_name={{$p->name}}">{{$p->description}}</option>
            @endforeach
        </select>
        <hr>
        <!-- Stay home and stay safe! <br><br>
        <a href="javascript:void(0);" class="btn btn-grey btn-disabled">REDEEM NOW</a> -->
    </div>
    @foreach ($products as $p)
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <img style="min-width: 145px; max-height: 200px;" class="img-responsive img-thumbnail img-fluid" src="{{ asset('assets/voucher/image') }}/{{$p->image}}" alt="Tiger" />
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p style="padding-bottom: 60%"> 
                <strong>{{$p->description}}</strong>
            </p>
            <form action="/voucher/{{$code}}/{{$vid}}/redeem">
                <input type="hidden" name="product_id" value="{{$p->id}}">
                <input type="hidden" name="product_name" value="{{$p->name}}">
                <button type="submit" class="btn btn-sm btn-warning btn-block">redeem</button>
            </form>
            <!-- <button type="button" onclick="location.href='/voucher/{{$code}}/{{$vid}}/redeem'"  class="btn btn-sm btn-warning btn-block">redeem</button> -->

        </div>
    </div>
    <br>
    @endforeach
    <div class="text-center">
        <div class="pagination">
          <ul>{{ $products->links() }}</ul>
        </div>
      </div>
    
    {{-- <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
            <img style="min-width: 145px;" class="img-responsive img-thumbnail img-fluid" src="{{ asset('assets/voucher/image/t2.png') }}" alt="Tiger" />
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p style="padding-bottom: 10%">
                <strong>Redeem 1 pint of Tiger Crystal </strong>
            </p>
            <form action="/voucher/{{$code}}/{{$vid}}/redeem">
                <input type="hidden" name="product_id" value="2">
                <button type="submit" class="btn btn-sm btn-warning btn-block">redeem</button>
            </form>
            <!-- <button type="button" onclick="location.href='/voucher/{{$code}}/{{$vid}}/redeem/2'"  class="btn btn-sm btn-warning btn-block">redeem</button> -->
        </div>
    </div> --}}

    <div class="text-center" style="margin-top:30px;">
        <p>Encountered an issue? Please contact us at <br> 
            <a href="mailto:enquiries@apb.com.sg">enquiries@apb.com.sg</a>
        </p>
    </div>

</div>
@endsection
@section('script')
<script src="{{ asset('assets/voucher/js/home.js') }}"></script>
@endsection
