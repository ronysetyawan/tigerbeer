@extends('voucher.layout')

@section('content')

<div class="main-container home-outlet-container">
    
    <h1 class="mt-0 text-center page-title">Outlets</h1>

    <ul class="list-inline list-zone">
        @foreach($properties as $zone=>$property)
        <li><a href="#{{ $zone }}">{{ $zone }}</a></li>
        @endforeach
    </ul>
    <hr>

    <div class="col-md-12 col-sm-12">
        <input ng-model="outlet_name" ng-change="findRegionByName(outlet_name)" type="text" class="form-control outlet-filter" placeholder="Search">
    </div>

    <div class="table-responsive">

        <table class="table">
            <thead>
                <tr class="thead">
                    <th>Outlet Name</th>
                    <th colspan="2">Address</th>
                    <!-- <th>Address 2</th> -->
                </tr>
            </thead>
            <tbody>
                @foreach($properties as $zone=>$property)
                    <tr id="{{ $zone }}" class="tzone">
                        <td colspan="3" class="text-uppercase"><strong>Zone: {{ $zone }}</strong></td>
                    </tr>
                    @foreach($property as $_property)
                    <tr>
                        <td>{{ $_property['name'] }}</td>
                        <td colspan="2">{{ $_property['address1'] }}</td>
                        <!-- <td>{{ $_property['address2'] }}</td> -->
                    </tr>
                    @endforeach
                @endforeach 
            </tbody>
        </table>

    </div>

    <p class="text-center">
        <a href="{{ $base_url }}" class="btn btn-default text-uppercase btn-block">Back</a>
    </p>

</div>
@endsection
