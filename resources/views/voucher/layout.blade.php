<!DOCTYPE html>
<html ng-app="CPRV-OctFestVoucherApp">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta property="og:title" content="Support Our Local F&B!">
        <meta property="og:type" content="article">
        <meta property="og:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        <meta property="og:description" content="Support Our Local F&B.">
        
        <meta name="twitter:image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        <meta itemprop="image" content="{{ asset('assets/web-purchase/image/social-share.jpg') }}">
        
        <meta name="description" content="Support Our Local F&B.">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="{{ asset('fav.png') }}">

        <!-- App title -->
        <title>Tiger #SunsetSocial</title>

        <!-- App CSS -->
        <link href="{{ asset('assets/voucher/css/voucher.css') }}" rel="stylesheet" type="text/css" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">

        <style>
            .thead {
                color: white;
                background-color: #002d72;
            }
            .tzone {
                background-color: lightgrey;
            }

            label {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            button{
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }

            body #reg_success {
                font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
            }
        </style>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-17"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-109851240-17');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '2450175968539678'); 
            fbq('track', 'PageView');
            </script>
            <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=2450175968539678&ev=PageView &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

    </head>

    <body 
    
    @yield('body')

    ng-controller="MainCtrl" ng-init="base_url = '{{ $base_url }}'">

        <div class="container-widget">
            <header>
                <div class="logo"></div>
                <div>
                    <img class="img-responsive" src="{{ asset('assets/voucher/image/voucherBar.png') }}" alt="Tiger" />
                </div>
            </header>
            @yield('content')

            <footer class="text-center">
                <p>© 2020 Copyright APB Singapore. All rights reserved.</p>
                
                <ul class="list-inline">
                    <li><a href="{{ url('terms-and-conditions') }}" target="_blank">Terms of Use</a></li>
                    <li>|</li>
                    <li><a href="{{ url('/') }}/#faqs" target="_blank">FAQs</a></li>
                </ul>
            </footer>
        </div>

        <script src="{{ asset('assets/voucher/js/voucher.js') }}"></script>

        @yield('script')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109851240-13"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109851240-13');
        </script>


    </body>
</html>