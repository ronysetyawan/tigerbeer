@extends('voucher.layout')

@section('content')

<div class="main-container account-container" ng-controller="AccountCtrl" ng-init="
    form.name = '{{ $user_info['name'] }}';
    form.gender = '{{ $user_info['gender'] }}';
    form.email = '{{ $user_info['email'] }}';
    form.mobile = '{{ $user_info['mobile'] }}';
    form.address_1 = '{{ $user_info['address_1'] }}';
    form.address_2 = '{{ $user_info['address_2'] }}';
    form.postal_code = '{{ $user_info['postal_code'] }}';
    form.country = {{ $user_info['country'] }};
    form.subscribe = {{ $user_info['subscribe']?'true':'false' }};
    countries={{ json_encode($countries) }};
    dobs.year='{{ $user_info['dobs']['year'] }}';
    dobs.month='{{ $user_info['dobs']['month'] }}';
    dobs.date='{{ $user_info['dobs']['date'] }}';
">

    <div ng-if="!deleted">
        <h1 class="mt-0 page-title text-center">Update your Particulars.</h1>
        <form>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('name')}">
                <input type="text" class="form-control" ng-model="form.name" placeholder="Your Name">
                <div class="help-block" ng-if="errors.hasOwnProperty('name')" ng-cloak>@{{errors.name}}</div>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('gender')}">
                        <select class="form-control" ng-model="form.gender">
                            <option value="">-- select --</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        <div class="help-block" ng-if="errors.hasOwnProperty('gender')" ng-cloak>@{{errors.gender}}</div>
                    </div>
                </div>
                <div class="col-xs-7">
                    <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('dob')}">
                        <input type="text" class="form-control" date="form.dob" placeholder="Date of Birth" dob-picker-ui max-year="2018" total-year="80" readonly />
                        <div class="help-block" ng-if="errors.hasOwnProperty('dob')" ng-cloak>@{{errors.dob}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group" ng-class="{'has-error': errors.hasOwnProperty('email')}">
                <input type="email" class="form-control" ng-model="form.email" placeholder="Email">
                <div class="help-block" ng-if="errors.hasOwnProperty('email')" ng-cloak>@{{errors.email}}</div>
            </div>

            <!-- <div class="form-group">
                <input type="text" class="form-control" ng-model="form.address_1" placeholder="address Line 1">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" ng-model="form.address_2" placeholder="Address Line 2">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" ng-model="form.postal_code" placeholder="Postal Code">
            </div> -->

            <div class="checkbox" ng-if="isCollapsedDeleteAccount">
                <label>
                    <input type="checkbox" ng-model="form.subscribe"> I would like to receive information about the goods and services which may be provided by Asia Pacific Breweries (Singapore) Pte Ltd and their partner outlets, including (but not limited to) offers,promotions and information about new goods and services, via newsletters, emails and/or text messages. 
                </label>
            </div>

            <ul class="list-unstyled list-buttons" ng-if="isCollapsedDeleteAccount">
                <li>
                    <button type="button" class="btn btn-primary btn-block text-uppercase" ng-click="update()">Update</button>
                </li>
                <li>
                    <a href="{{ $base_url }}" class="btn btn-default btn-block text-uppercase">Back</a>
                </li>
            </ul>

            <p class="text-center btn-delete-container">
                <a href="" class="text-uppecase text-danger" ng-click="isCollapsedDeleteAccount = !isCollapsedDeleteAccount">Delete My Account</a>
            </p>

            <div ng-if="!isCollapsedDeleteAccount">
                <div class="row">
                    <div class="col-xs-5">
                        To continue, please re-enter your PIN
                    </div>
                    <div class="col-xs-7">
                        <input type="password" class="form-control" ng-model="form_delete_account.pin">
                    </div>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="form_delete_account.confirm"> By acknowledging this statement and clicking ‘Permanently delete my account’ below, your points earned and purchases will not be recoverable. 
                    </label>
                </div>

                <div class="alert alert-danger" role="alert" ng-if="error_delete">
                    @{{ error_delete }}
                </div>

                <button class="btn btn-primary btn-block" ng-click="deleteAccount()">Permanently Delete My Account</button>
            </div>
        </form>
    </div>

    <div ng-if="deleted">
        <h3 class="text-center">We're sad to see you go</h3>
        <p class="text-center">Your Account has been permanently deleted.</p>
        <p><img src="{{ asset('assets/voucher/image/icon-sad.jpg') }}" class="img-responsive"></p>
        <a href="{{ url('/') }}" class="btn btn-default btn-block text-uppercase">Oktoberfest Homepage</a>
    </div>

</div>
@endsection
@section('script')
<script src="{{ asset('assets/voucher/js/account.js') }}"></script>
@endsection
