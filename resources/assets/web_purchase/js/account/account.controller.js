angular
    .module('CPRV-OctFestApp')
    .controller('AccountCtrl', ['$scope', 'cfg', '$log', '$uibModal', '$uibModal', 'AccountService', AccountCtrl]);

  function AccountCtrl($scope, cfg, $log, $uibModal, $uibModal, AccountService){

    $scope.dobs = {};

    $scope.form = {};

    $scope.errors = {};

    $scope.$watch('dobs', function(){
      if($scope.dobs.year && $scope.dobs.month && $scope.dobs.date){
        $scope.form.dob = new Date($scope.dobs.year, ($scope.dobs.month-1), $scope.dobs.date)
      }
    }, true);

    $scope.submit = function(){

      $scope.errors = {};
      
      if(!angular.isDefined($scope.form.name) || !$scope.form.name){
        $scope.errors.name = "Name cannot be empty!";
      }
      if(!angular.isDefined($scope.form.gender) || !$scope.form.gender){
        $scope.errors.gender = "Gender cannot be empty!";
      }
      if(!angular.isDefined($scope.form.email) || !$scope.form.email){
        $scope.errors.email = "Email cannot be empty or not valid!";
      }
      if(!angular.isDefined($scope.form.postal_code) || !$scope.form.postal_code){
        $scope.errors.postal_code = "Postal Code cannot be empty!";
      }
      /*if($scope.error_dob.data){
        $scope.errors.dob = $scope.error_dob.data;
      }*/
      
      if(angular.equals({}, $scope.errors)){

        var dob = $scope.form.dob.getFullYear() + "-" + ($scope.form.dob.getMonth()+1) + "-" + $scope.form.dob.getDate();

        var submitPromise = AccountService.submit({
          name: $scope.form.name,
          gender: $scope.form.gender,
          dob: dob,
          email: $scope.form.email,
          address_1: $scope.form.address_1,
          address_2: $scope.form.address_2,
          postal_code: $scope.form.postal_code,
          subscribe: $scope.form.subscribe
        });
        submitPromise.then(function(){
          $uibModal.open({
            template: 
                '<div class="modal-body">' +
                '   Data successfully updated' +
                '</div>' +
                '<div class="modal-footer">' +
                '   <button class="btn btn-primary" type="button" ng-click="close()">Close</button>' +
                '</div>',
            controller: ['$scope', '$uibModalInstance', 'error_term', function($scope, $uibModalInstance, error_term){
            $scope.error_term = error_term;
            $scope.close = function(){
              $uibModalInstance.dismiss();
            }
            }],
            resolve: {
            error_term: function(){
              return $scope.error_term;
            }
            }
          });
        }, function(){

        });
      }
    };

    $scope.deleteAccount = function(){
      var deletePromise = AccountService.deleteAccount();
      deletePromise.then(function(response){
        window.location.href = response.redirect;
      }, function(){

      });
    }

    $scope.calling_code = "-";

    $scope.$watch('form.country', function(){
      
      if(!$scope.form.country){
        $scope.form = "-";
      }else{
        for(var i=0; i<$scope.countries.length; i++){
          if($scope.countries[i].country_id == $scope.form.country){
            $scope.calling_code = $scope.countries[i].calling_code;
            break;
          }
        }
      }
    });

  }
