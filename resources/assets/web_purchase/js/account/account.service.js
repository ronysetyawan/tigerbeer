angular.module('CPRV-OctFestApp')
	.service("AccountService",['cfg', '$q', '$http', '$log', AccountService]);

function AccountService(cfg, $q, $http, $log){  

	this.submit = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/account',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };

    this.deleteAccount = function() {
		var deferred = $q.defer();
		$http({
			method	: 'DELETE',
			url     : cfg.api + '/account'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };
}
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
