angular
    .module('CPRV-OctFestApp')
    .controller('ConfirmationCtrl', ['$scope', '$rootScope', '$log', '$uibModal', 'CartService', 'AuthService', 'CheckoutService', ConfirmationCtrl]);

  function ConfirmationCtrl($scope, $rootScope, $log, $uibModal, CartService, AuthService, CheckoutService){

    $scope.form_login = {
      country: 192,
      mobile: "",
      pin: ""
    };

    $scope.form_checkout = {
      name: "",
      // gender: "",
      country: 192,
      mobile: "",
      //confirm_mobile: "",
      email: "",
      // dob: "",
      company: ""
    };

    $scope.form_promo_code = {
      code: ''
    }

    $scope.account_type = "new";
    $scope.agree_newsletter = false;
    $scope.agree_terms = false;

    $scope.countries = [];

    $scope.isLoggedIn = false;

    $scope.carts = [];

    $scope.total_qty = 0;


    $scope.$on('setConfirmationLoggedIn', function(event, data){
      $scope.form_checkout.name = data.name;
      // $scope.form_checkout.gender = data.gender;
      $scope.form_checkout.country = data.country;
      $scope.form_checkout.mobile = data.mobile;
      $scope.form_checkout.email = data.email;

      // $scope.dobs.dob_date = data.dob_date;
      // $scope.dobs.dob_month = data.dob_month;
      // $scope.dobs.dob_year = data.dob_year;

      $scope.isLoggedIn = true;
    });

    $scope.login_error = "";
    $scope.login = function(){
      var loginPromise = AuthService.login({
        country: $scope.form_login.country,
        mobile: $scope.form_login.mobile,
        pin: $scope.form_login.pin,
        return_data: true
      });

      loginPromise.then(function(data){
        $scope.form_checkout.name = data.name;
        // $scope.form_checkout.gender = data.gender;
        $scope.form_checkout.country = data.country;
        $scope.form_checkout.mobile = data.mobile;
        $scope.form_checkout.email = data.email;

        // $scope.dobs.dob_date = data.dob_date;
        // $scope.dobs.dob_month = data.dob_month;
        // $scope.dobs.dob_year = data.dob_year;

        $scope.isLoggedIn = true;

        $rootScope.$broadcast('setLoggedIn');
        
      }, function(data){
        $scope.login_error = data.message;
      });
    };

    $scope.form_login_calling_code = "-";

    $scope.$watch('form_login.country', function(){
      if(!$scope.form_login.country){
        $scope.form_login_calling_code = "-";
      }else{
        for(var i=0; i<$scope.countries.length; i++){
          if($scope.countries[i].country_id == $scope.form_login.country){
            $scope.form_login_calling_code = $scope.countries[i].calling_code;
            break;
          }
        }
      }
    });

    $scope.form_checkout_calling_code = "-";

    $scope.$watch('form_checkout.country', function(){
      
      if(!$scope.form_checkout.country){
        $scope.form_checkout_calling_code = "-";
      }else{
        for(var i=0; i<$scope.countries.length; i++){
          if($scope.countries[i].country_id == $scope.form_checkout.country){
            $scope.form_checkout_calling_code = $scope.countries[i].calling_code;
            break;
          }
        }
      }
    });

    // $scope.dobs = {
    //   dob_date : "",
    //   dob_month : "",
    //   dob_year : ""
    // };

    // $scope.$watch('[dobs.dob_date, dobs.dob_month, dobs.dob_year]', function(){
    //   if($scope.dobs.dob_date, $scope.dobs.dob_month, $scope.dobs.dob_year){
    //     $scope.form_checkout.dob = new Date($scope.dobs.dob_year, ($scope.dobs.dob_month-1), $scope.dobs.dob_date);
    //   }
    // });

    //$scope.form_checkout.dob = new Date(1987, 5, 21);

    $scope.decreaseQty = function (cart_id){
      angular.forEach($scope.carts, function(value, key){
        if(cart_id == value.id){
          var updateCartPromise = CartService.decreaseQty(cart_id);
          updateCartPromise.then(function(data){
            $scope.carts[key].qty = data.qty;
            $scope.carts[key].total_pints = data.total_pints;
            $scope.carts[key].subtotal = data.subtotal;
            
            $scope.grand_total_pints = data.grand_total_pints;
            $scope.grand_total = data.grand_total;
          }, function(){

          })
        }
      });
    };
    
    $scope.increaseQty = function(cart_id){
      angular.forEach($scope.carts, function(value, key){
        if(cart_id == value.id){
          var updateCartPromise = CartService.increaseQty(cart_id);
          updateCartPromise.then(function(data){
            $scope.carts[key].qty = data.qty;
            $scope.carts[key].total_pints = data.total_pints;
            $scope.carts[key].subtotal = data.subtotal;

            $scope.grand_total_pints = data.grand_total_pints;
            $scope.grand_total = data.grand_total;
          }, function(){

          })
        }
      });
    };

    $scope.delete = function(cart_id){
      var deleteCartPromise = CartService.deleteCart(cart_id);
      deleteCartPromise.then(function(response){

        if(response.is_cart_empty){
          window.location.href = $scope.create_url;
        }else{

          delete $scope.carts[cart_id]; 

          $scope.grand_total_pints = response.grand_total_pints;
          $scope.grand_total = response.grand_total;
        }

      }, function(){
          
      });

    }


    $scope.promo_code_alert = {
      type: '',
      message: ''
    }

    $scope.closePromoCodeAlert = function(){
      $scope.promo_code_alert.type = '';
      $scope.promo_code_alert.message = '';
    }

    $scope.applyPromoCode = function(){
      
      var promoCodePromise = CartService.applyPromoCode($scope.form_promo_code.code);

      promoCodePromise.then(function(response){
        $scope.form_promo_code.code = '';

        $scope.discount = response.discount;
        $scope.promo_code_applied = response.promo_code_applied;
        $scope.grand_total = response.grand_total;

        $scope.promo_code_alert = {
          type: 'alert-success',
          message: response.message
        }
      }, function(response){

        $scope.form_promo_code.code = '';

        $scope.promo_code_alert = {
          type: 'alert-danger',
          message: response.message
        }
      });
    };

    $scope.deletePromoCode = function(){
      var promoCodePromise = CartService.deletePromoCode($scope.form_promo_code.code);

      promoCodePromise.then(function(response){
          
        $scope.discount = response.discount;
        $scope.promo_code_applied = response.promo_code_applied;
        $scope.grand_total = response.grand_total;

        $scope.promo_code_alert = {
          type: 'alert-success',
          message: response.message
        }
         
      }, function(){
          
      });
    }


    $scope.errors_checkout = {};

    // $scope.error_dob = {
    //   data: false
    // };

    function validateNumber(mobile) {
      var re = /^-?\d+\.?\d*$/;
      return re.test(mobile);
    }

    $scope.checkout = function(){

      $scope.errors_checkout = {};

      $scope.error_term = [];
      
      if(!angular.isDefined($scope.form_checkout.name) || !$scope.form_checkout.name){
        $scope.errors_checkout.name = "Name cannot be empty!";
      }
      // if(!angular.isDefined($scope.form_checkout.gender) || !$scope.form_checkout.gender){
      //   $scope.errors_checkout.gender = "Gender cannot be empty!";
      // }
      if(!angular.isDefined($scope.form_checkout.country) || !$scope.form_checkout.country){
        $scope.errors_checkout.country = "Country cannot be empty!";
      }
      if(!angular.isDefined($scope.form_checkout.mobile) || !$scope.form_checkout.mobile){
        $scope.errors_checkout.mobile = "Mobile cannot be empty!";
      } else if (!validateNumber($scope.form_checkout.mobile)) {
        $scope.errors_checkout.mobile = "Mobile is invalid!";
      } 
      // else if ($scope.form_checkout.mobile.length < 8) {
      //   $scope.errors_checkout.mobile = "Mobile must be 8 digits";  //"Mobile number 8 digits.";
      // }
      /*if(!angular.isDefined($scope.form_checkout.confirm_mobile) || !$scope.form_checkout.confirm_mobile){
        $scope.errors_checkout.confirm_mobile = "Confirm Mobile cannot be empty!";
      }else if(angular.isDefined($scope.form_checkout.mobile) && $scope.form_checkout.mobile && $scope.form_checkout.mobile != $scope.form_checkout.confirm_mobile){
        $scope.errors_checkout.confirm_mobile = "Confirm mobile does not match!";
      }*/
      if(!angular.isDefined($scope.form_checkout.email) || !$scope.form_checkout.email){
        $scope.errors_checkout.email = "Email cannot be empty or not valid!";
      }
      // if(!angular.isDefined($scope.form_checkout.dob) || !$scope.form_checkout.dob){
      //   $scope.errors_checkout.dob = "Date of birth cannot be empty!";
      // }else if($scope.error_dob.data){
      //   $scope.errors_checkout.dob = $scope.error_dob.data;
      // }
      
      if(angular.equals({}, $scope.errors_checkout)){

        if($scope.agree_terms == false){
          $scope.error_term.push('Please check the box to indicate your agreement to our terms and conditions.');
        }

        if($scope.error_term.length > 0){
          $uibModal.open({
            template: '<div class="modal-header">'+
                      '   <h3 class="modal-title">Confirmation</h3>' +
                      '</div>' +
                      '<div class="modal-body" ng-cloak>' +
                      '   <p ng-repeat="term in error_term">{{term}}</p>' +
                      '</div>' +
                      '<div class="modal-footer">' +
                      '   <button class="btn btn-primary" type="button" ng-click="close()">Close</button>' +
                      '</div>',
            controller: ['$scope', '$uibModalInstance', 'error_term', function($scope, $uibModalInstance, error_term){
              $scope.error_term = error_term;
              $scope.close = function(){
                $uibModalInstance.dismiss();
              }
            }],
            resolve: {
              error_term: function(){
                return $scope.error_term;
              }
            }
          });
        }else{
          
          // var dob = $scope.form_checkout.dob.getFullYear() + "-" + ($scope.form_checkout.dob.getMonth()+1) + "-" + $scope.form_checkout.dob.getDate();
          
          var confirmationPromise = CheckoutService.confirmation({
            name: $scope.form_checkout.name,
            // gender: $scope.form_checkout.gender,
            country: $scope.form_checkout.country,
            mobile: $scope.form_checkout.mobile,
            // confirm_mobile: $scope.form_checkout.confirm_mobile,
            email: $scope.form_checkout.email,
            // dob: dob,
            agree_newsletter: $scope.agree_newsletter
          });

          confirmationPromise.then(function(data){
            //console.log(data);
            window.location.href = data.redirect;
          }, function(data){

          });
        }
      }
    };
  }

  
