angular.module('CPRV-OctFestApp')
	.service("CartService",['cfg', '$q', '$http', '$log', CartService]);

function CartService(cfg, $q, $http, $log){
	
	this.addToCart = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/cart',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };

    this.increaseQty = function(cart_id) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/cart/increase-qty/'+ cart_id
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};
	
	this.decreaseQty = function(cart_id) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/cart/decrease-qty/'+ cart_id
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };

    this.deleteCart = function(id) {
		var deferred = $q.defer();
		$http({
			method	: 'DELETE',
			url     : cfg.api + '/cart/'+ id,
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};
	
	this.applyPromoCode = function(code) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/cart/promo-code',
			data    : {
				code: code
			}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};
	
	this.deletePromoCode = function() {
		var deferred = $q.defer();
		$http({
			method	: 'DELETE',
			url     : cfg.api + '/cart/promo-code',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};
	
	this.hasCart = function() {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/cart/has-cart',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };
}
