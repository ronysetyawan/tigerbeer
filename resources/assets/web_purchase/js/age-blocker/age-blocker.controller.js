angular
    .module('CPRV-OctFestApp')
    .controller('AgeBlockerController', ['$scope', 'AgeBlockerService', AgeBlockerController]);

  function AgeBlockerController($scope, AgeBlockerService){

    $scope.form = {
      date: 0,
      month: 0,
      year: 0
    };

    $scope.error = "";

    $scope.IsDisabled = true;

    $scope.EnableDisable = function(){
      $scope.IsDisabled = false;
    }
    $scope.submit = function(){

      if(!$scope.IsDisabled){
        var submitPromise = AgeBlockerService.submit({
          date: $scope.form.date,
          month: $scope.form.month,
          year: $scope.form.year
        });
        
        submitPromise.then(function(data){
          window.location.href = data.redirect;
        }, function(data){
          // $scope.error = data.message;
        });
      }
    }

    $scope.reload = function(){
      $scope.error = "You need to be of legal age to enter the site";
    }

  }
