angular.module('CPRV-OctFestApp')
	.service("AgeBlockerService",['cfg', '$q', '$http', '$log', AgeBlockerService]);

function AgeBlockerService(cfg, $q, $http, $log){
	
	this.submit = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/age-blocker',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };

}
