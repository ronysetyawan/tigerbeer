angular
  .module('CPRV-EgiftingApp')
  .controller('OccasionDetailCtrl', ['$scope', '$log', 'CartService', '$uibModal', OccasionDetailCtrl]);

function OccasionDetailCtrl($scope, $log, CartService, $uibModal){

  $scope.form = {
    currency: '',
    amount: '',
    qty: 1
  };

  $scope.$watch('form.currency', function(){
    if($scope.form.currency){
      $scope.amountItems = $scope.amounts[$scope.form.currency].amounts;
    }else{
      $scope.amountItems = null;
    }
  })

  $scope.selectedImageIndex = 0;
  $scope.selectedImage = '';
  $scope.selectImage = function(index){
    $scope.selectedImageIndex = index;
    $scope.selectedImage = $scope.images[index].image;
  };

  $scope.addToCart = function(){
    if(!$scope.form.amount){
        console.log('empty');
    }else{
        var cartPromise = CartService.addToCart({
          occasion: $scope.occasion,
          image: $scope.images[$scope.selectedImageIndex].id,
          currency: $scope.form.currency,
          amount: $scope.form.amount,
          qty: $scope.form.qty
        });

        cartPromise.then(function(response){
          $uibModal.open({
            template: '<div class="modal-body">' +
                '   <p>{{ message }}</p>' +	
                '   <a href="{{ cart_url }}" class="btn btn-primary">Go to Cart Page</a>' +
                '   <button type="submit" class="btn btn-primary" ng-click="close()">Close</button>' +
                '</div>',
            controller: ['$scope', '$uibModalInstance', '$log', 'cfg', 'message', 'cart_url', function($scope, $uibModalInstance, $log, cfg, message, cart_url){
              $scope.message = message;
              $scope.cart_url = cart_url;

              $scope.close = function(){
                $uibModalInstance.dismiss();
              };
            }],
            resolve: {
              message: function(){
                console.log('Success!');
                return response.message;
              },
              cart_url : function(){
                return $scope.cart_url;
              }
            }
          });

          $scope.is_lock_currency = true;
          $scope.form.amount = '';
          $scope.form.qty = 1;
          $scope.selectedImageIndex = 0;
          $scope.selectedImage = $scope.images[0].image;

        }, function(){

        });
    }
  };

  $scope.buyNow = function(){
    if(!$scope.form.amount){
        console.log('empty');
    }else{
        var cartPromise = CartService.addToCart({
          occasion: $scope.occasion,
          image: $scope.images[$scope.selectedImageIndex].id,
          currency: $scope.form.currency,
          amount: $scope.form.amount,
          qty: $scope.form.qty
        });

        cartPromise.then(function(response){
          window.location.href = "/cart";
        }, function(){

        });
    }
  };
}
