angular
    .module('CPRV-OctFestApp')
    .controller('CreateController', ['$scope', '$rootScope', '$uibModal', '$log', 'CartService', CreateController]);

  function CreateController($scope, $rootScope, $uibModal, $log, CartService){

    $scope.selectedProductIndex = 0;

    $scope.product_id = 0;

    $scope.qtys = [];

    $scope.selectProduct = function(index){

      $scope.selectedProductIndex = index;

      for(var i=0; i<$scope.qtys.length; i++){
        $scope.qtys[i] = 1;
      }

      $scope.alert = {};

      $scope.product_id = $scope.products[index].id;
    }

    $scope.increaseQty = function(index){
      $scope.qtys[index]++;
    }

    $scope.decreaseQty = function(index){
      if($scope.qtys[index]>1){
        $scope.qtys[index]--;
      }
    }

    $scope.alert = {};

    $scope.closeAlert = function(){
      $scope.alert = {};
    }

    $scope.addToCart = function(index){
      $scope.alert = {};
      if($scope.qtys[index]<=0){
        $scope.alert.type = "alert-danger";
        $scope.alert.message = "Please fill the quantity";
      }else{
        var selected_product = $scope.products[index];

        var addToCartPromise = CartService.addToCart({
          product: selected_product.id,
          pint: selected_product.pint,
          qty: $scope.qtys[index]
        });
        addToCartPromise.then(function(data){

          $scope.qtys[index] = 0;

          $scope.alert.type = "alert-success";
          $scope.alert.message = data.message;
        }, function(data){
          $scope.alert.type = "alert-danger";
          $scope.alert.message = data.message;
        });
      }
    }

    $scope.checkOut = function(index){

      var selected_product = $scope.products[index];

      var addToCartPromise = CartService.addToCart({
        product: selected_product.id,
        pint: selected_product.pint,
        qty: $scope.qtys[index]
      });
      addToCartPromise.then(function(data){
        window.location.href=$scope.confirmation_url
      }, function(data){
      });
    }

    $scope.next = function(confirmation_url){
      var hasCartPromise = CartService.hasCart();
      hasCartPromise.then(function(data){
        if(data.has_cart){
          window.location.href = confirmation_url
        }else{
          $uibModal.open({
            template: '<div class="modal-body" ng-cloak>' +
                      '   Please add a promotion product' +
                      '</div>' +
                      '<div class="modal-footer">' +
                      '   <button class="btn btn-primary" type="button" ng-click="close()">Close</button>' +
                      '</div>',
            controller: ['$scope', '$uibModalInstance', 'error_term', function($scope, $uibModalInstance, error_term){
              $scope.error_term = error_term;
              $scope.close = function(){
                $uibModalInstance.dismiss();
              }
            }],
            resolve: {
              error_term: function(){
                return $scope.error_term;
              }
            }
          });
        }
      }, function(data){
      });
    }

  }
