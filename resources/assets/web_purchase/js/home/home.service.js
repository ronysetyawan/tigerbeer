angular.module('CPRV-OctFestApp')
	.service("HomeService",['cfg', '$q', '$http', '$log', HomeService]);

function HomeService(cfg, $q, $http, $log){  
	
	this.getOutletByName = function(data){
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: cfg.api + '/region/get-by-name',
			data: {
				'region': data.region,
				'name'	: data.name
			}
		}).then(function (data) {
			deferred.resolve(data.data);
		}, function (data) {
			deferred.reject(data.data);
		});

		return deferred.promise;
	}

	this.getOutletByRegion = function(data){
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: cfg.api + '/region/get',
			data: {
				'region'	: data
			}
		}).then(function (data) {
			deferred.resolve(data.data);
		}, function (data) {
			deferred.reject(data.data);
		});

		return deferred.promise;
  }
  
  this.sendRegistration = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/checkout',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };

  this.checkRedemtionCode = function(data) {
    
    var deferred = $q.defer();
      $http({
        method	: 'POST',
        url		  : cfg.api + '/redemtion_code/check',
        data    : data
      }).then(function(data) {
      deferred.resolve(data.data);
    }, function(data) {
      deferred.reject(data.data);
    });
      
    return deferred.promise;
  };

  this.checkAge = function(data) {
    
    var deferred = $q.defer();
      $http({
        method	: 'POST',
        url		  : cfg.api + '/check_age',
        data    : data
      }).then(function(data) {
      deferred.resolve(data.data);
    }, function(data) {
      deferred.reject(data.data);
    });
      
    return deferred.promise;
  };

	this.sendEnquiry = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/contact',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };

    this.getCaptcha = function() {
        var deferred = $q.defer();
        $http({
            method	: 'GET',
            url		: cfg.api + '/captcha/image'
        }).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
        
        return deferred.promise;
    };

    this.checkCaptcha = function(data) {
        var deferred = $q.defer();
        $http({
            method	: 'POST',
            url		: cfg.api + '/captcha/check',
			data    : data
        }).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
        
        return deferred.promise;
    };
}