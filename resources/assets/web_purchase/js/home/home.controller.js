angular
    .module('CPRV-OctFestApp')
    .controller('HomeController', ['$scope', '$log', '$uibModal', 'HomeService', '$window', HomeController]);

  function HomeController($scope, $log, $uibModal, HomeService, $window){

    $scope.regionList = [
      { id: 1, name: 'Central' }, 
      { id: 2, name: 'East' },
      { id: 3, name: 'North' },
      { id: 4, name: 'North East' },
      { id: 5, name: 'South' },
      { id: 6, name: 'West' }
    ];

    // data region
    $scope.outlet_name = "";
    $scope.regionData = {};
    $scope.selectedRegion = "";

    $scope.findRegionByName = function(params){
      // call api when text length more than 2
      if(params.length < 2){
        if ($scope.selectedRegion != ""){
          $scope.getRegion($scope.selectedRegion);
        }
      }
      else if (params.length > 2) {
        var outletList = HomeService.getOutletByName({
          'name': params
        });
        outletList.then(function (data) {
          $scope.regionData = data.data;
        },
          function () {
            console.log('error');
          }
        );
      }
    }

    $scope.getRegion = function(params){
      $scope.selectedRegion = params;
      var region = HomeService.getOutletByRegion(params);
      region.then(function (data) {
        $scope.regionData = data.data;
      }, 
      function () {
        console.log('error');
      }
      );
    }

    $scope.getCaptcha = function() {
      var captchaPromise = HomeService.getCaptcha();
      captchaPromise.then(function(data) {
        $scope.captchaimage = data.image;
      }, function(){
        });
    };

    $scope.getCaptcha();

    $scope.form_registration = {
      country: 192
    };

    $scope.errors_registration = {};

    $scope.IsDisabled = true;

    $scope.EnableDisableReg = function(){
      $scope.IsDisabled = false;
    }

    $scope.submitFormRegistration = function(){

      document.getElementById('help-block').style.visibility = 'visible';
      document.getElementById('help-block1').style.visibility = 'visible';
      document.getElementById('help-block2').style.visibility = 'visible';
      document.getElementById('help-block3').style.visibility = 'visible';
      document.getElementById('help-block4').style.display = 'block';

      document.getElementById('help-block').style.marginTop = '0px';
      document.getElementById('help-block1').style.marginTop = '0px';
      document.getElementById('help-block2').style.marginTop = '0px';
      document.getElementById('help-block3').style.marginBottom = '7px';
      
      $scope.errors_registration = {};
      $scope.redeem = $scope.form_registration.redemtion_code1+$scope.form_registration.redemtion_code2+$scope.form_registration.redemtion_code3+$scope.form_registration.redemtion_code4+$scope.form_registration.redemtion_code5;
      $scope.dob = $scope.form_registration.dob1+$scope.form_registration.dob2+$scope.form_registration.dob3;
      $scope.dob_view = $scope.form_registration.dob1+'/'+$scope.form_registration.dob2+'/'+$scope.form_registration.dob3;
      $scope.dob_check = $scope.form_registration.dob3+'/'+$scope.form_registration.dob2+'/'+$scope.form_registration.dob1;

      if(!angular.isDefined($scope.form_registration.name) || !$scope.form_registration.name){
        $scope.errors_registration.name = "Name cannot be empty!";
      }
      if(!angular.isDefined($scope.form_registration.mobile) || !$scope.form_registration.mobile){
        $scope.errors_registration.mobile = "Mobile cannot be empty!";
      }
      if(!angular.isDefined($scope.form_registration.email) || !$scope.form_registration.email){
        $scope.errors_registration.email = "Email cannot be empty or not valid!";
      }
      if(!angular.isDefined($scope.form_registration.dob1) || !$scope.form_registration.dob1){
        $scope.errors_registration.dob = "Date cannot be empty!";
      }
      if(!angular.isDefined($scope.form_registration.dob2) || !$scope.form_registration.dob2){
        $scope.errors_registration.dob = "Month cannot be empty!";
      }
      if(!angular.isDefined($scope.form_registration.dob3) || !$scope.form_registration.dob3){
        $scope.errors_registration.dob = "Year cannot be empty!";
      }
      if(!angular.isDefined($scope.dob) || !$scope.dob){
        $scope.errors_registration.dob = "Date of Birth cannot be empty!";
      }
      if(!angular.isDefined($scope.redeem) || !$scope.redeem){
        $scope.errors_registration.redemtion_code = "Redemption cannot be empty!";
      }

      if(angular.equals({}, $scope.errors_registration)){
        if(!$scope.IsDisabled){
          var checkAge = HomeService.checkAge({dob : $scope.dob_check});
          checkAge.then(function(response){
            var checkRedemtionCodePromise = HomeService.checkRedemtionCode({redemtion_code : $scope.redeem});
            checkRedemtionCodePromise.then(function(response){
              for(var i=0; i<$scope.countries.length; i++){
                if($scope.countries[i].country_id == $scope.form_registration.country){
                  var country_name = $scope.countries[i].country_name;
                }
              }
              // var date = new Date($scope.dob_view);
              // var date_format = ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear();
              // var date_format_submit = date.getFullYear() + '-' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate()));
              $uibModal.open({
                template: 
                          '<div class="modal-header" style="background-color: #e62550;"><h4 class="modal-title" style="color: #F9E3BF;">CONFIRMATION</h4></div>' +
                          '<div class="modal-body" ng-cloak style="background-color: #F4F3EF;">' +
                          '<div class="panel panel-default">' +
                            '<div class="panel-body" style="background-color: #fff;color:orange;">' +
                              '<ul style="list-style:none;margin-left: -35px;">' +
                                  '<li>' + $scope.form_registration.name + '</li>' +
                                  '<li>' + country_name + '</li>' +
                                  '<li>' + $scope.form_registration.email + '</li>' +
                                  '<li>' + $('#basic-addon1').text() +' '+ $scope.form_registration.mobile + '</li>' +
                                  '<li>' + $scope.dob_view + '</li>' +
                              '</ul>' +
                            '</div>' +
                          '</div>' +
                          '<h5 style="color:#e62550;font-weight: bold;text-align-last: center;">YOUR E-VOUCHER WILL BE SENT TO THE ABOVE</h5>' +
                          '<h5 style="color:#e62550;font-weight: bold;text-align-last: center;">REGISTERED MOBILE NUMBER.</h5>' +
                          '</div>' +
                          '<div class="modal-footer" style="border-top: 0px solid #e5e5e5;background-color: #f1f1f1;">' +
                          '<center><button class="btn btn-danger" type="button" ng-click="close()" style="background-color: #e62550;outline: none;">CANCEL</button>' +
                          '<button class="btn btn-danger" type="button" ng-click="confirmRegistration('+"'"+$scope.form_registration.name+"'"+','+"'"+$scope.form_registration.country+"'"+','+"'"+$scope.form_registration.email+"'"+','+"'"+$scope.form_registration.mobile+"'"+','+"'"+$scope.dob_check+"'"+','+"'"+$scope.dob_view+"'"+','+"'"+$scope.redeem+"'"+','+"'"+country_name+"'"+','+"'"+$('#basic-addon1').text()+"'"+')" style="background-color: #e62550;outline: none;">CONFIRM</button></center>' +
                          '</div>',
                controller: ['$scope', '$uibModalInstance', 'error_term', function($scope, $uibModalInstance, error_term){
                  $scope.error_term = error_term;
                  $scope.close = function(){
                    $uibModalInstance.dismiss();
                  }
                  $scope.confirmRegistration = function(name,country,email,mobile,dob,date_format,redemtion_code,country_name,code){
                    var Registration = HomeService.sendRegistration({
                      name: name,
                      country: country,
                      email: email,
                      mobile: mobile,
                      dob: dob,
                      date_format: date_format,
                      redemtion_code:redemtion_code,
                      country_name: country_name,
                      code: code
                    });

                    Registration.then(function(response){
                      $window.location.href = response.redirect;
                    })
                  }
                }],
                resolve: {
                  error_term: function(){
                    return $scope.error_term;
                  }
                }
              });
            
            }, function(response){
              $scope.errors_registration.redemtion_code = response.message;
            });
          }, function(response){
            $scope.errors_registration.dob = response.message;
          });
        } else {
          $scope.errors_registration.redemtion_code = "please check the terms and conditions";
        }
      
      }
        
    }

    $scope.form_contact = {
      country: 192
    };

    $scope.errors_contact = {};

    $scope.submitFormContact = function(){

      $scope.errors_contact = {};
      
      if(!angular.isDefined($scope.form_contact.enquiry) || !$scope.form_contact.enquiry){
        $scope.errors_contact.enquiry = "Please select Type of Enquiry!";
      }
      if(!angular.isDefined($scope.form_contact.name) || !$scope.form_contact.name){
        $scope.errors_contact.name = "Name cannot be empty!";
      }
      if(!angular.isDefined($scope.form_contact.mobile) || !$scope.form_contact.mobile){
        $scope.errors_contact.mobile = "Mobile cannot be empty!";
      }
      if(!angular.isDefined($scope.form_contact.email) || !$scope.form_contact.email){
        $scope.errors_contact.email = "Email cannot be empty or not valid!";
      }
      if(!angular.isDefined($scope.form_contact.message) || !$scope.form_contact.message){
        $scope.errors_contact.message = "Message cannot be empty!";
      }
      if(!angular.isDefined($scope.form_contact.captcha) || !$scope.form_contact.captcha){
        $scope.errors_contact.captcha = "Captcha cannot be empty!";
      }

      if(angular.equals({}, $scope.errors_contact)){

        var checkCaptchaPromise = HomeService.checkCaptcha({captcha : $scope.form_contact.captcha});
        checkCaptchaPromise.then(function(response){
            var enquirySendPromise = HomeService.sendEnquiry({
              enquiry: $scope.form_contact.enquiry,
              name: $scope.form_contact.name,
              mobile: $scope.form_contact.mobile,
              email: $scope.form_contact.email,
              message: $scope.form_contact.message,
              country: $scope.form_contact.country
            });

            enquirySendPromise.then(function(response){
                $uibModal.open({
                  template: 
                            '<div class="modal-body" ng-cloak>' +
                            '   Message has been sent!' +
                            '</div>' +
                            '<div class="modal-footer">' +
                            '   <button class="btn btn-primary" type="button" ng-click="close()">Close</button>' +
                            '</div>',
                  controller: ['$scope', '$uibModalInstance', 'error_term', function($scope, $uibModalInstance, error_term){
                    $scope.error_term = error_term;
                    $scope.close = function(){
                      $uibModalInstance.dismiss();
                    }
                  }],
                  resolve: {
                    error_term: function(){
                      return $scope.error_term;
                    }
                  }
                });

                $scope.getCaptcha();

                $scope.form_contact.enquiry = "";
                $scope.form_contact.name = "";
                $scope.form_contact.mobile = "";
                $scope.form_contact.email = "";
                $scope.form_contact.message = "";
                $scope.form_contact.captcha = "";
                $scope.form_contact.country = 192;

            }, function(){
                $scope.getCaptcha();
            });
          }, function(response){
            $scope.errors_contact.captcha = response.message;
            $scope.getCaptcha();
          });
      
      }
        
    }

    $scope.form_contact_calling_code = "-";

    $scope.$watch('form_contact.country', function(){
      
      if(!$scope.form_contact.country){
        $scope.form_contact_calling_code = "-";
      }else{
        for(var i=0; i<$scope.countries.length; i++){
          if($scope.countries[i].country_id == $scope.form_contact.country){
            $scope.form_contact_calling_code = $scope.countries[i].calling_code;
            break;
          }
        }
      }
    });

    $scope.form_registration_calling_code = "-";

    $scope.$watch('form_registration.country', function(){
      
      if(!$scope.form_registration.country){
        $scope.form_registration_calling_code = "-";
      }else{
        for(var i=0; i<$scope.countries.length; i++){
          if($scope.countries[i].country_id == $scope.form_registration.country){
            $scope.form_registration_calling_code = $scope.countries[i].calling_code;
            break;
          }
        }
      }
    });

  }