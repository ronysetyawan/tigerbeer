(function() {
  'use strict';

  angular
    .module('selfServePurchaseWidget')
    .controller('HeaderController', HeaderController);

  function HeaderController($scope, $cookies, $state, $localStorage){
	
	
	
    $scope.logout = function(){
      $cookies.remove("token");

      $localStorage.$reset();

      $state.go('login');
    };
  }
})();
