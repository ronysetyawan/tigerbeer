angular.module('CPRV-OctFestApp')
	.controller("MainCtrl",['$rootScope', '$scope', '$uibModal', MainCtrl]);

function MainCtrl($rootScope, $scope, $uibModal){
	'use strict';

	$scope.is_logged_in = false;

	$scope.$on('setLoggedIn', function(event, data){
		$scope.is_logged_in = true;
	});

	$scope.login = function(){

		$uibModal.open({
			templateUrl: 'loginModal.html',
			size: 'lg',
			controller: ['$scope', '$rootScope', '$uibModalInstance', '$log', 'AuthService', function($scope, $rootScope, $uibModalInstance, $log, AuthService){
				
				$scope.form_login = {
					country: 192,
					mobile: "",
					pin: ""
				};

				$scope.calling_code = "";

				$scope.$watch('form_login.country', function(){
					if(!$scope.form_login.country){
						$scope.calling_code = "-";
					}else{
						for(var i=0; i<$scope.countries.length; i++){
							if($scope.countries[i].country_id == $scope.form_login.country){
								$scope.calling_code = $scope.countries[i].calling_code;
								break;
							}
						}
					}
				});

				$scope.login = function(){
					
					var loginPromise = AuthService.login({
						country: $scope.form_login.country,
						mobile: $scope.form_login.mobile,
						pin: $scope.form_login.pin,
						return_data: true
					});
				
					loginPromise.then(function(data){
						$rootScope.$broadcast('setConfirmationLoggedIn', {
							name: data.name,
							gender: data.gender,
							country: data.country,
							mobile: data.gender,
							email: data.email,
							dob: data.dob,
							postal_code: data.postal_code
						});

						$rootScope.$broadcast('setLoggedIn');
						
						$uibModalInstance.close();

					}, function(data){
						$uibModal.open({
							template: '<div class="modal-body">' +
									  '   <p>{{ message }}</p>' +
							          '</div> ',
							controller: ['$scope', '$rootScope', '$uibModalInstance', '$log', 'message', function($scope, $rootScope, $uibModalInstance, $log, message){
								$scope.message = message;
							}],
							resolve: {
								message: function(){
									return data.message
								}
							}
						});

					});

				};
			}]
		});

	}

	$scope.showOutlets = function(){
		$uibModal.open({
			templateUrl: 'outletsModal.html',
			size: 'lg',
			controller: ['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
				$scope.close = function(){
					$uibModalInstance.dismiss();
				}
			}]
		});
	}

}
