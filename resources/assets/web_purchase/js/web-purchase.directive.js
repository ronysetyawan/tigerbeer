angular.module('CPRV-OctFestApp')
.directive('buttonConfirmation', ['$parse', '$uibModal', function ($parse, $uibModal) {
    return {
        restrict: 'A',
        scope: {
            "confirmationTitle": "@",
            "confirmationDescription": "@",
            "confirmationFunction": "&",
            "textYes": "@",
            "textNo": "@",
            "btnYesClass": "@",
            "btnNoClass": "@",
            "size": "@"
        },
        link: function(scope, element, attrs){

            if(!scope.textYes){
                scope.textYes = "Yes"
            }
            if(!scope.textNo){
                scope.textNo = "No"
            }
            if(!scope.btnYesClass){
                scope.btnYesClass = "btn btn-primary"
            }
            if(!scope.btnNoClass){
                scope.btnNoClass = "btn btn-warning"
            }

            $(element).on('click', function(e) {
                var modalInstance = $uibModal.open({
                    size: scope.size,
                    template: '<div class="modal-header">' +
                                '   <h3 class="modal-title" id="modal-title">{{title}}</h3>' +
                                '</div>' +
                                '<div class="modal-body" id="modal-body" ng-bind-html="description"></div>' +
                                '<div class="modal-footer">' +
                                '   <button class="{{btnYesClass}}" type="button" ng-click="yes()">{{textYes}}</button>' +
                                '   <button class="{{btnNoClass}}" type="button" ng-click="no()">{{textNo}}</button>' +
                                '</div>',
                    controller: ['$scope', 'title', 'description', 'textYes', 'textNo', 'btnYesClass', 'btnNoClass', '$uibModalInstance', 
                        function($scope, title, description, textYes, textNo, btnYesClass, btnNoClass, $uibModalInstance){
                        $scope.title = title;
                        $scope.description = description;
                        $scope.textYes = textYes;
                        $scope.textNo = textNo;
                        $scope.btnYesClass = btnYesClass;
                        $scope.btnNoClass = btnNoClass;

                        $scope.yes = function(){
                            $uibModalInstance.close();
                        };
                        $scope.no = function(){
                            $uibModalInstance.dismiss();
                        };
                    }],
                    resolve: {
                        title: function(){
                            return scope.confirmationTitle;
                        },
                        description: function(){
                            return scope.confirmationDescription;
                        },
                        textYes: function(){
                            return scope.textYes;
                        },
                        textNo: function(){
                            return scope.textNo;
                        },
                        btnYesClass: function(){
                            return scope.btnYesClass;
                        },
                        btnNoClass: function(){
                            return scope.btnNoClass;
                        }
                    }
                });

                modalInstance.result.then(function() {
                    scope.confirmationFunction();
                }, function () {
                    
                });
            });
            }
    };
}])

angular.module('CPRV-OctFestApp')
.directive('buttonEmailPreview', ['$parse', '$uibModal', function ($parse, $uibModal) {
    return {
        restrict: 'A',
        scope: {
            "key": "@"
        },
        link: function(scope, element, attrs){
            $(element).on('click', function(e) {
                
                $uibModal.open({
                    template: '<div class="modal-body">' +
                              '   <div ng-bind-html="template"></div>'+
                              '</div>',
                    controller: ['$scope', '$uibModalInstance', '$log', 'cfg', '$sce', '$http', function($scope, $uibModalInstance, $log, cfg, $sce, $http){
              
                      $http({
                        method : 'GET',
                        url : cfg.api + '/voucher-email-preview/' + scope.key
                      }).then(function(response) {
                          $scope.template = $sce.trustAsHtml(response.data);
                      },function() {
                          $scope.template = 'not found';
                      });
              
                      $scope.close = function(){
                        $uibModalInstance.dismiss();
                      };
                    }],
                    windowClass: 'app-modal-window',
                    resolve: {
                      
                    }
                  });
              

            });
            }
    };
}])

.directive('fixedMenu', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            var elOffset = element.offset();
            var elTop = elOffset.top;

            var parent_height = element.parent().outerHeight();

            element.parent().css("height", parent_height);

            angular.element($window).bind("scroll", function(event) {
                if($window.scrollY>=elTop){
                    element.addClass("fixed");
                }else{
                    element.removeClass("fixed");
                }
            });

            element.find('.navbar-nav li a').bind("click", function(event) {
                angular.element('#navbar-main-menu').collapse('hide');
            });
        }
    };
}])

.directive('homeScroll', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            
            angular.element($window).bind("scroll", function(event) {

                var scrollTop = $window.scrollY;
                var anchors = angular.element('body').find('.home-anchor');

                var heightOffset = 1;

                if(angular.element($window).width()<=767){
                    var heightBtnStartHere = angular.element('.btn-start-here-bottom').outerHeight();
                    heightOffset = heightBtnStartHere;
                }

                for (var i = 0; i < anchors.length; i++){
                    if (scrollTop > angular.element(anchors[i]).offset().top - heightOffset && scrollTop < angular.element(anchors[i]).offset().top + angular.element(anchors[i]).next().outerHeight() - heightOffset ) {
                    //if (scrollTop > angular.element(anchors[i]).offset().top - 50 && scrollTop < angular.element(anchors[i]).offset().top + angular.element(anchors[i]).height() - 50) {
                        angular.element('ul.navbar-nav li a[href="#' + angular.element(anchors[i]).attr('id') + '"]').parent().addClass('active');
                    } else {
                        angular.element('ul.navbar-nav li a[href="#' + angular.element(anchors[i]).attr('id') + '"]').parent().removeClass('active');
                    }
                }
            });
            
        }
    };
}])

.directive('localScroll', [function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            element.localScroll({
                duration: 500
            });
        }
    };
}])

.directive('stringToNumber', [function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(value) {
          return '' + value;
        });
        ngModel.$formatters.push(function(value) {
          return parseFloat(value);
        });
      }
    };
  }])

.directive('termAndCondition', ['$uibModal', function($uibModal) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs, ngModel) {
            element.bind('click', function() {
                $uibModal.open({
                    templateUrl: 'termAndConditionModal.html',
                    size: 'lg',
                    controller: ['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
                        $scope.close = function(){
                            $uibModalInstance.dismiss();
                        }
                    }]     
                });
            });
        }
    };
}])

.directive('dobPicker', ['$window', '$log', function ($window, $log) {
    return {
        restrict: 'E',
        scope:{
            totalYear: "@",
            maxYear: "@",
            model: "=",
            error: "=",
            minAge: "@"
        },
        link: function(scope, element, attrs){
            scope.days = [{value: "", label: "Day"}];
            for(var i=1; i<=31; i++){
                scope.days.push({value: i, label: i});
            }

            scope.months = [{value: "", label: "Month"}];
            scope.months.push({value: 1, label: "Jan"});
            scope.months.push({value: 2, label: "Feb"});
            scope.months.push({value: 3, label: "Mar"});
            scope.months.push({value: 4, label: "Apr"});
            scope.months.push({value: 5, label: "May"});
            scope.months.push({value: 6, label: "Jun"});
            scope.months.push({value: 7, label: "Jul"});
            scope.months.push({value: 8, label: "Aug"});
            scope.months.push({value: 9, label: "Sep"});
            scope.months.push({value: 10, label: "Oct"});
            scope.months.push({value: 11, label: "Nov"});
            scope.months.push({value: 12, label: "Dec"});

            scope.years = [{value: "", label: "Year"}];
            if(!scope.totalYear){
                scope.totalYear = 120;
            }
            if(!scope.maxYear){
                scope.maxYear = (new Date()).getFullYear();
            }
            for(var i=scope.maxYear; i>=(scope.maxYear-120); i--){
                scope.years.push({
                    value: i,
                    label: i
                });
            }

            function isValidDate(year, month, day) {
                var d = new Date(year, month, day);
                if (d.getFullYear() == year && d.getMonth() == month && d.getDate() == day) {
                    return true;
                }
                return false;
            }

            scope.form={
                day: '',
                month: '',
                year: ''
            };

            scope.$watch('model', function(){
                if(scope.model){
                    scope.form.day = scope.model.getDate();
                    scope.form.month = (scope.model.getMonth()+1);
                    scope.form.year = scope.model.getFullYear();

                    scope._error = '';
                    
                    if(scope.form.year=='' || scope.form.month=='' || scope.form.day==''){
                        scope.model = false;
                        scope._error = 'Please complete all the fields!';
                    }else if(!isValidDate(scope.form.year, (scope.form.month-1), scope.form.day)){
                        scope.model = false;
                        scope._error = 'The date is not valid!';
                    }

                    scope.error = scope._error;

                }
            });

            scope.changeDate = function(){

                scope._error = '';

                if(scope.form.year=='' || scope.form.month=='' || scope.form.day==''){
                    scope.model = false;
                    scope._error = 'Please complete all the fields!';
                }else if(!isValidDate(scope.form.year, (scope.form.month-1), scope.form.day)){
                    scope.model = false;
                    scope._error = 'The date is not valid!';
                }else{
                    var date = new Date(scope.form.year, (scope.form.month-1), scope.form.day);
                    date.setMinutes(date.getMinutes() - date.getTimezoneOffset());

                    var curr_date = new Date();
                    if(scope.minAge && (curr_date.getFullYear() - date.getFullYear()) < parseInt(scope.minAge) ){
                        scope.model = false;
                        scope._error = 'The birth of date is not allowed!';
                    }else{
                        scope.model = date;
                    }
                }

                scope.error = scope._error;
            };

            scope.changeDate();

        },
        template: "<ul class='list-inline dob-picker'>"+
                  "   <li><select class='form-control' ng-options='day.value as day.label for day in days' ng-model='form.day' ng-change='changeDate()'></select></li>" +
                  "   <li><select class='form-control' ng-options='month.value as month.label for month in months' ng-model='form.month' ng-change='changeDate()'></select></li>" +
                  "   <li><select class='form-control' ng-options='year.value as year.label for year in years' ng-model='form.year' ng-change='changeDate()'></select></li>" +
                  "</ul>"
    };
}])

.directive('dobPickerUi', function () {
    return {
        restrict: 'A',
        scope:{
            date:'=',
            totalYear: "@",
            maxYear: "@",
            error: "=",
            minAge: "@"
        },
        link: function (scope, element) {
            element.datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'dd/mm/yy',
                yearRange: (scope.maxYear - scope.totalYear) + ":" + scope.maxYear,
                onSelect: function (date) {

                    scope.error = '';

                    scope.date = element.datepicker( 'getDate' );
                   
                    var curr_date = new Date();

                    age = curr_date.getFullYear() - scope.date.getFullYear();
                    if ( curr_date.getMonth() < scope.date.getMonth()){
                        age--;
                    }
                    if ((scope.date.getMonth() == curr_date.getMonth()) && (curr_date.getDate() < scope.date.getDate())){
                        age--;
                    }

                    if(scope.minAge && ( age < parseInt(scope.minAge))){
                        scope.error = 'The birth of date is not allowed!';
                    }

                    scope.$apply();
                }
            });
            
            scope.$watch('date', function(){
                if(scope.date){
                    element.datepicker("setDate", scope.date );
                }
            })
           
            
            
        }
    };
})

.directive('checkcaptcha', ['$q', '$http', 'cfg', '$log', function($q, $http, cfg, $log) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$asyncValidators.checkcaptcha = function(modelValue) {
                return $http({
                        method  : 'POST',
                        url     : cfg.api + '/captcha/check/' + modelValue + '/' + scope.csrftoken,
			            data    : {captcha:modelValue}
                        }).then(function resolved(res) {
                         //validation
                         scope.captchacompare = res.data.compare;
                         //$log.info(scope.captchacompare);
                         if(scope.captchacompare === true){
                            return $q.resolve('pass');
                         }else{
                            return $q.reject('failed');
                         }

                       }, function rejected() {
                         //validation failed
                         return $q.reject('failed');
                       });


            };
        }
    };
}])

.directive('qtyPicker', ['$window', '$log', function ($window, $log) {
    return {
        restrict: 'E',
        scope: {
            ngModel: '='
        },
        replace: true,
        require: 'ngModel',
        link: function(scope, element, attrs){
            if(!scope.ngModel){
                scope.ngModel = 1;
            }

            scope.increase = function(){
                scope.ngModel++;
            }

            scope.decrease = function(){
                if(scope.ngModel>1){
                    scope.ngModel--;
                }
            }
        },
        template: '<div class="input-group">' +
                  '   <span class="input-group-btn">' +
                  '      <button class="btn btn-default" type="button" ng-click="decrease()">-</button>' +
                  '   </span>' +
                  '   <input type="number" class="form-control" ng-model="ngModel">' +
                  '   <span class="input-group-btn">' +
                  '      <button class="btn btn-default" type="button" ng-click="increase()">+</button>' +
                  '   </span>' +
                  '</div>'
    };
}])

;
