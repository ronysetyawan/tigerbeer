angular.module('CPRV-OctFestApp')
	.service("ResendVoucherService",['cfg', '$q', '$http', '$log', ResendVoucherService]);

function ResendVoucherService(cfg, $q, $http, $log){
	
	this.submit = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
      		url     : cfg.api + '/resend-voucher',
      		data    : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});

		return deferred.promise;
	};
	 
}
