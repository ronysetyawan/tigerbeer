angular.module('CPRV-OctFestApp')
	.controller("ResendVoucherCtrl",['$scope', 'AuthService', '$log', '$uibModal', 'ResendVoucherService', ResendVoucherCtrl]);

function ResendVoucherCtrl($scope, AuthService, $log, $uibModal, ResendVoucherService){

	$scope.form = {
    country: 192
  };

	$scope.calling_code = "-";

    $scope.$watch('form.country', function(){
      
      if(!$scope.form.country){
        $scope.calling_code = "-";
      }else{
        for(var i=0; i<$scope.countries.length; i++){
          if($scope.countries[i].country_id == $scope.form.country){
            $scope.calling_code = $scope.countries[i].calling_code;
            break;
          }
        }
      }
    });

  $scope.alert = {};

  $scope.closeAlert = function(){
    $scope.alert = {};
  }
  
	$scope.submit = function(){
    $scope.alert = {};

    if(!$scope.form.country || !$scope.form.mobile){
      $scope.alert.type = "error";
      $scope.alert.message = "Mobile cannot be empty";
    }else{
      var submitPromise = ResendVoucherService.submit({
        country: $scope.form.country,
        mobile: $scope.form.mobile
      });
      submitPromise.then(function(response){
        $scope.alert.type = "success";
        $scope.alert.message = "Voucher has been sent!";
      }, function(response){
        $scope.alert.type = "error";
        $scope.alert.message = response.message;
      });

      $scope.form.country = 192;
      $scope.form.mobile = "";
    }
	};
}


