angular
    .module('CPRV-OctFestApp')
    .controller('CheckoutCtrl', ['$scope', '$http', 'cfg', '$log', 'CheckoutService', '$uibModal', '$timeout', '$sce', '$filter', CheckoutCtrl]);
function CheckoutCtrl($scope, $http, cfg, $log, CheckoutService, $uibModal, $timeout, $sce, $filter){

  $scope.addRecipientModal = function(index){
    $uibModal.open({
      size: 'lg',
      template: '<div class="modal-body">' +
                '   <div ng-bind-html=""></div>'+
                '   <div class="panel panel-default">'+
                '     <div class="panel-body">{{ errors }}'+
                '         <div class="row">'+
                '             <div class="col-md-4">'+
                '                 <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'first_name\')}">'+
                '                     <label>Title</label>'+
                '                     <select class="form-control" ng-model="form_recipient.title" '+
                '                       ng-options="title.label for title in titles" '+
                '                       ng-required="true">'+
                '                         <option value="">--- Select Title ---</option>'+
                '                         @endforeach'+
                '                     </select>'+
                '                 </div>'+
                '             </div>'+
                '             <div class="col-md-4">'+
                '                 <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'first_name\')}">'+
                '                     <label>First Name<span class="required">*</span></label>'+
                '                     <input class="form-control" ng-model="form_recipient.first_name" ng-required="true">'+
                '                     <div class="help-block" ng-if="errors.hasOwnProperty(\'first_name\')">{{ errors.first_name }}</div>'+
                '                 </div>'+
                '             </div>'+
                '             <div class="col-md-4">'+
                '                 <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'last_name\')}">'+
                '                     <label>Last Name<span class="required">*</span></label>'+
                '                     <input class="form-control" ng-model="form_recipient.last_name" ng-required="true">'+
                '                     <div class="help-block" ng-if="errors.hasOwnProperty(\'first_name\')">{{ errors.last_name }}</div>'+
                '                 </div>'+
                '             </div>'+
                '         </div>'+
                '         <div class="row">'+
                '             <div class="col-md-4">'+
                '                 <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'email\')}">'+
                '                     <label>Email<span class="required">*</span></label>'+
                '                     <input class="form-control" ng-model="form_recipient.email" ng-required="true">'+
                '                     <div class="help-block" ng-if="errors.hasOwnProperty(\'email\')">{{ errors.email }}</div>'+
                '                 </div>'+
                '             </div>'+
                '             <div class="col-md-4">'+
                '                 <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'email_confirm\')}">'+
                '                     <label>Confirm Email<span class="required">*</span></label>'+
                '                     <input class="form-control" ng-model="form_recipient.email_confirm" ng-required="true">'+
                '                     <div class="help-block" ng-if="errors.hasOwnProperty(\'email_confirm\')">{{ errors.email_confirm }}</div>'+
                '                 </div>'+
                '             </div>'+
                '             <div class="col-md-4">'+
                '                 <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'country\')}">'+
                '                     <label>Country<span class="required">*</span></label>'+
                '                     <select class="form-control" '+
                '                           ng-options="country.label for country in countries" ng-model="form_recipient.country" '+
                '                           ng-required="true">'+
                '                         '+
                '                     </select>'+
                '                     <div class="help-block" ng-if="errors.hasOwnProperty(\'country\')">{{ errors.country }}</div>'+
                '                 </div>'+
                '             </div>'+
                '             <div class="col-md-4">'+
                '                 <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'mobile\')}">'+
                '                     <label>Mobile<span class="required">*</span></label>'+
                '                     <input class="form-control" ng-model="form_recipient.mobile" ng-required="true">'+
                '                     <div class="help-block" ng-if="errors.hasOwnProperty(\'first_name\')">{{ errors.mobile }}</div>'+
                '                 </div>'+
                '             </div>'+
                '         </div>'+
                '         <div class="row">'+
                '             <div class="col-md-12">'+
                '                 <div class="form-group">'+
                '                     <button class="btn btn-primary" ng-click="addRecipientToList()">Add Recipient</button>'+
                '                     <button class="btn btn-primary" ng-click="resetRecipient()">Cancel</button>'+
                '                 </div>'+
                '             </div>'+
                '         </div> '+
                '     </div>'+
                '  </div>'+
                '</div>',
      controller: ['$rootScope', '$scope', '$uibModalInstance', '$log', 'cfg', 'titles', 'countries', function($rootScope, $scope, $uibModalInstance, $log, cfg, titles, countries){
        $scope.titles = titles;

        $scope.countries = countries;
        
        $scope.form_recipient = {
          title: '',
          first_name: '',
          last_name: '',
          email: '',
          confirm_email: '',
          country: '',
          mobile: ''
        };

        $scope.resetRecipient = function(){
          $scope.form_recipient = {};
          $scope.close();
        };

        $scope.errors = {};
        $scope.addRecipientToList = function(){
          $scope.errors = {};
          
          if($scope.form_recipient.first_name){
            $scope.errors.first_name = "";
          }
          if($scope.form_recipient.last_name){
            $scope.errors.last_name = "";
          }
          if($scope.form_recipient.email){
            $scope.errors.email = "";
          }
          if($scope.form_recipient.email_confirm){
            $scope.errors.email_confirm = "";
          }
          if($scope.form_recipient.first_name){
            $scope.errors.first_name = "";
          }

          if(!angular.equals($scope.errors, {})){
            $scope.form_recipient.title = $scope.form_recipient.title.value;
            $scope.form_recipient.country = $scope.form_recipient.country.value;
            $rootScope.$broadcast('addRecipientToList', 
                {form_recipient: $scope.form_recipient, key: index}
            );
            $scope.close();
          }
        };

        $scope.close = function(){
          $scope.form_recipient = {};
          $uibModalInstance.dismiss();
        };
      }],
      resolve: {
        titles : function(){
          return $scope.titles;
        },
        countries : function(){
          return $scope.countries;
        }
      }
    });
  };

  $scope.$watch('recipient_details', function(){
    angular.forEach($scope.recipient_details, function(value, key){

      if(!angular.isDefined($scope.form_recipient_detail[key])){
        $scope.form_recipient_detail[key] = {};
      }

      var value_recipient = null;
      if(value.recipient){
        value_recipient = angular.fromJson(value.recipient);
      }
      $scope.form_recipient_detail[key].recipient = value_recipient;
      $scope.form_recipient_detail[key].message = value.message;
      $scope.form_recipient_detail[key].send_type = value.send_type;
      $scope.form_recipient_detail[key].date_send = new Date(value.date_send);

      $scope.selectRecipient(key);
    });
  });
  
  $scope.selected_recipient = [];

  $scope.selectRecipient = function(key){
    if($scope.form_recipient_detail[key].recipient){
      if(!angular.isDefined($scope.selected_recipient[key])){
        $scope.selected_recipient[key] = {};
      }

      var names = [];
      if($scope.form_recipient_detail[key].recipient.title){
        names.push($scope.form_recipient_detail[key].recipient.title);
      }
      if($scope.form_recipient_detail[key].recipient.first_name){
        names.push($scope.form_recipient_detail[key].recipient.first_name);
      }
      if($scope.form_recipient_detail[key].recipient.last_name){
        names.push($scope.form_recipient_detail[key].recipient.last_name);
      }

      var mobiles = [];
      if($scope.form_recipient_detail[key].recipient.calling_code){
        mobiles.push($scope.form_recipient_detail[key].recipient.calling_code);
      }
      if($scope.form_recipient_detail[key].recipient.mobile){
        mobiles.push($scope.form_recipient_detail[key].recipient.mobile);
      }

      $scope.selected_recipient[key].name = names.join(" ");
      $scope.selected_recipient[key].email = $scope.form_recipient_detail[key].recipient.email;
      $scope.selected_recipient[key].mobile = mobiles.join("") + " (" +  $scope.form_recipient_detail[key].recipient.country_name + ")";
      //remove error recipient if any
      if(angular.isDefined($scope.errorPrompt)){
          var indexError = $scope.errorPrompt[key].indexOf('recipient');
          if (indexError > -1) {
              $scope.errorPrompt[key].splice(indexError, 1);
          }
      }

    }else{
      $scope.selected_recipient = {};
    }
  };

  $scope.selectSendType = function(key){
    if(angular.isDefined($scope.form_recipient_detail[key].send_type) 
        && angular.isDefined($scope.errorPrompt)){
        var indexError = $scope.errorPrompt[key].indexOf('send_type');
        if (indexError > -1) {
            $scope.errorPrompt[key].splice(indexError, 1);
        }
    }
  };
}
