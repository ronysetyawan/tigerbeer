angular.module('CPRV-OctFestApp')
	.service("CheckoutService",['cfg', '$q', '$http', '$log', CheckoutService]);

function CheckoutService(cfg, $q, $http, $log){

	this.confirmation = function(data) {
		var deferred = $q.defer();
		$http({
			method : 'POST',
			url    : cfg.api + '/checkout',
			data   : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};
}
