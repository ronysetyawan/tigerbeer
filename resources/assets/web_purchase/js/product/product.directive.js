angular.module('selfServeWebPurchase')
    
    .directive('owlCarousel', function() {
	    return {
    	    restrict: 'C',
		    link: function(scope, element, attrs){
                scope.initCarousel = function(element) {
                    // provide any default options you want
                    var defaultOptions = {
						margin: 8,
                        dots: false,
                        responsive : {
                            // breakpoint from 0 up
                            0 : {
                                items : 1,
                            },
                            // breakpoint from 768 up
                            768 : {
                                items: 2
                            },
                            // breakpoint from 768 up
                            992 : {
                                items: 2
                            }
                        }
                    };
                    var customOptions = scope.$eval($(element).attr('data-options'));
                    // combine the two options objects
                    for(var key in customOptions) {
                        defaultOptions[key] = customOptions[key];
                    }
                    // init carousel
                    element.owlCarousel(defaultOptions);
                };

		    },
  	    };
    })
    .directive('owlCarouselItem', function() {
    	return {
		    restrict: 'A',
		    transclude: false,
		    link: function(scope, element) {
			    if(scope.$last) {
				    scope.initCarousel(element.parent());
		    	}
		    }
	    };
    });
