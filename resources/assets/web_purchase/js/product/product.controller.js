angular
    .module('selfServeWebPurchase')
    .controller('ProductController', ['$scope', '$rootScope','$http', 'cfg', '$log', '$localStorage', '$timeout', ProductController]);

  function ProductController($scope, $rootScope, $http, cfg, $log, $localStorage, $timeout){

    $scope.hideCounter = false;

    $scope.slickConfig = {
      enabled: true,
      autoplay: true,
      draggable: true,
      autoplaySpeed: 3000,
      method: {},
      event: {
          beforeChange: function (event, slick, currentSlide, nextSlide) {
          },
          afterChange: function (event, slick, currentSlide, nextSlide) {
          }
      },
      responsive: [
        {
            breakpoint: 767,
            settings: "unslick"
        }
      ]
  };

    $scope.selectedProductIndex = -1;

    $scope.form_cart = {
      qty: 1
    };

    $scope.selectProduct = function(index){

      $scope.selectedProductIndex = index;

      var brand_id = $scope.products[index].brand_id;
      var product_id = $scope.products[index].product_id;

      $scope.form_cart.product_id = product_id;
      $scope.form_cart.brand_id = brand_id;

      if($scope.products[index].fix_amount>1){
        $scope.hideCounter = true;

        $scope.form_cart.qty = $scope.products[index].fix_amount;
      }else{
        $scope.hideCounter = false;

        $scope.form_cart.qty = 1;
      }

      $scope.addToCart($scope.cart_url);

    }

    $scope.increaseQty = function(){
      $scope.form_cart.qty++;
    }

    $scope.decreaseQty = function(){
      if($scope.form_cart.qty>1){
        $scope.form_cart.qty--;
      }
    }

    $scope.errors = {};
    $scope.addToCart = function(cart_url){

      $scope.errors = {};

      if($scope.selectedProductIndex<0){
        $scope.errors.product = "Please select a product!";
      }else if(!angular.isDefined($scope.form_cart.qty) || $scope.form_cart.qty == null){
        $scope.errors.product = "Please add a quantity!";
      }

      if(angular.equals({}, $scope.errors)){

        if(!angular.isDefined($localStorage.carts)){
          $localStorage.carts = {};
        }
		  
        var product = $scope.products[$scope.selectedProductIndex];
        var cart_id = product.brand_id;
		  
        if(angular.isDefined($localStorage.carts[cart_id])){
          var old_qty = $localStorage.carts[cart_id].qty;
          $localStorage.carts[cart_id].qty = parseInt(old_qty) + parseInt($scope.form_cart.qty);
          $localStorage.carts[cart_id].total = ( $localStorage.carts[cart_id].qty *  parseFloat($localStorage.carts[cart_id].price));
        }else{
          $localStorage.carts[cart_id] = {
            image: product.image,
            product_id: product.product_id,
            brand_id: product.brand_id,
            brand_name: product.brand_name,
            qty: $scope.form_cart.qty,
            price: product.price,
            total: ( parseInt($scope.form_cart.qty) * parseFloat(product.price))
          };
        }

        //clear the data
        //$scope.selectedProductIndex = -1;
        $scope.form_cart.qty = 0;

        window.location.href = cart_url;
      }

    }
  }
