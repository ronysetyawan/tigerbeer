angular.module('CPRV-OctFestVoucherApp')
	.service("AuthService",['$q', '$http', AuthService]);

function AuthService($q, $http){
	
	this.setPin = function(url, pin) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/set-pin',
			data    : { pin: pin }
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.login = function(url, pin) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/login',
			data    : { pin: pin}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

}