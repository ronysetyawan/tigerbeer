angular.module('CPRV-OctFestVoucherApp')
	.controller("AuthCtrl",['$rootScope', '$scope', 'AuthService', '$uibModal', AuthCtrl]);
function AuthCtrl($rootScope, $scope, AuthService, $uibModal){
	'use strict';

	$scope.form_pin = {};

	angular.element('.list-input-pin li input').keyup(function(e){
		if(this.value){
			angular.element(this).parent().next().find('input').focus();
		}
	});

	$scope.error = "";
	$scope.setPin = function(){
		$scope.error= "";
		if(
			!$scope.form_pin.input_1 || 
			!$scope.form_pin.input_2 ||
			!$scope.form_pin.input_3 ||
			!$scope.form_pin.input_4 ||
			!$scope.form_pin.input_5){
			
			$scope.error = "Please complete the pin";
		}else{

			var pin =  $scope.form_pin.input_1.toString() + 
			$scope.form_pin.input_2.toString() + $scope.form_pin.input_3.toString() +
			$scope.form_pin.input_4.toString() + $scope.form_pin.input_5.toString();

			var setPinPromise = AuthService.setPin($scope.base_url, pin);
			setPinPromise.then(function(data){
				window.location.href = data.redirect;
			}, function(response){

			});
		}
	};

	$scope.login = function(){

		$scope.error= "";
		if(
			!$scope.form_pin.input_1 || 
			!$scope.form_pin.input_2 ||
			!$scope.form_pin.input_3 ||
			!$scope.form_pin.input_4 ||
			!$scope.form_pin.input_5){
			
			$scope.error = "Please complete the pin";
		}else{

			var pin =  $scope.form_pin.input_1.toString() + 
			$scope.form_pin.input_2.toString() + $scope.form_pin.input_3.toString() +
			$scope.form_pin.input_4.toString() + $scope.form_pin.input_5.toString();

			var loginPromise = AuthService.login($scope.base_url, pin);
			loginPromise.then(function(data){
				window.location.href = data.redirect;
			}, function(response){
				$scope.error = response.message;
			});
		}
	};

}
