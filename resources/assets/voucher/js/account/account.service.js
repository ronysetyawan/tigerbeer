angular.module('CPRV-OctFestVoucherApp')
	.service("AccountService",['$q', '$http', AccountService]);

function AccountService($q, $http){
	
	this.subscribeUpdate = function(url, status) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/set-pin',
			data    : { pin: pin }
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.update = function(url, data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/account',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.deleteAccount = function(url, pin) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/delete-account',
			data    : {
				pin: pin
			}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
    };

}