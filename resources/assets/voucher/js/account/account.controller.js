angular.module('CPRV-OctFestVoucherApp')
	.controller("AccountCtrl",['$rootScope', '$scope', 'AccountService', '$uibModal', AccountCtrl]);
function AccountCtrl($rootScope, $scope, AccountService, $uibModal){
	'use strict';

	$scope.deleted = false;

	$scope.dobs = {};

	$scope.isCollapsedDeleteAccount = true;

	$scope.subscribeUpdate = function(){

		var setPinPromise = HomeService.subscribeUpdate($scope.base_url, 12345);
		setPinPromise.then(function(data){
			window.location.href = data.redirect;
		}, function(response){

		});
	};

	$scope.form = {};

	$scope.$watch('dobs', function(){
		if($scope.dobs.year && $scope.dobs.month && $scope.dobs.date){
			$scope.form.dob = new Date($scope.dobs.year, ($scope.dobs.month-1), $scope.dobs.date)
		}
	}, true);

	$scope.errors = {};
	$scope.update = function(){

		$scope.errors = {};

		if(!angular.isDefined($scope.form.name) || !$scope.form.name){
			$scope.errors.name = "Name cannot be empty!";
		}
		if(!angular.isDefined($scope.form.gender) || !$scope.form.gender){
			$scope.errors.gender = "Gender cannot be empty!";
		}
		if(!angular.isDefined($scope.form.email) || !$scope.form.email){
			$scope.errors.email = "Email cannot be empty or not valid!";
		}
		if(!angular.isDefined($scope.form.dob) || !$scope.form.dob){
			$scope.errors.email = "Date of birth cannot be empty or not valid!";
		}

		if(angular.equals({}, $scope.errors)){
			var dob = $scope.form.dob.getFullYear() + "-" + ($scope.form.dob.getMonth()+1) + "-" + $scope.form.dob.getDate();

			var subscribe = 0;
			if($scope.form.subscribe){
				subscribe = 1;
			}
			var updatePromise = AccountService.update($scope.base_url, {
				name: $scope.form.name,
				email: $scope.form.email,
				dob: dob,
				gender: $scope.form.gender,
				address_1: $scope.form.address_1,
				address_2: $scope.form.address_2,
				postal_code: $scope.form.postal_code,
				subscribe: subscribe
			});
			updatePromise.then(function(data){
				
				$uibModal.open({
					template: 
							'<div class="modal-body">' +
							'   Data successfully updated' +
							'</div>' +
							'<div class="modal-footer">' +
							'   <button class="btn btn-primary" type="button" ng-click="close()">Close</button>' +
							'</div>',
					controller: ['$scope', '$uibModalInstance', 'error_term', function($scope, $uibModalInstance, error_term){
					$scope.error_term = error_term;
					$scope.close = function(){
						$uibModalInstance.dismiss();
					}
					}],
					resolve: {
					error_term: function(){
						return $scope.error_term;
					}
					}
				});

			}, function(response){

			});
		}
		
	}

	$scope.calling_code = "-";

	$scope.$watch('form.country', function(){
		
		if(!$scope.form.country){
			$scope.calling_code = "-";
		}else{
			for(var i=0; i<$scope.countries.length; i++){
				if($scope.countries[i].country_id == $scope.form.country){
					$scope.calling_code = $scope.countries[i].calling_code;
					break;
				}
			}
		}
	});

	$scope.error_delete = false;
	$scope.form_delete_account = {
		confirm: false,
		pin: ''
	};
	$scope.deleteAccount = function(){

		if(!$scope.form_delete_account.confirm || !$scope.form_delete_account.pin){
			$scope.error_delete = "Please complete the pin and confirm the statement";
		}else{
			var deletePromise = AccountService.deleteAccount($scope.base_url, $scope.form_delete_account.pin);
			deletePromise.then(function(response){
				$scope.deleted = true;
			}, function(response){
				$scope.error_delete = response.message;
			});
		}
	}
		
}
