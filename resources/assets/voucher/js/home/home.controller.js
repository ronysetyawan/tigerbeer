angular.module('CPRV-OctFestVoucherApp')
	.controller("HomeCtrl",['$rootScope', '$scope', 'HomeService', '$uibModal', HomeCtrl]);
function HomeCtrl($rootScope, $scope, HomeService, $uibModal){
	'use strict';
	
	$scope.newsletterpage = 1;

	$scope.newsletterNext = function(){
		$scope.newsletterpage = 2;
	}

	$scope.newsletterBack = function(){
		$scope.newsletterpage = 1;
	}

	$scope.alert_newsletter = {
		type: "",
		message: ""
	};
	
	$scope.closeAlertNewsletter = function(){
		$scope.alert_newsletter.type = '';
		$scope.alert_newsletter.message = '';
	}

	$scope.form_newsletter = {
		subscribe: false
	};

	$scope.show_newsletter = true;
	$scope.subscribeUpdate = function(){

		var subscribe = 0;
		if($scope.form_newsletter.subscribe){
			subscribe = 1;
		}
		var subscribeUpdatePromise = HomeService.subscribeUpdate($scope.base_url, subscribe);
		subscribeUpdatePromise.then(function(data){
			if(angular.isDefined(data.subscribe_first_time) && data.subscribe_first_time){
				$scope.alert_newsletter.type = "alert-success";
				$scope.alert_newsletter.message = "Preference updated successfully. You have received 1 additional point";

				$scope.show_newsletter = false;
			}

			$scope.total_points = data.total_points;
		}, function(response){

		});
	};

	$scope.logout = function(url){
		window.location.href=url;
	};
}
