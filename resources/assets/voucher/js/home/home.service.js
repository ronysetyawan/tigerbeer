angular.module('CPRV-OctFestVoucherApp')
	.service("HomeService",['$q', '$http', HomeService]);

function HomeService($q, $http){
	
	this.subscribeUpdate = function(url, status) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/account/subscribe-update',
			data    : { status: status }
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

}