angular.module('CPRV-OctFestVoucherApp')
	.service("PrizeService",['$q', '$http', '$log', PrizeService]);

function PrizeService($q, $http, $log){

	this.redeem = function(url, data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/prize',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

}