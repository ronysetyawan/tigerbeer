angular.module('CPRV-OctFestVoucherApp')
	.controller("PrizeCtrl",['$rootScope', '$scope', 'PrizeService', '$uibModal', PrizeCtrl]);
function PrizeCtrl($rootScope, $scope, PrizeService, $uibModal){
    'use strict';
    
    $scope.prize_shipping_product_success = false;

    $scope.prize_promocode_success = false;

    $scope.prize_type_shipping_product_selected = false;

    $scope.prizes = {
        data: ''
    };

    $scope.form_delivery = {
        address_1: "",
        address_2: "",
        postal_code: "",
        country: 192,
        mobile: "",
        email: ""
    };

    $scope.prize_promocode = {}

    $scope.selectedPrize = '';
    
	$scope.selectPrize = function(index){

        $scope.selectedPrize = $scope.prizes.data[index];
        
        if($scope.selectedPrize.type=='promocode'){
            var modalInstance = $uibModal.open({
                template: '<div class="modal-header">' +
                            '   <h3 class="modal-title" id="modal-title">Confirmation</h3>' +
                            '</div>' +
                            '<div class="modal-body" id="modal-body">' +
                            '<p>You have selected {{ product_name }}.<br>' +
                            'Voucher valid till 30 November 2018.</p>' +
                            'Points used and prizes issued are irreversible.' +
                            '</div>' +
                            '<div class="modal-footer">' +
                            '   <button class="btn btn-primary" type="button" ng-click="yes()">Confirm</button>' +
                            '   <button class="btn btn-default" type="button" ng-click="no()">Cancel</button>' +
                            '</div>',
                controller: ['$scope', '$uibModalInstance', 'product_name', function($scope, $uibModalInstance, product_name){
                    $scope.product_name = product_name;

                    $scope.yes = function(){
                        $uibModalInstance.close();
                    };
                    $scope.no = function(){
                        $uibModalInstance.dismiss();
                    };
                }],
                resolve: {
                    product_name: function(){
                        return $scope.selectedPrize.title1
                    }
                }
            });
        
            modalInstance.result.then(function() {
                
                var redeemPromise = PrizeService.redeem($scope.base_url, {
                    prize_id: $scope.selectedPrize.id
                });

                redeemPromise.then(function(response){

                    $scope.total_point = response.total_point;
                    
                    $scope.prize_promocode = {
                        image: $scope.selectedPrize.image,
                        purchase_link: response.purchase_link,
                        code: response.prize_promocode
                    };

                    $scope.prize_promocode_success = true;

                    $scope.promo_codes = response.promo_codes;

                }, function(response){

                    $uibModal.open({
                        template: '<div class="modal-body">{{ message }}</div>' +
                                    '<div class="modal-footer">' +
                                    '   <button class="btn btn-default" type="button" ng-click="ok()">OK</button>' +
                                    '</div>',
                        controller: ['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){

                            $scope.message = message;

                            $scope.ok = function(){
                                $uibModalInstance.dismiss();
                            };
                        }],
                        resolve: {
                            message: function(){
                                return response.message;
                            }
                        }
                    });

                });

            }, function () {
                
            });
        }else{
            $scope.prize_type_shipping_product_selected = true;
        }
    };

    $scope.$watch('form_delivery.country', function(){
        if(!$scope.form_delivery.country){
            $scope.form_delivery_calling_code = "-";
        }else{
            for(var i=0; i<$scope.countries.length; i++){
                if($scope.countries[i].country_id == $scope.form_delivery.country){
                    $scope.form_delivery_calling_code = $scope.countries[i].calling_code;
                    break;
                }
            }
        }
    });

    $scope.errors_delivery = {};

    $scope.submitDelivery = function(){

        $scope.errors_delivery = {};

		if(!angular.isDefined($scope.form_delivery.address_1) || !$scope.form_delivery.address_1){
			$scope.errors_delivery.address_1 = "Address 1 cannot be empty!";
		}
		if(!angular.isDefined($scope.form_delivery.postal_code) || !$scope.form_delivery.postal_code){
			$scope.errors_delivery.postal_code = "Postal Code cannot be empty!";
		}
		if(!angular.isDefined($scope.form_delivery.mobile) || !$scope.form_delivery.mobile){
			$scope.errors_delivery.mobile = "Mobile cannot be empty!";
		}
		if(!angular.isDefined($scope.form_delivery.email) || !$scope.form_delivery.email){
			$scope.errors_delivery.email = "Email cannot be empty or not valid!";
		}

        if(angular.equals({}, $scope.errors_delivery)){

            var modalInstance = $uibModal.open({
                template: '<div class="modal-header">' +
                        '   <h3 class="modal-title"><strong>Contact Confirmation</strong></h3>' +
                        '</div>' +
                            '<div class="modal-body">' +
                            '   <p>I have confirmed that the contact information provided is accurate.<br>Points used and prize issued are irrevisible.</p>' +
                            '   <p>Prize Selected: {{ product_name }}</p>' +
                            '</div>' +
                            '<div class="modal-footer">' +
                            '   <button class="btn btn-primary" type="button" ng-click="yes()">Confirm</button>' +
                            '   <button class="btn btn-default" type="button" ng-click="no()">Cancel</button>' +
                            '</div>',
                controller: ['$scope', '$uibModalInstance', 'product_name', function($scope, $uibModalInstance, product_name){
                    $scope.product_name = product_name;
                    
                    $scope.yes = function(){
                        $uibModalInstance.close();
                    };
                    $scope.no = function(){
                        $uibModalInstance.dismiss();
                    };
                }],
                resolve: {
                    product_name: function(){
                        return $scope.selectedPrize.title1
                    }
                }
            });

            modalInstance.result.then(function() {
                
                var redeemPromise = PrizeService.redeem($scope.base_url, {
                    prize_id: $scope.selectedPrize.id,
                    address_1: $scope.form_delivery.address_1,
                    address_2: $scope.form_delivery.address_2,
                    postal_code: $scope.form_delivery.postal_code,
                    country: $scope.form_delivery.country,
                    mobile: $scope.form_delivery.mobile,
                    email: $scope.form_delivery.email
                });

                redeemPromise.then(function(response){
                    
                    $scope.total_point = response.total_point;
                        
                    $scope.prize_shipping_product = {
                        image: $scope.selectedPrize.image
                    };

                    $scope.prize_shipping_product_success = true;

                    $scope.form_delivery.address_1 = '';
                    $scope.form_delivery.address_2 = '';
                    $scope.form_delivery.postal_code = '';
                    $scope.form_delivery.country = '';
                    $scope.form_delivery.mobile = '';
                    $scope.form_delivery.email = '';

                }, function(response){
                    $uibModal.open({
                        template: '<div class="modal-body">{{ message }}</div>' +
                                    '<div class="modal-footer">' +
                                    '   <button class="btn btn-default" type="button" ng-click="ok()">OK</button>' +
                                    '</div>',
                        controller: ['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){

                            $scope.message = message;

                            $scope.ok = function(){
                                $uibModalInstance.dismiss();
                            };
                        }],
                        resolve: {
                            message: function(){
                                return response.message;
                            }
                        }
                    });
                });

            }, function () {
                
            });
        }
    }

    $scope.cancelDelivery = function(){
        $scope.prize_type_shipping_product_selected = false;
    }

    $scope.back = function(){

        $scope.prize_shipping_product_success = false;

        $scope.prize_promocode_success = false;

        $scope.prize_type_shipping_product_selected = false;
        
    }
    
}
