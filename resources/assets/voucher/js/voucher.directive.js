angular.module('CPRV-OctFestVoucherApp')
.directive('buttonConfirmation', ['$parse', '$uibModal', function ($parse, $uibModal) {
    return {
        restrict: 'A',
        scope: {
            "confirmationTitle": "@",
            "confirmationDescription": "@",
            "confirmationFunction": "&"
        },
        link: function(scope, element, attrs){
            $(element).on('click', function(e) {
                var modalInstance = $uibModal.open({
                    template: '<div class="modal-header">' +
                                '   <h3 class="modal-title" id="modal-title">{{title}}</h3>' +
                                '</div>' +
                                '<div class="modal-body" id="modal-body" ng-bind-html="description"></div>' +
                                '<div class="modal-footer">' +
                                '   <button class="btn btn-primary" type="button" ng-click="yes()">Yes</button>' +
                                '   <button class="btn btn-default" type="button" ng-click="no()">No</button>' +
                                '</div>',
                    controller: ['$scope', 'title', 'description', '$uibModalInstance', function($scope, title, description, $uibModalInstance){
                        $scope.title = title;
                        $scope.description = description;

                        $scope.yes = function(){
                            $uibModalInstance.close();
                        };
                        $scope.no = function(){
                            $uibModalInstance.dismiss();
                        };
                    }],
                    resolve: {
                        title: function(){
                        return scope.confirmationTitle;
                        },
                        description: function(){
                        return scope.confirmationDescription;
                        }
                    }
                });

                modalInstance.result.then(function() {
                    scope.confirmationFunction();
                }, function () {
                    
                });
            });
            }
    };
}])

.directive('dobPickerUi', function () {
    return {
        restrict: 'A',
        scope:{
            date:'=',
            totalYear: "@",
            maxYear: "@",
            minAge: "@"
        },
        link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'dd/mm/yy',
                yearRange: (scope.maxYear - scope.totalYear) + ":" + scope.maxYear,
                onSelect: function (date) {
                    scope.date = element.datepicker( 'getDate' );
                    scope.$apply();
                }
            });
            
            scope.$watch('date', function(){
                if(scope.date){
                    element.datepicker("setDate", scope.date );
                }
            })
           
            
            
        }
    };
})

.directive('moveNextOnMaxlength', function () {
    return {
        restrict: "A",
        link: function($scope, element) {
            element.on("input", function(e) {
                if(element.val().length == element.attr("maxlength")) {
                    if(element.attr("next") !== undefined) {
                        angular.element(element.attr("next")).focus()
                    }
                }
            });
        }
    }
})

;
