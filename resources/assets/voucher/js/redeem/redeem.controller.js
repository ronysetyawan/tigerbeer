angular.module('CPRV-OctFestApp')
	.controller("RedeemCtrl",['$rootScope', '$scope', 'RedeemService', '$uibModal', RedeemCtrl]);
function RedeemCtrl($rootScope, $scope, RedeemService, $uibModal){
	'use strict';

	$scope.form_pin = {};

	angular.element('.list-input-pin li input').keyup(function(e){
		//console.log("kuuuk");
		//if(this.value){
			angular.element(this).parent().next().find('input').focus();
		//}
	});

	$scope.success = false;

	$scope.qty = 1;

	$scope.increaseQty = function(){
		if($scope.qty<$scope.balance){
			$scope.qty++;
		}
	};
	$scope.decreaseQty = function(){
		if($scope.qty>1){
			$scope.qty--;
		}
	};

	$scope.form_validate = {
		property_pin: ""
	};

	$scope.error_property = "";
	$scope.redeem = function(){
		$scope.error_property = ""; 
		console.log($scope.vid);

		if(
			!$scope.form_pin.input_1 || 
			!$scope.form_pin.input_2 ||
			!$scope.form_pin.input_3 ||
			!$scope.form_pin.input_4){
			$scope.error_property = "Please complete the pin";
		}else{

			var pin =  $scope.form_pin.input_1.toString() + 
			$scope.form_pin.input_2.toString() + $scope.form_pin.input_3.toString() +
			$scope.form_pin.input_4.toString();
			
			var redeemPromise = RedeemService.redeem($scope.base_url, {
				property_pin : pin,
				pint : $scope.qty,
				vid : $scope.vid
			});
		
			redeemPromise.then(function(response){
				$scope.success = true;
				$scope.balance = response.balance;
				$scope.qty = 1;

				$scope.form_pin.input_1 = '';
				$scope.form_pin.input_2 = '';
				$scope.form_pin.input_3 = '';
				$scope.form_pin.input_4 = '';

			}, function(response){
				$scope.error_property = response.message;
			});
		}

	};

	$scope.backSuccess = function(){
		$scope.success = false;
	};
}