angular.module('CPRV-OctFestApp')
	.service("RedeemService",['$q', '$http', '$log', RedeemService]);

function RedeemService($q, $http, $log){

	this.redeem = function(url, data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : url + '/redeem',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

}