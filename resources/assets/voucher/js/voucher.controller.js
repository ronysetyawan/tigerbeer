angular.module('CPRV-OctFestVoucherApp')
	.controller("MainCtrl",['$rootScope', '$scope', '$uibModal', MainCtrl]);
function MainCtrl($rootScope, $scope, $uibModal){
	'use strict';

	$scope.page_position = 1;

	$scope.form_validate = {
		property_pin: ""
	};

	$scope.show_alert = false;
	$scope.alert_type = "";
	$scope.alert_message = "";

	$scope.form_redeem = {
		property_pin : "",
		product : "",
		product_other : "",
		receipt : "",
		cashier : "",
		note : ""
	};

	$scope.voucher_detail = {};

	$scope.redeemed_info = {};

	$scope.validate = function(){

		$scope.show_alert = false;

		if(!$scope.form_validate.property_pin){
			$scope.alert_type = "alert-danger";
			$scope.alert_message = "Please enter property PIN";
			$scope.show_alert = true;
		}else{
			var validatePromise = RedeemService.validate($scope.voucher_id, $scope.form_validate.property_pin);
			validatePromise.then(function(data){

				$scope.voucher_detail.owner = data.owner;
				$scope.voucher_detail.voucher_id = data.voucher_id;
				$scope.voucher_detail.amount = data.amount;
				$scope.voucher_detail.status = data.status;
				$scope.voucher_detail.expiry_date = data.expiry_date;

				$scope.page_position = 2;

				$scope.form_redeem.property_pin = $scope.form_validate.property_pin;

				$scope.form_validate.property_pin = "";

			}, function(response){

				if(angular.isDefined(response.error_type) && response.error_type=='redeemed'){
					$scope.redeemed = true;
					$scope.redeemed_info = response.redeemed_info;
				}else{
					$scope.alert_type = "alert-danger";
					$scope.alert_message = response.message;
					$scope.show_alert = true;
				}

				$scope.form_validate.property_pin = "";
			});
		}
	};

	$scope.back = function(){
		$scope.redeemed = false;
	};
	
	$scope.closeAlert = function(){
		$scope.show_alert = false;
	};

	$scope.show_signature = false;
	$scope.continueToSignature = function(){
		$scope.error_redeem = {};

		if(!$scope.form_redeem.product){
			$scope.error_redeem.product = "Product cannot be empty!"
		}else if($scope.form_redeem.product=='other' && !$scope.form_redeem.product_other){
			$scope.error_redeem.product_other = "Product cannot be empty!";
		}
		if(!$scope.form_redeem.receipt){
			$scope.error_redeem.receipt = "Invoice cannot be empty!";
		}
		if(!$scope.form_redeem.cashier){
			$scope.error_redeem.cashier = "Cashier cannot be empty!";
		}

		if(angular.equals($scope.error_redeem, {})){
			$scope.show_signature = true;
		}
	};

	$scope.cancelSignature = function(){
		$scope.show_signature = false;
	};

	$scope.redeem_success = false;
	$scope.redeem_success_info = {};

	$scope.error_redeem = {};
	$scope.acceptSignature = {};
	$scope.redeem = function(){

		var signature = $scope.acceptSignature.accept();
	
		if (signature.isEmpty) {
			alert("Please sign first");
		} else {
			var modalInstance = $uibModal.open({
				template: '<div class="modal-header">' +
							'   <h3 class="modal-title" id="modal-title">Confirmation</h3>' +
							'</div>' +
							'<div class="modal-body" id="modal-body">Are you sure want to continue?</div>' +
							'<div class="modal-footer">' +
							'   <button class="btn btn-primary" type="button" ng-click="yes()">Yes</button>' +
							'   <button class="btn btn-warning" type="button" ng-click="no()">No</button>' +
							'</div>',
				controller: ['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
					$scope.yes = function(){
						$uibModalInstance.close();
					};
					$scope.no = function(){
						$uibModalInstance.dismiss();
					};
				}]
			});
	
			modalInstance.result.then(function() {
				
				var redeemPromise = RedeemService.redeem($scope.voucher_id, {
					property_pin : $scope.form_redeem.property_pin,
					product : $scope.form_redeem.product,
					product_other : $scope.form_redeem.product_other,
					receipt : $scope.form_redeem.receipt,
					cashier : $scope.form_redeem.cashier,
					note : $scope.form_redeem.note,
					signature: signature.dataUrl
				});
	
				redeemPromise.then(function(response){
	
					$scope.form_redeem.property_pin = "";
					$scope.form_redeem.product = "";
					$scope.form_redeem.product_other = "";
					$scope.form_redeem.receipt = "";
					$scope.form_redeem.cashier = "";
					$scope.form_redeem.note = "";
		
					$scope.redeem_success_info = response.redeem_success_info;
					$scope.redeem_success = true;
				}, function(){
	
				});
	
			}, function () {
				
			});
		}
	};

	$scope.cancelRedeem = function(){
		$scope.page_position = 1;
	};

	$scope.redeemAnother = function(){
		$scope.page_position = 1;
		$scope.redeem_success = false;
	};

}
