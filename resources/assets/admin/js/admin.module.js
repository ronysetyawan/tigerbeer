var app = angular.module('CPRV-OctFest-Admin', [
	'ui.bootstrap',
	'ngSanitize',
	'angular-loading-bar',
	'googlechart',
	'ngFileUpload',
    'daterangepicker'
]);
