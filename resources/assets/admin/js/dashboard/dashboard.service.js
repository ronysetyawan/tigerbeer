angular.module('CPRV-OctFest-Admin')
	.service("DashboardService",['cfg', '$q', '$http', '$log', DashboardService]);

function DashboardService(cfg, $q, $http, $log){
	
	this.getWidgetPurchased = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/dashboard/widget/purchased',
			params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.getWidgetPintPurchased = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/dashboard/widget/pint-purchased',
			params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

  	this.getWidgetTransaction = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
            url     : cfg.api + '/dashboard/widget/transaction',
            params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
  	};

  	this.getWidgetPintRedeemed = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
            url     : cfg.api + '/dashboard/widget/pint-redeemed',
            params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};
	
	this.getWidgetContributors = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/dashboard/widget/contributors',
			params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	
	this.getChartTotalTransaction = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/dashboard/chart/total-transaction',
			params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};
		
	this.getChartRevenue = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
						url     : cfg.api + '/dashboard/chart/revenue',
						params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

  this.getChartAverageTransaction = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
            url     : cfg.api + '/dashboard/chart/average-transaction',
            params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
  };

  this.getChartVoucherByOccasion = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
            url     : cfg.api + '/dashboard/chart/voucher-by-occasion',
            params  : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
  };
}