angular.module('CPRV-OctFest-Admin')
	.controller("DashboardCtrl",['$scope', 'DashboardService', '$log', DashboardCtrl]);

function DashboardCtrl($scope, DashboardService, $log){

	$scope.form_filter = {
		retailer: ""
	};

	$scope.widget = {
		purchased: '',
		pint_purchased: '',
		transaction: '',
		pint_redeemed: '',
		contributors: ''
	};

	$scope.chart = {
		transaction: {},
		revenue: {},
		average_transaction: {},
		voucher_by_occasion: {}
	};

	function setWidget(){
		var WidgetPurchasedPromise = DashboardService.getWidgetPurchased();
		WidgetPurchasedPromise.then(function(data){
			$scope.widget.purchased = data.data;
		},function(){

		});

		var WidgetPintPurchasedPromise = DashboardService.getWidgetPintPurchased();
		WidgetPintPurchasedPromise.then(function(data){
			$scope.widget.pint_purchased = data.data;
		}, function(){

		});

		var WidgetTransactionPromise = DashboardService.getWidgetTransaction();
		WidgetTransactionPromise.then(function(data){
			$scope.widget.transaction = data.data;
		}, function(){

		});

		var WidgetPintRedeemedPromise = DashboardService.getWidgetPintRedeemed();
		WidgetPintRedeemedPromise.then(function(data){
			$scope.widget.pint_redeemed = data.data;
		}, function(){

		});
		
		var WidgetContributors = DashboardService.getWidgetContributors();
		WidgetContributors.then(function(data){
			$scope.widget.contributors = data.data;
		},function(){

		});
	}

	function setChart(){
		var ChartTotalTransactionPromise = DashboardService.getChartTotalTransaction();
		ChartTotalTransactionPromise.then(function(data){
			
			$scope.chart.transaction = {
				type: "ColumnChart",
				data: data.data
			};
    
		}, function(){
			
		});

		var ChartRevenuePromise = DashboardService.getChartRevenue({retailer: $scope.form_filter.retailer});
		ChartRevenuePromise.then(function(data){

			$scope.chart.revenue = {
				type: "ColumnChart",
				data: data.data
			};

		}, function(){

		});

		var ChartAverageTransactionPromise = DashboardService.getChartAverageTransaction({retailer: $scope.form_filter.retailer});
		ChartAverageTransactionPromise.then(function(data){

			$scope.chart.average_transaction = {
				type: "ColumnChart",
				data: data.data
			};

		}, function(){

		});
	}

	setWidget();
	setChart();

	$scope.filter = function(){
		setWidget();
		setChart();
	}
	
}


