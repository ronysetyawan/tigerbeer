angular.module('CPRV-OctFest-Admin')
	.service("PropertyService",['cfg', '$q', '$http', '$log', PropertyService]);

function PropertyService(cfg, $q, $http, $log){
	
	this.insert = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/setting/property',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.getProperty = function(property_id) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/setting/property/' + property_id + '/edit'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.update = function(property_id, data) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/setting/property/' + property_id,
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.updateStatus = function(property_id, status) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/setting/property/' + property_id + '/status',
			data    : {status: status}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.delete = function(property_id) {
		var deferred = $q.defer();
		$http({
			method	: 'DELETE',
			url     : cfg.api + '/setting/property/' + property_id,
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

}