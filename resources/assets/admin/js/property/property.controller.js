angular.module('CPRV-OctFest-Admin')
	.controller("PropertyListCtrl",['$scope', 'PropertyService', '$uibModal', '$timeout', PropertyListCtrl]);

function PropertyListCtrl($scope, PropertyService, $uibModal, $timeout){

	$scope.delete = function(index){
		var selected_property = $scope.properties[index];
		var deletePromise = PropertyService.delete(selected_property.id);
		deletePromise.then(function(){
			window.location.href = $scope.current_url;
		}, function(){

		});
	};

	$scope.create = function(){

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', '$uibModalInstance', '$log', 'cfg', '$uibModal', 'Upload', function($scope, $uibModalInstance, $log, cfg, $uibModal, Upload){

				$scope.title = "Add New Property";
				
				$scope.form = {
					name : "",
					address1 : "",
					address2 : "",
					zone : "",
					pin : "",
					status : 1
				};
			
				$scope.errors = {};
				$scope.save = function(){

					$scope.errors = {};

					if(!$scope.form.name){
						$scope.errors.name = "Name cannot be empty!";
					}

					if(!$scope.form.address1){
						$scope.errors.address1 = "Address 1 cannot be empty!";
					}

					if(!$scope.form.zone){
						$scope.errors.zone = "Please select a zone!";
					}

					if(!$scope.form.pin){
						$scope.errors.pin = "PIN cannot be empty (must be 5 digit numbers from 11111-99999)!";
					}

					if(angular.equals($scope.errors, {})){
						var insertPromise = PropertyService.insert({
							name : $scope.form.name,
							address1 : $scope.form.address1,
							address2 : $scope.form.address2,
							zone : $scope.form.zone,
							pin : $scope.form.pin,
							status : $scope.form.status
						});
						insertPromise.then(function(response){
							window.location.href = $scope.current_url;
						}, function(response){
							if(angular.isDefined(response.name)){
								$scope.errors.name = response.name[0];
							}
							if(angular.isDefined(response.pin)){
								$scope.errors.pin = response.pin[0];
							}
						});
					}
				};
			}]
		});

	};

	$scope.edit = function(index){

		var selected_property = $scope.properties[index];

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', 'property_id', '$uibModalInstance', '$log', function($scope, property_id, $uibModalInstance, $log){
				
				$scope.title = "Edit Property";

				$scope.form = {};

				var propertyPromise = PropertyService.getProperty(property_id);
				propertyPromise.then(function(response){
					$scope.form.name = response.name;
					$scope.form.address1 = response.address1;
					$scope.form.address2 = response.address2;
					$scope.form.zone = response.zone;
					$scope.form.pin = parseInt(response.pin);
					$scope.form.status = response.status;
				});

				$scope.errors = {};
				$scope.save = function(){
				
					$scope.errors = {};

					if(!$scope.form.name){
						$scope.errors.name = "Name cannot be empty!";
					}

					if(!$scope.form.address1){
						$scope.errors.address1 = "Address 1 cannot be empty!";
					}

					if(!$scope.form.zone){
						$scope.errors.zone = "Please select a zone!";
					}

					if(!$scope.form.pin){
						$scope.errors.pin = "PIN cannot be empty!";
					}

					if(angular.equals($scope.errors, {})){

						var updatePromise = PropertyService.update(property_id, {
							name: $scope.form.name,
							address1: $scope.form.address1,
							address2: $scope.form.address2,
							zone: $scope.form.zone,
							pin: $scope.form.pin,
							status: $scope.form.status
						});
						updatePromise.then(function(response){
							$uibModalInstance.close({
								name: response.name,
								address1: response.address1,
								address2: response.address2,
								zone: response.zone,
								pin: response.pin,
								status: response.status,
								status_string: response.status_string
							});
						}, function(response){
							if(angular.isDefined(response.pin)){
								$scope.errors.pin = response.pin[0];
							}
						});
					}
				};
			}],
			resolve: {
				property_id : function(){
					return selected_property.id;
				}
			}
		});

		modalInstance.result.then(function(response) {
			$scope.properties[index].name = response.name;
			$scope.properties[index].address_1 = response.address1;
			$scope.properties[index].address_2 = response.address2;
			$scope.properties[index].zone = response.zone;
			$scope.properties[index].pin = response.pin;
			$scope.properties[index].status = response.status;
			$scope.properties[index].status_string = response.status_string;
		}, function (response) {
			
		});
	}

	$scope.toggleStatus = function(index){
		var selected_property = $scope.properties[index];
		var updatePromise = PropertyService.updateStatus(selected_property.id, !selected_property.status);
		updatePromise.then(function(response){
			$scope.properties[index].status = response.status;
			$scope.properties[index].status_string = response.status_string;
		}, function(){

		});
	};

}


angular.module('CPRV-OctFest-Admin')
	.controller("PropertyFormCtrl",['$scope', 'PropertyService', '$uibModal', '$timeout', '$log', PropertyFormCtrl]);

function PropertyFormCtrl($scope, PropertyService, $uibModal, $timeout, $log){
	

	$scope.submit = function(){

		$scope.errors = {};

		if(!$scope.form.mobile){
			$scope.errors.mobile = "Mobile cannot be empty!";
		}
		if(!$scope.form.name){
			$scope.errors.name = "Name cannot be empty!";
		}

		if(angular.equals($scope.errors, {})){

			var datas = {
				
				qty: $scope.form.qty,
				unlimited: $scope.form.unlimited,
				country_id: $scope.form.country_id,
				mobile: $scope.form.mobile,
				name: $scope.form.name,
				email: $scope.form.email,
				receipt: $scope.form.receipt
			};

			var insertPromise = PropertyService.insert(datas);
			insertPromise.then(function(){

				$scope.form.branch_id = '';
				$scope.form.brand_id = '';
				$scope.form.qty = '';
				$scope.form.unlimited = false;
				$scope.form.country_id = 192;
				$scope.form.calling_code = '';
				$scope.form.mobile = '';
				$scope.form.name = '';
				$scope.form.email = '';
				$scope.form.receipt = '';

				$uibModal.open({
					template: '<div class="modal-body">' +
						  '   <div>Voucher Created Successfully!</div>' +
						  '</div>' +
						  '<div class="modal-footer">' +
						  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
						  '</div>',
					controller:['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
						$scope.close = function(){
							$uibModalInstance.dismiss();
						}
					}]
				});

			}, function(){

			});
		}
	}
}