angular.module('CPRV-OctFest-Admin')
	.service("PrizeService",['cfg', '$q', '$http', '$log', PrizeService]);

function PrizeService(cfg, $q, $http, $log){
	
	this.delete = function(prize_id) {
		var deferred = $q.defer();
		$http({
			method	: 'DELETE',
			url     : cfg.api + '/prize/' + prize_id
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
    };

	this.update = function(prize_id, data) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/prize/' + prize_id,
			data    : data
		}).then(function(response) {
			deferred.resolve(response.data);
		}, function(response) {
			deferred.reject(response.data);
		});
		
		return deferred.promise;
	};

	this.updateStatus = function(prize_id, status) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/prize/' + prize_id + '/status',
			data    : {status: status}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.getPrize = function(prize_id) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/prize/' + prize_id + '/edit'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.getImages = function(prize_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/prize/' + prize_id + '/image'
		}).then(function(response) {
			deferred.resolve(response.data);
		}, function() {
			deferred.reject(response.data);
		});
		
		return deferred.promise;
	};
}