angular.module('CPRV-OctFest-Admin')
	.controller("PrizeListCtrl",['$scope', 'PrizeService', '$uibModal', '$timeout', '$log', PrizeListCtrl]);

function PrizeListCtrl($scope, PrizeService, $uibModal, $timeout, $log){

	$scope.delete = function(index){
		var selected_prize = $scope.prizes[index];
		var deletePromise = PrizeService.delete(selected_prize.id);
		deletePromise.then(function(){
			window.location.href = $scope.current_url;
		}, function(){

		});
	};

	$scope.create = function(){

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', '$uibModalInstance', '$log', 'cfg', '$uibModal', 'Upload', function($scope, $uibModalInstance, $log, cfg, $uibModal, Upload){

				$scope.title = "Add New Prize";
				
				$scope.form = {
					image : "",
					name : "",
					point_needed : "",
					value : "",
					qty : "",
					sort_order : "",
					status : 1
				};
			
				$scope.errors = {};
				$scope.save = function(){

					$scope.errors = {};

					if(!$scope.form.image){
						$scope.errors.image = "Image cannot be empty!";
					}
					if(!$scope.form.name){
						$scope.errors.name = "Name cannot be empty!";
					}
					if(!$scope.form.point_needed){
						$scope.errors.pint = "Point Needed cannot be empty!";
					}
					if(!$scope.form.qty){
						$scope.errors.stock = "Qty cannot be empty!";
					}

					$log.info($scope.errorImage);

					if(angular.equals($scope.errors, {})){

						var upload = Upload.upload({
							url: cfg.api + '/prize',
							data: {
								image: $scope.form.image,
								name: $scope.form.name,
								point_needed: $scope.form.point_needed,
								value: $scope.form.value,
								qty: $scope.form.qty,
								sort_order: $scope.form.sort_order,
								status: $scope.form.status
							}
						});

						upload.then(function(){
							window.location.href = $scope.current_url;
						}, function(response){
							
						});
					}
				};
			}]
		});

	};

	$scope.edit = function(index){

		var selected_prize = $scope.prizes[index];

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', 'prize_id', '$uibModalInstance', '$log', 'Upload', 'cfg', function($scope, prize_id, $uibModalInstance, $log, Upload, cfg){
				
				$scope.title = "Edit Prize";

				$scope.form = {};

				var prizePromise = PrizeService.getPrize(prize_id);
				prizePromise.then(function(response){
					$scope.form.name = response.name;
					$scope.form.point_needed = response.point_needed;
					$scope.form.value = response.value;
					$scope.form.qty = response.qty;
					$scope.form.type = response.type;
					$scope.form.sort_order = response.sort_order;
					$scope.form.status = response.status;
				});

				$scope.errors = {};
				$scope.save = function(){
					
					
					$scope.errors = {};

					if(!$scope.form.name){
						$scope.errors.name = "Name cannot be empty!";
					}
					if(!$scope.form.pint){
						$scope.errors.pint = "Pint cannot be empty!";
					}
					if(!$scope.form.price){
						$scope.errors.price = "Price cannot be empty!";
					}
					if(!$scope.form.stock){
						$scope.errors.stock = "Stock cannot be empty!";
					}

					if(angular.equals($scope.errors, {})){

						var upload = Upload.upload({
							url: cfg.api + '/prize',
							data: {
								_method: 'PUT',
								name: $scope.form.name,
								description: $scope.form.description,
								pint: $scope.form.pint,
								price: $scope.form.price,
								stock: $scope.form.stock,
								sort_order: $scope.form.sort_order,
								status: $scope.form.status
							}
						});

						upload.then(function(){
							
						}, function(response){
							
						});


						
					}
				};
			}],
			resolve: {
				prize_id : function(){
					return selected_prize.id;
				}
			}
		});

		modalInstance.result.then(function(response) {
			$scope.api_clients[index].branch_name = response.branch_name;
			$scope.api_clients[index].title = response.title;
			$scope.api_clients[index].status = response.status;
			$scope.api_clients[index].status_string = response.status_string;
		}, function (response) {
			
		});
	}

	$scope.toggleStatus = function(index){
		var selected_prize = $scope.prizes[index];
		var updatePromise = PrizesService.updateStatus(selected_prize.id, !selected_prize.status);
		updatePromise.then(function(response){
			$scope.prizes[index].status = response.status;
			$scope.prizes[index].status_string = response.status_string;
		}, function(){

		});
	};

}
