angular.module('CPRV-OctFest-Admin')
	.controller("CountryListCtrl",['$scope', 'CountryService', '$uibModal', '$timeout', CountryListCtrl]);

function CountryListCtrl($scope, CountryService, $uibModal, $timeout){

	$scope.delete = function(index){
		var selected_country = $scope.countries[index];
		var deletePromise = CountryService.delete(selected_country.id);
		deletePromise.then(function(){
			window.location.href = $scope.current_url;
		}, function(){

		});
	};

	$scope.create = function(){

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', '$uibModalInstance', '$log', '$uibModal', function($scope, $uibModalInstance, $log, $uibModal){

				$scope.title = "Add New Country";
				
				$scope.form = {
					name : "",
					code2 : "",
					code3 : "",
					calling_code : "",
					status : 1
				};
			
				$scope.errors = {};
				$scope.save = function(){

					$scope.errors = {};

					if(!$scope.form.name){
						$scope.errors.name = "Country Name cannot be empty!";
					}

					if(!$scope.form.code2){
						$scope.errors.code2 = "Country Code 2 cannot be empty!";
					}

					if(!$scope.form.code3){
						$scope.errors.code3 = "Country Code 3 cannot be empty!";
					}

					if(!$scope.form.calling_code){
						$scope.errors.calling_code = "Calling Code cannot be empty!";
					}

					if(angular.equals($scope.errors, {})){
						var insertPromise = CountryService.insert({
							name : $scope.form.name,
							code2 : $scope.form.code2,
							code3 : $scope.form.code3,
							calling_code : $scope.form.calling_code,
							status : $scope.form.status
						});
						insertPromise.then(function(response){
							window.location.href = $scope.current_url;
						}, function(response){

						});
					}
				};
			}]
		});

	};

	$scope.edit = function(index){

		var selected_country = $scope.countries[index];

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', 'country_id', '$uibModalInstance', '$log', function($scope, country_id, $uibModalInstance, $log){
				
				$scope.title = "Edit Country";

				$scope.form = {};

				var countryPromise = CountryService.getCountry(country_id);
				countryPromise.then(function(response){
					$scope.form.name = response.name;
					$scope.form.code2 = response.code2;
					$scope.form.code3 = response.code3;
					$scope.form.calling_code = response.calling_code;
					$scope.form.status = response.status;
				});

				$scope.errors = {};
				$scope.save = function(){
				
					$scope.errors = {};

					if(!$scope.form.name){
						$scope.errors.name = "Country Name cannot be empty!";
					}

					if(!$scope.form.code2){
						$scope.errors.code2 = "Country Code 2 cannot be empty!";
					}

					if(!$scope.form.code3){
						$scope.errors.code3 = "Country Code 3 cannot be empty!";
					}

					if(!$scope.form.calling_code){
						$scope.errors.calling_code = "Calling Code cannot be empty!";
					}

					if(angular.equals($scope.errors, {})){

						var updatePromise = CountryService.update(country_id, {
							name: $scope.form.name,
							code2: $scope.form.code2,
							code3: $scope.form.code3,
							calling_code: $scope.form.calling_code,
							status: $scope.form.status
						});
						updatePromise.then(function(response){
							$uibModalInstance.close({
								name: response.name,
								code2: response.code2,
								code3: response.code3,
								calling_code: response.calling_code,
								status: response.status,
								status_string: response.status_string
							});
						}, function(response){
							
						});
					}
				};
			}],
			resolve: {
				country_id : function(){
					return selected_country.id;
				}
			}
		});

		modalInstance.result.then(function(response) {
			$scope.countries[index].name = response.name;
			$scope.countries[index].code_2 = response.code2;
			$scope.countries[index].code_3 = response.code3;
			$scope.countries[index].calling_code = response.calling_code;
			$scope.countries[index].status = response.status;
			$scope.countries[index].status_string = response.status_string;
		}, function (response) {
			
		});
	}

	$scope.toggleStatus = function(index){
		var selected_country = $scope.countries[index];
		var updatePromise = CountryService.updateStatus(selected_country.id, !selected_country.status);
		updatePromise.then(function(response){
			$scope.countries[index].status = response.status;
			$scope.countries[index].status_string = response.status_string;
		}, function(){

		});
	};

}