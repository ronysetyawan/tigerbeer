angular.module('CPRV-OctFest-Admin')
	.service("CountryService",['cfg', '$q', '$http', '$log', CountryService]);

function CountryService(cfg, $q, $http, $log){
	
	this.insert = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/setting/country',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.getCountry = function(country_id) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/setting/country/' + country_id + '/edit'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.update = function(country_id, data) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/setting/country/' + country_id,
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.updateStatus = function(country_id, status) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/setting/country/' + country_id + '/status',
			data    : {status: status}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.delete = function(country_id) {
		var deferred = $q.defer();
		$http({
			method	: 'DELETE',
			url     : cfg.api + '/setting/country/' + country_id,
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

}