angular.module('CPRV-OctFest-Admin')
	.controller("VoucherViewCtrl",['$scope', 'VoucherService', '$uibModal', VoucherViewCtrl]);

function VoucherViewCtrl($scope, VoucherService, $uibModal){

	$scope.owner_info = {};
	
	$scope.redeem = function(){
		$uibModal.open({
			template: '<div class="modal-header">' +
						'   <h3 class="modal-title">Redeem</h3>' +
						'</div>' +
						'<div class="modal-body">' +
						'   <div class="alert alert-danger" ng-if="errors.length>=1">' +
						'      <ul class="list-unstyled">' +
						'         <li ng-repeat="error in errors">{{error}}</li>' +
						'      </ul>' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Receipt No<span class="required">*</span></label>' +
						'      <input type="text" class="form-control" name="receipt" ng-model="form.receipt">' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Cashier Name<span class="required">*</span></label>' +
						'      <input type="text" class="form-control" name="cashier" ng-model="form.cashier">' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Amount for this transaction<span class="required">*</span></label>' +
						'      <input type="number" class="form-control" name="amount" ng-model="form.amount">' +
						'      <div class="help-block">Only number in ML. Dot (.) as decimal</div>' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Product/Service</label>' +
						'      <textarea class="form-control" name="note" ng-model="form.note"></textarea>' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Redeem Date<span class="required">*</span></label>' +
						'      <div class="input-group">' +
						'         <input type="text" readonly class="form-control" uib-datepicker-popup="d MMM yyyy" ng-model="form.date" is-open="redeem_datepicker.opened" datepicker-options="dateOptions" close-text="Close" />' +
						'         <span class="input-group-btn">' +
						'            <button type="button" class="btn btn-default" ng-click="openDatepicker()"><i class="glyphicon glyphicon-calendar"></i></button>' +
						'         </span>' +
					    '      </div>' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <div uib-timepicker ng-model="form.date" show-meridian="true" show-seconds="true"></div>' +
						'   </div>' +
						'</div>' +
						'<div class="modal-footer">' +
						'   <button class="btn btn-primary" type="button" ng-click="redeem()">Redeem</button>' +
						'   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
						'</div>',
			controller:['$scope', '$uibModalInstance', 'voucher_id', '$log', 'VoucherService', '$uibModal', '$rootScope',
			function($scope, $uibModalInstance, voucher_id, $log, VoucherService, $uibModal, $rootScope){

				$scope.form = {
					receipt: "",
					cashier: "",
					amount: "",
					date: new Date(),
					note: "",
				};

				$scope.dateOptions = {
					formatYear: 'yy',
				};

				$scope.redeem_datepicker = {
					opened: false
				};
				
				$scope.openDatepicker = function(){
					$scope.redeem_datepicker.opened = true;
				};

				$scope.redeem = function(){
					$scope.errors = [];

					if(!$scope.form.amount){
						$scope.errors.push('Amount cannot be empty');
					}

					if(!$scope.form.date){
						$scope.errors.push('Redeem Date cannot be empty');
					}

					if($scope.errors.length<=0){

						var dt = $scope.form.date;
						var dt_string = dt.getFullYear() + "-" + ( dt.getMonth()+1 ) + "-" + dt.getDate() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

						var datas = {
							receipt: $scope.form.receipt,
							cashier: $scope.form.cashier,
							amount: $scope.form.amount,
							date: dt_string,
							note: $scope.form.note,
						};
						var redeemPromise = VoucherService.redeem(voucher_id, datas);
						redeemPromise.then(function(response){

							$scope.form.receipt = "";
							$scope.form.amount = "";
							$scope.form.date = new Date();
							$scope.form.note = "";

							$uibModalInstance.dismiss();

							$rootScope.$broadcast('update-balance', { balance: response.balance });

							$uibModal.open({
								template: '<div class="modal-body">' +
									  '   <div>{{message}}</div>' +
									  '</div>' +
									  '<div class="modal-footer">' +
									  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
									  '</div>',
								controller:['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
									$scope.message = message;
									$scope.close = function(){
										$uibModalInstance.dismiss();
									}
								}],
								resolve: {
									message: function(){
										return response.message;
									}
								}
							});

						}, function(response){

							$uibModal.open({
								template: '<div class="modal-body">' +
									  '   <div class="text-danger">{{message}}</div>' +
									  '</div>' +
									  '<div class="modal-footer">' +
									  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
									  '</div>',
								controller:['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
									$scope.message = message;
									$scope.close = function(){
										$uibModalInstance.dismiss();
									}
								}],
								resolve: {
									message: function(){
										return response.message;
									}
								}
							});

						});
					}
				}

				$scope.close = function(){
					$uibModalInstance.dismiss();
				}
			}],
			resolve: {
				voucher_id: function () {
					return $scope.voucher_id;
				}
			}
		});
	};

	$scope.extend = function(){
		var modalInstance = $uibModal.open({
			template: '<div class="modal-header">' +
						'   <h3 class="modal-title">Extend Voucher</h3>' +
						'</div>' +
						'<div class="modal-body">' +
						'   <div class="form-group">' +
						'      <label>Extend</label>' +
						'      <div class="input-group">' +
						'         <input type="text" class="form-control" uib-datepicker-popup="dd MMMM yyyy" ng-model="extend_date" is-open="redeem_datepicker.opened" datepicker-options="redeem_datepicker_options" close-text="Close" />' +
						'         <span class="input-group-btn">' +
						'            <button type="button" class="btn btn-default" ng-click="openDatePicker()"><i class="glyphicon glyphicon-calendar"></i></button>' +
						'         </span>' +
					    '      </div>' +
						'   </div>' +
						/*'   <div class="form-group">' +
						'      <label>Extend by period</label>' +
						'      <ul class="list-inline">' +
						'         <li><button class="btn btn-default">1 Week</button></li>'+
						'         <li><button class="btn btn-default">2 Weeks</button></li>'+
						'         <li><button class="btn btn-default">1 Month</button></li>'+
						'         <li><button class="btn btn-default">2 Months</button></li>'+
						'      </ul>' +
						'   </div>' +
						'</div>' +
						*/
						'<div class="modal-footer">' +
						'   <button class="btn btn-primary" type="button" ng-click="extend()">Extend</button>' +
						'   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
						'</div>',
			controller:['$scope', '$uibModalInstance', 'voucher_id', '$log', 'VoucherService', '$uibModal', '$rootScope', 'expiry_date',
			function($scope, $uibModalInstance, voucher_id, $log, VoucherService, $uibModal, $rootScope, expiry_date){

				$scope.form = {
					receipt: "",
					amount: "",
					date: new Date(),
					note: "",
				};

				$scope.dateOptions = {
					formatYear: 'yy',
				};

				$scope.redeem_datepicker = {
					opened: false
				};
				
				$scope.openDatePicker = function(){
					$scope.redeem_datepicker.opened = true;
				};

				$scope.extend = function(){
					$scope.errors = [];

					//timezone adjustment
					var dt = new Date($scope.extend_date);
					dt.setMinutes(dt.getMinutes() - dt.getTimezoneOffset());

					var datas = {
						date: dt.getFullYear() + "-" + (dt.getMonth()+1) + "-" + dt.getDate()
					};

					var extendPromise = VoucherService.extend(voucher_id, datas);
					extendPromise.then(function(response){

						$uibModalInstance.close({
							new_expiry_date: response.new_expiry_date,
							new_expiry_date_formatted: response.new_expiry_date_formatted
						});

					}, function(){});

				}

				$scope.close = function(){
					$uibModalInstance.dismiss();
				}

				var minDate = new Date(expiry_date);
				minDate.setDate(minDate.getDate() + 1);
				$scope.redeem_datepicker_options = {
					minDate: minDate
				};
			}],
			resolve: {
				voucher_id: function () {
					return $scope.voucher_id;
				},
				expiry_date: function(){
					return $scope.voucher_info.expiry_date;
				}
			}
		});

		modalInstance.result.then(function(response){
			$scope.voucher_info.expiry_date = response.new_expiry_date;
			$scope.voucher_info.expiry_date_formatted = response.new_expiry_date_formatted;
		}, function(){

		});
	};

    $scope.$on('update-balance', function(event, args) {
        var balance = args;
        console.log(balance);
        $scope.voucher_info.balance = args.balance;
        var redemptionsPromise = VoucherService.getRedemptions($scope.voucher_id);
		redemptionsPromise.then(function(response){
            $scope.redemptions = response.redemptions;
            }, function(data){
        });
    });

	$scope.void = function(){
		var voidPromise = VoucherService.void($scope.voucher_id);
		voidPromise.then(function(response){
			$scope.voucher_info.voided = true;
		}, function(){});
	};

	$scope.resendSmsOwner = function(){
		var resendSmsPromise = VoucherService.resendSms($scope.voucher_id);
		resendSmsPromise.then(function(response){
			console.log(response);
			alert(response.message);
		}, function(){

		});
	};

	$scope.resendEmail = function(){
		var resendEmailPromise = VoucherService.resendEmail($scope.voucher_id);
		resendEmailPromise.then(function(response){
			console.log(response);
			alert(response.message);
		}, function(){
			console.log(response);
		});
	};

	$scope.resendSmsSender = function(){
		var resendSmsPromise = VoucherService.resendSms($scope.voucher_id, 'sender');
		resendSmsPromise.then(function(response){

		}, function(){

		});
	};

	$scope.resendVoucher = function(){};

	$scope.editOwner = function(){
		var modalInstance = $uibModal.open({
			template: '<div class="modal-header">' +
						'   <h3 class="modal-title">Edit Owner</h3>' +
						'</div>' +
						'<div class="modal-body">' +
						// '   <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'title\')}">' +
						// '      <label>Title<span class="required">*</span></label>' +
						// '      <select class="form-control" ng-model="form.title" ng-options="title.value as title.label for title in titles"></select>' +
						// '      <div class="help-block" ng-if="errors.hasOwnProperty(\'title\')">{{ errors.title }}</div>'+
						// '   </div>' +
						'   <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'first_name\')}">' +
						'      <label>Name<span class="required">*</span></label>' +
						'      <input type="text" class="form-control" ng-model="form.first_name">' +
						'      <div class="help-block" ng-if="errors.hasOwnProperty(\'first_name\')">{{ errors.first_name }}</div>'+
						'   </div>' +
						// '   <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'last_name\')}">' +
						// '      <label>Last Name<span class="required"></span></label>' +
						// '      <input type="text" class="form-control" ng-model="form.last_name">' +
						// '      <div class="help-block" ng-if="errors.hasOwnProperty(\'last_name\')">{{ errors.last_name }}</div>'+
						// '   </div>' +
						'   <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'email\')}">' +
						'      <label>Email<span class="required">*</span></label>' +
						'      <input type="email" class="form-control" ng-model="form.email">' +
						'      <div class="help-block" ng-if="errors.hasOwnProperty(\'email\')">{{ errors.email }}</div>'+
						'   </div>' +
						'   <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'country\')}">' +
						'      <label>Country<span class="required">*</span></label>' +
						'      <select type="text" class="form-control" ng-model="form.country" ng-options="country.value as country.label for country in countries"></select>' +
						'      <div class="help-block" ng-if="errors.hasOwnProperty(\'country\')">{{ errors.country }}</div>'+
						'   </div>' +
						'   <div class="form-group" ng-class="{\'has-error\': errors.hasOwnProperty(\'mobile\')}">' +
						'      <label>Phone No<span class="required">*</span></label>' +
						'      <div class="input-group">' +
						'         <span class="input-group-addon">{{ calling_code }}</span>' +
					    '         <input type="text" class="form-control" ng-model="form.mobile">' +
						'      </div>' +
						'      <div class="help-block" ng-if="errors.hasOwnProperty(\'mobile\')">{{ errors.mobile }}</div>'+
						'   </div>' +
						'</div>' +
						'<div class="modal-footer">' +
						'   <button class="btn btn-primary" type="button" ng-click="save()">Save</button>' +
						'   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
						'</div>',
			controller:['$scope', '$uibModalInstance', 'voucher_id', '$log', 'VoucherService', '$uibModal', '$rootScope',
			function($scope, $uibModalInstance, voucher_id, $log, VoucherService, $uibModal, $rootScope){

				$scope.form = {
					//title: "",
					first_name: "",
					last_name: "",
					email: "",
					country: "",
					mobile: "",
				};

				$scope.countries = [{
					value: "",
					label: "-- Select Country --",
					calling_code: "Select Country"
				}];

				var ownerPromise = VoucherService.getOwner(voucher_id);
				ownerPromise.then(function(data){
					// $scope.titles = [{
					// 	value: "",
					// 	label: "-- Select Title --"
					// }];
					// for(var i=0; i<data.titles.length; i++){
					// 	$scope.titles.push(data.titles[i]);
					// }

					for(var i=0; i<data.countries.length; i++){
						$scope.countries.push(data.countries[i]);
					}

					//$scope.form.title = data.owner_info.title;
					$scope.form.first_name = data.owner_info.first_name;
					//$scope.form.last_name = data.owner_info.last_name;
					$scope.form.email = data.owner_info.email;
					$scope.form.country = data.owner_info.country;
					$scope.form.mobile = data.owner_info.mobile;

				}, function(){

				});

				$scope.calling_code = "";
				$scope.$watch('form.country', function(){
					for(var i=0; i<$scope.countries.length; i++){
						if($scope.form.country == $scope.countries[i].value){
							$scope.calling_code = $scope.countries[i].calling_code;
							break;
						}
					}
				});

				$scope.save = function(){
					$scope.errors = {};

					// if(!$scope.form.title){
					// 	$scope.errors['title'] = "Title cannot be empty!";
					// }
					if(!$scope.form.first_name){
						$scope.errors['first_name'] = "Name cannot be empty!";
					}
					// if(!$scope.form.last_name){
					// 	$scope.errors['last_name'] = "Last Name cannot be empty!";
					// }
					if(!$scope.form.email){
						$scope.errors['email'] = "Email cannot be empty or not valid!";
					}
					if(!$scope.form.country){
						$scope.errors['country'] = "Country cannot be empty!";
					}
					if(!$scope.form.mobile){
						$scope.errors['mobile'] = "Mobile cannot be empty!";
					}

					if(angular.equals($scope.errors, {})){

						var datas = {
							//title: $scope.form.title,
							first_name: $scope.form.first_name,
							//last_name: $scope.form.last_name,
							email: $scope.form.email,
							country: $scope.form.country,
							mobile: $scope.form.mobile
						};
						var updatePromise = VoucherService.updateOwner(voucher_id, datas);
						updatePromise.then(function(response){
							console.log(response);
							$uibModalInstance.close({
								fullname: response.fullname,
								email: response.email,
								mobile: response.mobile,
								country: response.country
							});
						
						}, function(response){
							if(angular.isDefined(response.email)){
								$scope.errors['email'] = response.email[0];
							}
						});
					}
				}

				$scope.close = function(){
					$uibModalInstance.dismiss();
				}
			}],
			resolve: {
				voucher_id: function () {
					return $scope.voucher_id;
				}
			}
		});

		modalInstance.result.then(function (data) {
			$scope.owner_info.fullname = data.fullname;
			$scope.owner_info.email = data.email;
			$scope.owner_info.mobile = data.mobile;
			$scope.owner_info.country = data.country;
		}, function () {
			
		});
	};

	$scope.$watch('voucher_id', function(){
		if($scope.voucher_id){
			var emailLogsPromise = VoucherService.getEmailLogs($scope.voucher_id);
			emailLogsPromise.then(function(){}, function(){});

			var smsLogsPromise = VoucherService.getSMSLogs($scope.voucher_id);
			smsLogsPromise.then(function(){}, function(){});

			var extendHistoriesPromise = VoucherService.getExtendHistories($scope.voucher_id);
			extendHistoriesPromise.then(function(){}, function(){});

			var redemptionsPromise = VoucherService.getRedemptions($scope.voucher_id);
			redemptionsPromise.then(function(){}, function(){});
		}
	})
}
