angular.module('CPRV-OctFest-Admin')
	.controller("VoucherListCtrl",['$scope', 'VoucherService', '$uibModal', '$timeout', '$log',  VoucherListCtrl]);

function VoucherListCtrl($scope, VoucherService, $uibModal, $timeout, $log){
    $timeout(function(){
        $log.info($scope.vouchers);
    });
    $scope.form_search = {};
 
    $scope.datePicker = {};
    $scope.datePicker.date = {startDate: null, endDate: null};

	$scope.$on('update-balance', function(event, args) {
		$scope.vouchers[args.index].balance = args.balance;

		angular.element('.voucher-row-' + args.index).find('.balance').addClass("highlighted");

		$timeout(function(){
			angular.element('.voucher-row-' + args.index).find('.balance').removeClass("highlighted");
		}, 3000);

	});

	$scope.showHistories = function(voucher_id){
		$uibModal.open({
			size: 'lg',
			template: '<div class="modal-header">' +
			          '   <h3 class="modal-title">Transaction History</h3>' +
		              '</div>' +
					  '<div class="modal-body">' +
					  '   <p ng-if="histories.length<=0">There is no data</p>' +
					  '   <table class="table" ng-if="histories.length>=1">' +
					  '      <thead>' +
					  '         <tr>' +
					  '            <th>Transaction Date</th>' +
					  '            <th>Transaction Type</th>' +
					  '            <th>Purchase Method</th>' +
					  '            <th style="text-align: right;">Purchase Qty</th>' +
					  '            <th style="text-align: right;">Redemption Qty</th>' +
					  '         </tr>' +
					  '      </thead>' +
					  '      <tbody>' +
					  '         <tr ng-repeat="voucher_history in histories">' +
					  '            <td>{{voucher_history.transaction_date}}</td>' +
					  '            <td ng-if="voucher_history.redeem_type!=\'admin\'">{{voucher_history.transaction_type_string}}</td>' +
					  '            <td ng-if="voucher_history.redeem_type==\'admin\'"><a href="" ng-click="detailTransaction(voucher_history.transaction_type, voucher_history.transaction_id)">{{voucher_history.transaction_type_string}}</a></td>' +
					  '            <td>{{voucher_history.purchase_method}}</td>' +
					  '            <td style="text-align: right;">{{voucher_history.purchase_qty}}</td>' +
					  '            <td style="text-align: right;">{{voucher_history.redemption_qty}}</td>' +
					  '         </tr>' +
					  '      </tbody>' +
				      '   </table>' +
		              '</div>' +
		              '<div class="modal-footer">' +
			          '   <button class="btn btn-warning" type="button" ng-click="close()">Close</button>' +
		              '</div>',
			controller:['$scope', '$uibModal', '$uibModalInstance', 'voucher_id', '$log', 'VoucherService', function($scope, $uibModal, $uibModalInstance, voucher_id, $log, VoucherService){

				var historiesPromise = VoucherService.getHistories(voucher_id);
				historiesPromise.then(function(data){	
					$scope.histories = data.histories;
				}, function(){

				});

				$scope.close = function(){
					$uibModalInstance.dismiss();
				}

				$scope.detailTransaction = function(transaction_type, transaction_id){
					$uibModal.open({
						template: '<div class="modal-header">' +
								  '   <h3 class="modal-title">Transaction Detail</h3>' +
								  '</div>' +
								  '<div class="modal-body">' +
								  '   <p ng-if="data.receipt"><strong>Receipt:</strong><br>{{ data.receipt }}</p>' +
								  '   <p ng-if="data.note"><strong>Note:</strong><br>{{ data.note }}</p>' +
								  '</div>' +
								  '<div class="modal-footer">' +
								  '   <button class="btn btn-warning" type="button" ng-click="close()">Close</button>' +
								  '</div>',
						controller:['$scope', '$uibModalInstance', '$log', 'VoucherService', 'voucher_id', function($scope, $uibModalInstance, $log, VoucherService, voucher_id){
			
							var historiesPromise = VoucherService.detailTransaction(voucher_id, transaction_type, transaction_id);
							historiesPromise.then(function(response){	
								$scope.data = response;
							}, function(){
			
							});
			
							$scope.close = function(){
								$uibModalInstance.dismiss();
							}
						}],
						resolve: {
							voucher_id: function () {
								return voucher_id;
							}
						}
					});
				}
			}],
			resolve: {
				voucher_id: function () {
					return voucher_id;
				}
			}
		  });
	};


	$scope.showVoucherUrl = function(voucher_id){
		$uibModal.open({
		template: '<div class="modal-header">' +
					'   <h3 class="modal-title">Voucher Url</h3>' +
					'</div>' +
					'<div class="modal-body">' +
					'   <p class="text-center" ng-if="link" ng-cloak>Voucher Url:<br><strong><a href="{{link}}" target="_blank">{{link}}</a></strong></p>' +
					'   <p class="text-center mt30"><button class="btn btn-primary" ng-click="resendSms()">Resend SMS</button></p>' +
					'</div>' +
					'<div class="modal-footer">' +
					'   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
					'</div>',
		controller:['$scope', '$uibModal', '$uibModalInstance', 'voucher_id', '$log', 'VoucherService', function($scope, $uibModal, $uibModalInstance, voucher_id, $log, VoucherService){

			
			var voucherUrlPromise = VoucherService.getVoucherUrl(voucher_id);
			voucherUrlPromise.then(function(data){	
				$scope.link = data.link;
			}, function(){

			});

			$scope.close = function(){
				$uibModalInstance.dismiss();
			}

			$scope.resendSms = function(){
				$uibModal.open({
				template: '<div class="modal-header">' +
							'   <h3 class="modal-title">Confirmation</h3>' +
							'</div>' +
							'<div class="modal-body">' +
							'   <p>Are you sure want to resend the sms?</p>' +
							'</div>' +
							'<div class="modal-footer">' +
							'   <button class="btn btn-primary" type="button" ng-click="resend()">Resend</button>' +
							'   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
							'</div>',
				controller:['$scope', '$uibModal', '$uibModalInstance', 'voucher_id', 'VoucherService', function($scope, $uibModal, $uibModalInstance,voucher_id, VoucherService){
		
					$scope.resend = function(){
						$uibModalInstance.dismiss();

						var resendSmsPromise = VoucherService.resendSms(voucher_id);
						resendSmsPromise.then(function(response){	
		
							$uibModal.open({
								template: '<div class="modal-body">' +
									  '   <div>{{message}}</div>' +
									  '</div>' +
									  '<div class="modal-footer">' +
									  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
									  '</div>',
								controller:['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
									$scope.message = message;
									$scope.close = function(){
										$uibModalInstance.dismiss();
									}
								}],
								resolve: {
									message: function(){
										return response.message;
									}
								}
							});

						}, function(response){

							$uibModal.open({
								template: '<div class="modal-body">' +
									  '   <div class="text-danger">{{message}}</div>' +
									  '</div>' +
									  '<div class="modal-footer">' +
									  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
									  '</div>',
								controller:['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
									$scope.message = message;
									$scope.close = function(){
										$uibModalInstance.dismiss();
									}
								}],
								resolve: {
									message: function(){
										return response.message;
									}
								}
							});

						});
					}
		
					$scope.close = function(){
						$uibModalInstance.dismiss();
					}
				}],
				resolve: {
					voucher_id: function () {
						return voucher_id;
					}
				}
				});
			};

		}],
		resolve: {
			voucher_id: function () {
				return voucher_id;
			}
		}
		});
	};

	$scope.redeem = function(index, voucher_id){
		$uibModal.open({
			template: '<div class="modal-header">' +
						'   <h3 class="modal-title">Redeem - {{voucher_id}}</h3>' +
						'</div>' +
						'<div class="modal-body">' +
						'   <div class="alert alert-danger" ng-if="errors.length>=1">' +
						'      <ul class="list-unstyled">' +
						'         <li ng-repeat="error in errors">{{error}}</li>' +
						'      </ul>' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Receipt</label>' +
						'      <input type="text" class="form-control" name="receipt" ng-model="form.receipt">' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Cashier</label>' +
						'      <input type="text" class="form-control" name="cashier" ng-model="form.cashier">' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Amount<span class="required">*</span></label>' +
						'      <input type="number" class="form-control" name="amount" ng-model="form.amount">' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Redeem Date<span class="required">*</span></label>' +
						'      <div class="input-group">' +
						'         <input type="text" readonly class="form-control" uib-datepicker-popup="d MMM yyyy" ng-model="form.date" is-open="redeem_datepicker.opened" datepicker-options="dateOptions" close-text="Close" />' +
						'         <span class="input-group-btn">' +
						'            <button type="button" class="btn btn-default" ng-click="openDatepicker()"><i class="glyphicon glyphicon-calendar"></i></button>' +
						'         </span>' +
					    '      </div>' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <div uib-timepicker ng-model="form.date" show-meridian="true" show-seconds="true"></div>' +
						'   </div>' +
						'   <div class="form-group">' +
						'      <label>Note</label>' +
						'      <textarea class="form-control" name="note" ng-model="form.note"></textarea>' +
						'   </div>' +
						'</div>' +
						'<div class="modal-footer">' +
						'   <button class="btn btn-primary" type="button" ng-click="redeem()">Redeem</button>' +
						'   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
						'</div>',
			controller:['$scope', '$uibModalInstance', 'voucher_id', '$log', 'VoucherService', '$uibModal', '$rootScope', 'index',
			function($scope, $uibModalInstance, voucher_id, $log, VoucherService, $uibModal, $rootScope, index){
                $scope.voucher_id = voucher_id;

				$scope.form = {
					cashier: "",
					receipt: "",
					amount: "",
					date: new Date(),
					note: "",
				};

				$scope.dateOptions = {
					formatYear: 'yy',
				};

				$scope.redeem_datepicker = {
					opened: false
				};
				
				$scope.openDatepicker = function(){
					$scope.redeem_datepicker.opened = true;
				};

				$scope.redeem = function(){
					$scope.errors = [];
                    if(!$scope.form.cashier){
						$scope.errors.push('Cashier cannot be empty');
					}
					if(!$scope.form.amount){
						$scope.errors.push('Amount cannot be empty');
					}

					if(!$scope.form.date){
						$scope.errors.push('Redeem Date cannot be empty');
					}

					if($scope.errors.length<=0){

						var dt = $scope.form.date;
						var dt_string = dt.getFullYear() + "-" + ( dt.getMonth()+1 ) + "-" + dt.getDate() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

						var datas = {
							receipt: $scope.form.receipt,
							cashier: $scope.form.cashier,
							amount: $scope.form.amount,
							date: dt_string,
							note: $scope.form.note,
						};
						var redeemPromise = VoucherService.redeem(voucher_id, datas);
						redeemPromise.then(function(response){

							$scope.form.receipt = "";
							$scope.form.cashier = "";
							$scope.form.amount = "";
							$scope.form.date = new Date();
							$scope.form.note = "";

							$uibModalInstance.dismiss();

							$rootScope.$broadcast('update-balance', { index: index, balance: response.balance });

							$uibModal.open({
								template: '<div class="modal-body">' +
									  '   <div>{{message}}</div>' +
									  '</div>' +
									  '<div class="modal-footer">' +
									  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
									  '</div>',
								controller:['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
									$scope.message = message;
									$scope.close = function(){
										$uibModalInstance.dismiss();
									}
								}],
								resolve: {
									message: function(){
										return response.message;
									}
								}
							});

						}, function(response){

							$uibModal.open({
								template: '<div class="modal-body">' +
									  '   <div class="text-danger">{{message}}</div>' +
									  '</div>' +
									  '<div class="modal-footer">' +
									  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
									  '</div>',
								controller:['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
									$scope.message = message;
									$scope.close = function(){
										$uibModalInstance.dismiss();
									}
								}],
								resolve: {
									message: function(){
										return response.message;
									}
								}
							});

						});
					}
				}

				$scope.close = function(){
					$uibModalInstance.dismiss();
				}
			}],
			resolve: {
				voucher_id: function () {
					return voucher_id;
				},
				index: function(){
					return index;
				}
			}
		});
	};

    $scope.searchVouchers = function(){
        var searchVouchersPromise = VoucherService.searchVouchers($scope.form_search);
        searchVouchersPromise.then(function(data){
            console.log(data);    
            $scope.vouchers = data.vouchers;
        }, function(){

        });
};

$scope.toggleStatus = function(index){
    var selected_voucher = $scope.vouchers[index];
		var updatePromise = VoucherService.updateStatus(selected_voucher.voucher_id, !selected_voucher.status);
		updatePromise.then(function(response){
			$scope.vouchers[index].status = response.status;
			$scope.vouchers[index].status_string = response.status_string;
		}, function(){

		});
	};

}


angular.module('CPRV-OctFest-Admin')
	.controller("VoucherFormCtrl",['$scope', 'VoucherService', '$uibModal', '$timeout', '$log', VoucherFormCtrl]);

function VoucherFormCtrl($scope, VoucherService, $uibModal, $timeout, $log){
	$scope.form = {
		branch_id: '',
		brand_id: '',
		qty: '',
		unlimited: false,
		country_id: 192,
		calling_code: '',
		mobile: '',
		name: '',
		email: '',
		receipt: ''
	};

	$scope.brands = [{
		value: '',
		label: '-- select product --'
	}];

	$scope.$watch('form.branch_id', function(){
		$scope.brands = [{
			value: '',
			label: '-- select product --'
		}];

		if($scope.form.branch_id){
			var brandsPromise = VoucherService.getBranchBrands($scope.form.branch_id);
			brandsPromise.then(function(response){
				if(response.brands.length>=1){
					for(var i=0; i<response.brands.length; i++){
						$scope.brands.push({
							value: response.brands[i].value,
							label: response.brands[i].label
						});
					}
				}
			});
		}
	});

	$scope.readonly_qty = false;

	$scope.$watch('form.unlimited', function(){
		if($scope.form.unlimited){
			$scope.readonly_qty = true;
		}else{
			$scope.readonly_qty = false;
		}
	});

	$scope.countries = [];

	$scope.$watch('[form.country_id, countries]', function(){

		for(var i=0; i<$scope.countries.length; i++){
			var country_selected = $scope.countries[i];
			
			if(country_selected.country_id==$scope.form.country_id){
				$scope.form.calling_code = country_selected.calling_code;
				break;
			}
		}
	});

	$scope.submit = function(){

		$scope.errors = {};

		if(!$scope.form.branch_id){
			$scope.errors.branch_id = "Please select a retailer!";
		}
		if(!$scope.form.brand_id){
			$scope.errors.brand_id = "Please select a product!";
		}
		if(!$scope.form.qty && !$scope.form.unlimited){
			$scope.errors.qty = "Quantity cannot be empty!";
		}
		if(!$scope.form.mobile){
			$scope.errors.mobile = "Mobile cannot be empty!";
		}
		if(!$scope.form.name){
			$scope.errors.name = "Name cannot be empty!";
		}

		if(angular.equals($scope.errors, {})){

			var datas = {
				branch_id: $scope.form.branch_id,
				brand_id: $scope.form.brand_id,
				qty: $scope.form.qty,
				unlimited: $scope.form.unlimited,
				country_id: $scope.form.country_id,
				mobile: $scope.form.mobile,
				name: $scope.form.name,
				email: $scope.form.email,
				receipt: $scope.form.receipt
			};

			var insertPromise = VoucherService.insert(datas);
			insertPromise.then(function(){

				$scope.form.branch_id = '';
				$scope.form.brand_id = '';
				$scope.form.qty = '';
				$scope.form.unlimited = false;
				$scope.form.country_id = 192;
				$scope.form.calling_code = '';
				$scope.form.mobile = '';
				$scope.form.name = '';
				$scope.form.email = '';
				$scope.form.receipt = '';

				$uibModal.open({
					template: '<div class="modal-body">' +
						  '   <div>Voucher Created Successfully!</div>' +
						  '</div>' +
						  '<div class="modal-footer">' +
						  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
						  '</div>',
					controller:['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
						$scope.close = function(){
							$uibModalInstance.dismiss();
						}
					}]
				});

			}, function(){

			});
		}
	}
}
