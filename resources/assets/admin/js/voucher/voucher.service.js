angular.module('CPRV-OctFest-Admin')
	.service("VoucherService",['cfg', '$q', '$http', '$log', VoucherService]);

function VoucherService(cfg, $q, $http, $log){
	
	this.getVoucherUrl = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/voucher/' + voucher_id + '/url'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.resendVoucher = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/voucher/' + voucher_id + '/resend-sms'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

    this.searchVouchers = function(data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/voucher/search',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.resendSms = function(voucher_id, type) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/voucher/' + voucher_id + '/resend-sms'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.resendEmail = function(voucher_id, type) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/voucher/' + voucher_id + '/resend-email'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.redeem = function(voucher_id, data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/voucher/' + voucher_id + '/redeem',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.extend = function(voucher_id, data) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
			url     : cfg.api + '/voucher/' + voucher_id + '/extend',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.void = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/voucher/' + voucher_id + '/void',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.detailTransaction = function(voucher_id, transaction_type, transaction_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/voucher/' + voucher_id + '/detail-transaction/' + transaction_type + '/' + transaction_id,
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.getOwner = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/voucher/' + voucher_id + '/owner',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.updateOwner = function(voucher_id, data) {
		var deferred = $q.defer();
		$http({
			method  : 'PUT',
			url     : cfg.api + '/voucher/' + voucher_id + '/owner',
			data    : data
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.getEmailLogs = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/voucher/' + voucher_id + '/email-log',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.getSMSLogs = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/voucher/' + voucher_id + '/sms-log',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.getExtendHistories = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/voucher/' + voucher_id + '/extend-history',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

	this.getRedemptions = function(voucher_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/voucher/' + voucher_id + '/redemption',
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});
		
		return deferred.promise;
	};

}
