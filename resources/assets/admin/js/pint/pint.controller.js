angular.module('CPRV-OctFest-Admin')
	.controller("PintListCtrl",['$scope', 'PintService', '$uibModal', '$timeout', '$log', PintListCtrl]);

function PintListCtrl($scope, PintService, $uibModal, $timeout, $log){

	$scope.delete = function(index){
		var selected_pint = $scope.pints[index];
		var deletePromise = PintService.delete(selected_pint.id);
		deletePromise.then(function(){
			window.location.href = $scope.current_url;
		}, function(){

		});
	};

	$scope.create = function(){

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', '$uibModalInstance', '$log', 'cfg', '$uibModal', 'Upload', function($scope, $uibModalInstance, $log, cfg, $uibModal, Upload){

				$scope.title = "Add New Pint";
				
				$scope.form = {
					image : "",
					pint : "",
					price : "",
					sort_order : "",
					status : 1
				};
			
				$scope.errors = {};
				$scope.save = function(){

					$scope.errors = {};

					if(!$scope.form.image){
						$scope.errors.image = "Image cannot be empty!";
					}
					if(!$scope.form.pint){
						$scope.errors.pint = "Pints cannot be empty!";
					}
					if(!$scope.form.price){
						$scope.errors.price = "Price cannot be empty!";
					}

					$log.info($scope.errorImage);

					if(angular.equals($scope.errors, {})){

						var upload = Upload.upload({
							url: cfg.api + '/pint',
							data: {
								image: $scope.form.image,
								pint: $scope.form.pint,
								price: $scope.form.price,
								sort_order: $scope.form.sort_order,
								status: $scope.form.status
							}
						});

						upload.then(function(){
							window.location.href = $scope.current_url;
						}, function(response){
							if(angular.isDefined(response.data.pint)){
								$scope.errors.pint = response.data.pint[0];
							}
						});
					}
				};
			}]
		});

	};

	$scope.edit = function(index){

		var selected_pint = $scope.pints[index];

		var modalInstance = $uibModal.open({
			templateUrl: 'modalEdit.html',
			controller: ['$scope', 'pint_id', '$uibModalInstance', '$log', 'Upload', 'cfg', function($scope, pint_id, $uibModalInstance, $log, Upload, cfg){
				
				$scope.title = "Edit Pint";

				$scope.form = {};

				var pintPromise = PintService.getPint(pint_id);
				pintPromise.then(function(response){
					$scope.form.pint = response.pint;
					$scope.form.price = response.price;
					$scope.form.sort_order = response.sort_order;
					$scope.form.status = response.status;
				});

				$scope.errors = {};
				$scope.save = function(){
					
					$scope.errors = {};

					if(!$scope.form.pint){
						$scope.errors.pint = "Pints cannot be empty!";
					}
					if(!$scope.form.price){
						$scope.errors.price = "Price cannot be empty!";
					}

					if(angular.equals($scope.errors, {})){

						var upload = Upload.upload({
							url: cfg.api + '/pint/' + pint_id,
							data: {
								_method: 'PUT',
								image: $scope.form.image,
								pint: $scope.form.pint,
								price: $scope.form.price,
								sort_order: $scope.form.sort_order,
								status: $scope.form.status
							}
						});

						upload.then(function(data){
							
							$uibModalInstance.close({
								image: data.data.image,
								name: data.data.name,
								price: data.data.price,
								sort_order: data.data.sort_order,
								status: data.data.status,
								status_string: data.data.status_string
							});

						}, function(response){
							if(angular.isDefined(response.data.pint)){
								$scope.errors.pint = response.data.pint[0];
							}
						});
					}
				};
			}],
			resolve: {
				pint_id : function(){
					return selected_pint.id;
				}
			}
		});

		modalInstance.result.then(function(response) {
			$scope.pints[index].image = response.image;
			$scope.pints[index].name = response.name;
			$scope.pints[index].price = response.price;
			$scope.pints[index].sort_order = response.sort_order;
			$scope.pints[index].status = response.status;
			$scope.pints[index].status_string = response.status_string;
		}, function (response) {
			
		});
	}

	$scope.toggleStatus = function(index){
		var selected_pint = $scope.pints[index];
		var updatePromise = PintService.updateStatus(selected_pint.id, !selected_pint.status);
		updatePromise.then(function(response){
			$scope.pints[index].status = response.status;
			$scope.pints[index].status_string = response.status_string;
		}, function(){

		});
	};

}
