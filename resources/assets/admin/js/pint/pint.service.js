angular.module('CPRV-OctFest-Admin')
	.service("PintService",['cfg', '$q', '$http', '$log', PintService]);

function PintService(cfg, $q, $http, $log){
	
	this.delete = function(pint_id) {
		var deferred = $q.defer();
		$http({
			method	: 'DELETE',
			url     : cfg.api + '/pint/' + pint_id
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
    };

	this.update = function(pint_id, data) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/pint/' + pint_id,
			data    : data
		}).then(function(response) {
			deferred.resolve(response.data);
		}, function(response) {
			deferred.reject(response.data);
		});
		
		return deferred.promise;
	};

	this.updateStatus = function(pint_id, status) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/pint/' + pint_id + '/status',
			data    : {status: status}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.getPint = function(pint_id) {
		var deferred = $q.defer();
		$http({
			method	: 'GET',
			url     : cfg.api + '/pint/' + pint_id + '/edit'
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

	this.getImages = function(pint_id) {
		var deferred = $q.defer();
		$http({
			url     : cfg.api + '/pint/' + pint_id + '/image'
		}).then(function(response) {
			deferred.resolve(response.data);
		}, function() {
			deferred.reject(response.data);
		});
		
		return deferred.promise;
	};
}