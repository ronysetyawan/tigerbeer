angular.module('CPRV-OctFest-Admin')
	.controller("PrizeRedemptionCtrl",['$scope', '$uibModal', '$log', PrizeRedemptionCtrl]);

function PrizeRedemptionCtrl($scope, $uibModal, $log){

	$scope.viewDetail = function(index){

		var redemption = $scope.redemptions[index];

		$uibModal.open({
			templateUrl: 'modalDetail.html',
			controller: ['$scope', '$uibModalInstance', '$log', 'redemption', 
			function($scope, $uibModalInstance, $log, redemption){

				console.log(redemption);
				
				$scope.redemption = redemption;

				$scope.close = function(){
					$uibModalInstance.dismiss();
				}
			}],
			resolve: {
				redemption: function(){
					return redemption;
				}
			}
		});

	};

}
