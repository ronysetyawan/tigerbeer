angular.module('CPRV-OctFest-Admin')
	.service("ReportService",['cfg', '$q', '$http', '$log', ReportService]);

function ReportService(cfg, $q, $http, $log){
	
	this.updateEventTicketStatus = function(voucherid) {
		var deferred = $q.defer();
		$http({
			method	: 'PUT',
			url     : cfg.api + '/voucher/' + voucherid + '/event-status',
			data    : {}
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function() {
			deferred.reject();
		});
		
		return deferred.promise;
	};

}