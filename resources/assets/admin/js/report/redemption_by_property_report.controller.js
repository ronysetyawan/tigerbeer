angular.module('CPRV-OctFest-Admin')
	.controller("RedemptionByPropertyReportCtrl",['$scope', '$log', RedemptionByPropertyReportCtrl]);

function RedemptionByPropertyReportCtrl($scope, $log){
	$scope.filter_date = {
		date: { startDate: null, endDate: null }
	}

	$scope.selected_status = {
		value: '',
		label: 'All'	
	}
	$scope.selected_property = {
		value: '',
		label: 'All'
	}
	$scope.selected_currency = {
		value: '',
		label: 'All'
	}
	$scope.selected_redemption_type = {
		value: '',
		label: 'All'
	}

	$scope.selectRedemptionType = function(value, label){
		angular.element('#filter-redemption-type').val(value);
		$scope.selected_redemption_type.value = value;
		$scope.selected_redemption_type.label = label;
	}

	$scope.$watch('ft_selected_status', function(){
		for(var i=0; i<$scope.statuses.length; i++){
			if($scope.statuses[i].value == $scope.ft_selected_status){
				$scope.selected_status.value = $scope.statuses[i].value;
				$scope.selected_status.label = $scope.statuses[i].label;
				break;
			}
		}
	}, true);

	$scope.applyDateRange = function(ev, picker){
		//console.log('test');
		$log.info(ev);
		$log.info(picker);
	};

	$scope.$watch('ft_selected_property', function(){
		for(var i=0; i<$scope.properties.length; i++){
			if($scope.properties[i].value == $scope.ft_selected_property){
				$scope.selected_property.value = $scope.properties[i].value;
				$scope.selected_property.label = $scope.properties[i].label;
				break;
			}
		}
	}, true);

	$scope.$watch('ft_selected_currency', function(){
		for(var i=0; i<$scope.currencies.length; i++){
			if($scope.currencies[i].value == $scope.ft_selected_currency){
				$scope.selected_currency.value = $scope.currencies[i].value;
				$scope.selected_currency.label = $scope.currencies[i].label;
				break;
			}
		}
	}, true);
};