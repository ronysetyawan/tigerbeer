angular.module('CPRV-OctFest-Admin')
	.controller("FullTransactionReportCtrl",['$scope', '$window', 'ReportService', '$uibModal', '$timeout', '$log', FullTransactionReportCtrl]);

function FullTransactionReportCtrl($scope, $window, ReportService, $uibModal, $timeout, $log){
	$scope.form_filter = {
		date: {startDate: null, endDate: null},
		status: "",
		property: "",
		currency: ""
	}

	$scope.$watch('form_filter.date', function(){
		$log.info($scope.form_filter.date);
	});

	$scope.updateEventTicketStatus = function(voucherid){
		
		var updatePromise = ReportService.updateEventTicketStatus(voucherid);
		updatePromise.then(function(response){
			$window.location.reload();
		}, function(){

		});
	};
};