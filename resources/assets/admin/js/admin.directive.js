angular.module('CPRV-OctFest-Admin')
.directive('buttonConfirmation', ['$parse', '$uibModal', function ($parse, $uibModal) {
    return {
        restrict: 'A',
        scope: {
            "confirmationTitle": "@",
            "confirmationDescription": "@",
            "confirmationFunction": "&"
        },
        link: function(scope, element, attrs){
            $(element).on('click', function(e) {
                var modalInstance = $uibModal.open({
                    template: '<div class="modal-header">' +
                                '   <h3 class="modal-title" id="modal-title">{{title}}</h3>' +
                                '</div>' +
                                '<div class="modal-body" id="modal-body" ng-bind-html="description"></div>' +
                                '<div class="modal-footer">' +
                                '   <button class="btn btn-primary btn-sm" type="button" ng-click="yes()">Yes</button>' +
                                '   <button class="btn btn-default btn-sm" type="button" ng-click="no()">No</button>' +
                                '</div>',
                    controller: ['$scope', 'title', 'description', '$uibModalInstance', function($scope, title, description, $uibModalInstance){
                        $scope.title = title;
                        $scope.description = description;

                        $scope.yes = function(){
                            $uibModalInstance.close();
                        };
                        $scope.no = function(){
                            $uibModalInstance.dismiss();
                        };
                    }],
                    resolve: {
                        title: function(){
                        return scope.confirmationTitle;
                        },
                        description: function(){
                        return scope.confirmationDescription;
                        }
                    }
                });

                modalInstance.result.then(function() {
                    scope.confirmationFunction();
                }, function () {
                    
                });
            });
            }
    };
}])

.directive('fixedMenu', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            var elOffset = element.offset();
            var elTop = elOffset.top;

            var parent_height = element.parent().outerHeight();

            element.parent().css("height", parent_height);

            angular.element($window).bind("scroll", function(event) {
                if($window.scrollY>=elTop){
                    element.addClass("fixed");
                }else{
                    element.removeClass("fixed");
                }
            });

            element.find('.navbar-nav li a').bind("click", function(event) {
                angular.element('#navbar-main-menu').collapse('hide');
            });
        }
    };
}])

.directive('stringToNumber', [function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(value) {
          return '' + value;
        });
        ngModel.$formatters.push(function(value) {
          return parseFloat(value);
        });
      }
    };
  }])

;
