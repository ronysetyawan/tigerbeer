angular.module('CPRV-OctFest-Admin')
	.service("AuthService",['cfg', '$q', '$http', '$log', AuthService]);

function AuthService(cfg, $q, $http, $log){
	
	this.login = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
      		url     : cfg.api + '/login',
      		data    : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});

		return deferred.promise;
	};

	this.loginOutlet = function(params) {
		var deferred = $q.defer();
		$http({
			method	: 'POST',
      		url     : cfg.api + '/login-outlet',
      		data    : params
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(data) {
			deferred.reject(data.data);
		});

		return deferred.promise;
	};
	 
}
