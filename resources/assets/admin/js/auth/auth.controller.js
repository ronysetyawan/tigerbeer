angular.module('CPRV-OctFest-Admin')
	.controller("LoginCtrl",['$scope', 'AuthService', '$log', '$uibModal', LoginCtrl]);

function LoginCtrl($scope, AuthService, $log, $uibModal){

	$scope.form_login = {
		username: "",
		password: ""
	};

	$scope.login = function(){
        console.log($scope.form_login);
		var loginPromise = AuthService.login($scope.form_login);
		loginPromise.then(function(response){
			window.location.href = response.redirect;
		}, function(response){
			$uibModal.open({
				template: '<div class="modal-body">' +
					  '   <div>{{message}}</div>' +
					  '</div>' +
					  '<div class="modal-footer">' +
					  '   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
					  '</div>',
				controller:['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
					$scope.message = message;
					$scope.close = function(){
						$uibModalInstance.dismiss();
					}
				}],
				resolve: {
					message: function(){
						return response.message;
					}
				}
			});
		});
	};

	$scope.form_pin = {
		input_pin : ''
	};

	$scope.login_outlet = function(){
		var pin = $scope.form_pin.input_pin;
		var loginPromise = AuthService.loginOutlet({
		  	pin: pin,
		  	return_data: false
		});
	
		loginPromise.then(function(data){
			console.log(data);
			window.location.href = data.redirect;
		}, function(data){
			$uibModal.open({
				template: '<div class="modal-body">' +
					'   <p class="mb-0">{{message}}</p>' +
						'</div> ' +
						'<div class="modal-footer">' +
						'   <button class="btn btn-default" type="button" ng-click="close()">Close</button>' +
						'</div>',
				controller: ['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
					console.log("message:" + message)
					$scope.message = message;
						$scope.close = function(){
							$uibModalInstance.dismiss();
						}
					}],
					resolve: {
					message: function(){
						return data.message
					}
				}
			});
		});
	};
}


