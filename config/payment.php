<?php
return [
    'local' =>'ccf35d',
    'live'=>'7eBYeL5FeyiHb8JZ9lGrgb6cpz1J9yARlvB99lqF9xJsrecYUf7yFpoyC8S8jJxa1SLLMyAhZzeMxqk4aVpwPGyGrR8zPr1Ux0TVF8hDGx3YCmeffZqFtlshgECUKxEo',
    'url_firststep_local' =>'https://secure-dev.reddotpayment.com/service/payment-api',
    'url_firststep_live' =>'https://secure.reddotpayment.com/service/payment-api',
    'url_result_local' =>'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_redirection',
    'url_result_live' =>'https://secure.reddotpayment.com/service/Merchant_processor/query_redirection',
    'mid_local' =>'1000089318',
    'mid_live' =>'0000022338',
];
